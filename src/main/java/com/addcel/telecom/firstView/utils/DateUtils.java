package com.addcel.telecom.firstView.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
	
	public static final String DATE_DDMMYYYY = "dd/MM/yyyy";
	
	public static final String DATE_MMDDYY = "MMddyyyy";
	
	public static final String DATE_HHMMSS = "hhmmss";
	
	public static final String DATE_YYYYMMDD_HHMMSS = "YYYYMMDD hhmmss";
	
	public static final String DATE_YYYYMMDDHHMMSS = "yyyy/MM/dd HH:mm:ss";
	
	public static final String DATE_YYYYMMDDTHHMMSS = "yyyy-MM-dd'T'HH:mm:ss";
	
	private static SimpleDateFormat SDF = null;
    	
	public static String getFecha(String formato){
        String dateString = null;
        SDF = new SimpleDateFormat(formato);
        try {
        	Date now = new Date();
        	dateString = SDF.format(now);
//        	LOGGER.debug("[FIRST VIEW] - DATE UTILS - FECHA GENERADA: "+dateString);
        }
        catch(Exception pe) {
            System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
        }
		return dateString;
	}
	
	public static String getFechaVigencia(String formato){
        String dateString = null;
        SDF = new SimpleDateFormat(formato);
        try {
        	Date now = new Date();
        	Calendar fecha = Calendar.getInstance();
        	fecha.set(Calendar.YEAR, Calendar.YEAR + 5);
        	now = fecha.getTime();
        	dateString = SDF.format(now);
//        	LOGGER.debug("[FIRST VIEW] - DATE UTILS - FECHA GENERADA: "+dateString);
        }
        catch(Exception pe) {
            System.out.println("ERROR: Cannot parse \"" + dateString + "\"");
        }
		return dateString;
	}

}