package com.addcel.telecom.firstView.model;

public class Clearing {

	private long id;
	
	private long idUsuario;
	
	private String accountNumber;
	
	private String cardNumber;
	
	private String amount;
	
	private String tranCode;
	
	private String idBitacora;
	
	private String description;
	
	private String tranType;
	
	private String approvalCode;
	
	private String postingNote;
	
	private double currencyBalance;
	
	private String transactionId;

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTranCode() {
		return tranCode;
	}

	public void setTranCode(String tranCode) {
		this.tranCode = tranCode;
	}

	public String getIdBitacora() {
		return idBitacora;
	}

	public void setIdBitacora(String idBitacora) {
		this.idBitacora = idBitacora;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getTranType() {
		return tranType;
	}

	public void setTranType(String tranType) {
		this.tranType = tranType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApprovalCode() {
		return approvalCode;
	}

	public void setApprovalCode(String approvalCode) {
		this.approvalCode = approvalCode;
	}

	public long getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getPostingNote() {
		return postingNote;
	}

	public void setPostingNote(String postingNote) {
		this.postingNote = postingNote;
	}

	public double getCurrencyBalance() {
		return currencyBalance;
	}

	public void setCurrencyBalance(double currencyBalance) {
		this.currencyBalance = currencyBalance;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
