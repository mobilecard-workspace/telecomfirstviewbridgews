package com.addcel.telecom.firstView.bsns;

import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest;

public class TransactionHistoryBsns {

	public String getTransactionHistory(String json){
		try {
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			proxy.transactionHistory(new TransactionHistoryRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
}
