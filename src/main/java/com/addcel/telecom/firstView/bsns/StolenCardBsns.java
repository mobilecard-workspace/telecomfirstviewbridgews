package com.addcel.telecom.firstView.bsns;

import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request;

public class StolenCardBsns {

	
	public String reportedCard2(String json){
		try {
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			proxy.reportStolenCard2(new ReportStolenCard2Request());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
}
