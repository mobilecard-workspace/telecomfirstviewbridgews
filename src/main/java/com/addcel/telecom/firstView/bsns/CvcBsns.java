package com.addcel.telecom.firstView.bsns;

import com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest;
import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;

public class CvcBsns {

	public String getCVC(String json){
		try {
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			proxy.getCVC(new GetCVCRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
}
