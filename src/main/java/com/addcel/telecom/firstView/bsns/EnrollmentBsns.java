package com.addcel.telecom.firstView.bsns;

import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest;

public class EnrollmentBsns {

	public String sms(String json){
		try {
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			proxy.SMSEnrollment(new SMSEnrollmentRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
}
