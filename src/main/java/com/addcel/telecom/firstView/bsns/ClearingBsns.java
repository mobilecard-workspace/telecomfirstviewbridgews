package com.addcel.telecom.firstView.bsns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.firstView.client.axis.ClearingRequest;
import com.addcel.telecom.firstView.client.axis.ClearingResponse;
import com.addcel.telecom.firstView.client.axis.FVServiceSoapProxy;
import com.addcel.telecom.firstView.dao.JdbcDao;
import com.addcel.telecom.firstView.model.Clearing;
import com.addcel.telecom.firstView.utils.DateUtils;
import com.google.gson.Gson;

public class ClearingBsns {

	private static final Logger LOGGER = LoggerFactory.getLogger(ClearingBsns.class);
	
	private static JdbcDao DAO = new JdbcDao();
	
	private static Gson GSON = new Gson();
	
	public String clearing(String json){
		Clearing request = null;
		ClearingResponse response = null;
		try {
			request = GSON.fromJson(json, Clearing.class);
			LOGGER.info("REQUESTING CLEARING - ACCOUNT NUMBER : "+request.getAccountNumber());
			DAO.insertTransacionFirstView(request);
			FVServiceSoapProxy proxy = new FVServiceSoapProxy();
			response = proxy.clearing(getClearingRequest(request));
			request.setPostingNote(response.getPostingNote());
			request.setTransactionId(response.getTransactionId());
			request.setCurrencyBalance(Double.valueOf(response.getCurrentBalance()));
			DAO.updateTransacionFirstView(request);
			json = GSON.toJson(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	
		
	private ClearingRequest getClearingRequest(Clearing request) {
		ClearingRequest clearingRequest = new ClearingRequest();
		try {
			clearingRequest.setMessageTypeIdentifier("01");
			clearingRequest.setCustNbr(request.getAccountNumber());
			clearingRequest.setPrimaryAccountNumber(request.getCardNumber());
			clearingRequest.setTranType(request.getTranType());
			clearingRequest.setTransactionAmount(request.getAmount());
			clearingRequest.setTransmissionDateTime(DateUtils.getFecha(DateUtils.DATE_YYYYMMDDHHMMSS));
			clearingRequest.setLineItemSeqNumber("2");
			clearingRequest.setInventoryCode("2504");
			clearingRequest.setQuantity("4");
			clearingRequest.setUnitPrice("25");
			clearingRequest.setCreditPlanMaster("999999");
			clearingRequest.setTransactionDescription(request.getDescription());
			clearingRequest.setTransactionCurrencyCode("840");
			clearingRequest.setSpecialMerchantIdentifier("Melinda");
			clearingRequest.setCardAcceptorIdCode("123");
			clearingRequest.setCardAcceptorTerminalId("1");
			clearingRequest.setCardAcceptorBusinessCode("0000");
			clearingRequest.setDateTimeLocalTransaction(DateUtils.getFecha(DateUtils.DATE_YYYYMMDDHHMMSS));
			clearingRequest.setSystemTraceAuditNumber("2");
			clearingRequest.setMerchantType("6011");
			clearingRequest.setRetrievalReferenceNumber("123456");
			clearingRequest.setApprovalCode(request.getApprovalCode());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return clearingRequest;
	}

}