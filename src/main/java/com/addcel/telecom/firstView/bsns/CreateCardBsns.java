package com.addcel.telecom.firstView.bsns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.addcel.telecom.firstView.client.axis.CardUpdateRequest;
import com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest;
import com.addcel.telecom.firstView.client.axis.FVServiceSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray;
import com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest;
import com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse;
import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest;
import com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse;
import com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest;
import com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse;
import com.addcel.telecom.firstView.client.firstview.CreateCardRequest;
import com.addcel.telecom.firstView.client.firstview.ObjectFactory;
import com.addcel.telecom.firstView.client.firstview.UserCredentials;
import com.addcel.telecom.firstView.dao.JdbcDao;
import com.addcel.telecom.firstView.model.CreateCard;
import com.addcel.telecom.firstView.model.CreateCardResponse;
import com.addcel.telecom.firstView.spring.SpringFactory;
import com.addcel.telecom.firstView.utils.DateUtils;
import com.google.gson.Gson;
import com.sun.xml.bind.v2.TODO;

public class CreateCardBsns {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateCardBsns.class);
	
	private static Gson GSON = new Gson();
	
	private static ObjectFactory FACTORY = new ObjectFactory();
	
	private static JdbcDao DAO = new JdbcDao();	
	
		
	public String createCard(String json){
		CreateCard card = null;
		CreateCardResponse response = new CreateCardResponse();
		try {
			LOGGER.info("JSON: "+json);
			card = GSON.fromJson(json, CreateCard.class);
			LOGGER.info("TELECOM FIRST VIEW - CREATE CARD - USER "+card.getEmail()+"");
			FVServiceSoapProxy proxy = new FVServiceSoapProxy();
			com.addcel.telecom.firstView.client.axis.CreateCIPCardResponse resp = 
					proxy.createCIPCard(getCreateCardCPI(card));			
			
			if("1".equals(resp.getResponseCode())){
				card.setAccountNumber(resp.getAccountNumber());
				card.setDdaNo(resp.getDdaNo());
				card.setPan(resp.getPan());
				card.setReferenceNo(resp.getReferenceNo());
				card.setCurrencyCode(resp.getCurrencyCode());
				card.setResponseCode(resp.getResponseCode());
				card.setReasonDescription(resp.getReasonDescription());
				response.setCardNumber(getCardNumber(resp.getAccountNumber()));
				response.setCvv(getCVC(response.getCardNumber()));
				response.setIdUsuario(card.getIdUsuario());
				response.setExpirationDate(getExpirationDate(response.getCardNumber()));
				resp.setResponseCode("0");				
				DAO = (JdbcDao) SpringFactory.getApplicationContexInstance().getBean("JdbcDao");				
				DAO.createCard(card);				
			} else {
				LOGGER.info("TELECOM FIRST VIEW - CREATE CARD RESPONSE - USER "+card.getEmail()
						+" - CODE:"+resp.getResponseCode()
						+" DESCRIPTION: "+resp.getReasonDescription());
				resp = new com.addcel.telecom.firstView.client.axis.CreateCIPCardResponse();
				response.setErrorCode(-1);
				response.setErrorDescription("Ocurrio un error al crear la tarjeta virtual.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.setErrorCode(-1);
			response.setErrorDescription("FirstView no esta respondiendo");
			LOGGER.error("ERROR AL CREAR LA TARJETA - "+e.getMessage());
		}
		json = GSON.toJson(response);
		LOGGER.info("RESPUESTA DE CREACION DE CARD: "+json);
		return json;
	}
	

	private String getExpirationDate(String cardNumber) {
		ValidateCardRequest request = null;
		ValidateCardResponse response = null;
		String expDate = null;
		try {
			request = new ValidateCardRequest();
			request.setCARD_NUMBER(cardNumber);
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			response = proxy.validateCard(request);
			expDate = response.getExpirationDate();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL OBTENER LA FECHA DE EXPIRACION - EXCEPCTION: "+e.getMessage());
		}
		LOGGER.info("EXPIRATION TIME: "+expDate+" CARDNUMBER: "+cardNumber);
		return cardNumber;
	}

	public String getCVC(String cardNumber){
		GetCVCRequest cvcRequest = new GetCVCRequest();
		GetCVCResponse cvcReponse = null;
		String cvc = null;
		try {
			cvcRequest.setCARD_NUMBER(cardNumber);
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			cvcReponse = proxy.getCVC(cvcRequest);
			cvc = cvcReponse.getCVC();			
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL GET CVC - EXCEPCTION: "+e.getMessage());
		}
		return cvc;
	}
	
	public String getCardUpdate(String json){
		try {
			FVServiceSoapProxy proxy = new FVServiceSoapProxy();
			com.addcel.telecom.firstView.client.axis.CardUpdateResponse resp = proxy.cardUpdate(new CardUpdateRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	public String getCreateCIPCard(String json){
		try {
			FVServiceSoapProxy proxy = new FVServiceSoapProxy();
			com.addcel.telecom.firstView.client.axis.CreateCIPCardResponse resp = proxy.createCIPCard(new CreateCIPCardRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	private String getCardNumber(String accountNumber) {
		PromoWorkRequest request = null;
		PromoWorkResponse response = null;
		String cardNumber = null;
		try {
			request = new PromoWorkRequest();
			request.setAccountNumber(accountNumber);
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			response = proxy.promoWork(request);
			CardDetailArray[] cardList = response.getCARD_LIST();
			if(cardList != null){
				LOGGER.info("CARDLIST: "+cardList.toString());
				if(cardList.length > 0){
					cardNumber = cardList[0].getCARD_NUM();
				}
			} else {
				proxy = new PrepaidServicesSoapProxy();
				response = proxy.promoWork(request);
				cardList = response.getCARD_LIST();
				if(cardList != null){
					if(cardList.length > 0){
						cardNumber = cardList[0].getCARD_NUM();
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		LOGGER.info("SOLICITANDO NUMERO DE TARJETA: "+cardNumber + " ACCOUNT NUMBER: "+accountNumber);
		return cardNumber;
	}

	private com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest getCreateCardCPI(CreateCard card) {
//		patern
//		address
//		state
//		city
//		postalCode
//		email
//		phone
//		birthday
//		ssn
//		idUsuario
//		govtId
//		govtIdExpirationDate
//		govtIdIssueDate
//		govtIdIssueState
		com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest request = new com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest();
		try {
			request.setDePpAcctCreationDate(DateUtils.getFecha(DateUtils.DATE_MMDDYY));
			request.setDePpAcctCreationTime(DateUtils.getFecha(DateUtils.DATE_HHMMSS));
			
			request.setUsername("mobilecardusaAPI");
			request.setPassword("M86uCp39/sR7");
			
			request.setDeBuacctProductId("51899");
			request.setDeSvcStoreName("Mobilecard");
			request.setDePpCurrencyCode("840");
			request.setDePpAccountType("I");
			request.setDePpCountryOfIssue("USA");
			request.setDePpGovtIdCountryofIssuance("USA");
			request.setDePpcipType("D");
			request.setDePpGovtIdType("11");
			request.setDePpComment("1");			
			request.setDePpReferenceNo(String.valueOf(card.getIdUsuario()));								
			request.setDePpName(card.getName());
			request.setDePpName2(card.getPatern());
			request.setDePpAddress1(card.getAddress());
			request.setDePpState(card.getState());
			request.setDePpCity(card.getCity());
			request.setDePpPostalCode(card.getPostalCode());
			request.setDePpEmailId(card.getEmail());
			request.setDePpPhoneNumber(card.getPhone());
			request.setDePpDateOfBirth(card.getBirthday());
			request.setDePpssn(card.getSsn());
			request.setDePpGovtId(card.getGovtId());
			request.setDePpGovtIdIssueState(card.getGovtIdIssueState());
			request.setDePpGovtIdExpirationDate(card.getGovtIdExpirationDate());
			request.setDePpGovtIdIssueDate(card.getGovtIdIssueDate());
//			TODO CAMBIAR DATOS PRODUCTIVOS
//			request.setDeBuacctProductId("53756");
			//TODO CAMBIAR DATOS DESARROLLO
//			request.setDePpGovtIdExpirationDate(DateUtils.getFechaVigencia(DateUtils.DATE_MMDDYY));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	private com.addcel.telecom.firstView.client.axis.CreateCardRequest getCreateCardRequest(CreateCard card) {
		com.addcel.telecom.firstView.client.axis.CreateCardRequest request = new com.addcel.telecom.firstView.client.axis.CreateCardRequest();
		try {
			request.setDePpAcctCreationDate(DateUtils.getFecha(DateUtils.DATE_MMDDYY));
			request.setDePpAcctCreationTime(DateUtils.getFecha(DateUtils.DATE_HHMMSS));
			request.setDePpReferenceNo("2");
			request.setDePpCurrencyCode("840");
			request.setDePpAccountType("I");
			request.setDePpName(card.getName());
			request.setDePpName2(card.getPatern());
			request.setDePpAddress1(card.getAddress());
			request.setDePpState(card.getState());
			request.setDePpCity(card.getCity());
			request.setDePpGovtIdType("01");
			request.setDePpGovtIdIssueState("CA");
			request.setDePpPostalCode(card.getPostalCode());
			request.setDePpCountryOfIssue("USA");
			request.setDePpEmailId(card.getEmail());
			request.setDePpPhoneNumber(card.getPhone());
			request.setDePpComment("1");
			request.setDePpDateOfBirth(card.getBirthday());
			request.setDeBuacctProductId("51899");
			request.setDeSvcStoreName("Mobilecard");
			request.setDePpGovtIdIssueDate("");
//			request.setDePpGovtIdExpirationDate(DateUtils.getFechaVigencia(DateUtils.DATE_MMDDYY));
			request.setDePpGovtIdExpirationDate("12312020");
			request.setDePpGovtIdCountryofIssuance("2");
			request.setDePpcipType("D");
			request.setDePpssn(card.getSsn());
			request.setDePpGovtId(card.getSsn());
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CREATE CARD - EXCEPCTION: "+e.getMessage());
		}
		return request;
	}

	@SuppressWarnings("unused")
	private com.addcel.telecom.firstView.client.firstview.CreateCard requestCreateCard(CreateCard card){
		com.addcel.telecom.firstView.client.firstview.CreateCard cardRqst = null;
		UserCredentials credentials = null;
		try {
			cardRqst = FACTORY.createCreateCard();
			credentials = FACTORY.createUserCredentials();
			credentials.setPassword("Sept@xummer09201621");
			credentials.setUsername("mobilecardusaAPI");
			FACTORY.createUserCredentials(credentials);
			cardRqst.setRequest(createRequestCard(card));
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("ERROR AL CREATE CARD - EXCEPCTION: "+e.getMessage());
		}
		return cardRqst;
	}

	private CreateCardRequest createRequestCard(CreateCard card) {
		CreateCardRequest cardRqst = new CreateCardRequest();
		try {
			cardRqst.setDePpAcctCreationDate("");
			cardRqst.setDePpAcctCreationTime("");
			cardRqst.setDePpReferenceNo("");
			cardRqst.setDePpCurrencyCode("");
			cardRqst.setDePpAccountType("I");
			cardRqst.setDePpName(card.getName());
			cardRqst.setDePpName2(card.getPatern());
			cardRqst.setDePpAddress1(card.getAddress());
			cardRqst.setDePpState(card.getState());
			cardRqst.setDePpCity(card.getCity());
			cardRqst.setDePpGovtIdType("01");
			cardRqst.setDePpGovtIdIssueState("CA");
			cardRqst.setDePpPostalCode(card.getPostalCode());
			cardRqst.setDePpCountryOfIssue("USA");
			cardRqst.setDePpEmailId(card.getEmail());
			cardRqst.setDePpPhoneNumber(card.getPhone());
			cardRqst.setDePpComment("1");
			cardRqst.setDePpDateOfBirth(card.getBirthday());
			cardRqst.setDeBuacctProductId("50651");
			cardRqst.setDeSvcStoreName("FNB Payroll Test");
			cardRqst.setDePpGovtIdIssueDate("");
			cardRqst.setDePpGovtIdExpirationDate("");
			cardRqst.setDePpGovtIdCountryofIssuance("2");
			cardRqst.setDePpcipType("D");
			cardRqst.setDePpssn(card.getSsn());
			cardRqst.setDePpGovtId(card.getSsn());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cardRqst;
	}
	
}
