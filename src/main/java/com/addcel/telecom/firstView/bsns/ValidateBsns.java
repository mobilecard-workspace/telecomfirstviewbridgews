package com.addcel.telecom.firstView.bsns;

import com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapProxy;
import com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest;

public class ValidateBsns {

	public String validateCard(String json){
		try {
			PrepaidServicesSoapProxy proxy = new PrepaidServicesSoapProxy();
			proxy.validateCard(new ValidateCardRequest());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
}