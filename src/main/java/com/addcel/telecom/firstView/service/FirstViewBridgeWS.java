/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.telecom.firstView.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 *
 * @author JOCAMPO
 */
@WebService(name= "Services")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.WRAPPED)
public interface FirstViewBridgeWS {
    
	@WebMethod(operationName = "cardUpdate")
	public String cardUpdate(@WebParam(name = "json") String json);
	
	@WebMethod(operationName = "getInfoUser")
	public String getInfoUser(@WebParam(name = "json")String json);
	
	@WebMethod(operationName = "transactionHistory")
	public String transactionHistory(@WebParam(name = "json") String json);
	
	@WebMethod(operationName = "createCard")
	public String createCard(@WebParam(name = "json") String json);
	
	
	
}