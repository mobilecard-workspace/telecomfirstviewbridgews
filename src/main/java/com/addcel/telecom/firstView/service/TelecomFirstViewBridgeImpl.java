/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.addcel.telecom.firstView.service;

import javax.jws.WebService;

import com.addcel.telecom.firstView.bsns.ClearingBsns;
import com.addcel.telecom.firstView.bsns.CreateCardBsns;

/**
 *
 * @author JOCAMPO
 */
@WebService(name= "MCFirstViewBridgeService", 
	serviceName = "MCFirstViewServiceImpl", 
	targetNamespace = "http://com.addcel.telecom.services.firstView.ws")
public class TelecomFirstViewBridgeImpl implements FirstViewBridgeWS{

	@Override
	public String transactionHistory(String json) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createCard(String json) {
		return new CreateCardBsns().createCard(json);
	}

	@Override
	public String getInfoUser(String json) {
		return new ClearingBsns().clearing(json);
	}

	@Override
	public String cardUpdate(String json) {
		// TODO Auto-generated method stub
		return null;
	}

	
}


