/**
 * ValidateCardResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ValidateCardResponse  implements java.io.Serializable {
    private java.lang.String ERROR_FOUND;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERRMSG;

    private java.lang.String CARD_NUMBER;

    private java.lang.String ACCOUNT_NO;

    private java.lang.String DDA_NO;

    private java.lang.String CARD_STATUS;

    private java.lang.String MANUAL_CARD_STATUS;

    private java.lang.String CURRENT_CARD_BALANCE;

    private java.lang.String FIRSTNAME;

    private java.lang.String MIDDLENAME;

    private java.lang.String LASTNAME;

    private java.lang.String ADDRESS1;

    private java.lang.String ADDRESS2;

    private java.lang.String CITY;

    private java.lang.String STATE;

    private java.lang.String POSTALCODE;

    private java.lang.String COUNTRY_CODE;

    private java.lang.String CARDHOLDER_IDENTIFIED;

    private java.lang.String ACTIVATION_REQUIRED;

    private java.lang.String BSAccountNumber;

    private java.lang.String expirationDate;

    public ValidateCardResponse() {
    }

    public ValidateCardResponse(
           java.lang.String ERROR_FOUND,
           java.lang.String ERR_NUMBER,
           java.lang.String ERRMSG,
           java.lang.String CARD_NUMBER,
           java.lang.String ACCOUNT_NO,
           java.lang.String DDA_NO,
           java.lang.String CARD_STATUS,
           java.lang.String MANUAL_CARD_STATUS,
           java.lang.String CURRENT_CARD_BALANCE,
           java.lang.String FIRSTNAME,
           java.lang.String MIDDLENAME,
           java.lang.String LASTNAME,
           java.lang.String ADDRESS1,
           java.lang.String ADDRESS2,
           java.lang.String CITY,
           java.lang.String STATE,
           java.lang.String POSTALCODE,
           java.lang.String COUNTRY_CODE,
           java.lang.String CARDHOLDER_IDENTIFIED,
           java.lang.String ACTIVATION_REQUIRED,
           java.lang.String BSAccountNumber,
           java.lang.String expirationDate) {
           this.ERROR_FOUND = ERROR_FOUND;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERRMSG = ERRMSG;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.DDA_NO = DDA_NO;
           this.CARD_STATUS = CARD_STATUS;
           this.MANUAL_CARD_STATUS = MANUAL_CARD_STATUS;
           this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
           this.FIRSTNAME = FIRSTNAME;
           this.MIDDLENAME = MIDDLENAME;
           this.LASTNAME = LASTNAME;
           this.ADDRESS1 = ADDRESS1;
           this.ADDRESS2 = ADDRESS2;
           this.CITY = CITY;
           this.STATE = STATE;
           this.POSTALCODE = POSTALCODE;
           this.COUNTRY_CODE = COUNTRY_CODE;
           this.CARDHOLDER_IDENTIFIED = CARDHOLDER_IDENTIFIED;
           this.ACTIVATION_REQUIRED = ACTIVATION_REQUIRED;
           this.BSAccountNumber = BSAccountNumber;
           this.expirationDate = expirationDate;
    }


    /**
     * Gets the ERROR_FOUND value for this ValidateCardResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this ValidateCardResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the ERR_NUMBER value for this ValidateCardResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this ValidateCardResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERRMSG value for this ValidateCardResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this ValidateCardResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the CARD_NUMBER value for this ValidateCardResponse.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this ValidateCardResponse.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ACCOUNT_NO value for this ValidateCardResponse.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this ValidateCardResponse.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the DDA_NO value for this ValidateCardResponse.
     * 
     * @return DDA_NO
     */
    public java.lang.String getDDA_NO() {
        return DDA_NO;
    }


    /**
     * Sets the DDA_NO value for this ValidateCardResponse.
     * 
     * @param DDA_NO
     */
    public void setDDA_NO(java.lang.String DDA_NO) {
        this.DDA_NO = DDA_NO;
    }


    /**
     * Gets the CARD_STATUS value for this ValidateCardResponse.
     * 
     * @return CARD_STATUS
     */
    public java.lang.String getCARD_STATUS() {
        return CARD_STATUS;
    }


    /**
     * Sets the CARD_STATUS value for this ValidateCardResponse.
     * 
     * @param CARD_STATUS
     */
    public void setCARD_STATUS(java.lang.String CARD_STATUS) {
        this.CARD_STATUS = CARD_STATUS;
    }


    /**
     * Gets the MANUAL_CARD_STATUS value for this ValidateCardResponse.
     * 
     * @return MANUAL_CARD_STATUS
     */
    public java.lang.String getMANUAL_CARD_STATUS() {
        return MANUAL_CARD_STATUS;
    }


    /**
     * Sets the MANUAL_CARD_STATUS value for this ValidateCardResponse.
     * 
     * @param MANUAL_CARD_STATUS
     */
    public void setMANUAL_CARD_STATUS(java.lang.String MANUAL_CARD_STATUS) {
        this.MANUAL_CARD_STATUS = MANUAL_CARD_STATUS;
    }


    /**
     * Gets the CURRENT_CARD_BALANCE value for this ValidateCardResponse.
     * 
     * @return CURRENT_CARD_BALANCE
     */
    public java.lang.String getCURRENT_CARD_BALANCE() {
        return CURRENT_CARD_BALANCE;
    }


    /**
     * Sets the CURRENT_CARD_BALANCE value for this ValidateCardResponse.
     * 
     * @param CURRENT_CARD_BALANCE
     */
    public void setCURRENT_CARD_BALANCE(java.lang.String CURRENT_CARD_BALANCE) {
        this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
    }


    /**
     * Gets the FIRSTNAME value for this ValidateCardResponse.
     * 
     * @return FIRSTNAME
     */
    public java.lang.String getFIRSTNAME() {
        return FIRSTNAME;
    }


    /**
     * Sets the FIRSTNAME value for this ValidateCardResponse.
     * 
     * @param FIRSTNAME
     */
    public void setFIRSTNAME(java.lang.String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }


    /**
     * Gets the MIDDLENAME value for this ValidateCardResponse.
     * 
     * @return MIDDLENAME
     */
    public java.lang.String getMIDDLENAME() {
        return MIDDLENAME;
    }


    /**
     * Sets the MIDDLENAME value for this ValidateCardResponse.
     * 
     * @param MIDDLENAME
     */
    public void setMIDDLENAME(java.lang.String MIDDLENAME) {
        this.MIDDLENAME = MIDDLENAME;
    }


    /**
     * Gets the LASTNAME value for this ValidateCardResponse.
     * 
     * @return LASTNAME
     */
    public java.lang.String getLASTNAME() {
        return LASTNAME;
    }


    /**
     * Sets the LASTNAME value for this ValidateCardResponse.
     * 
     * @param LASTNAME
     */
    public void setLASTNAME(java.lang.String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }


    /**
     * Gets the ADDRESS1 value for this ValidateCardResponse.
     * 
     * @return ADDRESS1
     */
    public java.lang.String getADDRESS1() {
        return ADDRESS1;
    }


    /**
     * Sets the ADDRESS1 value for this ValidateCardResponse.
     * 
     * @param ADDRESS1
     */
    public void setADDRESS1(java.lang.String ADDRESS1) {
        this.ADDRESS1 = ADDRESS1;
    }


    /**
     * Gets the ADDRESS2 value for this ValidateCardResponse.
     * 
     * @return ADDRESS2
     */
    public java.lang.String getADDRESS2() {
        return ADDRESS2;
    }


    /**
     * Sets the ADDRESS2 value for this ValidateCardResponse.
     * 
     * @param ADDRESS2
     */
    public void setADDRESS2(java.lang.String ADDRESS2) {
        this.ADDRESS2 = ADDRESS2;
    }


    /**
     * Gets the CITY value for this ValidateCardResponse.
     * 
     * @return CITY
     */
    public java.lang.String getCITY() {
        return CITY;
    }


    /**
     * Sets the CITY value for this ValidateCardResponse.
     * 
     * @param CITY
     */
    public void setCITY(java.lang.String CITY) {
        this.CITY = CITY;
    }


    /**
     * Gets the STATE value for this ValidateCardResponse.
     * 
     * @return STATE
     */
    public java.lang.String getSTATE() {
        return STATE;
    }


    /**
     * Sets the STATE value for this ValidateCardResponse.
     * 
     * @param STATE
     */
    public void setSTATE(java.lang.String STATE) {
        this.STATE = STATE;
    }


    /**
     * Gets the POSTALCODE value for this ValidateCardResponse.
     * 
     * @return POSTALCODE
     */
    public java.lang.String getPOSTALCODE() {
        return POSTALCODE;
    }


    /**
     * Sets the POSTALCODE value for this ValidateCardResponse.
     * 
     * @param POSTALCODE
     */
    public void setPOSTALCODE(java.lang.String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }


    /**
     * Gets the COUNTRY_CODE value for this ValidateCardResponse.
     * 
     * @return COUNTRY_CODE
     */
    public java.lang.String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }


    /**
     * Sets the COUNTRY_CODE value for this ValidateCardResponse.
     * 
     * @param COUNTRY_CODE
     */
    public void setCOUNTRY_CODE(java.lang.String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }


    /**
     * Gets the CARDHOLDER_IDENTIFIED value for this ValidateCardResponse.
     * 
     * @return CARDHOLDER_IDENTIFIED
     */
    public java.lang.String getCARDHOLDER_IDENTIFIED() {
        return CARDHOLDER_IDENTIFIED;
    }


    /**
     * Sets the CARDHOLDER_IDENTIFIED value for this ValidateCardResponse.
     * 
     * @param CARDHOLDER_IDENTIFIED
     */
    public void setCARDHOLDER_IDENTIFIED(java.lang.String CARDHOLDER_IDENTIFIED) {
        this.CARDHOLDER_IDENTIFIED = CARDHOLDER_IDENTIFIED;
    }


    /**
     * Gets the ACTIVATION_REQUIRED value for this ValidateCardResponse.
     * 
     * @return ACTIVATION_REQUIRED
     */
    public java.lang.String getACTIVATION_REQUIRED() {
        return ACTIVATION_REQUIRED;
    }


    /**
     * Sets the ACTIVATION_REQUIRED value for this ValidateCardResponse.
     * 
     * @param ACTIVATION_REQUIRED
     */
    public void setACTIVATION_REQUIRED(java.lang.String ACTIVATION_REQUIRED) {
        this.ACTIVATION_REQUIRED = ACTIVATION_REQUIRED;
    }


    /**
     * Gets the BSAccountNumber value for this ValidateCardResponse.
     * 
     * @return BSAccountNumber
     */
    public java.lang.String getBSAccountNumber() {
        return BSAccountNumber;
    }


    /**
     * Sets the BSAccountNumber value for this ValidateCardResponse.
     * 
     * @param BSAccountNumber
     */
    public void setBSAccountNumber(java.lang.String BSAccountNumber) {
        this.BSAccountNumber = BSAccountNumber;
    }


    /**
     * Gets the expirationDate value for this ValidateCardResponse.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this ValidateCardResponse.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidateCardResponse)) return false;
        ValidateCardResponse other = (ValidateCardResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.DDA_NO==null && other.getDDA_NO()==null) || 
             (this.DDA_NO!=null &&
              this.DDA_NO.equals(other.getDDA_NO()))) &&
            ((this.CARD_STATUS==null && other.getCARD_STATUS()==null) || 
             (this.CARD_STATUS!=null &&
              this.CARD_STATUS.equals(other.getCARD_STATUS()))) &&
            ((this.MANUAL_CARD_STATUS==null && other.getMANUAL_CARD_STATUS()==null) || 
             (this.MANUAL_CARD_STATUS!=null &&
              this.MANUAL_CARD_STATUS.equals(other.getMANUAL_CARD_STATUS()))) &&
            ((this.CURRENT_CARD_BALANCE==null && other.getCURRENT_CARD_BALANCE()==null) || 
             (this.CURRENT_CARD_BALANCE!=null &&
              this.CURRENT_CARD_BALANCE.equals(other.getCURRENT_CARD_BALANCE()))) &&
            ((this.FIRSTNAME==null && other.getFIRSTNAME()==null) || 
             (this.FIRSTNAME!=null &&
              this.FIRSTNAME.equals(other.getFIRSTNAME()))) &&
            ((this.MIDDLENAME==null && other.getMIDDLENAME()==null) || 
             (this.MIDDLENAME!=null &&
              this.MIDDLENAME.equals(other.getMIDDLENAME()))) &&
            ((this.LASTNAME==null && other.getLASTNAME()==null) || 
             (this.LASTNAME!=null &&
              this.LASTNAME.equals(other.getLASTNAME()))) &&
            ((this.ADDRESS1==null && other.getADDRESS1()==null) || 
             (this.ADDRESS1!=null &&
              this.ADDRESS1.equals(other.getADDRESS1()))) &&
            ((this.ADDRESS2==null && other.getADDRESS2()==null) || 
             (this.ADDRESS2!=null &&
              this.ADDRESS2.equals(other.getADDRESS2()))) &&
            ((this.CITY==null && other.getCITY()==null) || 
             (this.CITY!=null &&
              this.CITY.equals(other.getCITY()))) &&
            ((this.STATE==null && other.getSTATE()==null) || 
             (this.STATE!=null &&
              this.STATE.equals(other.getSTATE()))) &&
            ((this.POSTALCODE==null && other.getPOSTALCODE()==null) || 
             (this.POSTALCODE!=null &&
              this.POSTALCODE.equals(other.getPOSTALCODE()))) &&
            ((this.COUNTRY_CODE==null && other.getCOUNTRY_CODE()==null) || 
             (this.COUNTRY_CODE!=null &&
              this.COUNTRY_CODE.equals(other.getCOUNTRY_CODE()))) &&
            ((this.CARDHOLDER_IDENTIFIED==null && other.getCARDHOLDER_IDENTIFIED()==null) || 
             (this.CARDHOLDER_IDENTIFIED!=null &&
              this.CARDHOLDER_IDENTIFIED.equals(other.getCARDHOLDER_IDENTIFIED()))) &&
            ((this.ACTIVATION_REQUIRED==null && other.getACTIVATION_REQUIRED()==null) || 
             (this.ACTIVATION_REQUIRED!=null &&
              this.ACTIVATION_REQUIRED.equals(other.getACTIVATION_REQUIRED()))) &&
            ((this.BSAccountNumber==null && other.getBSAccountNumber()==null) || 
             (this.BSAccountNumber!=null &&
              this.BSAccountNumber.equals(other.getBSAccountNumber()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getDDA_NO() != null) {
            _hashCode += getDDA_NO().hashCode();
        }
        if (getCARD_STATUS() != null) {
            _hashCode += getCARD_STATUS().hashCode();
        }
        if (getMANUAL_CARD_STATUS() != null) {
            _hashCode += getMANUAL_CARD_STATUS().hashCode();
        }
        if (getCURRENT_CARD_BALANCE() != null) {
            _hashCode += getCURRENT_CARD_BALANCE().hashCode();
        }
        if (getFIRSTNAME() != null) {
            _hashCode += getFIRSTNAME().hashCode();
        }
        if (getMIDDLENAME() != null) {
            _hashCode += getMIDDLENAME().hashCode();
        }
        if (getLASTNAME() != null) {
            _hashCode += getLASTNAME().hashCode();
        }
        if (getADDRESS1() != null) {
            _hashCode += getADDRESS1().hashCode();
        }
        if (getADDRESS2() != null) {
            _hashCode += getADDRESS2().hashCode();
        }
        if (getCITY() != null) {
            _hashCode += getCITY().hashCode();
        }
        if (getSTATE() != null) {
            _hashCode += getSTATE().hashCode();
        }
        if (getPOSTALCODE() != null) {
            _hashCode += getPOSTALCODE().hashCode();
        }
        if (getCOUNTRY_CODE() != null) {
            _hashCode += getCOUNTRY_CODE().hashCode();
        }
        if (getCARDHOLDER_IDENTIFIED() != null) {
            _hashCode += getCARDHOLDER_IDENTIFIED().hashCode();
        }
        if (getACTIVATION_REQUIRED() != null) {
            _hashCode += getACTIVATION_REQUIRED().hashCode();
        }
        if (getBSAccountNumber() != null) {
            _hashCode += getBSAccountNumber().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidateCardResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCardResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDA_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DDA_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MANUAL_CARD_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MANUAL_CARD_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENT_CARD_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CURRENT_CARD_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRSTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FIRSTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MIDDLENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MIDDLENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTALCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "POSTALCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTRY_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "COUNTRY_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDHOLDER_IDENTIFIED");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDHOLDER_IDENTIFIED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACTIVATION_REQUIRED");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACTIVATION_REQUIRED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BSAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "BSAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
