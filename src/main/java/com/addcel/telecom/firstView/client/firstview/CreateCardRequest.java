//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CreateCardRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateCardRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DePpAcctCreationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpAcctCreationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpReferenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpAccountType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeMiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtIdType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtIdIssueState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpCountryOfIssue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpEmailId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpDateOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeBuacctProductId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpWorkPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpMobilePhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpOtherPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpFollowUpDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpEmployername" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpEmployerContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpEmployerContactPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpEmployerContactFaxNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpMemos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpCustRefName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeSvcStoreName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtIdIssueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtIdExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpGovtIdCountryofIssuance" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpcipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpcipNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpcipStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpssn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeIvrEmbossingLine4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeIvrEmbossingHotStamp" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpSecondaryPan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCardRequest", propOrder = {
    "dePpAcctCreationDate",
    "dePpAcctCreationTime",
    "dePpReferenceNo",
    "dePpCurrencyCode",
    "dePpAccountType",
    "dePpName",
    "deMiddleName",
    "dePpName2",
    "dePpAddress1",
    "dePpAddress2",
    "dePpState",
    "dePpCity",
    "dePpGovtIdType",
    "dePpGovtIdIssueState",
    "dePpPostalCode",
    "dePpCountryOfIssue",
    "dePpEmailId",
    "dePpGovtId",
    "dePpPhoneNumber",
    "dePpComment",
    "dePpDateOfBirth",
    "deBuacctProductId",
    "dePpWorkPhoneNumber",
    "dePpMobilePhoneNumber",
    "dePpOtherPhoneNumber",
    "dePpFollowUpDate",
    "dePpEmployername",
    "dePpEmployerContactName",
    "dePpEmployerContactPhoneNumber",
    "dePpEmployerContactFaxNumber",
    "dePpMemos",
    "dePpCustRefName",
    "deSvcStoreName",
    "dePpGovtIdIssueDate",
    "dePpGovtIdExpirationDate",
    "dePpGovtIdCountryofIssuance",
    "dePpcipType",
    "dePpcipNumber",
    "dePpcipStatus",
    "dePpssn",
    "deIvrEmbossingLine4",
    "deIvrEmbossingHotStamp",
    "dePpSecondaryPan"
})
public class CreateCardRequest {

    @XmlElement(name = "DePpAcctCreationDate")
    protected String dePpAcctCreationDate;
    @XmlElement(name = "DePpAcctCreationTime")
    protected String dePpAcctCreationTime;
    @XmlElement(name = "DePpReferenceNo")
    protected String dePpReferenceNo;
    @XmlElement(name = "DePpCurrencyCode")
    protected String dePpCurrencyCode;
    @XmlElement(name = "DePpAccountType")
    protected String dePpAccountType;
    @XmlElement(name = "DePpName")
    protected String dePpName;
    @XmlElement(name = "DeMiddleName")
    protected String deMiddleName;
    @XmlElement(name = "DePpName2")
    protected String dePpName2;
    @XmlElement(name = "DePpAddress1")
    protected String dePpAddress1;
    @XmlElement(name = "DePpAddress2")
    protected String dePpAddress2;
    @XmlElement(name = "DePpState")
    protected String dePpState;
    @XmlElement(name = "DePpCity")
    protected String dePpCity;
    @XmlElement(name = "DePpGovtIdType")
    protected String dePpGovtIdType;
    @XmlElement(name = "DePpGovtIdIssueState")
    protected String dePpGovtIdIssueState;
    @XmlElement(name = "DePpPostalCode")
    protected String dePpPostalCode;
    @XmlElement(name = "DePpCountryOfIssue")
    protected String dePpCountryOfIssue;
    @XmlElement(name = "DePpEmailId")
    protected String dePpEmailId;
    @XmlElement(name = "DePpGovtId")
    protected String dePpGovtId;
    @XmlElement(name = "DePpPhoneNumber")
    protected String dePpPhoneNumber;
    @XmlElement(name = "DePpComment")
    protected String dePpComment;
    @XmlElement(name = "DePpDateOfBirth")
    protected String dePpDateOfBirth;
    @XmlElement(name = "DeBuacctProductId")
    protected String deBuacctProductId;
    @XmlElement(name = "DePpWorkPhoneNumber")
    protected String dePpWorkPhoneNumber;
    @XmlElement(name = "DePpMobilePhoneNumber")
    protected String dePpMobilePhoneNumber;
    @XmlElement(name = "DePpOtherPhoneNumber")
    protected String dePpOtherPhoneNumber;
    @XmlElement(name = "DePpFollowUpDate")
    protected String dePpFollowUpDate;
    @XmlElement(name = "DePpEmployername")
    protected String dePpEmployername;
    @XmlElement(name = "DePpEmployerContactName")
    protected String dePpEmployerContactName;
    @XmlElement(name = "DePpEmployerContactPhoneNumber")
    protected String dePpEmployerContactPhoneNumber;
    @XmlElement(name = "DePpEmployerContactFaxNumber")
    protected String dePpEmployerContactFaxNumber;
    @XmlElement(name = "DePpMemos")
    protected String dePpMemos;
    @XmlElement(name = "DePpCustRefName")
    protected String dePpCustRefName;
    @XmlElement(name = "DeSvcStoreName")
    protected String deSvcStoreName;
    @XmlElement(name = "DePpGovtIdIssueDate")
    protected String dePpGovtIdIssueDate;
    @XmlElement(name = "DePpGovtIdExpirationDate")
    protected String dePpGovtIdExpirationDate;
    @XmlElement(name = "DePpGovtIdCountryofIssuance")
    protected String dePpGovtIdCountryofIssuance;
    @XmlElement(name = "DePpcipType")
    protected String dePpcipType;
    @XmlElement(name = "DePpcipNumber")
    protected String dePpcipNumber;
    @XmlElement(name = "DePpcipStatus")
    protected String dePpcipStatus;
    @XmlElement(name = "DePpssn")
    protected String dePpssn;
    @XmlElement(name = "DeIvrEmbossingLine4")
    protected String deIvrEmbossingLine4;
    @XmlElement(name = "DeIvrEmbossingHotStamp")
    protected String deIvrEmbossingHotStamp;
    @XmlElement(name = "DePpSecondaryPan")
    protected String dePpSecondaryPan;

    /**
     * Obtiene el valor de la propiedad dePpAcctCreationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAcctCreationDate() {
        return dePpAcctCreationDate;
    }

    /**
     * Define el valor de la propiedad dePpAcctCreationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAcctCreationDate(String value) {
        this.dePpAcctCreationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpAcctCreationTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAcctCreationTime() {
        return dePpAcctCreationTime;
    }

    /**
     * Define el valor de la propiedad dePpAcctCreationTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAcctCreationTime(String value) {
        this.dePpAcctCreationTime = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpReferenceNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpReferenceNo() {
        return dePpReferenceNo;
    }

    /**
     * Define el valor de la propiedad dePpReferenceNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpReferenceNo(String value) {
        this.dePpReferenceNo = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpCurrencyCode() {
        return dePpCurrencyCode;
    }

    /**
     * Define el valor de la propiedad dePpCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpCurrencyCode(String value) {
        this.dePpCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpAccountType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAccountType() {
        return dePpAccountType;
    }

    /**
     * Define el valor de la propiedad dePpAccountType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAccountType(String value) {
        this.dePpAccountType = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpName() {
        return dePpName;
    }

    /**
     * Define el valor de la propiedad dePpName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpName(String value) {
        this.dePpName = value;
    }

    /**
     * Obtiene el valor de la propiedad deMiddleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeMiddleName() {
        return deMiddleName;
    }

    /**
     * Define el valor de la propiedad deMiddleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeMiddleName(String value) {
        this.deMiddleName = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpName2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpName2() {
        return dePpName2;
    }

    /**
     * Define el valor de la propiedad dePpName2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpName2(String value) {
        this.dePpName2 = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpAddress1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAddress1() {
        return dePpAddress1;
    }

    /**
     * Define el valor de la propiedad dePpAddress1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAddress1(String value) {
        this.dePpAddress1 = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpAddress2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAddress2() {
        return dePpAddress2;
    }

    /**
     * Define el valor de la propiedad dePpAddress2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAddress2(String value) {
        this.dePpAddress2 = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpState() {
        return dePpState;
    }

    /**
     * Define el valor de la propiedad dePpState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpState(String value) {
        this.dePpState = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpCity() {
        return dePpCity;
    }

    /**
     * Define el valor de la propiedad dePpCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpCity(String value) {
        this.dePpCity = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtIdType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtIdType() {
        return dePpGovtIdType;
    }

    /**
     * Define el valor de la propiedad dePpGovtIdType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtIdType(String value) {
        this.dePpGovtIdType = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtIdIssueState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtIdIssueState() {
        return dePpGovtIdIssueState;
    }

    /**
     * Define el valor de la propiedad dePpGovtIdIssueState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtIdIssueState(String value) {
        this.dePpGovtIdIssueState = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpPostalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpPostalCode() {
        return dePpPostalCode;
    }

    /**
     * Define el valor de la propiedad dePpPostalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpPostalCode(String value) {
        this.dePpPostalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpCountryOfIssue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpCountryOfIssue() {
        return dePpCountryOfIssue;
    }

    /**
     * Define el valor de la propiedad dePpCountryOfIssue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpCountryOfIssue(String value) {
        this.dePpCountryOfIssue = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpEmailId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpEmailId() {
        return dePpEmailId;
    }

    /**
     * Define el valor de la propiedad dePpEmailId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpEmailId(String value) {
        this.dePpEmailId = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtId() {
        return dePpGovtId;
    }

    /**
     * Define el valor de la propiedad dePpGovtId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtId(String value) {
        this.dePpGovtId = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpPhoneNumber() {
        return dePpPhoneNumber;
    }

    /**
     * Define el valor de la propiedad dePpPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpPhoneNumber(String value) {
        this.dePpPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpComment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpComment() {
        return dePpComment;
    }

    /**
     * Define el valor de la propiedad dePpComment.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpComment(String value) {
        this.dePpComment = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpDateOfBirth.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpDateOfBirth() {
        return dePpDateOfBirth;
    }

    /**
     * Define el valor de la propiedad dePpDateOfBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpDateOfBirth(String value) {
        this.dePpDateOfBirth = value;
    }

    /**
     * Obtiene el valor de la propiedad deBuacctProductId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeBuacctProductId() {
        return deBuacctProductId;
    }

    /**
     * Define el valor de la propiedad deBuacctProductId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeBuacctProductId(String value) {
        this.deBuacctProductId = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpWorkPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpWorkPhoneNumber() {
        return dePpWorkPhoneNumber;
    }

    /**
     * Define el valor de la propiedad dePpWorkPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpWorkPhoneNumber(String value) {
        this.dePpWorkPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpMobilePhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpMobilePhoneNumber() {
        return dePpMobilePhoneNumber;
    }

    /**
     * Define el valor de la propiedad dePpMobilePhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpMobilePhoneNumber(String value) {
        this.dePpMobilePhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpOtherPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpOtherPhoneNumber() {
        return dePpOtherPhoneNumber;
    }

    /**
     * Define el valor de la propiedad dePpOtherPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpOtherPhoneNumber(String value) {
        this.dePpOtherPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpFollowUpDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpFollowUpDate() {
        return dePpFollowUpDate;
    }

    /**
     * Define el valor de la propiedad dePpFollowUpDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpFollowUpDate(String value) {
        this.dePpFollowUpDate = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpEmployername.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpEmployername() {
        return dePpEmployername;
    }

    /**
     * Define el valor de la propiedad dePpEmployername.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpEmployername(String value) {
        this.dePpEmployername = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpEmployerContactName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpEmployerContactName() {
        return dePpEmployerContactName;
    }

    /**
     * Define el valor de la propiedad dePpEmployerContactName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpEmployerContactName(String value) {
        this.dePpEmployerContactName = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpEmployerContactPhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpEmployerContactPhoneNumber() {
        return dePpEmployerContactPhoneNumber;
    }

    /**
     * Define el valor de la propiedad dePpEmployerContactPhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpEmployerContactPhoneNumber(String value) {
        this.dePpEmployerContactPhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpEmployerContactFaxNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpEmployerContactFaxNumber() {
        return dePpEmployerContactFaxNumber;
    }

    /**
     * Define el valor de la propiedad dePpEmployerContactFaxNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpEmployerContactFaxNumber(String value) {
        this.dePpEmployerContactFaxNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpMemos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpMemos() {
        return dePpMemos;
    }

    /**
     * Define el valor de la propiedad dePpMemos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpMemos(String value) {
        this.dePpMemos = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpCustRefName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpCustRefName() {
        return dePpCustRefName;
    }

    /**
     * Define el valor de la propiedad dePpCustRefName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpCustRefName(String value) {
        this.dePpCustRefName = value;
    }

    /**
     * Obtiene el valor de la propiedad deSvcStoreName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeSvcStoreName() {
        return deSvcStoreName;
    }

    /**
     * Define el valor de la propiedad deSvcStoreName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeSvcStoreName(String value) {
        this.deSvcStoreName = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtIdIssueDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtIdIssueDate() {
        return dePpGovtIdIssueDate;
    }

    /**
     * Define el valor de la propiedad dePpGovtIdIssueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtIdIssueDate(String value) {
        this.dePpGovtIdIssueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtIdExpirationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtIdExpirationDate() {
        return dePpGovtIdExpirationDate;
    }

    /**
     * Define el valor de la propiedad dePpGovtIdExpirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtIdExpirationDate(String value) {
        this.dePpGovtIdExpirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpGovtIdCountryofIssuance.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpGovtIdCountryofIssuance() {
        return dePpGovtIdCountryofIssuance;
    }

    /**
     * Define el valor de la propiedad dePpGovtIdCountryofIssuance.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpGovtIdCountryofIssuance(String value) {
        this.dePpGovtIdCountryofIssuance = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpcipType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpcipType() {
        return dePpcipType;
    }

    /**
     * Define el valor de la propiedad dePpcipType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpcipType(String value) {
        this.dePpcipType = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpcipNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpcipNumber() {
        return dePpcipNumber;
    }

    /**
     * Define el valor de la propiedad dePpcipNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpcipNumber(String value) {
        this.dePpcipNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpcipStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpcipStatus() {
        return dePpcipStatus;
    }

    /**
     * Define el valor de la propiedad dePpcipStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpcipStatus(String value) {
        this.dePpcipStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpssn.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpssn() {
        return dePpssn;
    }

    /**
     * Define el valor de la propiedad dePpssn.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpssn(String value) {
        this.dePpssn = value;
    }

    /**
     * Obtiene el valor de la propiedad deIvrEmbossingLine4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeIvrEmbossingLine4() {
        return deIvrEmbossingLine4;
    }

    /**
     * Define el valor de la propiedad deIvrEmbossingLine4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeIvrEmbossingLine4(String value) {
        this.deIvrEmbossingLine4 = value;
    }

    /**
     * Obtiene el valor de la propiedad deIvrEmbossingHotStamp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeIvrEmbossingHotStamp() {
        return deIvrEmbossingHotStamp;
    }

    /**
     * Define el valor de la propiedad deIvrEmbossingHotStamp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeIvrEmbossingHotStamp(String value) {
        this.deIvrEmbossingHotStamp = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpSecondaryPan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpSecondaryPan() {
        return dePpSecondaryPan;
    }

    /**
     * Define el valor de la propiedad dePpSecondaryPan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpSecondaryPan(String value) {
        this.dePpSecondaryPan = value;
    }

}
