/**
 * FVServiceSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public interface FVServiceSoap extends java.rmi.Remote {
    public com.addcel.telecom.firstView.client.axis.GetCvcResponse getCVC(com.addcel.telecom.firstView.client.axis.GetCvcRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.ClearingResponse clearing(com.addcel.telecom.firstView.client.axis.ClearingRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.CardUpdateResponse cardUpdate(com.addcel.telecom.firstView.client.axis.CardUpdateRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.ValidatePinResponse validatePin(com.addcel.telecom.firstView.client.axis.ValidatePinRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.CreateCardResponse createCard(com.addcel.telecom.firstView.client.axis.CreateCardRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.PictureCardResponse pictureCard(com.addcel.telecom.firstView.client.axis.PictureCardRequest request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.GetStatusResponse getStatusDesc(com.addcel.telecom.firstView.client.axis.GetStatusRequest statReq) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.CreateCIPCardResponse createCIPCard(com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest CACr) throws java.rmi.RemoteException;
}
