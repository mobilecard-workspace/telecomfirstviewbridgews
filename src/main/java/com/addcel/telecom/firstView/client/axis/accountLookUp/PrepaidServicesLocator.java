/**
 * PrepaidServicesLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PrepaidServicesLocator extends org.apache.axis.client.Service implements com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServices {

    public PrepaidServicesLocator() {
    }


    public PrepaidServicesLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PrepaidServicesLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PrepaidServicesSoap
    private java.lang.String PrepaidServicesSoap_address = "https://firstviewcorp.com/dbbapplications/ServicesSS/Selfservice.asmx";

    public java.lang.String getPrepaidServicesSoapAddress() {
        return PrepaidServicesSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PrepaidServicesSoapWSDDServiceName = "PrepaidServicesSoap";

    public java.lang.String getPrepaidServicesSoapWSDDServiceName() {
        return PrepaidServicesSoapWSDDServiceName;
    }

    public void setPrepaidServicesSoapWSDDServiceName(java.lang.String name) {
        PrepaidServicesSoapWSDDServiceName = name;
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap getPrepaidServicesSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PrepaidServicesSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPrepaidServicesSoap(endpoint);
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap getPrepaidServicesSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapStub _stub = new com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapStub(portAddress, this);
            _stub.setPortName(getPrepaidServicesSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPrepaidServicesSoapEndpointAddress(java.lang.String address) {
        PrepaidServicesSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapStub _stub = new com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoapStub(new java.net.URL(PrepaidServicesSoap_address), this);
                _stub.setPortName(getPrepaidServicesSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PrepaidServicesSoap".equals(inputPortName)) {
            return getPrepaidServicesSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PrepaidServices");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PrepaidServicesSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PrepaidServicesSoap".equals(portName)) {
            setPrepaidServicesSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
