/**
 * ValidatePinResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class ValidatePinResponse  implements java.io.Serializable {
    private java.lang.String cardNumber;

    private java.lang.String pin;

    private java.lang.String resErrorCode;

    private java.lang.String resErrorMsg;

    public ValidatePinResponse() {
    }

    public ValidatePinResponse(
           java.lang.String cardNumber,
           java.lang.String pin,
           java.lang.String resErrorCode,
           java.lang.String resErrorMsg) {
           this.cardNumber = cardNumber;
           this.pin = pin;
           this.resErrorCode = resErrorCode;
           this.resErrorMsg = resErrorMsg;
    }


    /**
     * Gets the cardNumber value for this ValidatePinResponse.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this ValidatePinResponse.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the pin value for this ValidatePinResponse.
     * 
     * @return pin
     */
    public java.lang.String getPin() {
        return pin;
    }


    /**
     * Sets the pin value for this ValidatePinResponse.
     * 
     * @param pin
     */
    public void setPin(java.lang.String pin) {
        this.pin = pin;
    }


    /**
     * Gets the resErrorCode value for this ValidatePinResponse.
     * 
     * @return resErrorCode
     */
    public java.lang.String getResErrorCode() {
        return resErrorCode;
    }


    /**
     * Sets the resErrorCode value for this ValidatePinResponse.
     * 
     * @param resErrorCode
     */
    public void setResErrorCode(java.lang.String resErrorCode) {
        this.resErrorCode = resErrorCode;
    }


    /**
     * Gets the resErrorMsg value for this ValidatePinResponse.
     * 
     * @return resErrorMsg
     */
    public java.lang.String getResErrorMsg() {
        return resErrorMsg;
    }


    /**
     * Sets the resErrorMsg value for this ValidatePinResponse.
     * 
     * @param resErrorMsg
     */
    public void setResErrorMsg(java.lang.String resErrorMsg) {
        this.resErrorMsg = resErrorMsg;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidatePinResponse)) return false;
        ValidatePinResponse other = (ValidatePinResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.pin==null && other.getPin()==null) || 
             (this.pin!=null &&
              this.pin.equals(other.getPin()))) &&
            ((this.resErrorCode==null && other.getResErrorCode()==null) || 
             (this.resErrorCode!=null &&
              this.resErrorCode.equals(other.getResErrorCode()))) &&
            ((this.resErrorMsg==null && other.getResErrorMsg()==null) || 
             (this.resErrorMsg!=null &&
              this.resErrorMsg.equals(other.getResErrorMsg())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getPin() != null) {
            _hashCode += getPin().hashCode();
        }
        if (getResErrorCode() != null) {
            _hashCode += getResErrorCode().hashCode();
        }
        if (getResErrorMsg() != null) {
            _hashCode += getResErrorMsg().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidatePinResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "ValidatePinResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pin");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Pin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resErrorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resErrorMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResErrorMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
