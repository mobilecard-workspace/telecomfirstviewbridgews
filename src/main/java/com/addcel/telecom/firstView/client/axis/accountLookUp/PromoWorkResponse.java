/**
 * PromoWorkResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PromoWorkResponse  implements java.io.Serializable {
    private java.lang.String accountNumber;

    private com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray[] CARD_LIST;

    private java.lang.String errorMessage;

    private java.lang.String errorNumber;

    public PromoWorkResponse() {
    }

    public PromoWorkResponse(
           java.lang.String accountNumber,
           com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray[] CARD_LIST,
           java.lang.String errorMessage,
           java.lang.String errorNumber) {
           this.accountNumber = accountNumber;
           this.CARD_LIST = CARD_LIST;
           this.errorMessage = errorMessage;
           this.errorNumber = errorNumber;
    }


    /**
     * Gets the accountNumber value for this PromoWorkResponse.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this PromoWorkResponse.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the CARD_LIST value for this PromoWorkResponse.
     * 
     * @return CARD_LIST
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray[] getCARD_LIST() {
        return CARD_LIST;
    }


    /**
     * Sets the CARD_LIST value for this PromoWorkResponse.
     * 
     * @param CARD_LIST
     */
    public void setCARD_LIST(com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray[] CARD_LIST) {
        this.CARD_LIST = CARD_LIST;
    }


    /**
     * Gets the errorMessage value for this PromoWorkResponse.
     * 
     * @return errorMessage
     */
    public java.lang.String getErrorMessage() {
        return errorMessage;
    }


    /**
     * Sets the errorMessage value for this PromoWorkResponse.
     * 
     * @param errorMessage
     */
    public void setErrorMessage(java.lang.String errorMessage) {
        this.errorMessage = errorMessage;
    }


    /**
     * Gets the errorNumber value for this PromoWorkResponse.
     * 
     * @return errorNumber
     */
    public java.lang.String getErrorNumber() {
        return errorNumber;
    }


    /**
     * Sets the errorNumber value for this PromoWorkResponse.
     * 
     * @param errorNumber
     */
    public void setErrorNumber(java.lang.String errorNumber) {
        this.errorNumber = errorNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PromoWorkResponse)) return false;
        PromoWorkResponse other = (PromoWorkResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.CARD_LIST==null && other.getCARD_LIST()==null) || 
             (this.CARD_LIST!=null &&
              java.util.Arrays.equals(this.CARD_LIST, other.getCARD_LIST()))) &&
            ((this.errorMessage==null && other.getErrorMessage()==null) || 
             (this.errorMessage!=null &&
              this.errorMessage.equals(other.getErrorMessage()))) &&
            ((this.errorNumber==null && other.getErrorNumber()==null) || 
             (this.errorNumber!=null &&
              this.errorNumber.equals(other.getErrorNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getCARD_LIST() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCARD_LIST());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCARD_LIST(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErrorMessage() != null) {
            _hashCode += getErrorMessage().hashCode();
        }
        if (getErrorNumber() != null) {
            _hashCode += getErrorNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PromoWorkResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_LIST");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_LIST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardDetailArray"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDDETAIL"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorMessage");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ErrorMessage"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ErrorNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
