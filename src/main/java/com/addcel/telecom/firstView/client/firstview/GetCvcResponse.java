//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GetCvcResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetCvcResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cvc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Errmsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorFound" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetCvcResponse", propOrder = {
    "cvc",
    "errNumber",
    "errmsg",
    "errorFound"
})
public class GetCvcResponse {

    @XmlElement(name = "Cvc")
    protected String cvc;
    @XmlElement(name = "ErrNumber")
    protected String errNumber;
    @XmlElement(name = "Errmsg")
    protected String errmsg;
    @XmlElement(name = "ErrorFound")
    protected String errorFound;

    /**
     * Obtiene el valor de la propiedad cvc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCvc() {
        return cvc;
    }

    /**
     * Define el valor de la propiedad cvc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCvc(String value) {
        this.cvc = value;
    }

    /**
     * Obtiene el valor de la propiedad errNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrNumber() {
        return errNumber;
    }

    /**
     * Define el valor de la propiedad errNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrNumber(String value) {
        this.errNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad errmsg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrmsg() {
        return errmsg;
    }

    /**
     * Define el valor de la propiedad errmsg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrmsg(String value) {
        this.errmsg = value;
    }

    /**
     * Obtiene el valor de la propiedad errorFound.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorFound() {
        return errorFound;
    }

    /**
     * Define el valor de la propiedad errorFound.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorFound(String value) {
        this.errorFound = value;
    }

}
