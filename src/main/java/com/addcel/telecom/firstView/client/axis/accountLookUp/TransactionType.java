/**
 * TransactionType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class TransactionType  implements java.io.Serializable {
    private java.lang.String DATE;  // attribute

    private java.lang.String TIME;  // attribute

    private java.lang.String AMOUNT;  // attribute

    private java.lang.String FEE;  // attribute

    private java.lang.String STATUS;  // attribute

    private java.lang.String TRANSACTION_TYPE;  // attribute

    private java.lang.String TRANSACTION_MCC;  // attribute

    private java.lang.String DESCRIPTION1;  // attribute

    private java.lang.String DESCRIPTION2;  // attribute

    private java.lang.String DESCRIPTION3;  // attribute

    private java.lang.String DESCRIPTION4;  // attribute

    private java.lang.String TRANSACTION_NUMBER;  // attribute

    private java.lang.String TRANSACTION_LifeCycleUniqueID;  // attribute

    private java.lang.String MERCHANT_ID;  // attribute

    private java.lang.String MERCHANT_NAME;  // attribute

    private java.lang.String MERCHANT_CITY;  // attribute

    private java.lang.String MERCHANT_STATE;  // attribute

    public TransactionType() {
    }

    public TransactionType(
           java.lang.String DATE,
           java.lang.String TIME,
           java.lang.String AMOUNT,
           java.lang.String FEE,
           java.lang.String STATUS,
           java.lang.String TRANSACTION_TYPE,
           java.lang.String TRANSACTION_MCC,
           java.lang.String DESCRIPTION1,
           java.lang.String DESCRIPTION2,
           java.lang.String DESCRIPTION3,
           java.lang.String DESCRIPTION4,
           java.lang.String TRANSACTION_NUMBER,
           java.lang.String TRANSACTION_LifeCycleUniqueID,
           java.lang.String MERCHANT_ID,
           java.lang.String MERCHANT_NAME,
           java.lang.String MERCHANT_CITY,
           java.lang.String MERCHANT_STATE) {
           this.DATE = DATE;
           this.TIME = TIME;
           this.AMOUNT = AMOUNT;
           this.FEE = FEE;
           this.STATUS = STATUS;
           this.TRANSACTION_TYPE = TRANSACTION_TYPE;
           this.TRANSACTION_MCC = TRANSACTION_MCC;
           this.DESCRIPTION1 = DESCRIPTION1;
           this.DESCRIPTION2 = DESCRIPTION2;
           this.DESCRIPTION3 = DESCRIPTION3;
           this.DESCRIPTION4 = DESCRIPTION4;
           this.TRANSACTION_NUMBER = TRANSACTION_NUMBER;
           this.TRANSACTION_LifeCycleUniqueID = TRANSACTION_LifeCycleUniqueID;
           this.MERCHANT_ID = MERCHANT_ID;
           this.MERCHANT_NAME = MERCHANT_NAME;
           this.MERCHANT_CITY = MERCHANT_CITY;
           this.MERCHANT_STATE = MERCHANT_STATE;
    }


    /**
     * Gets the DATE value for this TransactionType.
     * 
     * @return DATE
     */
    public java.lang.String getDATE() {
        return DATE;
    }


    /**
     * Sets the DATE value for this TransactionType.
     * 
     * @param DATE
     */
    public void setDATE(java.lang.String DATE) {
        this.DATE = DATE;
    }


    /**
     * Gets the TIME value for this TransactionType.
     * 
     * @return TIME
     */
    public java.lang.String getTIME() {
        return TIME;
    }


    /**
     * Sets the TIME value for this TransactionType.
     * 
     * @param TIME
     */
    public void setTIME(java.lang.String TIME) {
        this.TIME = TIME;
    }


    /**
     * Gets the AMOUNT value for this TransactionType.
     * 
     * @return AMOUNT
     */
    public java.lang.String getAMOUNT() {
        return AMOUNT;
    }


    /**
     * Sets the AMOUNT value for this TransactionType.
     * 
     * @param AMOUNT
     */
    public void setAMOUNT(java.lang.String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }


    /**
     * Gets the FEE value for this TransactionType.
     * 
     * @return FEE
     */
    public java.lang.String getFEE() {
        return FEE;
    }


    /**
     * Sets the FEE value for this TransactionType.
     * 
     * @param FEE
     */
    public void setFEE(java.lang.String FEE) {
        this.FEE = FEE;
    }


    /**
     * Gets the STATUS value for this TransactionType.
     * 
     * @return STATUS
     */
    public java.lang.String getSTATUS() {
        return STATUS;
    }


    /**
     * Sets the STATUS value for this TransactionType.
     * 
     * @param STATUS
     */
    public void setSTATUS(java.lang.String STATUS) {
        this.STATUS = STATUS;
    }


    /**
     * Gets the TRANSACTION_TYPE value for this TransactionType.
     * 
     * @return TRANSACTION_TYPE
     */
    public java.lang.String getTRANSACTION_TYPE() {
        return TRANSACTION_TYPE;
    }


    /**
     * Sets the TRANSACTION_TYPE value for this TransactionType.
     * 
     * @param TRANSACTION_TYPE
     */
    public void setTRANSACTION_TYPE(java.lang.String TRANSACTION_TYPE) {
        this.TRANSACTION_TYPE = TRANSACTION_TYPE;
    }


    /**
     * Gets the TRANSACTION_MCC value for this TransactionType.
     * 
     * @return TRANSACTION_MCC
     */
    public java.lang.String getTRANSACTION_MCC() {
        return TRANSACTION_MCC;
    }


    /**
     * Sets the TRANSACTION_MCC value for this TransactionType.
     * 
     * @param TRANSACTION_MCC
     */
    public void setTRANSACTION_MCC(java.lang.String TRANSACTION_MCC) {
        this.TRANSACTION_MCC = TRANSACTION_MCC;
    }


    /**
     * Gets the DESCRIPTION1 value for this TransactionType.
     * 
     * @return DESCRIPTION1
     */
    public java.lang.String getDESCRIPTION1() {
        return DESCRIPTION1;
    }


    /**
     * Sets the DESCRIPTION1 value for this TransactionType.
     * 
     * @param DESCRIPTION1
     */
    public void setDESCRIPTION1(java.lang.String DESCRIPTION1) {
        this.DESCRIPTION1 = DESCRIPTION1;
    }


    /**
     * Gets the DESCRIPTION2 value for this TransactionType.
     * 
     * @return DESCRIPTION2
     */
    public java.lang.String getDESCRIPTION2() {
        return DESCRIPTION2;
    }


    /**
     * Sets the DESCRIPTION2 value for this TransactionType.
     * 
     * @param DESCRIPTION2
     */
    public void setDESCRIPTION2(java.lang.String DESCRIPTION2) {
        this.DESCRIPTION2 = DESCRIPTION2;
    }


    /**
     * Gets the DESCRIPTION3 value for this TransactionType.
     * 
     * @return DESCRIPTION3
     */
    public java.lang.String getDESCRIPTION3() {
        return DESCRIPTION3;
    }


    /**
     * Sets the DESCRIPTION3 value for this TransactionType.
     * 
     * @param DESCRIPTION3
     */
    public void setDESCRIPTION3(java.lang.String DESCRIPTION3) {
        this.DESCRIPTION3 = DESCRIPTION3;
    }


    /**
     * Gets the DESCRIPTION4 value for this TransactionType.
     * 
     * @return DESCRIPTION4
     */
    public java.lang.String getDESCRIPTION4() {
        return DESCRIPTION4;
    }


    /**
     * Sets the DESCRIPTION4 value for this TransactionType.
     * 
     * @param DESCRIPTION4
     */
    public void setDESCRIPTION4(java.lang.String DESCRIPTION4) {
        this.DESCRIPTION4 = DESCRIPTION4;
    }


    /**
     * Gets the TRANSACTION_NUMBER value for this TransactionType.
     * 
     * @return TRANSACTION_NUMBER
     */
    public java.lang.String getTRANSACTION_NUMBER() {
        return TRANSACTION_NUMBER;
    }


    /**
     * Sets the TRANSACTION_NUMBER value for this TransactionType.
     * 
     * @param TRANSACTION_NUMBER
     */
    public void setTRANSACTION_NUMBER(java.lang.String TRANSACTION_NUMBER) {
        this.TRANSACTION_NUMBER = TRANSACTION_NUMBER;
    }


    /**
     * Gets the TRANSACTION_LifeCycleUniqueID value for this TransactionType.
     * 
     * @return TRANSACTION_LifeCycleUniqueID
     */
    public java.lang.String getTRANSACTION_LifeCycleUniqueID() {
        return TRANSACTION_LifeCycleUniqueID;
    }


    /**
     * Sets the TRANSACTION_LifeCycleUniqueID value for this TransactionType.
     * 
     * @param TRANSACTION_LifeCycleUniqueID
     */
    public void setTRANSACTION_LifeCycleUniqueID(java.lang.String TRANSACTION_LifeCycleUniqueID) {
        this.TRANSACTION_LifeCycleUniqueID = TRANSACTION_LifeCycleUniqueID;
    }


    /**
     * Gets the MERCHANT_ID value for this TransactionType.
     * 
     * @return MERCHANT_ID
     */
    public java.lang.String getMERCHANT_ID() {
        return MERCHANT_ID;
    }


    /**
     * Sets the MERCHANT_ID value for this TransactionType.
     * 
     * @param MERCHANT_ID
     */
    public void setMERCHANT_ID(java.lang.String MERCHANT_ID) {
        this.MERCHANT_ID = MERCHANT_ID;
    }


    /**
     * Gets the MERCHANT_NAME value for this TransactionType.
     * 
     * @return MERCHANT_NAME
     */
    public java.lang.String getMERCHANT_NAME() {
        return MERCHANT_NAME;
    }


    /**
     * Sets the MERCHANT_NAME value for this TransactionType.
     * 
     * @param MERCHANT_NAME
     */
    public void setMERCHANT_NAME(java.lang.String MERCHANT_NAME) {
        this.MERCHANT_NAME = MERCHANT_NAME;
    }


    /**
     * Gets the MERCHANT_CITY value for this TransactionType.
     * 
     * @return MERCHANT_CITY
     */
    public java.lang.String getMERCHANT_CITY() {
        return MERCHANT_CITY;
    }


    /**
     * Sets the MERCHANT_CITY value for this TransactionType.
     * 
     * @param MERCHANT_CITY
     */
    public void setMERCHANT_CITY(java.lang.String MERCHANT_CITY) {
        this.MERCHANT_CITY = MERCHANT_CITY;
    }


    /**
     * Gets the MERCHANT_STATE value for this TransactionType.
     * 
     * @return MERCHANT_STATE
     */
    public java.lang.String getMERCHANT_STATE() {
        return MERCHANT_STATE;
    }


    /**
     * Sets the MERCHANT_STATE value for this TransactionType.
     * 
     * @param MERCHANT_STATE
     */
    public void setMERCHANT_STATE(java.lang.String MERCHANT_STATE) {
        this.MERCHANT_STATE = MERCHANT_STATE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionType)) return false;
        TransactionType other = (TransactionType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DATE==null && other.getDATE()==null) || 
             (this.DATE!=null &&
              this.DATE.equals(other.getDATE()))) &&
            ((this.TIME==null && other.getTIME()==null) || 
             (this.TIME!=null &&
              this.TIME.equals(other.getTIME()))) &&
            ((this.AMOUNT==null && other.getAMOUNT()==null) || 
             (this.AMOUNT!=null &&
              this.AMOUNT.equals(other.getAMOUNT()))) &&
            ((this.FEE==null && other.getFEE()==null) || 
             (this.FEE!=null &&
              this.FEE.equals(other.getFEE()))) &&
            ((this.STATUS==null && other.getSTATUS()==null) || 
             (this.STATUS!=null &&
              this.STATUS.equals(other.getSTATUS()))) &&
            ((this.TRANSACTION_TYPE==null && other.getTRANSACTION_TYPE()==null) || 
             (this.TRANSACTION_TYPE!=null &&
              this.TRANSACTION_TYPE.equals(other.getTRANSACTION_TYPE()))) &&
            ((this.TRANSACTION_MCC==null && other.getTRANSACTION_MCC()==null) || 
             (this.TRANSACTION_MCC!=null &&
              this.TRANSACTION_MCC.equals(other.getTRANSACTION_MCC()))) &&
            ((this.DESCRIPTION1==null && other.getDESCRIPTION1()==null) || 
             (this.DESCRIPTION1!=null &&
              this.DESCRIPTION1.equals(other.getDESCRIPTION1()))) &&
            ((this.DESCRIPTION2==null && other.getDESCRIPTION2()==null) || 
             (this.DESCRIPTION2!=null &&
              this.DESCRIPTION2.equals(other.getDESCRIPTION2()))) &&
            ((this.DESCRIPTION3==null && other.getDESCRIPTION3()==null) || 
             (this.DESCRIPTION3!=null &&
              this.DESCRIPTION3.equals(other.getDESCRIPTION3()))) &&
            ((this.DESCRIPTION4==null && other.getDESCRIPTION4()==null) || 
             (this.DESCRIPTION4!=null &&
              this.DESCRIPTION4.equals(other.getDESCRIPTION4()))) &&
            ((this.TRANSACTION_NUMBER==null && other.getTRANSACTION_NUMBER()==null) || 
             (this.TRANSACTION_NUMBER!=null &&
              this.TRANSACTION_NUMBER.equals(other.getTRANSACTION_NUMBER()))) &&
            ((this.TRANSACTION_LifeCycleUniqueID==null && other.getTRANSACTION_LifeCycleUniqueID()==null) || 
             (this.TRANSACTION_LifeCycleUniqueID!=null &&
              this.TRANSACTION_LifeCycleUniqueID.equals(other.getTRANSACTION_LifeCycleUniqueID()))) &&
            ((this.MERCHANT_ID==null && other.getMERCHANT_ID()==null) || 
             (this.MERCHANT_ID!=null &&
              this.MERCHANT_ID.equals(other.getMERCHANT_ID()))) &&
            ((this.MERCHANT_NAME==null && other.getMERCHANT_NAME()==null) || 
             (this.MERCHANT_NAME!=null &&
              this.MERCHANT_NAME.equals(other.getMERCHANT_NAME()))) &&
            ((this.MERCHANT_CITY==null && other.getMERCHANT_CITY()==null) || 
             (this.MERCHANT_CITY!=null &&
              this.MERCHANT_CITY.equals(other.getMERCHANT_CITY()))) &&
            ((this.MERCHANT_STATE==null && other.getMERCHANT_STATE()==null) || 
             (this.MERCHANT_STATE!=null &&
              this.MERCHANT_STATE.equals(other.getMERCHANT_STATE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDATE() != null) {
            _hashCode += getDATE().hashCode();
        }
        if (getTIME() != null) {
            _hashCode += getTIME().hashCode();
        }
        if (getAMOUNT() != null) {
            _hashCode += getAMOUNT().hashCode();
        }
        if (getFEE() != null) {
            _hashCode += getFEE().hashCode();
        }
        if (getSTATUS() != null) {
            _hashCode += getSTATUS().hashCode();
        }
        if (getTRANSACTION_TYPE() != null) {
            _hashCode += getTRANSACTION_TYPE().hashCode();
        }
        if (getTRANSACTION_MCC() != null) {
            _hashCode += getTRANSACTION_MCC().hashCode();
        }
        if (getDESCRIPTION1() != null) {
            _hashCode += getDESCRIPTION1().hashCode();
        }
        if (getDESCRIPTION2() != null) {
            _hashCode += getDESCRIPTION2().hashCode();
        }
        if (getDESCRIPTION3() != null) {
            _hashCode += getDESCRIPTION3().hashCode();
        }
        if (getDESCRIPTION4() != null) {
            _hashCode += getDESCRIPTION4().hashCode();
        }
        if (getTRANSACTION_NUMBER() != null) {
            _hashCode += getTRANSACTION_NUMBER().hashCode();
        }
        if (getTRANSACTION_LifeCycleUniqueID() != null) {
            _hashCode += getTRANSACTION_LifeCycleUniqueID().hashCode();
        }
        if (getMERCHANT_ID() != null) {
            _hashCode += getMERCHANT_ID().hashCode();
        }
        if (getMERCHANT_NAME() != null) {
            _hashCode += getMERCHANT_NAME().hashCode();
        }
        if (getMERCHANT_CITY() != null) {
            _hashCode += getMERCHANT_CITY().hashCode();
        }
        if (getMERCHANT_STATE() != null) {
            _hashCode += getMERCHANT_STATE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionType"));
        org.apache.axis.description.AttributeDesc attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("DATE");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DATE"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("TIME");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TIME"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("AMOUNT");
        attrField.setXmlName(new javax.xml.namespace.QName("", "AMOUNT"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("FEE");
        attrField.setXmlName(new javax.xml.namespace.QName("", "FEE"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("STATUS");
        attrField.setXmlName(new javax.xml.namespace.QName("", "STATUS"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("TRANSACTION_TYPE");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TRANSACTION_TYPE"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("TRANSACTION_MCC");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TRANSACTION_MCC"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("DESCRIPTION1");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DESCRIPTION1"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("DESCRIPTION2");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DESCRIPTION2"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("DESCRIPTION3");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DESCRIPTION3"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("DESCRIPTION4");
        attrField.setXmlName(new javax.xml.namespace.QName("", "DESCRIPTION4"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("TRANSACTION_NUMBER");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TRANSACTION_NUMBER"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("TRANSACTION_LifeCycleUniqueID");
        attrField.setXmlName(new javax.xml.namespace.QName("", "TRANSACTION_LifeCycleUniqueID"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("MERCHANT_ID");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MERCHANT_ID"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("MERCHANT_NAME");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MERCHANT_NAME"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("MERCHANT_CITY");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MERCHANT_CITY"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
        attrField = new org.apache.axis.description.AttributeDesc();
        attrField.setFieldName("MERCHANT_STATE");
        attrField.setXmlName(new javax.xml.namespace.QName("", "MERCHANT_STATE"));
        attrField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        typeDesc.addFieldDesc(attrField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
