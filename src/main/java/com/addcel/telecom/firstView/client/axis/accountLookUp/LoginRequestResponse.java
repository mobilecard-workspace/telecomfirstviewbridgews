/**
 * LoginRequestResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class LoginRequestResponse  implements java.io.Serializable {
    private java.lang.String ERRMSG;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERROR_FOUND;

    private java.lang.String FIRSTNAME;

    private java.lang.String MIDDLENAME;

    private java.lang.String LASTNAME;

    private java.lang.String ADDRESS1;

    private java.lang.String ADDRESS2;

    private java.lang.String ADDRESS3;

    private java.lang.String CITY;

    private java.lang.String STATE;

    private java.lang.String COUNTRY;

    private java.lang.String POSTALCODE;

    private java.lang.String PHONE;

    private java.lang.String EMAIL;

    private java.lang.String SECURITYQUESTION;

    private java.lang.String SECURITYANSWER;

    private java.lang.String ALERT_DAILY_BAL_TIME;

    private java.lang.String ALERT_DAILY_BALANCE;

    private java.lang.String ALERT_DIRECT_DEPOSIT;

    private java.lang.String ALERT_FUNDS_TRANSFER;

    private java.lang.String ALERT_INSF;

    private java.lang.String ALERT_LOW_BALANCE;

    private java.lang.String ALERT_LOW_BALANCE_AM;

    private java.lang.String ALERT_MONTHLY_BAL;

    private java.lang.String ALERT_ONLINE_PASSWRD;

    private java.lang.String ALERT_ONLINE_SIGNON;

    private java.lang.String ALERT_ONLINE_USER;

    private java.lang.String ALERT_TRANS_OVER;

    private java.lang.String ALERT_TRANS_OVER_AMT;

    private java.lang.String ALERT_WEEKLY_BAL_DAY;

    private java.lang.String ALERT_WEEKLY_BALANCE;

    private java.lang.String HISTORY_SORT;

    private java.lang.String SKIN;

    private java.lang.String DATE_SEARCH;

    private java.lang.String SMS_PROVIDER;

    private java.lang.String SMS_PHONE;

    private java.lang.String SMS_EMAIL;

    private java.lang.String LAST_LOGIN_DATE;

    private java.lang.String LAST_LOGIN_TIME;

    private java.lang.String CARD_BALANCE;

    private java.lang.String ALLOW_C2C;

    private java.lang.String ALLOW_ATM;

    private java.lang.String ALLOW_DDP;

    private java.lang.String ALLOW_CHK;

    private java.lang.String ALLOW_CMP;

    private java.lang.String REGISTERED;

    private java.lang.String ADMIN_NUMBER;

    private com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType[] TRANSACTION_FEE_LIST;

    private java.lang.String CARDTYPE;

    private java.lang.String REQUIRE_PIN_CHG;

    private java.lang.String REQUIRE_SSN;

    private java.lang.String REQUIRE_DOB;

    private java.lang.String REQUIRE_PASS_CHG;

    public LoginRequestResponse() {
    }

    public LoginRequestResponse(
           java.lang.String ERRMSG,
           java.lang.String ERR_NUMBER,
           java.lang.String ERROR_FOUND,
           java.lang.String FIRSTNAME,
           java.lang.String MIDDLENAME,
           java.lang.String LASTNAME,
           java.lang.String ADDRESS1,
           java.lang.String ADDRESS2,
           java.lang.String ADDRESS3,
           java.lang.String CITY,
           java.lang.String STATE,
           java.lang.String COUNTRY,
           java.lang.String POSTALCODE,
           java.lang.String PHONE,
           java.lang.String EMAIL,
           java.lang.String SECURITYQUESTION,
           java.lang.String SECURITYANSWER,
           java.lang.String ALERT_DAILY_BAL_TIME,
           java.lang.String ALERT_DAILY_BALANCE,
           java.lang.String ALERT_DIRECT_DEPOSIT,
           java.lang.String ALERT_FUNDS_TRANSFER,
           java.lang.String ALERT_INSF,
           java.lang.String ALERT_LOW_BALANCE,
           java.lang.String ALERT_LOW_BALANCE_AM,
           java.lang.String ALERT_MONTHLY_BAL,
           java.lang.String ALERT_ONLINE_PASSWRD,
           java.lang.String ALERT_ONLINE_SIGNON,
           java.lang.String ALERT_ONLINE_USER,
           java.lang.String ALERT_TRANS_OVER,
           java.lang.String ALERT_TRANS_OVER_AMT,
           java.lang.String ALERT_WEEKLY_BAL_DAY,
           java.lang.String ALERT_WEEKLY_BALANCE,
           java.lang.String HISTORY_SORT,
           java.lang.String SKIN,
           java.lang.String DATE_SEARCH,
           java.lang.String SMS_PROVIDER,
           java.lang.String SMS_PHONE,
           java.lang.String SMS_EMAIL,
           java.lang.String LAST_LOGIN_DATE,
           java.lang.String LAST_LOGIN_TIME,
           java.lang.String CARD_BALANCE,
           java.lang.String ALLOW_C2C,
           java.lang.String ALLOW_ATM,
           java.lang.String ALLOW_DDP,
           java.lang.String ALLOW_CHK,
           java.lang.String ALLOW_CMP,
           java.lang.String REGISTERED,
           java.lang.String ADMIN_NUMBER,
           com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType[] TRANSACTION_FEE_LIST,
           java.lang.String CARDTYPE,
           java.lang.String REQUIRE_PIN_CHG,
           java.lang.String REQUIRE_SSN,
           java.lang.String REQUIRE_DOB,
           java.lang.String REQUIRE_PASS_CHG) {
           this.ERRMSG = ERRMSG;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERROR_FOUND = ERROR_FOUND;
           this.FIRSTNAME = FIRSTNAME;
           this.MIDDLENAME = MIDDLENAME;
           this.LASTNAME = LASTNAME;
           this.ADDRESS1 = ADDRESS1;
           this.ADDRESS2 = ADDRESS2;
           this.ADDRESS3 = ADDRESS3;
           this.CITY = CITY;
           this.STATE = STATE;
           this.COUNTRY = COUNTRY;
           this.POSTALCODE = POSTALCODE;
           this.PHONE = PHONE;
           this.EMAIL = EMAIL;
           this.SECURITYQUESTION = SECURITYQUESTION;
           this.SECURITYANSWER = SECURITYANSWER;
           this.ALERT_DAILY_BAL_TIME = ALERT_DAILY_BAL_TIME;
           this.ALERT_DAILY_BALANCE = ALERT_DAILY_BALANCE;
           this.ALERT_DIRECT_DEPOSIT = ALERT_DIRECT_DEPOSIT;
           this.ALERT_FUNDS_TRANSFER = ALERT_FUNDS_TRANSFER;
           this.ALERT_INSF = ALERT_INSF;
           this.ALERT_LOW_BALANCE = ALERT_LOW_BALANCE;
           this.ALERT_LOW_BALANCE_AM = ALERT_LOW_BALANCE_AM;
           this.ALERT_MONTHLY_BAL = ALERT_MONTHLY_BAL;
           this.ALERT_ONLINE_PASSWRD = ALERT_ONLINE_PASSWRD;
           this.ALERT_ONLINE_SIGNON = ALERT_ONLINE_SIGNON;
           this.ALERT_ONLINE_USER = ALERT_ONLINE_USER;
           this.ALERT_TRANS_OVER = ALERT_TRANS_OVER;
           this.ALERT_TRANS_OVER_AMT = ALERT_TRANS_OVER_AMT;
           this.ALERT_WEEKLY_BAL_DAY = ALERT_WEEKLY_BAL_DAY;
           this.ALERT_WEEKLY_BALANCE = ALERT_WEEKLY_BALANCE;
           this.HISTORY_SORT = HISTORY_SORT;
           this.SKIN = SKIN;
           this.DATE_SEARCH = DATE_SEARCH;
           this.SMS_PROVIDER = SMS_PROVIDER;
           this.SMS_PHONE = SMS_PHONE;
           this.SMS_EMAIL = SMS_EMAIL;
           this.LAST_LOGIN_DATE = LAST_LOGIN_DATE;
           this.LAST_LOGIN_TIME = LAST_LOGIN_TIME;
           this.CARD_BALANCE = CARD_BALANCE;
           this.ALLOW_C2C = ALLOW_C2C;
           this.ALLOW_ATM = ALLOW_ATM;
           this.ALLOW_DDP = ALLOW_DDP;
           this.ALLOW_CHK = ALLOW_CHK;
           this.ALLOW_CMP = ALLOW_CMP;
           this.REGISTERED = REGISTERED;
           this.ADMIN_NUMBER = ADMIN_NUMBER;
           this.TRANSACTION_FEE_LIST = TRANSACTION_FEE_LIST;
           this.CARDTYPE = CARDTYPE;
           this.REQUIRE_PIN_CHG = REQUIRE_PIN_CHG;
           this.REQUIRE_SSN = REQUIRE_SSN;
           this.REQUIRE_DOB = REQUIRE_DOB;
           this.REQUIRE_PASS_CHG = REQUIRE_PASS_CHG;
    }


    /**
     * Gets the ERRMSG value for this LoginRequestResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this LoginRequestResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the ERR_NUMBER value for this LoginRequestResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this LoginRequestResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this LoginRequestResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this LoginRequestResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the FIRSTNAME value for this LoginRequestResponse.
     * 
     * @return FIRSTNAME
     */
    public java.lang.String getFIRSTNAME() {
        return FIRSTNAME;
    }


    /**
     * Sets the FIRSTNAME value for this LoginRequestResponse.
     * 
     * @param FIRSTNAME
     */
    public void setFIRSTNAME(java.lang.String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }


    /**
     * Gets the MIDDLENAME value for this LoginRequestResponse.
     * 
     * @return MIDDLENAME
     */
    public java.lang.String getMIDDLENAME() {
        return MIDDLENAME;
    }


    /**
     * Sets the MIDDLENAME value for this LoginRequestResponse.
     * 
     * @param MIDDLENAME
     */
    public void setMIDDLENAME(java.lang.String MIDDLENAME) {
        this.MIDDLENAME = MIDDLENAME;
    }


    /**
     * Gets the LASTNAME value for this LoginRequestResponse.
     * 
     * @return LASTNAME
     */
    public java.lang.String getLASTNAME() {
        return LASTNAME;
    }


    /**
     * Sets the LASTNAME value for this LoginRequestResponse.
     * 
     * @param LASTNAME
     */
    public void setLASTNAME(java.lang.String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }


    /**
     * Gets the ADDRESS1 value for this LoginRequestResponse.
     * 
     * @return ADDRESS1
     */
    public java.lang.String getADDRESS1() {
        return ADDRESS1;
    }


    /**
     * Sets the ADDRESS1 value for this LoginRequestResponse.
     * 
     * @param ADDRESS1
     */
    public void setADDRESS1(java.lang.String ADDRESS1) {
        this.ADDRESS1 = ADDRESS1;
    }


    /**
     * Gets the ADDRESS2 value for this LoginRequestResponse.
     * 
     * @return ADDRESS2
     */
    public java.lang.String getADDRESS2() {
        return ADDRESS2;
    }


    /**
     * Sets the ADDRESS2 value for this LoginRequestResponse.
     * 
     * @param ADDRESS2
     */
    public void setADDRESS2(java.lang.String ADDRESS2) {
        this.ADDRESS2 = ADDRESS2;
    }


    /**
     * Gets the ADDRESS3 value for this LoginRequestResponse.
     * 
     * @return ADDRESS3
     */
    public java.lang.String getADDRESS3() {
        return ADDRESS3;
    }


    /**
     * Sets the ADDRESS3 value for this LoginRequestResponse.
     * 
     * @param ADDRESS3
     */
    public void setADDRESS3(java.lang.String ADDRESS3) {
        this.ADDRESS3 = ADDRESS3;
    }


    /**
     * Gets the CITY value for this LoginRequestResponse.
     * 
     * @return CITY
     */
    public java.lang.String getCITY() {
        return CITY;
    }


    /**
     * Sets the CITY value for this LoginRequestResponse.
     * 
     * @param CITY
     */
    public void setCITY(java.lang.String CITY) {
        this.CITY = CITY;
    }


    /**
     * Gets the STATE value for this LoginRequestResponse.
     * 
     * @return STATE
     */
    public java.lang.String getSTATE() {
        return STATE;
    }


    /**
     * Sets the STATE value for this LoginRequestResponse.
     * 
     * @param STATE
     */
    public void setSTATE(java.lang.String STATE) {
        this.STATE = STATE;
    }


    /**
     * Gets the COUNTRY value for this LoginRequestResponse.
     * 
     * @return COUNTRY
     */
    public java.lang.String getCOUNTRY() {
        return COUNTRY;
    }


    /**
     * Sets the COUNTRY value for this LoginRequestResponse.
     * 
     * @param COUNTRY
     */
    public void setCOUNTRY(java.lang.String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }


    /**
     * Gets the POSTALCODE value for this LoginRequestResponse.
     * 
     * @return POSTALCODE
     */
    public java.lang.String getPOSTALCODE() {
        return POSTALCODE;
    }


    /**
     * Sets the POSTALCODE value for this LoginRequestResponse.
     * 
     * @param POSTALCODE
     */
    public void setPOSTALCODE(java.lang.String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }


    /**
     * Gets the PHONE value for this LoginRequestResponse.
     * 
     * @return PHONE
     */
    public java.lang.String getPHONE() {
        return PHONE;
    }


    /**
     * Sets the PHONE value for this LoginRequestResponse.
     * 
     * @param PHONE
     */
    public void setPHONE(java.lang.String PHONE) {
        this.PHONE = PHONE;
    }


    /**
     * Gets the EMAIL value for this LoginRequestResponse.
     * 
     * @return EMAIL
     */
    public java.lang.String getEMAIL() {
        return EMAIL;
    }


    /**
     * Sets the EMAIL value for this LoginRequestResponse.
     * 
     * @param EMAIL
     */
    public void setEMAIL(java.lang.String EMAIL) {
        this.EMAIL = EMAIL;
    }


    /**
     * Gets the SECURITYQUESTION value for this LoginRequestResponse.
     * 
     * @return SECURITYQUESTION
     */
    public java.lang.String getSECURITYQUESTION() {
        return SECURITYQUESTION;
    }


    /**
     * Sets the SECURITYQUESTION value for this LoginRequestResponse.
     * 
     * @param SECURITYQUESTION
     */
    public void setSECURITYQUESTION(java.lang.String SECURITYQUESTION) {
        this.SECURITYQUESTION = SECURITYQUESTION;
    }


    /**
     * Gets the SECURITYANSWER value for this LoginRequestResponse.
     * 
     * @return SECURITYANSWER
     */
    public java.lang.String getSECURITYANSWER() {
        return SECURITYANSWER;
    }


    /**
     * Sets the SECURITYANSWER value for this LoginRequestResponse.
     * 
     * @param SECURITYANSWER
     */
    public void setSECURITYANSWER(java.lang.String SECURITYANSWER) {
        this.SECURITYANSWER = SECURITYANSWER;
    }


    /**
     * Gets the ALERT_DAILY_BAL_TIME value for this LoginRequestResponse.
     * 
     * @return ALERT_DAILY_BAL_TIME
     */
    public java.lang.String getALERT_DAILY_BAL_TIME() {
        return ALERT_DAILY_BAL_TIME;
    }


    /**
     * Sets the ALERT_DAILY_BAL_TIME value for this LoginRequestResponse.
     * 
     * @param ALERT_DAILY_BAL_TIME
     */
    public void setALERT_DAILY_BAL_TIME(java.lang.String ALERT_DAILY_BAL_TIME) {
        this.ALERT_DAILY_BAL_TIME = ALERT_DAILY_BAL_TIME;
    }


    /**
     * Gets the ALERT_DAILY_BALANCE value for this LoginRequestResponse.
     * 
     * @return ALERT_DAILY_BALANCE
     */
    public java.lang.String getALERT_DAILY_BALANCE() {
        return ALERT_DAILY_BALANCE;
    }


    /**
     * Sets the ALERT_DAILY_BALANCE value for this LoginRequestResponse.
     * 
     * @param ALERT_DAILY_BALANCE
     */
    public void setALERT_DAILY_BALANCE(java.lang.String ALERT_DAILY_BALANCE) {
        this.ALERT_DAILY_BALANCE = ALERT_DAILY_BALANCE;
    }


    /**
     * Gets the ALERT_DIRECT_DEPOSIT value for this LoginRequestResponse.
     * 
     * @return ALERT_DIRECT_DEPOSIT
     */
    public java.lang.String getALERT_DIRECT_DEPOSIT() {
        return ALERT_DIRECT_DEPOSIT;
    }


    /**
     * Sets the ALERT_DIRECT_DEPOSIT value for this LoginRequestResponse.
     * 
     * @param ALERT_DIRECT_DEPOSIT
     */
    public void setALERT_DIRECT_DEPOSIT(java.lang.String ALERT_DIRECT_DEPOSIT) {
        this.ALERT_DIRECT_DEPOSIT = ALERT_DIRECT_DEPOSIT;
    }


    /**
     * Gets the ALERT_FUNDS_TRANSFER value for this LoginRequestResponse.
     * 
     * @return ALERT_FUNDS_TRANSFER
     */
    public java.lang.String getALERT_FUNDS_TRANSFER() {
        return ALERT_FUNDS_TRANSFER;
    }


    /**
     * Sets the ALERT_FUNDS_TRANSFER value for this LoginRequestResponse.
     * 
     * @param ALERT_FUNDS_TRANSFER
     */
    public void setALERT_FUNDS_TRANSFER(java.lang.String ALERT_FUNDS_TRANSFER) {
        this.ALERT_FUNDS_TRANSFER = ALERT_FUNDS_TRANSFER;
    }


    /**
     * Gets the ALERT_INSF value for this LoginRequestResponse.
     * 
     * @return ALERT_INSF
     */
    public java.lang.String getALERT_INSF() {
        return ALERT_INSF;
    }


    /**
     * Sets the ALERT_INSF value for this LoginRequestResponse.
     * 
     * @param ALERT_INSF
     */
    public void setALERT_INSF(java.lang.String ALERT_INSF) {
        this.ALERT_INSF = ALERT_INSF;
    }


    /**
     * Gets the ALERT_LOW_BALANCE value for this LoginRequestResponse.
     * 
     * @return ALERT_LOW_BALANCE
     */
    public java.lang.String getALERT_LOW_BALANCE() {
        return ALERT_LOW_BALANCE;
    }


    /**
     * Sets the ALERT_LOW_BALANCE value for this LoginRequestResponse.
     * 
     * @param ALERT_LOW_BALANCE
     */
    public void setALERT_LOW_BALANCE(java.lang.String ALERT_LOW_BALANCE) {
        this.ALERT_LOW_BALANCE = ALERT_LOW_BALANCE;
    }


    /**
     * Gets the ALERT_LOW_BALANCE_AM value for this LoginRequestResponse.
     * 
     * @return ALERT_LOW_BALANCE_AM
     */
    public java.lang.String getALERT_LOW_BALANCE_AM() {
        return ALERT_LOW_BALANCE_AM;
    }


    /**
     * Sets the ALERT_LOW_BALANCE_AM value for this LoginRequestResponse.
     * 
     * @param ALERT_LOW_BALANCE_AM
     */
    public void setALERT_LOW_BALANCE_AM(java.lang.String ALERT_LOW_BALANCE_AM) {
        this.ALERT_LOW_BALANCE_AM = ALERT_LOW_BALANCE_AM;
    }


    /**
     * Gets the ALERT_MONTHLY_BAL value for this LoginRequestResponse.
     * 
     * @return ALERT_MONTHLY_BAL
     */
    public java.lang.String getALERT_MONTHLY_BAL() {
        return ALERT_MONTHLY_BAL;
    }


    /**
     * Sets the ALERT_MONTHLY_BAL value for this LoginRequestResponse.
     * 
     * @param ALERT_MONTHLY_BAL
     */
    public void setALERT_MONTHLY_BAL(java.lang.String ALERT_MONTHLY_BAL) {
        this.ALERT_MONTHLY_BAL = ALERT_MONTHLY_BAL;
    }


    /**
     * Gets the ALERT_ONLINE_PASSWRD value for this LoginRequestResponse.
     * 
     * @return ALERT_ONLINE_PASSWRD
     */
    public java.lang.String getALERT_ONLINE_PASSWRD() {
        return ALERT_ONLINE_PASSWRD;
    }


    /**
     * Sets the ALERT_ONLINE_PASSWRD value for this LoginRequestResponse.
     * 
     * @param ALERT_ONLINE_PASSWRD
     */
    public void setALERT_ONLINE_PASSWRD(java.lang.String ALERT_ONLINE_PASSWRD) {
        this.ALERT_ONLINE_PASSWRD = ALERT_ONLINE_PASSWRD;
    }


    /**
     * Gets the ALERT_ONLINE_SIGNON value for this LoginRequestResponse.
     * 
     * @return ALERT_ONLINE_SIGNON
     */
    public java.lang.String getALERT_ONLINE_SIGNON() {
        return ALERT_ONLINE_SIGNON;
    }


    /**
     * Sets the ALERT_ONLINE_SIGNON value for this LoginRequestResponse.
     * 
     * @param ALERT_ONLINE_SIGNON
     */
    public void setALERT_ONLINE_SIGNON(java.lang.String ALERT_ONLINE_SIGNON) {
        this.ALERT_ONLINE_SIGNON = ALERT_ONLINE_SIGNON;
    }


    /**
     * Gets the ALERT_ONLINE_USER value for this LoginRequestResponse.
     * 
     * @return ALERT_ONLINE_USER
     */
    public java.lang.String getALERT_ONLINE_USER() {
        return ALERT_ONLINE_USER;
    }


    /**
     * Sets the ALERT_ONLINE_USER value for this LoginRequestResponse.
     * 
     * @param ALERT_ONLINE_USER
     */
    public void setALERT_ONLINE_USER(java.lang.String ALERT_ONLINE_USER) {
        this.ALERT_ONLINE_USER = ALERT_ONLINE_USER;
    }


    /**
     * Gets the ALERT_TRANS_OVER value for this LoginRequestResponse.
     * 
     * @return ALERT_TRANS_OVER
     */
    public java.lang.String getALERT_TRANS_OVER() {
        return ALERT_TRANS_OVER;
    }


    /**
     * Sets the ALERT_TRANS_OVER value for this LoginRequestResponse.
     * 
     * @param ALERT_TRANS_OVER
     */
    public void setALERT_TRANS_OVER(java.lang.String ALERT_TRANS_OVER) {
        this.ALERT_TRANS_OVER = ALERT_TRANS_OVER;
    }


    /**
     * Gets the ALERT_TRANS_OVER_AMT value for this LoginRequestResponse.
     * 
     * @return ALERT_TRANS_OVER_AMT
     */
    public java.lang.String getALERT_TRANS_OVER_AMT() {
        return ALERT_TRANS_OVER_AMT;
    }


    /**
     * Sets the ALERT_TRANS_OVER_AMT value for this LoginRequestResponse.
     * 
     * @param ALERT_TRANS_OVER_AMT
     */
    public void setALERT_TRANS_OVER_AMT(java.lang.String ALERT_TRANS_OVER_AMT) {
        this.ALERT_TRANS_OVER_AMT = ALERT_TRANS_OVER_AMT;
    }


    /**
     * Gets the ALERT_WEEKLY_BAL_DAY value for this LoginRequestResponse.
     * 
     * @return ALERT_WEEKLY_BAL_DAY
     */
    public java.lang.String getALERT_WEEKLY_BAL_DAY() {
        return ALERT_WEEKLY_BAL_DAY;
    }


    /**
     * Sets the ALERT_WEEKLY_BAL_DAY value for this LoginRequestResponse.
     * 
     * @param ALERT_WEEKLY_BAL_DAY
     */
    public void setALERT_WEEKLY_BAL_DAY(java.lang.String ALERT_WEEKLY_BAL_DAY) {
        this.ALERT_WEEKLY_BAL_DAY = ALERT_WEEKLY_BAL_DAY;
    }


    /**
     * Gets the ALERT_WEEKLY_BALANCE value for this LoginRequestResponse.
     * 
     * @return ALERT_WEEKLY_BALANCE
     */
    public java.lang.String getALERT_WEEKLY_BALANCE() {
        return ALERT_WEEKLY_BALANCE;
    }


    /**
     * Sets the ALERT_WEEKLY_BALANCE value for this LoginRequestResponse.
     * 
     * @param ALERT_WEEKLY_BALANCE
     */
    public void setALERT_WEEKLY_BALANCE(java.lang.String ALERT_WEEKLY_BALANCE) {
        this.ALERT_WEEKLY_BALANCE = ALERT_WEEKLY_BALANCE;
    }


    /**
     * Gets the HISTORY_SORT value for this LoginRequestResponse.
     * 
     * @return HISTORY_SORT
     */
    public java.lang.String getHISTORY_SORT() {
        return HISTORY_SORT;
    }


    /**
     * Sets the HISTORY_SORT value for this LoginRequestResponse.
     * 
     * @param HISTORY_SORT
     */
    public void setHISTORY_SORT(java.lang.String HISTORY_SORT) {
        this.HISTORY_SORT = HISTORY_SORT;
    }


    /**
     * Gets the SKIN value for this LoginRequestResponse.
     * 
     * @return SKIN
     */
    public java.lang.String getSKIN() {
        return SKIN;
    }


    /**
     * Sets the SKIN value for this LoginRequestResponse.
     * 
     * @param SKIN
     */
    public void setSKIN(java.lang.String SKIN) {
        this.SKIN = SKIN;
    }


    /**
     * Gets the DATE_SEARCH value for this LoginRequestResponse.
     * 
     * @return DATE_SEARCH
     */
    public java.lang.String getDATE_SEARCH() {
        return DATE_SEARCH;
    }


    /**
     * Sets the DATE_SEARCH value for this LoginRequestResponse.
     * 
     * @param DATE_SEARCH
     */
    public void setDATE_SEARCH(java.lang.String DATE_SEARCH) {
        this.DATE_SEARCH = DATE_SEARCH;
    }


    /**
     * Gets the SMS_PROVIDER value for this LoginRequestResponse.
     * 
     * @return SMS_PROVIDER
     */
    public java.lang.String getSMS_PROVIDER() {
        return SMS_PROVIDER;
    }


    /**
     * Sets the SMS_PROVIDER value for this LoginRequestResponse.
     * 
     * @param SMS_PROVIDER
     */
    public void setSMS_PROVIDER(java.lang.String SMS_PROVIDER) {
        this.SMS_PROVIDER = SMS_PROVIDER;
    }


    /**
     * Gets the SMS_PHONE value for this LoginRequestResponse.
     * 
     * @return SMS_PHONE
     */
    public java.lang.String getSMS_PHONE() {
        return SMS_PHONE;
    }


    /**
     * Sets the SMS_PHONE value for this LoginRequestResponse.
     * 
     * @param SMS_PHONE
     */
    public void setSMS_PHONE(java.lang.String SMS_PHONE) {
        this.SMS_PHONE = SMS_PHONE;
    }


    /**
     * Gets the SMS_EMAIL value for this LoginRequestResponse.
     * 
     * @return SMS_EMAIL
     */
    public java.lang.String getSMS_EMAIL() {
        return SMS_EMAIL;
    }


    /**
     * Sets the SMS_EMAIL value for this LoginRequestResponse.
     * 
     * @param SMS_EMAIL
     */
    public void setSMS_EMAIL(java.lang.String SMS_EMAIL) {
        this.SMS_EMAIL = SMS_EMAIL;
    }


    /**
     * Gets the LAST_LOGIN_DATE value for this LoginRequestResponse.
     * 
     * @return LAST_LOGIN_DATE
     */
    public java.lang.String getLAST_LOGIN_DATE() {
        return LAST_LOGIN_DATE;
    }


    /**
     * Sets the LAST_LOGIN_DATE value for this LoginRequestResponse.
     * 
     * @param LAST_LOGIN_DATE
     */
    public void setLAST_LOGIN_DATE(java.lang.String LAST_LOGIN_DATE) {
        this.LAST_LOGIN_DATE = LAST_LOGIN_DATE;
    }


    /**
     * Gets the LAST_LOGIN_TIME value for this LoginRequestResponse.
     * 
     * @return LAST_LOGIN_TIME
     */
    public java.lang.String getLAST_LOGIN_TIME() {
        return LAST_LOGIN_TIME;
    }


    /**
     * Sets the LAST_LOGIN_TIME value for this LoginRequestResponse.
     * 
     * @param LAST_LOGIN_TIME
     */
    public void setLAST_LOGIN_TIME(java.lang.String LAST_LOGIN_TIME) {
        this.LAST_LOGIN_TIME = LAST_LOGIN_TIME;
    }


    /**
     * Gets the CARD_BALANCE value for this LoginRequestResponse.
     * 
     * @return CARD_BALANCE
     */
    public java.lang.String getCARD_BALANCE() {
        return CARD_BALANCE;
    }


    /**
     * Sets the CARD_BALANCE value for this LoginRequestResponse.
     * 
     * @param CARD_BALANCE
     */
    public void setCARD_BALANCE(java.lang.String CARD_BALANCE) {
        this.CARD_BALANCE = CARD_BALANCE;
    }


    /**
     * Gets the ALLOW_C2C value for this LoginRequestResponse.
     * 
     * @return ALLOW_C2C
     */
    public java.lang.String getALLOW_C2C() {
        return ALLOW_C2C;
    }


    /**
     * Sets the ALLOW_C2C value for this LoginRequestResponse.
     * 
     * @param ALLOW_C2C
     */
    public void setALLOW_C2C(java.lang.String ALLOW_C2C) {
        this.ALLOW_C2C = ALLOW_C2C;
    }


    /**
     * Gets the ALLOW_ATM value for this LoginRequestResponse.
     * 
     * @return ALLOW_ATM
     */
    public java.lang.String getALLOW_ATM() {
        return ALLOW_ATM;
    }


    /**
     * Sets the ALLOW_ATM value for this LoginRequestResponse.
     * 
     * @param ALLOW_ATM
     */
    public void setALLOW_ATM(java.lang.String ALLOW_ATM) {
        this.ALLOW_ATM = ALLOW_ATM;
    }


    /**
     * Gets the ALLOW_DDP value for this LoginRequestResponse.
     * 
     * @return ALLOW_DDP
     */
    public java.lang.String getALLOW_DDP() {
        return ALLOW_DDP;
    }


    /**
     * Sets the ALLOW_DDP value for this LoginRequestResponse.
     * 
     * @param ALLOW_DDP
     */
    public void setALLOW_DDP(java.lang.String ALLOW_DDP) {
        this.ALLOW_DDP = ALLOW_DDP;
    }


    /**
     * Gets the ALLOW_CHK value for this LoginRequestResponse.
     * 
     * @return ALLOW_CHK
     */
    public java.lang.String getALLOW_CHK() {
        return ALLOW_CHK;
    }


    /**
     * Sets the ALLOW_CHK value for this LoginRequestResponse.
     * 
     * @param ALLOW_CHK
     */
    public void setALLOW_CHK(java.lang.String ALLOW_CHK) {
        this.ALLOW_CHK = ALLOW_CHK;
    }


    /**
     * Gets the ALLOW_CMP value for this LoginRequestResponse.
     * 
     * @return ALLOW_CMP
     */
    public java.lang.String getALLOW_CMP() {
        return ALLOW_CMP;
    }


    /**
     * Sets the ALLOW_CMP value for this LoginRequestResponse.
     * 
     * @param ALLOW_CMP
     */
    public void setALLOW_CMP(java.lang.String ALLOW_CMP) {
        this.ALLOW_CMP = ALLOW_CMP;
    }


    /**
     * Gets the REGISTERED value for this LoginRequestResponse.
     * 
     * @return REGISTERED
     */
    public java.lang.String getREGISTERED() {
        return REGISTERED;
    }


    /**
     * Sets the REGISTERED value for this LoginRequestResponse.
     * 
     * @param REGISTERED
     */
    public void setREGISTERED(java.lang.String REGISTERED) {
        this.REGISTERED = REGISTERED;
    }


    /**
     * Gets the ADMIN_NUMBER value for this LoginRequestResponse.
     * 
     * @return ADMIN_NUMBER
     */
    public java.lang.String getADMIN_NUMBER() {
        return ADMIN_NUMBER;
    }


    /**
     * Sets the ADMIN_NUMBER value for this LoginRequestResponse.
     * 
     * @param ADMIN_NUMBER
     */
    public void setADMIN_NUMBER(java.lang.String ADMIN_NUMBER) {
        this.ADMIN_NUMBER = ADMIN_NUMBER;
    }


    /**
     * Gets the TRANSACTION_FEE_LIST value for this LoginRequestResponse.
     * 
     * @return TRANSACTION_FEE_LIST
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType[] getTRANSACTION_FEE_LIST() {
        return TRANSACTION_FEE_LIST;
    }


    /**
     * Sets the TRANSACTION_FEE_LIST value for this LoginRequestResponse.
     * 
     * @param TRANSACTION_FEE_LIST
     */
    public void setTRANSACTION_FEE_LIST(com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType[] TRANSACTION_FEE_LIST) {
        this.TRANSACTION_FEE_LIST = TRANSACTION_FEE_LIST;
    }


    /**
     * Gets the CARDTYPE value for this LoginRequestResponse.
     * 
     * @return CARDTYPE
     */
    public java.lang.String getCARDTYPE() {
        return CARDTYPE;
    }


    /**
     * Sets the CARDTYPE value for this LoginRequestResponse.
     * 
     * @param CARDTYPE
     */
    public void setCARDTYPE(java.lang.String CARDTYPE) {
        this.CARDTYPE = CARDTYPE;
    }


    /**
     * Gets the REQUIRE_PIN_CHG value for this LoginRequestResponse.
     * 
     * @return REQUIRE_PIN_CHG
     */
    public java.lang.String getREQUIRE_PIN_CHG() {
        return REQUIRE_PIN_CHG;
    }


    /**
     * Sets the REQUIRE_PIN_CHG value for this LoginRequestResponse.
     * 
     * @param REQUIRE_PIN_CHG
     */
    public void setREQUIRE_PIN_CHG(java.lang.String REQUIRE_PIN_CHG) {
        this.REQUIRE_PIN_CHG = REQUIRE_PIN_CHG;
    }


    /**
     * Gets the REQUIRE_SSN value for this LoginRequestResponse.
     * 
     * @return REQUIRE_SSN
     */
    public java.lang.String getREQUIRE_SSN() {
        return REQUIRE_SSN;
    }


    /**
     * Sets the REQUIRE_SSN value for this LoginRequestResponse.
     * 
     * @param REQUIRE_SSN
     */
    public void setREQUIRE_SSN(java.lang.String REQUIRE_SSN) {
        this.REQUIRE_SSN = REQUIRE_SSN;
    }


    /**
     * Gets the REQUIRE_DOB value for this LoginRequestResponse.
     * 
     * @return REQUIRE_DOB
     */
    public java.lang.String getREQUIRE_DOB() {
        return REQUIRE_DOB;
    }


    /**
     * Sets the REQUIRE_DOB value for this LoginRequestResponse.
     * 
     * @param REQUIRE_DOB
     */
    public void setREQUIRE_DOB(java.lang.String REQUIRE_DOB) {
        this.REQUIRE_DOB = REQUIRE_DOB;
    }


    /**
     * Gets the REQUIRE_PASS_CHG value for this LoginRequestResponse.
     * 
     * @return REQUIRE_PASS_CHG
     */
    public java.lang.String getREQUIRE_PASS_CHG() {
        return REQUIRE_PASS_CHG;
    }


    /**
     * Sets the REQUIRE_PASS_CHG value for this LoginRequestResponse.
     * 
     * @param REQUIRE_PASS_CHG
     */
    public void setREQUIRE_PASS_CHG(java.lang.String REQUIRE_PASS_CHG) {
        this.REQUIRE_PASS_CHG = REQUIRE_PASS_CHG;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LoginRequestResponse)) return false;
        LoginRequestResponse other = (LoginRequestResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.FIRSTNAME==null && other.getFIRSTNAME()==null) || 
             (this.FIRSTNAME!=null &&
              this.FIRSTNAME.equals(other.getFIRSTNAME()))) &&
            ((this.MIDDLENAME==null && other.getMIDDLENAME()==null) || 
             (this.MIDDLENAME!=null &&
              this.MIDDLENAME.equals(other.getMIDDLENAME()))) &&
            ((this.LASTNAME==null && other.getLASTNAME()==null) || 
             (this.LASTNAME!=null &&
              this.LASTNAME.equals(other.getLASTNAME()))) &&
            ((this.ADDRESS1==null && other.getADDRESS1()==null) || 
             (this.ADDRESS1!=null &&
              this.ADDRESS1.equals(other.getADDRESS1()))) &&
            ((this.ADDRESS2==null && other.getADDRESS2()==null) || 
             (this.ADDRESS2!=null &&
              this.ADDRESS2.equals(other.getADDRESS2()))) &&
            ((this.ADDRESS3==null && other.getADDRESS3()==null) || 
             (this.ADDRESS3!=null &&
              this.ADDRESS3.equals(other.getADDRESS3()))) &&
            ((this.CITY==null && other.getCITY()==null) || 
             (this.CITY!=null &&
              this.CITY.equals(other.getCITY()))) &&
            ((this.STATE==null && other.getSTATE()==null) || 
             (this.STATE!=null &&
              this.STATE.equals(other.getSTATE()))) &&
            ((this.COUNTRY==null && other.getCOUNTRY()==null) || 
             (this.COUNTRY!=null &&
              this.COUNTRY.equals(other.getCOUNTRY()))) &&
            ((this.POSTALCODE==null && other.getPOSTALCODE()==null) || 
             (this.POSTALCODE!=null &&
              this.POSTALCODE.equals(other.getPOSTALCODE()))) &&
            ((this.PHONE==null && other.getPHONE()==null) || 
             (this.PHONE!=null &&
              this.PHONE.equals(other.getPHONE()))) &&
            ((this.EMAIL==null && other.getEMAIL()==null) || 
             (this.EMAIL!=null &&
              this.EMAIL.equals(other.getEMAIL()))) &&
            ((this.SECURITYQUESTION==null && other.getSECURITYQUESTION()==null) || 
             (this.SECURITYQUESTION!=null &&
              this.SECURITYQUESTION.equals(other.getSECURITYQUESTION()))) &&
            ((this.SECURITYANSWER==null && other.getSECURITYANSWER()==null) || 
             (this.SECURITYANSWER!=null &&
              this.SECURITYANSWER.equals(other.getSECURITYANSWER()))) &&
            ((this.ALERT_DAILY_BAL_TIME==null && other.getALERT_DAILY_BAL_TIME()==null) || 
             (this.ALERT_DAILY_BAL_TIME!=null &&
              this.ALERT_DAILY_BAL_TIME.equals(other.getALERT_DAILY_BAL_TIME()))) &&
            ((this.ALERT_DAILY_BALANCE==null && other.getALERT_DAILY_BALANCE()==null) || 
             (this.ALERT_DAILY_BALANCE!=null &&
              this.ALERT_DAILY_BALANCE.equals(other.getALERT_DAILY_BALANCE()))) &&
            ((this.ALERT_DIRECT_DEPOSIT==null && other.getALERT_DIRECT_DEPOSIT()==null) || 
             (this.ALERT_DIRECT_DEPOSIT!=null &&
              this.ALERT_DIRECT_DEPOSIT.equals(other.getALERT_DIRECT_DEPOSIT()))) &&
            ((this.ALERT_FUNDS_TRANSFER==null && other.getALERT_FUNDS_TRANSFER()==null) || 
             (this.ALERT_FUNDS_TRANSFER!=null &&
              this.ALERT_FUNDS_TRANSFER.equals(other.getALERT_FUNDS_TRANSFER()))) &&
            ((this.ALERT_INSF==null && other.getALERT_INSF()==null) || 
             (this.ALERT_INSF!=null &&
              this.ALERT_INSF.equals(other.getALERT_INSF()))) &&
            ((this.ALERT_LOW_BALANCE==null && other.getALERT_LOW_BALANCE()==null) || 
             (this.ALERT_LOW_BALANCE!=null &&
              this.ALERT_LOW_BALANCE.equals(other.getALERT_LOW_BALANCE()))) &&
            ((this.ALERT_LOW_BALANCE_AM==null && other.getALERT_LOW_BALANCE_AM()==null) || 
             (this.ALERT_LOW_BALANCE_AM!=null &&
              this.ALERT_LOW_BALANCE_AM.equals(other.getALERT_LOW_BALANCE_AM()))) &&
            ((this.ALERT_MONTHLY_BAL==null && other.getALERT_MONTHLY_BAL()==null) || 
             (this.ALERT_MONTHLY_BAL!=null &&
              this.ALERT_MONTHLY_BAL.equals(other.getALERT_MONTHLY_BAL()))) &&
            ((this.ALERT_ONLINE_PASSWRD==null && other.getALERT_ONLINE_PASSWRD()==null) || 
             (this.ALERT_ONLINE_PASSWRD!=null &&
              this.ALERT_ONLINE_PASSWRD.equals(other.getALERT_ONLINE_PASSWRD()))) &&
            ((this.ALERT_ONLINE_SIGNON==null && other.getALERT_ONLINE_SIGNON()==null) || 
             (this.ALERT_ONLINE_SIGNON!=null &&
              this.ALERT_ONLINE_SIGNON.equals(other.getALERT_ONLINE_SIGNON()))) &&
            ((this.ALERT_ONLINE_USER==null && other.getALERT_ONLINE_USER()==null) || 
             (this.ALERT_ONLINE_USER!=null &&
              this.ALERT_ONLINE_USER.equals(other.getALERT_ONLINE_USER()))) &&
            ((this.ALERT_TRANS_OVER==null && other.getALERT_TRANS_OVER()==null) || 
             (this.ALERT_TRANS_OVER!=null &&
              this.ALERT_TRANS_OVER.equals(other.getALERT_TRANS_OVER()))) &&
            ((this.ALERT_TRANS_OVER_AMT==null && other.getALERT_TRANS_OVER_AMT()==null) || 
             (this.ALERT_TRANS_OVER_AMT!=null &&
              this.ALERT_TRANS_OVER_AMT.equals(other.getALERT_TRANS_OVER_AMT()))) &&
            ((this.ALERT_WEEKLY_BAL_DAY==null && other.getALERT_WEEKLY_BAL_DAY()==null) || 
             (this.ALERT_WEEKLY_BAL_DAY!=null &&
              this.ALERT_WEEKLY_BAL_DAY.equals(other.getALERT_WEEKLY_BAL_DAY()))) &&
            ((this.ALERT_WEEKLY_BALANCE==null && other.getALERT_WEEKLY_BALANCE()==null) || 
             (this.ALERT_WEEKLY_BALANCE!=null &&
              this.ALERT_WEEKLY_BALANCE.equals(other.getALERT_WEEKLY_BALANCE()))) &&
            ((this.HISTORY_SORT==null && other.getHISTORY_SORT()==null) || 
             (this.HISTORY_SORT!=null &&
              this.HISTORY_SORT.equals(other.getHISTORY_SORT()))) &&
            ((this.SKIN==null && other.getSKIN()==null) || 
             (this.SKIN!=null &&
              this.SKIN.equals(other.getSKIN()))) &&
            ((this.DATE_SEARCH==null && other.getDATE_SEARCH()==null) || 
             (this.DATE_SEARCH!=null &&
              this.DATE_SEARCH.equals(other.getDATE_SEARCH()))) &&
            ((this.SMS_PROVIDER==null && other.getSMS_PROVIDER()==null) || 
             (this.SMS_PROVIDER!=null &&
              this.SMS_PROVIDER.equals(other.getSMS_PROVIDER()))) &&
            ((this.SMS_PHONE==null && other.getSMS_PHONE()==null) || 
             (this.SMS_PHONE!=null &&
              this.SMS_PHONE.equals(other.getSMS_PHONE()))) &&
            ((this.SMS_EMAIL==null && other.getSMS_EMAIL()==null) || 
             (this.SMS_EMAIL!=null &&
              this.SMS_EMAIL.equals(other.getSMS_EMAIL()))) &&
            ((this.LAST_LOGIN_DATE==null && other.getLAST_LOGIN_DATE()==null) || 
             (this.LAST_LOGIN_DATE!=null &&
              this.LAST_LOGIN_DATE.equals(other.getLAST_LOGIN_DATE()))) &&
            ((this.LAST_LOGIN_TIME==null && other.getLAST_LOGIN_TIME()==null) || 
             (this.LAST_LOGIN_TIME!=null &&
              this.LAST_LOGIN_TIME.equals(other.getLAST_LOGIN_TIME()))) &&
            ((this.CARD_BALANCE==null && other.getCARD_BALANCE()==null) || 
             (this.CARD_BALANCE!=null &&
              this.CARD_BALANCE.equals(other.getCARD_BALANCE()))) &&
            ((this.ALLOW_C2C==null && other.getALLOW_C2C()==null) || 
             (this.ALLOW_C2C!=null &&
              this.ALLOW_C2C.equals(other.getALLOW_C2C()))) &&
            ((this.ALLOW_ATM==null && other.getALLOW_ATM()==null) || 
             (this.ALLOW_ATM!=null &&
              this.ALLOW_ATM.equals(other.getALLOW_ATM()))) &&
            ((this.ALLOW_DDP==null && other.getALLOW_DDP()==null) || 
             (this.ALLOW_DDP!=null &&
              this.ALLOW_DDP.equals(other.getALLOW_DDP()))) &&
            ((this.ALLOW_CHK==null && other.getALLOW_CHK()==null) || 
             (this.ALLOW_CHK!=null &&
              this.ALLOW_CHK.equals(other.getALLOW_CHK()))) &&
            ((this.ALLOW_CMP==null && other.getALLOW_CMP()==null) || 
             (this.ALLOW_CMP!=null &&
              this.ALLOW_CMP.equals(other.getALLOW_CMP()))) &&
            ((this.REGISTERED==null && other.getREGISTERED()==null) || 
             (this.REGISTERED!=null &&
              this.REGISTERED.equals(other.getREGISTERED()))) &&
            ((this.ADMIN_NUMBER==null && other.getADMIN_NUMBER()==null) || 
             (this.ADMIN_NUMBER!=null &&
              this.ADMIN_NUMBER.equals(other.getADMIN_NUMBER()))) &&
            ((this.TRANSACTION_FEE_LIST==null && other.getTRANSACTION_FEE_LIST()==null) || 
             (this.TRANSACTION_FEE_LIST!=null &&
              java.util.Arrays.equals(this.TRANSACTION_FEE_LIST, other.getTRANSACTION_FEE_LIST()))) &&
            ((this.CARDTYPE==null && other.getCARDTYPE()==null) || 
             (this.CARDTYPE!=null &&
              this.CARDTYPE.equals(other.getCARDTYPE()))) &&
            ((this.REQUIRE_PIN_CHG==null && other.getREQUIRE_PIN_CHG()==null) || 
             (this.REQUIRE_PIN_CHG!=null &&
              this.REQUIRE_PIN_CHG.equals(other.getREQUIRE_PIN_CHG()))) &&
            ((this.REQUIRE_SSN==null && other.getREQUIRE_SSN()==null) || 
             (this.REQUIRE_SSN!=null &&
              this.REQUIRE_SSN.equals(other.getREQUIRE_SSN()))) &&
            ((this.REQUIRE_DOB==null && other.getREQUIRE_DOB()==null) || 
             (this.REQUIRE_DOB!=null &&
              this.REQUIRE_DOB.equals(other.getREQUIRE_DOB()))) &&
            ((this.REQUIRE_PASS_CHG==null && other.getREQUIRE_PASS_CHG()==null) || 
             (this.REQUIRE_PASS_CHG!=null &&
              this.REQUIRE_PASS_CHG.equals(other.getREQUIRE_PASS_CHG())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getFIRSTNAME() != null) {
            _hashCode += getFIRSTNAME().hashCode();
        }
        if (getMIDDLENAME() != null) {
            _hashCode += getMIDDLENAME().hashCode();
        }
        if (getLASTNAME() != null) {
            _hashCode += getLASTNAME().hashCode();
        }
        if (getADDRESS1() != null) {
            _hashCode += getADDRESS1().hashCode();
        }
        if (getADDRESS2() != null) {
            _hashCode += getADDRESS2().hashCode();
        }
        if (getADDRESS3() != null) {
            _hashCode += getADDRESS3().hashCode();
        }
        if (getCITY() != null) {
            _hashCode += getCITY().hashCode();
        }
        if (getSTATE() != null) {
            _hashCode += getSTATE().hashCode();
        }
        if (getCOUNTRY() != null) {
            _hashCode += getCOUNTRY().hashCode();
        }
        if (getPOSTALCODE() != null) {
            _hashCode += getPOSTALCODE().hashCode();
        }
        if (getPHONE() != null) {
            _hashCode += getPHONE().hashCode();
        }
        if (getEMAIL() != null) {
            _hashCode += getEMAIL().hashCode();
        }
        if (getSECURITYQUESTION() != null) {
            _hashCode += getSECURITYQUESTION().hashCode();
        }
        if (getSECURITYANSWER() != null) {
            _hashCode += getSECURITYANSWER().hashCode();
        }
        if (getALERT_DAILY_BAL_TIME() != null) {
            _hashCode += getALERT_DAILY_BAL_TIME().hashCode();
        }
        if (getALERT_DAILY_BALANCE() != null) {
            _hashCode += getALERT_DAILY_BALANCE().hashCode();
        }
        if (getALERT_DIRECT_DEPOSIT() != null) {
            _hashCode += getALERT_DIRECT_DEPOSIT().hashCode();
        }
        if (getALERT_FUNDS_TRANSFER() != null) {
            _hashCode += getALERT_FUNDS_TRANSFER().hashCode();
        }
        if (getALERT_INSF() != null) {
            _hashCode += getALERT_INSF().hashCode();
        }
        if (getALERT_LOW_BALANCE() != null) {
            _hashCode += getALERT_LOW_BALANCE().hashCode();
        }
        if (getALERT_LOW_BALANCE_AM() != null) {
            _hashCode += getALERT_LOW_BALANCE_AM().hashCode();
        }
        if (getALERT_MONTHLY_BAL() != null) {
            _hashCode += getALERT_MONTHLY_BAL().hashCode();
        }
        if (getALERT_ONLINE_PASSWRD() != null) {
            _hashCode += getALERT_ONLINE_PASSWRD().hashCode();
        }
        if (getALERT_ONLINE_SIGNON() != null) {
            _hashCode += getALERT_ONLINE_SIGNON().hashCode();
        }
        if (getALERT_ONLINE_USER() != null) {
            _hashCode += getALERT_ONLINE_USER().hashCode();
        }
        if (getALERT_TRANS_OVER() != null) {
            _hashCode += getALERT_TRANS_OVER().hashCode();
        }
        if (getALERT_TRANS_OVER_AMT() != null) {
            _hashCode += getALERT_TRANS_OVER_AMT().hashCode();
        }
        if (getALERT_WEEKLY_BAL_DAY() != null) {
            _hashCode += getALERT_WEEKLY_BAL_DAY().hashCode();
        }
        if (getALERT_WEEKLY_BALANCE() != null) {
            _hashCode += getALERT_WEEKLY_BALANCE().hashCode();
        }
        if (getHISTORY_SORT() != null) {
            _hashCode += getHISTORY_SORT().hashCode();
        }
        if (getSKIN() != null) {
            _hashCode += getSKIN().hashCode();
        }
        if (getDATE_SEARCH() != null) {
            _hashCode += getDATE_SEARCH().hashCode();
        }
        if (getSMS_PROVIDER() != null) {
            _hashCode += getSMS_PROVIDER().hashCode();
        }
        if (getSMS_PHONE() != null) {
            _hashCode += getSMS_PHONE().hashCode();
        }
        if (getSMS_EMAIL() != null) {
            _hashCode += getSMS_EMAIL().hashCode();
        }
        if (getLAST_LOGIN_DATE() != null) {
            _hashCode += getLAST_LOGIN_DATE().hashCode();
        }
        if (getLAST_LOGIN_TIME() != null) {
            _hashCode += getLAST_LOGIN_TIME().hashCode();
        }
        if (getCARD_BALANCE() != null) {
            _hashCode += getCARD_BALANCE().hashCode();
        }
        if (getALLOW_C2C() != null) {
            _hashCode += getALLOW_C2C().hashCode();
        }
        if (getALLOW_ATM() != null) {
            _hashCode += getALLOW_ATM().hashCode();
        }
        if (getALLOW_DDP() != null) {
            _hashCode += getALLOW_DDP().hashCode();
        }
        if (getALLOW_CHK() != null) {
            _hashCode += getALLOW_CHK().hashCode();
        }
        if (getALLOW_CMP() != null) {
            _hashCode += getALLOW_CMP().hashCode();
        }
        if (getREGISTERED() != null) {
            _hashCode += getREGISTERED().hashCode();
        }
        if (getADMIN_NUMBER() != null) {
            _hashCode += getADMIN_NUMBER().hashCode();
        }
        if (getTRANSACTION_FEE_LIST() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTRANSACTION_FEE_LIST());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTRANSACTION_FEE_LIST(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCARDTYPE() != null) {
            _hashCode += getCARDTYPE().hashCode();
        }
        if (getREQUIRE_PIN_CHG() != null) {
            _hashCode += getREQUIRE_PIN_CHG().hashCode();
        }
        if (getREQUIRE_SSN() != null) {
            _hashCode += getREQUIRE_SSN().hashCode();
        }
        if (getREQUIRE_DOB() != null) {
            _hashCode += getREQUIRE_DOB().hashCode();
        }
        if (getREQUIRE_PASS_CHG() != null) {
            _hashCode += getREQUIRE_PASS_CHG().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LoginRequestResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoginRequestResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRSTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FIRSTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MIDDLENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MIDDLENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTALCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "POSTALCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PHONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMAIL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "EMAIL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECURITYQUESTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SECURITYQUESTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SECURITYANSWER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SECURITYANSWER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_DAILY_BAL_TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_DAILY_BAL_TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_DAILY_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_DAILY_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_DIRECT_DEPOSIT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_DIRECT_DEPOSIT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_FUNDS_TRANSFER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_FUNDS_TRANSFER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_INSF");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_INSF"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_LOW_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_LOW_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_LOW_BALANCE_AM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_LOW_BALANCE_AM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_MONTHLY_BAL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_MONTHLY_BAL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_ONLINE_PASSWRD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_ONLINE_PASSWRD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_ONLINE_SIGNON");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_ONLINE_SIGNON"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_ONLINE_USER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_ONLINE_USER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_TRANS_OVER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_TRANS_OVER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_TRANS_OVER_AMT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_TRANS_OVER_AMT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_WEEKLY_BAL_DAY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_WEEKLY_BAL_DAY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALERT_WEEKLY_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALERT_WEEKLY_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HISTORY_SORT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "HISTORY_SORT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SKIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SKIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATE_SEARCH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DATE_SEARCH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_PROVIDER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_PROVIDER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_PHONE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_PHONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_EMAIL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_EMAIL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_LOGIN_DATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LAST_LOGIN_DATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LAST_LOGIN_TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LAST_LOGIN_TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALLOW_C2C");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALLOW_C2C"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALLOW_ATM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALLOW_ATM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALLOW_DDP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALLOW_DDP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALLOW_CHK");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALLOW_CHK"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ALLOW_CMP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ALLOW_CMP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REGISTERED");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REGISTERED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACTION_FEE_LIST");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_FEE_LIST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FeeDetailType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REQUIRE_PIN_CHG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUIRE_PIN_CHG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REQUIRE_SSN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUIRE_SSN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REQUIRE_DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUIRE_DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REQUIRE_PASS_CHG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUIRE_PASS_CHG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
