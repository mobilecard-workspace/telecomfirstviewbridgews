/**
 * ReportStolenCard2Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ReportStolenCard2Response  implements java.io.Serializable {
    private java.lang.String ERRMSG;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERROR_FOUND;

    private java.lang.String NEW_CARD_NUMBER;

    private java.lang.String NEW_ACCOUNT_NO;

    private java.lang.String OLD_CARD_NUMBER;

    private java.lang.String OLD_ACCOUNT_NO;

    public ReportStolenCard2Response() {
    }

    public ReportStolenCard2Response(
           java.lang.String ERRMSG,
           java.lang.String ERR_NUMBER,
           java.lang.String ERROR_FOUND,
           java.lang.String NEW_CARD_NUMBER,
           java.lang.String NEW_ACCOUNT_NO,
           java.lang.String OLD_CARD_NUMBER,
           java.lang.String OLD_ACCOUNT_NO) {
           this.ERRMSG = ERRMSG;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERROR_FOUND = ERROR_FOUND;
           this.NEW_CARD_NUMBER = NEW_CARD_NUMBER;
           this.NEW_ACCOUNT_NO = NEW_ACCOUNT_NO;
           this.OLD_CARD_NUMBER = OLD_CARD_NUMBER;
           this.OLD_ACCOUNT_NO = OLD_ACCOUNT_NO;
    }


    /**
     * Gets the ERRMSG value for this ReportStolenCard2Response.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this ReportStolenCard2Response.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the ERR_NUMBER value for this ReportStolenCard2Response.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this ReportStolenCard2Response.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this ReportStolenCard2Response.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this ReportStolenCard2Response.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the NEW_CARD_NUMBER value for this ReportStolenCard2Response.
     * 
     * @return NEW_CARD_NUMBER
     */
    public java.lang.String getNEW_CARD_NUMBER() {
        return NEW_CARD_NUMBER;
    }


    /**
     * Sets the NEW_CARD_NUMBER value for this ReportStolenCard2Response.
     * 
     * @param NEW_CARD_NUMBER
     */
    public void setNEW_CARD_NUMBER(java.lang.String NEW_CARD_NUMBER) {
        this.NEW_CARD_NUMBER = NEW_CARD_NUMBER;
    }


    /**
     * Gets the NEW_ACCOUNT_NO value for this ReportStolenCard2Response.
     * 
     * @return NEW_ACCOUNT_NO
     */
    public java.lang.String getNEW_ACCOUNT_NO() {
        return NEW_ACCOUNT_NO;
    }


    /**
     * Sets the NEW_ACCOUNT_NO value for this ReportStolenCard2Response.
     * 
     * @param NEW_ACCOUNT_NO
     */
    public void setNEW_ACCOUNT_NO(java.lang.String NEW_ACCOUNT_NO) {
        this.NEW_ACCOUNT_NO = NEW_ACCOUNT_NO;
    }


    /**
     * Gets the OLD_CARD_NUMBER value for this ReportStolenCard2Response.
     * 
     * @return OLD_CARD_NUMBER
     */
    public java.lang.String getOLD_CARD_NUMBER() {
        return OLD_CARD_NUMBER;
    }


    /**
     * Sets the OLD_CARD_NUMBER value for this ReportStolenCard2Response.
     * 
     * @param OLD_CARD_NUMBER
     */
    public void setOLD_CARD_NUMBER(java.lang.String OLD_CARD_NUMBER) {
        this.OLD_CARD_NUMBER = OLD_CARD_NUMBER;
    }


    /**
     * Gets the OLD_ACCOUNT_NO value for this ReportStolenCard2Response.
     * 
     * @return OLD_ACCOUNT_NO
     */
    public java.lang.String getOLD_ACCOUNT_NO() {
        return OLD_ACCOUNT_NO;
    }


    /**
     * Sets the OLD_ACCOUNT_NO value for this ReportStolenCard2Response.
     * 
     * @param OLD_ACCOUNT_NO
     */
    public void setOLD_ACCOUNT_NO(java.lang.String OLD_ACCOUNT_NO) {
        this.OLD_ACCOUNT_NO = OLD_ACCOUNT_NO;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReportStolenCard2Response)) return false;
        ReportStolenCard2Response other = (ReportStolenCard2Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.NEW_CARD_NUMBER==null && other.getNEW_CARD_NUMBER()==null) || 
             (this.NEW_CARD_NUMBER!=null &&
              this.NEW_CARD_NUMBER.equals(other.getNEW_CARD_NUMBER()))) &&
            ((this.NEW_ACCOUNT_NO==null && other.getNEW_ACCOUNT_NO()==null) || 
             (this.NEW_ACCOUNT_NO!=null &&
              this.NEW_ACCOUNT_NO.equals(other.getNEW_ACCOUNT_NO()))) &&
            ((this.OLD_CARD_NUMBER==null && other.getOLD_CARD_NUMBER()==null) || 
             (this.OLD_CARD_NUMBER!=null &&
              this.OLD_CARD_NUMBER.equals(other.getOLD_CARD_NUMBER()))) &&
            ((this.OLD_ACCOUNT_NO==null && other.getOLD_ACCOUNT_NO()==null) || 
             (this.OLD_ACCOUNT_NO!=null &&
              this.OLD_ACCOUNT_NO.equals(other.getOLD_ACCOUNT_NO())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getNEW_CARD_NUMBER() != null) {
            _hashCode += getNEW_CARD_NUMBER().hashCode();
        }
        if (getNEW_ACCOUNT_NO() != null) {
            _hashCode += getNEW_ACCOUNT_NO().hashCode();
        }
        if (getOLD_CARD_NUMBER() != null) {
            _hashCode += getOLD_CARD_NUMBER().hashCode();
        }
        if (getOLD_ACCOUNT_NO() != null) {
            _hashCode += getOLD_ACCOUNT_NO().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReportStolenCard2Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NEW_CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NEW_CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NEW_ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NEW_ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OLD_CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "OLD_CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("OLD_ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "OLD_ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
