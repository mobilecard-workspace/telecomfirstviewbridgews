/**
 * LoadsBasedOnHoldingAccountRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class LoadsBasedOnHoldingAccountRequest  implements java.io.Serializable {
    private java.lang.String CARD_NUMBER;

    private java.lang.String accountNumber;

    private java.lang.String DDA_NO;

    private java.lang.String transactionAmount;

    private java.lang.String TRANSACTION_CODE_TO;

    private java.lang.String reversalTargetTranID;

    private java.lang.String FILTER;

    private java.lang.String TNPFlag;

    private java.lang.String referenceId;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    private java.lang.String loadType;

    public LoadsBasedOnHoldingAccountRequest() {
    }

    public LoadsBasedOnHoldingAccountRequest(
           java.lang.String CARD_NUMBER,
           java.lang.String accountNumber,
           java.lang.String DDA_NO,
           java.lang.String transactionAmount,
           java.lang.String TRANSACTION_CODE_TO,
           java.lang.String reversalTargetTranID,
           java.lang.String FILTER,
           java.lang.String TNPFlag,
           java.lang.String referenceId,
           java.lang.String ANI,
           java.lang.String DNIS,
           java.lang.String loadType) {
           this.CARD_NUMBER = CARD_NUMBER;
           this.accountNumber = accountNumber;
           this.DDA_NO = DDA_NO;
           this.transactionAmount = transactionAmount;
           this.TRANSACTION_CODE_TO = TRANSACTION_CODE_TO;
           this.reversalTargetTranID = reversalTargetTranID;
           this.FILTER = FILTER;
           this.TNPFlag = TNPFlag;
           this.referenceId = referenceId;
           this.ANI = ANI;
           this.DNIS = DNIS;
           this.loadType = loadType;
    }


    /**
     * Gets the CARD_NUMBER value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the accountNumber value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the DDA_NO value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return DDA_NO
     */
    public java.lang.String getDDA_NO() {
        return DDA_NO;
    }


    /**
     * Sets the DDA_NO value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param DDA_NO
     */
    public void setDDA_NO(java.lang.String DDA_NO) {
        this.DDA_NO = DDA_NO;
    }


    /**
     * Gets the transactionAmount value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return transactionAmount
     */
    public java.lang.String getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.lang.String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the TRANSACTION_CODE_TO value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return TRANSACTION_CODE_TO
     */
    public java.lang.String getTRANSACTION_CODE_TO() {
        return TRANSACTION_CODE_TO;
    }


    /**
     * Sets the TRANSACTION_CODE_TO value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param TRANSACTION_CODE_TO
     */
    public void setTRANSACTION_CODE_TO(java.lang.String TRANSACTION_CODE_TO) {
        this.TRANSACTION_CODE_TO = TRANSACTION_CODE_TO;
    }


    /**
     * Gets the reversalTargetTranID value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return reversalTargetTranID
     */
    public java.lang.String getReversalTargetTranID() {
        return reversalTargetTranID;
    }


    /**
     * Sets the reversalTargetTranID value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param reversalTargetTranID
     */
    public void setReversalTargetTranID(java.lang.String reversalTargetTranID) {
        this.reversalTargetTranID = reversalTargetTranID;
    }


    /**
     * Gets the FILTER value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return FILTER
     */
    public java.lang.String getFILTER() {
        return FILTER;
    }


    /**
     * Sets the FILTER value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param FILTER
     */
    public void setFILTER(java.lang.String FILTER) {
        this.FILTER = FILTER;
    }


    /**
     * Gets the TNPFlag value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return TNPFlag
     */
    public java.lang.String getTNPFlag() {
        return TNPFlag;
    }


    /**
     * Sets the TNPFlag value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param TNPFlag
     */
    public void setTNPFlag(java.lang.String TNPFlag) {
        this.TNPFlag = TNPFlag;
    }


    /**
     * Gets the referenceId value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return referenceId
     */
    public java.lang.String getReferenceId() {
        return referenceId;
    }


    /**
     * Sets the referenceId value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param referenceId
     */
    public void setReferenceId(java.lang.String referenceId) {
        this.referenceId = referenceId;
    }


    /**
     * Gets the ANI value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }


    /**
     * Gets the loadType value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @return loadType
     */
    public java.lang.String getLoadType() {
        return loadType;
    }


    /**
     * Sets the loadType value for this LoadsBasedOnHoldingAccountRequest.
     * 
     * @param loadType
     */
    public void setLoadType(java.lang.String loadType) {
        this.loadType = loadType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LoadsBasedOnHoldingAccountRequest)) return false;
        LoadsBasedOnHoldingAccountRequest other = (LoadsBasedOnHoldingAccountRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.DDA_NO==null && other.getDDA_NO()==null) || 
             (this.DDA_NO!=null &&
              this.DDA_NO.equals(other.getDDA_NO()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.TRANSACTION_CODE_TO==null && other.getTRANSACTION_CODE_TO()==null) || 
             (this.TRANSACTION_CODE_TO!=null &&
              this.TRANSACTION_CODE_TO.equals(other.getTRANSACTION_CODE_TO()))) &&
            ((this.reversalTargetTranID==null && other.getReversalTargetTranID()==null) || 
             (this.reversalTargetTranID!=null &&
              this.reversalTargetTranID.equals(other.getReversalTargetTranID()))) &&
            ((this.FILTER==null && other.getFILTER()==null) || 
             (this.FILTER!=null &&
              this.FILTER.equals(other.getFILTER()))) &&
            ((this.TNPFlag==null && other.getTNPFlag()==null) || 
             (this.TNPFlag!=null &&
              this.TNPFlag.equals(other.getTNPFlag()))) &&
            ((this.referenceId==null && other.getReferenceId()==null) || 
             (this.referenceId!=null &&
              this.referenceId.equals(other.getReferenceId()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS()))) &&
            ((this.loadType==null && other.getLoadType()==null) || 
             (this.loadType!=null &&
              this.loadType.equals(other.getLoadType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getDDA_NO() != null) {
            _hashCode += getDDA_NO().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTRANSACTION_CODE_TO() != null) {
            _hashCode += getTRANSACTION_CODE_TO().hashCode();
        }
        if (getReversalTargetTranID() != null) {
            _hashCode += getReversalTargetTranID().hashCode();
        }
        if (getFILTER() != null) {
            _hashCode += getFILTER().hashCode();
        }
        if (getTNPFlag() != null) {
            _hashCode += getTNPFlag().hashCode();
        }
        if (getReferenceId() != null) {
            _hashCode += getReferenceId().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        if (getLoadType() != null) {
            _hashCode += getLoadType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LoadsBasedOnHoldingAccountRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccountRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDA_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DDA_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACTION_CODE_TO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_CODE_TO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reversalTargetTranID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReversalTargetTranID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FILTER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FILTER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TNPFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TNPFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReferenceId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loadType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
