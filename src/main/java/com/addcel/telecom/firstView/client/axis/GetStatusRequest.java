/**
 * GetStatusRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class GetStatusRequest  implements java.io.Serializable {
    private java.lang.String dePpCode;

    private java.lang.String dePpAppID;

    public GetStatusRequest() {
    }

    public GetStatusRequest(
           java.lang.String dePpCode,
           java.lang.String dePpAppID) {
           this.dePpCode = dePpCode;
           this.dePpAppID = dePpAppID;
    }


    /**
     * Gets the dePpCode value for this GetStatusRequest.
     * 
     * @return dePpCode
     */
    public java.lang.String getDePpCode() {
        return dePpCode;
    }


    /**
     * Sets the dePpCode value for this GetStatusRequest.
     * 
     * @param dePpCode
     */
    public void setDePpCode(java.lang.String dePpCode) {
        this.dePpCode = dePpCode;
    }


    /**
     * Gets the dePpAppID value for this GetStatusRequest.
     * 
     * @return dePpAppID
     */
    public java.lang.String getDePpAppID() {
        return dePpAppID;
    }


    /**
     * Sets the dePpAppID value for this GetStatusRequest.
     * 
     * @param dePpAppID
     */
    public void setDePpAppID(java.lang.String dePpAppID) {
        this.dePpAppID = dePpAppID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetStatusRequest)) return false;
        GetStatusRequest other = (GetStatusRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dePpCode==null && other.getDePpCode()==null) || 
             (this.dePpCode!=null &&
              this.dePpCode.equals(other.getDePpCode()))) &&
            ((this.dePpAppID==null && other.getDePpAppID()==null) || 
             (this.dePpAppID!=null &&
              this.dePpAppID.equals(other.getDePpAppID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDePpCode() != null) {
            _hashCode += getDePpCode().hashCode();
        }
        if (getDePpAppID() != null) {
            _hashCode += getDePpAppID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetStatusRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "GetStatusRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAppID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAppID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
