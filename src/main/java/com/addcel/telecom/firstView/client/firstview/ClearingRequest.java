//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ClearingRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ClearingRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AdminNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ApprovalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardAcceptorBusinessCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardAcceptorIdCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CardAcceptorTerminalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CreditPlanMaster" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CustNbr" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateTimeLocalTransaction" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InventoryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LineItemSeqNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MerchantType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MessageTypeIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PrimaryAccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RetrievalReferenceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SpecialMerchantIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SystemTraceAuditNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TnpFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TranType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionCurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransactionDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TransmissionDateTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClearingRequest", propOrder = {
    "adminNo",
    "approvalCode",
    "cardAcceptorBusinessCode",
    "cardAcceptorIdCode",
    "cardAcceptorTerminalId",
    "creditPlanMaster",
    "custNbr",
    "dateTimeLocalTransaction",
    "inventoryCode",
    "lineItemSeqNumber",
    "merchantType",
    "messageTypeIdentifier",
    "primaryAccountNumber",
    "quantity",
    "retrievalReferenceNumber",
    "specialMerchantIdentifier",
    "systemTraceAuditNumber",
    "tnpFlag",
    "tranType",
    "transactionAmount",
    "transactionCurrencyCode",
    "transactionDescription",
    "transmissionDateTime",
    "unitPrice"
})
public class ClearingRequest {

    @XmlElement(name = "AdminNo")
    protected String adminNo;
    @XmlElement(name = "ApprovalCode")
    protected String approvalCode;
    @XmlElement(name = "CardAcceptorBusinessCode")
    protected String cardAcceptorBusinessCode;
    @XmlElement(name = "CardAcceptorIdCode")
    protected String cardAcceptorIdCode;
    @XmlElement(name = "CardAcceptorTerminalId")
    protected String cardAcceptorTerminalId;
    @XmlElement(name = "CreditPlanMaster")
    protected String creditPlanMaster;
    @XmlElement(name = "CustNbr")
    protected String custNbr;
    @XmlElement(name = "DateTimeLocalTransaction")
    protected String dateTimeLocalTransaction;
    @XmlElement(name = "InventoryCode")
    protected String inventoryCode;
    @XmlElement(name = "LineItemSeqNumber")
    protected String lineItemSeqNumber;
    @XmlElement(name = "MerchantType")
    protected String merchantType;
    @XmlElement(name = "MessageTypeIdentifier")
    protected String messageTypeIdentifier;
    @XmlElement(name = "PrimaryAccountNumber")
    protected String primaryAccountNumber;
    @XmlElement(name = "Quantity")
    protected String quantity;
    @XmlElement(name = "RetrievalReferenceNumber")
    protected String retrievalReferenceNumber;
    @XmlElement(name = "SpecialMerchantIdentifier")
    protected String specialMerchantIdentifier;
    @XmlElement(name = "SystemTraceAuditNumber")
    protected String systemTraceAuditNumber;
    @XmlElement(name = "TnpFlag")
    protected String tnpFlag;
    @XmlElement(name = "TranType")
    protected String tranType;
    @XmlElement(name = "TransactionAmount")
    protected String transactionAmount;
    @XmlElement(name = "TransactionCurrencyCode")
    protected String transactionCurrencyCode;
    @XmlElement(name = "TransactionDescription")
    protected String transactionDescription;
    @XmlElement(name = "TransmissionDateTime")
    protected String transmissionDateTime;
    @XmlElement(name = "UnitPrice")
    protected String unitPrice;

    /**
     * Obtiene el valor de la propiedad adminNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdminNo() {
        return adminNo;
    }

    /**
     * Define el valor de la propiedad adminNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdminNo(String value) {
        this.adminNo = value;
    }

    /**
     * Obtiene el valor de la propiedad approvalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovalCode() {
        return approvalCode;
    }

    /**
     * Define el valor de la propiedad approvalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovalCode(String value) {
        this.approvalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorBusinessCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorBusinessCode() {
        return cardAcceptorBusinessCode;
    }

    /**
     * Define el valor de la propiedad cardAcceptorBusinessCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorBusinessCode(String value) {
        this.cardAcceptorBusinessCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorIdCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorIdCode() {
        return cardAcceptorIdCode;
    }

    /**
     * Define el valor de la propiedad cardAcceptorIdCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorIdCode(String value) {
        this.cardAcceptorIdCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cardAcceptorTerminalId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardAcceptorTerminalId() {
        return cardAcceptorTerminalId;
    }

    /**
     * Define el valor de la propiedad cardAcceptorTerminalId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardAcceptorTerminalId(String value) {
        this.cardAcceptorTerminalId = value;
    }

    /**
     * Obtiene el valor de la propiedad creditPlanMaster.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditPlanMaster() {
        return creditPlanMaster;
    }

    /**
     * Define el valor de la propiedad creditPlanMaster.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditPlanMaster(String value) {
        this.creditPlanMaster = value;
    }

    /**
     * Obtiene el valor de la propiedad custNbr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustNbr() {
        return custNbr;
    }

    /**
     * Define el valor de la propiedad custNbr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustNbr(String value) {
        this.custNbr = value;
    }

    /**
     * Obtiene el valor de la propiedad dateTimeLocalTransaction.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateTimeLocalTransaction() {
        return dateTimeLocalTransaction;
    }

    /**
     * Define el valor de la propiedad dateTimeLocalTransaction.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateTimeLocalTransaction(String value) {
        this.dateTimeLocalTransaction = value;
    }

    /**
     * Obtiene el valor de la propiedad inventoryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInventoryCode() {
        return inventoryCode;
    }

    /**
     * Define el valor de la propiedad inventoryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInventoryCode(String value) {
        this.inventoryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad lineItemSeqNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemSeqNumber() {
        return lineItemSeqNumber;
    }

    /**
     * Define el valor de la propiedad lineItemSeqNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemSeqNumber(String value) {
        this.lineItemSeqNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad merchantType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantType() {
        return merchantType;
    }

    /**
     * Define el valor de la propiedad merchantType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantType(String value) {
        this.merchantType = value;
    }

    /**
     * Obtiene el valor de la propiedad messageTypeIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageTypeIdentifier() {
        return messageTypeIdentifier;
    }

    /**
     * Define el valor de la propiedad messageTypeIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageTypeIdentifier(String value) {
        this.messageTypeIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad primaryAccountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }

    /**
     * Define el valor de la propiedad primaryAccountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimaryAccountNumber(String value) {
        this.primaryAccountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad quantity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Define el valor de la propiedad quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuantity(String value) {
        this.quantity = value;
    }

    /**
     * Obtiene el valor de la propiedad retrievalReferenceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }

    /**
     * Define el valor de la propiedad retrievalReferenceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetrievalReferenceNumber(String value) {
        this.retrievalReferenceNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad specialMerchantIdentifier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecialMerchantIdentifier() {
        return specialMerchantIdentifier;
    }

    /**
     * Define el valor de la propiedad specialMerchantIdentifier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecialMerchantIdentifier(String value) {
        this.specialMerchantIdentifier = value;
    }

    /**
     * Obtiene el valor de la propiedad systemTraceAuditNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemTraceAuditNumber() {
        return systemTraceAuditNumber;
    }

    /**
     * Define el valor de la propiedad systemTraceAuditNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemTraceAuditNumber(String value) {
        this.systemTraceAuditNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad tnpFlag.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTnpFlag() {
        return tnpFlag;
    }

    /**
     * Define el valor de la propiedad tnpFlag.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTnpFlag(String value) {
        this.tnpFlag = value;
    }

    /**
     * Obtiene el valor de la propiedad tranType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTranType() {
        return tranType;
    }

    /**
     * Define el valor de la propiedad tranType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTranType(String value) {
        this.tranType = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionAmount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionAmount() {
        return transactionAmount;
    }

    /**
     * Define el valor de la propiedad transactionAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionAmount(String value) {
        this.transactionAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionCurrencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }

    /**
     * Define el valor de la propiedad transactionCurrencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionCurrencyCode(String value) {
        this.transactionCurrencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad transactionDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionDescription() {
        return transactionDescription;
    }

    /**
     * Define el valor de la propiedad transactionDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionDescription(String value) {
        this.transactionDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad transmissionDateTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransmissionDateTime() {
        return transmissionDateTime;
    }

    /**
     * Define el valor de la propiedad transmissionDateTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransmissionDateTime(String value) {
        this.transmissionDateTime = value;
    }

    /**
     * Obtiene el valor de la propiedad unitPrice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitPrice() {
        return unitPrice;
    }

    /**
     * Define el valor de la propiedad unitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitPrice(String value) {
        this.unitPrice = value;
    }

}
