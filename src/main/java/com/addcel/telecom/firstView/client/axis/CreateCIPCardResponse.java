/**
 * CreateCIPCardResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class CreateCIPCardResponse  implements java.io.Serializable {
    private java.lang.String accountNumber;

    private java.lang.String ddaNo;

    private java.lang.String pan;

    private java.lang.String accountCreationDate;

    private java.lang.String accountCreationTime;

    private java.lang.String referenceNo;

    private java.lang.String expirationDate;

    private java.lang.String currencyCode;

    private java.lang.String governmentIdType;

    private java.lang.String governmentId;

    private java.lang.String storeName;

    private java.lang.String responseCode;

    private java.lang.String reasonDescription;

    public CreateCIPCardResponse() {
    }

    public CreateCIPCardResponse(
           java.lang.String accountNumber,
           java.lang.String ddaNo,
           java.lang.String pan,
           java.lang.String accountCreationDate,
           java.lang.String accountCreationTime,
           java.lang.String referenceNo,
           java.lang.String expirationDate,
           java.lang.String currencyCode,
           java.lang.String governmentIdType,
           java.lang.String governmentId,
           java.lang.String storeName,
           java.lang.String responseCode,
           java.lang.String reasonDescription) {
           this.accountNumber = accountNumber;
           this.ddaNo = ddaNo;
           this.pan = pan;
           this.accountCreationDate = accountCreationDate;
           this.accountCreationTime = accountCreationTime;
           this.referenceNo = referenceNo;
           this.expirationDate = expirationDate;
           this.currencyCode = currencyCode;
           this.governmentIdType = governmentIdType;
           this.governmentId = governmentId;
           this.storeName = storeName;
           this.responseCode = responseCode;
           this.reasonDescription = reasonDescription;
    }


    /**
     * Gets the accountNumber value for this CreateCIPCardResponse.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this CreateCIPCardResponse.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the ddaNo value for this CreateCIPCardResponse.
     * 
     * @return ddaNo
     */
    public java.lang.String getDdaNo() {
        return ddaNo;
    }


    /**
     * Sets the ddaNo value for this CreateCIPCardResponse.
     * 
     * @param ddaNo
     */
    public void setDdaNo(java.lang.String ddaNo) {
        this.ddaNo = ddaNo;
    }


    /**
     * Gets the pan value for this CreateCIPCardResponse.
     * 
     * @return pan
     */
    public java.lang.String getPan() {
        return pan;
    }


    /**
     * Sets the pan value for this CreateCIPCardResponse.
     * 
     * @param pan
     */
    public void setPan(java.lang.String pan) {
        this.pan = pan;
    }


    /**
     * Gets the accountCreationDate value for this CreateCIPCardResponse.
     * 
     * @return accountCreationDate
     */
    public java.lang.String getAccountCreationDate() {
        return accountCreationDate;
    }


    /**
     * Sets the accountCreationDate value for this CreateCIPCardResponse.
     * 
     * @param accountCreationDate
     */
    public void setAccountCreationDate(java.lang.String accountCreationDate) {
        this.accountCreationDate = accountCreationDate;
    }


    /**
     * Gets the accountCreationTime value for this CreateCIPCardResponse.
     * 
     * @return accountCreationTime
     */
    public java.lang.String getAccountCreationTime() {
        return accountCreationTime;
    }


    /**
     * Sets the accountCreationTime value for this CreateCIPCardResponse.
     * 
     * @param accountCreationTime
     */
    public void setAccountCreationTime(java.lang.String accountCreationTime) {
        this.accountCreationTime = accountCreationTime;
    }


    /**
     * Gets the referenceNo value for this CreateCIPCardResponse.
     * 
     * @return referenceNo
     */
    public java.lang.String getReferenceNo() {
        return referenceNo;
    }


    /**
     * Sets the referenceNo value for this CreateCIPCardResponse.
     * 
     * @param referenceNo
     */
    public void setReferenceNo(java.lang.String referenceNo) {
        this.referenceNo = referenceNo;
    }


    /**
     * Gets the expirationDate value for this CreateCIPCardResponse.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this CreateCIPCardResponse.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the currencyCode value for this CreateCIPCardResponse.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this CreateCIPCardResponse.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the governmentIdType value for this CreateCIPCardResponse.
     * 
     * @return governmentIdType
     */
    public java.lang.String getGovernmentIdType() {
        return governmentIdType;
    }


    /**
     * Sets the governmentIdType value for this CreateCIPCardResponse.
     * 
     * @param governmentIdType
     */
    public void setGovernmentIdType(java.lang.String governmentIdType) {
        this.governmentIdType = governmentIdType;
    }


    /**
     * Gets the governmentId value for this CreateCIPCardResponse.
     * 
     * @return governmentId
     */
    public java.lang.String getGovernmentId() {
        return governmentId;
    }


    /**
     * Sets the governmentId value for this CreateCIPCardResponse.
     * 
     * @param governmentId
     */
    public void setGovernmentId(java.lang.String governmentId) {
        this.governmentId = governmentId;
    }


    /**
     * Gets the storeName value for this CreateCIPCardResponse.
     * 
     * @return storeName
     */
    public java.lang.String getStoreName() {
        return storeName;
    }


    /**
     * Sets the storeName value for this CreateCIPCardResponse.
     * 
     * @param storeName
     */
    public void setStoreName(java.lang.String storeName) {
        this.storeName = storeName;
    }


    /**
     * Gets the responseCode value for this CreateCIPCardResponse.
     * 
     * @return responseCode
     */
    public java.lang.String getResponseCode() {
        return responseCode;
    }


    /**
     * Sets the responseCode value for this CreateCIPCardResponse.
     * 
     * @param responseCode
     */
    public void setResponseCode(java.lang.String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Gets the reasonDescription value for this CreateCIPCardResponse.
     * 
     * @return reasonDescription
     */
    public java.lang.String getReasonDescription() {
        return reasonDescription;
    }


    /**
     * Sets the reasonDescription value for this CreateCIPCardResponse.
     * 
     * @param reasonDescription
     */
    public void setReasonDescription(java.lang.String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateCIPCardResponse)) return false;
        CreateCIPCardResponse other = (CreateCIPCardResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.ddaNo==null && other.getDdaNo()==null) || 
             (this.ddaNo!=null &&
              this.ddaNo.equals(other.getDdaNo()))) &&
            ((this.pan==null && other.getPan()==null) || 
             (this.pan!=null &&
              this.pan.equals(other.getPan()))) &&
            ((this.accountCreationDate==null && other.getAccountCreationDate()==null) || 
             (this.accountCreationDate!=null &&
              this.accountCreationDate.equals(other.getAccountCreationDate()))) &&
            ((this.accountCreationTime==null && other.getAccountCreationTime()==null) || 
             (this.accountCreationTime!=null &&
              this.accountCreationTime.equals(other.getAccountCreationTime()))) &&
            ((this.referenceNo==null && other.getReferenceNo()==null) || 
             (this.referenceNo!=null &&
              this.referenceNo.equals(other.getReferenceNo()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.governmentIdType==null && other.getGovernmentIdType()==null) || 
             (this.governmentIdType!=null &&
              this.governmentIdType.equals(other.getGovernmentIdType()))) &&
            ((this.governmentId==null && other.getGovernmentId()==null) || 
             (this.governmentId!=null &&
              this.governmentId.equals(other.getGovernmentId()))) &&
            ((this.storeName==null && other.getStoreName()==null) || 
             (this.storeName!=null &&
              this.storeName.equals(other.getStoreName()))) &&
            ((this.responseCode==null && other.getResponseCode()==null) || 
             (this.responseCode!=null &&
              this.responseCode.equals(other.getResponseCode()))) &&
            ((this.reasonDescription==null && other.getReasonDescription()==null) || 
             (this.reasonDescription!=null &&
              this.reasonDescription.equals(other.getReasonDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getDdaNo() != null) {
            _hashCode += getDdaNo().hashCode();
        }
        if (getPan() != null) {
            _hashCode += getPan().hashCode();
        }
        if (getAccountCreationDate() != null) {
            _hashCode += getAccountCreationDate().hashCode();
        }
        if (getAccountCreationTime() != null) {
            _hashCode += getAccountCreationTime().hashCode();
        }
        if (getReferenceNo() != null) {
            _hashCode += getReferenceNo().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getGovernmentIdType() != null) {
            _hashCode += getGovernmentIdType().hashCode();
        }
        if (getGovernmentId() != null) {
            _hashCode += getGovernmentId().hashCode();
        }
        if (getStoreName() != null) {
            _hashCode += getStoreName().hashCode();
        }
        if (getResponseCode() != null) {
            _hashCode += getResponseCode().hashCode();
        }
        if (getReasonDescription() != null) {
            _hashCode += getReasonDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateCIPCardResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CreateCIPCardResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ddaNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DdaNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Pan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCreationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountCreationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCreationTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountCreationTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ReferenceNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("governmentIdType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GovernmentIdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("governmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GovernmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "StoreName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResponseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reasonDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ReasonDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
