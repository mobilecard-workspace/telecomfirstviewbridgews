/**
 * TransactionHistoryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class TransactionHistoryResponse  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType[] TRANSACTION_LIST;

    private java.lang.String LASTTRANSACTION;

    private java.lang.String BEGINNING_BALANCE;

    private java.lang.String ENDING_BALANCE;

    private java.lang.String TOTAL_CREDITS;

    private java.lang.String TOTAL_DEBITS;

    private java.lang.String TOTAL_FEES;

    private java.lang.String TRANSACTION_DT;

    private java.lang.String ERRMSG;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERROR_FOUND;

    private java.lang.String CURRENT_CARD_BALANCE;

    private java.lang.String expirationDate;

    public TransactionHistoryResponse() {
    }

    public TransactionHistoryResponse(
           com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType[] TRANSACTION_LIST,
           java.lang.String LASTTRANSACTION,
           java.lang.String BEGINNING_BALANCE,
           java.lang.String ENDING_BALANCE,
           java.lang.String TOTAL_CREDITS,
           java.lang.String TOTAL_DEBITS,
           java.lang.String TOTAL_FEES,
           java.lang.String TRANSACTION_DT,
           java.lang.String ERRMSG,
           java.lang.String ERR_NUMBER,
           java.lang.String ERROR_FOUND,
           java.lang.String CURRENT_CARD_BALANCE,
           java.lang.String expirationDate) {
           this.TRANSACTION_LIST = TRANSACTION_LIST;
           this.LASTTRANSACTION = LASTTRANSACTION;
           this.BEGINNING_BALANCE = BEGINNING_BALANCE;
           this.ENDING_BALANCE = ENDING_BALANCE;
           this.TOTAL_CREDITS = TOTAL_CREDITS;
           this.TOTAL_DEBITS = TOTAL_DEBITS;
           this.TOTAL_FEES = TOTAL_FEES;
           this.TRANSACTION_DT = TRANSACTION_DT;
           this.ERRMSG = ERRMSG;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERROR_FOUND = ERROR_FOUND;
           this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
           this.expirationDate = expirationDate;
    }


    /**
     * Gets the TRANSACTION_LIST value for this TransactionHistoryResponse.
     * 
     * @return TRANSACTION_LIST
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType[] getTRANSACTION_LIST() {
        return TRANSACTION_LIST;
    }


    /**
     * Sets the TRANSACTION_LIST value for this TransactionHistoryResponse.
     * 
     * @param TRANSACTION_LIST
     */
    public void setTRANSACTION_LIST(com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType[] TRANSACTION_LIST) {
        this.TRANSACTION_LIST = TRANSACTION_LIST;
    }


    /**
     * Gets the LASTTRANSACTION value for this TransactionHistoryResponse.
     * 
     * @return LASTTRANSACTION
     */
    public java.lang.String getLASTTRANSACTION() {
        return LASTTRANSACTION;
    }


    /**
     * Sets the LASTTRANSACTION value for this TransactionHistoryResponse.
     * 
     * @param LASTTRANSACTION
     */
    public void setLASTTRANSACTION(java.lang.String LASTTRANSACTION) {
        this.LASTTRANSACTION = LASTTRANSACTION;
    }


    /**
     * Gets the BEGINNING_BALANCE value for this TransactionHistoryResponse.
     * 
     * @return BEGINNING_BALANCE
     */
    public java.lang.String getBEGINNING_BALANCE() {
        return BEGINNING_BALANCE;
    }


    /**
     * Sets the BEGINNING_BALANCE value for this TransactionHistoryResponse.
     * 
     * @param BEGINNING_BALANCE
     */
    public void setBEGINNING_BALANCE(java.lang.String BEGINNING_BALANCE) {
        this.BEGINNING_BALANCE = BEGINNING_BALANCE;
    }


    /**
     * Gets the ENDING_BALANCE value for this TransactionHistoryResponse.
     * 
     * @return ENDING_BALANCE
     */
    public java.lang.String getENDING_BALANCE() {
        return ENDING_BALANCE;
    }


    /**
     * Sets the ENDING_BALANCE value for this TransactionHistoryResponse.
     * 
     * @param ENDING_BALANCE
     */
    public void setENDING_BALANCE(java.lang.String ENDING_BALANCE) {
        this.ENDING_BALANCE = ENDING_BALANCE;
    }


    /**
     * Gets the TOTAL_CREDITS value for this TransactionHistoryResponse.
     * 
     * @return TOTAL_CREDITS
     */
    public java.lang.String getTOTAL_CREDITS() {
        return TOTAL_CREDITS;
    }


    /**
     * Sets the TOTAL_CREDITS value for this TransactionHistoryResponse.
     * 
     * @param TOTAL_CREDITS
     */
    public void setTOTAL_CREDITS(java.lang.String TOTAL_CREDITS) {
        this.TOTAL_CREDITS = TOTAL_CREDITS;
    }


    /**
     * Gets the TOTAL_DEBITS value for this TransactionHistoryResponse.
     * 
     * @return TOTAL_DEBITS
     */
    public java.lang.String getTOTAL_DEBITS() {
        return TOTAL_DEBITS;
    }


    /**
     * Sets the TOTAL_DEBITS value for this TransactionHistoryResponse.
     * 
     * @param TOTAL_DEBITS
     */
    public void setTOTAL_DEBITS(java.lang.String TOTAL_DEBITS) {
        this.TOTAL_DEBITS = TOTAL_DEBITS;
    }


    /**
     * Gets the TOTAL_FEES value for this TransactionHistoryResponse.
     * 
     * @return TOTAL_FEES
     */
    public java.lang.String getTOTAL_FEES() {
        return TOTAL_FEES;
    }


    /**
     * Sets the TOTAL_FEES value for this TransactionHistoryResponse.
     * 
     * @param TOTAL_FEES
     */
    public void setTOTAL_FEES(java.lang.String TOTAL_FEES) {
        this.TOTAL_FEES = TOTAL_FEES;
    }


    /**
     * Gets the TRANSACTION_DT value for this TransactionHistoryResponse.
     * 
     * @return TRANSACTION_DT
     */
    public java.lang.String getTRANSACTION_DT() {
        return TRANSACTION_DT;
    }


    /**
     * Sets the TRANSACTION_DT value for this TransactionHistoryResponse.
     * 
     * @param TRANSACTION_DT
     */
    public void setTRANSACTION_DT(java.lang.String TRANSACTION_DT) {
        this.TRANSACTION_DT = TRANSACTION_DT;
    }


    /**
     * Gets the ERRMSG value for this TransactionHistoryResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this TransactionHistoryResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the ERR_NUMBER value for this TransactionHistoryResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this TransactionHistoryResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this TransactionHistoryResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this TransactionHistoryResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the CURRENT_CARD_BALANCE value for this TransactionHistoryResponse.
     * 
     * @return CURRENT_CARD_BALANCE
     */
    public java.lang.String getCURRENT_CARD_BALANCE() {
        return CURRENT_CARD_BALANCE;
    }


    /**
     * Sets the CURRENT_CARD_BALANCE value for this TransactionHistoryResponse.
     * 
     * @param CURRENT_CARD_BALANCE
     */
    public void setCURRENT_CARD_BALANCE(java.lang.String CURRENT_CARD_BALANCE) {
        this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
    }


    /**
     * Gets the expirationDate value for this TransactionHistoryResponse.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this TransactionHistoryResponse.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionHistoryResponse)) return false;
        TransactionHistoryResponse other = (TransactionHistoryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.TRANSACTION_LIST==null && other.getTRANSACTION_LIST()==null) || 
             (this.TRANSACTION_LIST!=null &&
              java.util.Arrays.equals(this.TRANSACTION_LIST, other.getTRANSACTION_LIST()))) &&
            ((this.LASTTRANSACTION==null && other.getLASTTRANSACTION()==null) || 
             (this.LASTTRANSACTION!=null &&
              this.LASTTRANSACTION.equals(other.getLASTTRANSACTION()))) &&
            ((this.BEGINNING_BALANCE==null && other.getBEGINNING_BALANCE()==null) || 
             (this.BEGINNING_BALANCE!=null &&
              this.BEGINNING_BALANCE.equals(other.getBEGINNING_BALANCE()))) &&
            ((this.ENDING_BALANCE==null && other.getENDING_BALANCE()==null) || 
             (this.ENDING_BALANCE!=null &&
              this.ENDING_BALANCE.equals(other.getENDING_BALANCE()))) &&
            ((this.TOTAL_CREDITS==null && other.getTOTAL_CREDITS()==null) || 
             (this.TOTAL_CREDITS!=null &&
              this.TOTAL_CREDITS.equals(other.getTOTAL_CREDITS()))) &&
            ((this.TOTAL_DEBITS==null && other.getTOTAL_DEBITS()==null) || 
             (this.TOTAL_DEBITS!=null &&
              this.TOTAL_DEBITS.equals(other.getTOTAL_DEBITS()))) &&
            ((this.TOTAL_FEES==null && other.getTOTAL_FEES()==null) || 
             (this.TOTAL_FEES!=null &&
              this.TOTAL_FEES.equals(other.getTOTAL_FEES()))) &&
            ((this.TRANSACTION_DT==null && other.getTRANSACTION_DT()==null) || 
             (this.TRANSACTION_DT!=null &&
              this.TRANSACTION_DT.equals(other.getTRANSACTION_DT()))) &&
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.CURRENT_CARD_BALANCE==null && other.getCURRENT_CARD_BALANCE()==null) || 
             (this.CURRENT_CARD_BALANCE!=null &&
              this.CURRENT_CARD_BALANCE.equals(other.getCURRENT_CARD_BALANCE()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTRANSACTION_LIST() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTRANSACTION_LIST());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTRANSACTION_LIST(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLASTTRANSACTION() != null) {
            _hashCode += getLASTTRANSACTION().hashCode();
        }
        if (getBEGINNING_BALANCE() != null) {
            _hashCode += getBEGINNING_BALANCE().hashCode();
        }
        if (getENDING_BALANCE() != null) {
            _hashCode += getENDING_BALANCE().hashCode();
        }
        if (getTOTAL_CREDITS() != null) {
            _hashCode += getTOTAL_CREDITS().hashCode();
        }
        if (getTOTAL_DEBITS() != null) {
            _hashCode += getTOTAL_DEBITS().hashCode();
        }
        if (getTOTAL_FEES() != null) {
            _hashCode += getTOTAL_FEES().hashCode();
        }
        if (getTRANSACTION_DT() != null) {
            _hashCode += getTRANSACTION_DT().hashCode();
        }
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getCURRENT_CARD_BALANCE() != null) {
            _hashCode += getCURRENT_CARD_BALANCE().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionHistoryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACTION_LIST");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_LIST"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTTRANSACTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTTRANSACTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("BEGINNING_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "BEGINNING_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENDING_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ENDING_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_CREDITS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TOTAL_CREDITS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_DEBITS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TOTAL_DEBITS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TOTAL_FEES");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TOTAL_FEES"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACTION_DT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_DT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENT_CARD_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CURRENT_CARD_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
