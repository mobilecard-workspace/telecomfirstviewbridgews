//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GetStatusRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetStatusRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DePpCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpAppID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatusRequest", propOrder = {
    "dePpCode",
    "dePpAppID"
})
public class GetStatusRequest {

    @XmlElement(name = "DePpCode")
    protected String dePpCode;
    @XmlElement(name = "DePpAppID")
    protected String dePpAppID;

    /**
     * Obtiene el valor de la propiedad dePpCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpCode() {
        return dePpCode;
    }

    /**
     * Define el valor de la propiedad dePpCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpCode(String value) {
        this.dePpCode = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpAppID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpAppID() {
        return dePpAppID;
    }

    /**
     * Define el valor de la propiedad dePpAppID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpAppID(String value) {
        this.dePpAppID = value;
    }

}
