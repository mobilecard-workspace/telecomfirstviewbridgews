/**
 * CreateCardRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class CreateCardRequest  implements java.io.Serializable {
    private java.lang.String dePpAcctCreationDate;

    private java.lang.String dePpAcctCreationTime;

    private java.lang.String dePpReferenceNo;

    private java.lang.String dePpCurrencyCode;

    private java.lang.String dePpAccountType;

    private java.lang.String dePpName;

    private java.lang.String deMiddleName;

    private java.lang.String dePpName2;

    private java.lang.String dePpAddress1;

    private java.lang.String dePpAddress2;

    private java.lang.String dePpState;

    private java.lang.String dePpCity;

    private java.lang.String dePpGovtIdType;

    private java.lang.String dePpGovtIdIssueState;

    private java.lang.String dePpPostalCode;

    private java.lang.String dePpCountryOfIssue;

    private java.lang.String dePpEmailId;

    private java.lang.String dePpGovtId;

    private java.lang.String dePpPhoneNumber;

    private java.lang.String dePpComment;

    private java.lang.String dePpDateOfBirth;

    private java.lang.String deBuacctProductId;

    private java.lang.String dePpWorkPhoneNumber;

    private java.lang.String dePpMobilePhoneNumber;

    private java.lang.String dePpOtherPhoneNumber;

    private java.lang.String dePpFollowUpDate;

    private java.lang.String dePpEmployername;

    private java.lang.String dePpEmployerContactName;

    private java.lang.String dePpEmployerContactPhoneNumber;

    private java.lang.String dePpEmployerContactFaxNumber;

    private java.lang.String dePpMemos;

    private java.lang.String dePpCustRefName;

    private java.lang.String deSvcStoreName;

    private java.lang.String dePpGovtIdIssueDate;

    private java.lang.String dePpGovtIdExpirationDate;

    private java.lang.String dePpGovtIdCountryofIssuance;

    private java.lang.String dePpcipType;

    private java.lang.String dePpcipNumber;

    private java.lang.String dePpcipStatus;

    private java.lang.String dePpssn;

    private java.lang.String deIvrEmbossingLine4;

    private java.lang.String deIvrEmbossingHotStamp;

    private java.lang.String dePpSecondaryPan;

    public CreateCardRequest() {
    }

    public CreateCardRequest(
           java.lang.String dePpAcctCreationDate,
           java.lang.String dePpAcctCreationTime,
           java.lang.String dePpReferenceNo,
           java.lang.String dePpCurrencyCode,
           java.lang.String dePpAccountType,
           java.lang.String dePpName,
           java.lang.String deMiddleName,
           java.lang.String dePpName2,
           java.lang.String dePpAddress1,
           java.lang.String dePpAddress2,
           java.lang.String dePpState,
           java.lang.String dePpCity,
           java.lang.String dePpGovtIdType,
           java.lang.String dePpGovtIdIssueState,
           java.lang.String dePpPostalCode,
           java.lang.String dePpCountryOfIssue,
           java.lang.String dePpEmailId,
           java.lang.String dePpGovtId,
           java.lang.String dePpPhoneNumber,
           java.lang.String dePpComment,
           java.lang.String dePpDateOfBirth,
           java.lang.String deBuacctProductId,
           java.lang.String dePpWorkPhoneNumber,
           java.lang.String dePpMobilePhoneNumber,
           java.lang.String dePpOtherPhoneNumber,
           java.lang.String dePpFollowUpDate,
           java.lang.String dePpEmployername,
           java.lang.String dePpEmployerContactName,
           java.lang.String dePpEmployerContactPhoneNumber,
           java.lang.String dePpEmployerContactFaxNumber,
           java.lang.String dePpMemos,
           java.lang.String dePpCustRefName,
           java.lang.String deSvcStoreName,
           java.lang.String dePpGovtIdIssueDate,
           java.lang.String dePpGovtIdExpirationDate,
           java.lang.String dePpGovtIdCountryofIssuance,
           java.lang.String dePpcipType,
           java.lang.String dePpcipNumber,
           java.lang.String dePpcipStatus,
           java.lang.String dePpssn,
           java.lang.String deIvrEmbossingLine4,
           java.lang.String deIvrEmbossingHotStamp,
           java.lang.String dePpSecondaryPan) {
           this.dePpAcctCreationDate = dePpAcctCreationDate;
           this.dePpAcctCreationTime = dePpAcctCreationTime;
           this.dePpReferenceNo = dePpReferenceNo;
           this.dePpCurrencyCode = dePpCurrencyCode;
           this.dePpAccountType = dePpAccountType;
           this.dePpName = dePpName;
           this.deMiddleName = deMiddleName;
           this.dePpName2 = dePpName2;
           this.dePpAddress1 = dePpAddress1;
           this.dePpAddress2 = dePpAddress2;
           this.dePpState = dePpState;
           this.dePpCity = dePpCity;
           this.dePpGovtIdType = dePpGovtIdType;
           this.dePpGovtIdIssueState = dePpGovtIdIssueState;
           this.dePpPostalCode = dePpPostalCode;
           this.dePpCountryOfIssue = dePpCountryOfIssue;
           this.dePpEmailId = dePpEmailId;
           this.dePpGovtId = dePpGovtId;
           this.dePpPhoneNumber = dePpPhoneNumber;
           this.dePpComment = dePpComment;
           this.dePpDateOfBirth = dePpDateOfBirth;
           this.deBuacctProductId = deBuacctProductId;
           this.dePpWorkPhoneNumber = dePpWorkPhoneNumber;
           this.dePpMobilePhoneNumber = dePpMobilePhoneNumber;
           this.dePpOtherPhoneNumber = dePpOtherPhoneNumber;
           this.dePpFollowUpDate = dePpFollowUpDate;
           this.dePpEmployername = dePpEmployername;
           this.dePpEmployerContactName = dePpEmployerContactName;
           this.dePpEmployerContactPhoneNumber = dePpEmployerContactPhoneNumber;
           this.dePpEmployerContactFaxNumber = dePpEmployerContactFaxNumber;
           this.dePpMemos = dePpMemos;
           this.dePpCustRefName = dePpCustRefName;
           this.deSvcStoreName = deSvcStoreName;
           this.dePpGovtIdIssueDate = dePpGovtIdIssueDate;
           this.dePpGovtIdExpirationDate = dePpGovtIdExpirationDate;
           this.dePpGovtIdCountryofIssuance = dePpGovtIdCountryofIssuance;
           this.dePpcipType = dePpcipType;
           this.dePpcipNumber = dePpcipNumber;
           this.dePpcipStatus = dePpcipStatus;
           this.dePpssn = dePpssn;
           this.deIvrEmbossingLine4 = deIvrEmbossingLine4;
           this.deIvrEmbossingHotStamp = deIvrEmbossingHotStamp;
           this.dePpSecondaryPan = dePpSecondaryPan;
    }


    /**
     * Gets the dePpAcctCreationDate value for this CreateCardRequest.
     * 
     * @return dePpAcctCreationDate
     */
    public java.lang.String getDePpAcctCreationDate() {
        return dePpAcctCreationDate;
    }


    /**
     * Sets the dePpAcctCreationDate value for this CreateCardRequest.
     * 
     * @param dePpAcctCreationDate
     */
    public void setDePpAcctCreationDate(java.lang.String dePpAcctCreationDate) {
        this.dePpAcctCreationDate = dePpAcctCreationDate;
    }


    /**
     * Gets the dePpAcctCreationTime value for this CreateCardRequest.
     * 
     * @return dePpAcctCreationTime
     */
    public java.lang.String getDePpAcctCreationTime() {
        return dePpAcctCreationTime;
    }


    /**
     * Sets the dePpAcctCreationTime value for this CreateCardRequest.
     * 
     * @param dePpAcctCreationTime
     */
    public void setDePpAcctCreationTime(java.lang.String dePpAcctCreationTime) {
        this.dePpAcctCreationTime = dePpAcctCreationTime;
    }


    /**
     * Gets the dePpReferenceNo value for this CreateCardRequest.
     * 
     * @return dePpReferenceNo
     */
    public java.lang.String getDePpReferenceNo() {
        return dePpReferenceNo;
    }


    /**
     * Sets the dePpReferenceNo value for this CreateCardRequest.
     * 
     * @param dePpReferenceNo
     */
    public void setDePpReferenceNo(java.lang.String dePpReferenceNo) {
        this.dePpReferenceNo = dePpReferenceNo;
    }


    /**
     * Gets the dePpCurrencyCode value for this CreateCardRequest.
     * 
     * @return dePpCurrencyCode
     */
    public java.lang.String getDePpCurrencyCode() {
        return dePpCurrencyCode;
    }


    /**
     * Sets the dePpCurrencyCode value for this CreateCardRequest.
     * 
     * @param dePpCurrencyCode
     */
    public void setDePpCurrencyCode(java.lang.String dePpCurrencyCode) {
        this.dePpCurrencyCode = dePpCurrencyCode;
    }


    /**
     * Gets the dePpAccountType value for this CreateCardRequest.
     * 
     * @return dePpAccountType
     */
    public java.lang.String getDePpAccountType() {
        return dePpAccountType;
    }


    /**
     * Sets the dePpAccountType value for this CreateCardRequest.
     * 
     * @param dePpAccountType
     */
    public void setDePpAccountType(java.lang.String dePpAccountType) {
        this.dePpAccountType = dePpAccountType;
    }


    /**
     * Gets the dePpName value for this CreateCardRequest.
     * 
     * @return dePpName
     */
    public java.lang.String getDePpName() {
        return dePpName;
    }


    /**
     * Sets the dePpName value for this CreateCardRequest.
     * 
     * @param dePpName
     */
    public void setDePpName(java.lang.String dePpName) {
        this.dePpName = dePpName;
    }


    /**
     * Gets the deMiddleName value for this CreateCardRequest.
     * 
     * @return deMiddleName
     */
    public java.lang.String getDeMiddleName() {
        return deMiddleName;
    }


    /**
     * Sets the deMiddleName value for this CreateCardRequest.
     * 
     * @param deMiddleName
     */
    public void setDeMiddleName(java.lang.String deMiddleName) {
        this.deMiddleName = deMiddleName;
    }


    /**
     * Gets the dePpName2 value for this CreateCardRequest.
     * 
     * @return dePpName2
     */
    public java.lang.String getDePpName2() {
        return dePpName2;
    }


    /**
     * Sets the dePpName2 value for this CreateCardRequest.
     * 
     * @param dePpName2
     */
    public void setDePpName2(java.lang.String dePpName2) {
        this.dePpName2 = dePpName2;
    }


    /**
     * Gets the dePpAddress1 value for this CreateCardRequest.
     * 
     * @return dePpAddress1
     */
    public java.lang.String getDePpAddress1() {
        return dePpAddress1;
    }


    /**
     * Sets the dePpAddress1 value for this CreateCardRequest.
     * 
     * @param dePpAddress1
     */
    public void setDePpAddress1(java.lang.String dePpAddress1) {
        this.dePpAddress1 = dePpAddress1;
    }


    /**
     * Gets the dePpAddress2 value for this CreateCardRequest.
     * 
     * @return dePpAddress2
     */
    public java.lang.String getDePpAddress2() {
        return dePpAddress2;
    }


    /**
     * Sets the dePpAddress2 value for this CreateCardRequest.
     * 
     * @param dePpAddress2
     */
    public void setDePpAddress2(java.lang.String dePpAddress2) {
        this.dePpAddress2 = dePpAddress2;
    }


    /**
     * Gets the dePpState value for this CreateCardRequest.
     * 
     * @return dePpState
     */
    public java.lang.String getDePpState() {
        return dePpState;
    }


    /**
     * Sets the dePpState value for this CreateCardRequest.
     * 
     * @param dePpState
     */
    public void setDePpState(java.lang.String dePpState) {
        this.dePpState = dePpState;
    }


    /**
     * Gets the dePpCity value for this CreateCardRequest.
     * 
     * @return dePpCity
     */
    public java.lang.String getDePpCity() {
        return dePpCity;
    }


    /**
     * Sets the dePpCity value for this CreateCardRequest.
     * 
     * @param dePpCity
     */
    public void setDePpCity(java.lang.String dePpCity) {
        this.dePpCity = dePpCity;
    }


    /**
     * Gets the dePpGovtIdType value for this CreateCardRequest.
     * 
     * @return dePpGovtIdType
     */
    public java.lang.String getDePpGovtIdType() {
        return dePpGovtIdType;
    }


    /**
     * Sets the dePpGovtIdType value for this CreateCardRequest.
     * 
     * @param dePpGovtIdType
     */
    public void setDePpGovtIdType(java.lang.String dePpGovtIdType) {
        this.dePpGovtIdType = dePpGovtIdType;
    }


    /**
     * Gets the dePpGovtIdIssueState value for this CreateCardRequest.
     * 
     * @return dePpGovtIdIssueState
     */
    public java.lang.String getDePpGovtIdIssueState() {
        return dePpGovtIdIssueState;
    }


    /**
     * Sets the dePpGovtIdIssueState value for this CreateCardRequest.
     * 
     * @param dePpGovtIdIssueState
     */
    public void setDePpGovtIdIssueState(java.lang.String dePpGovtIdIssueState) {
        this.dePpGovtIdIssueState = dePpGovtIdIssueState;
    }


    /**
     * Gets the dePpPostalCode value for this CreateCardRequest.
     * 
     * @return dePpPostalCode
     */
    public java.lang.String getDePpPostalCode() {
        return dePpPostalCode;
    }


    /**
     * Sets the dePpPostalCode value for this CreateCardRequest.
     * 
     * @param dePpPostalCode
     */
    public void setDePpPostalCode(java.lang.String dePpPostalCode) {
        this.dePpPostalCode = dePpPostalCode;
    }


    /**
     * Gets the dePpCountryOfIssue value for this CreateCardRequest.
     * 
     * @return dePpCountryOfIssue
     */
    public java.lang.String getDePpCountryOfIssue() {
        return dePpCountryOfIssue;
    }


    /**
     * Sets the dePpCountryOfIssue value for this CreateCardRequest.
     * 
     * @param dePpCountryOfIssue
     */
    public void setDePpCountryOfIssue(java.lang.String dePpCountryOfIssue) {
        this.dePpCountryOfIssue = dePpCountryOfIssue;
    }


    /**
     * Gets the dePpEmailId value for this CreateCardRequest.
     * 
     * @return dePpEmailId
     */
    public java.lang.String getDePpEmailId() {
        return dePpEmailId;
    }


    /**
     * Sets the dePpEmailId value for this CreateCardRequest.
     * 
     * @param dePpEmailId
     */
    public void setDePpEmailId(java.lang.String dePpEmailId) {
        this.dePpEmailId = dePpEmailId;
    }


    /**
     * Gets the dePpGovtId value for this CreateCardRequest.
     * 
     * @return dePpGovtId
     */
    public java.lang.String getDePpGovtId() {
        return dePpGovtId;
    }


    /**
     * Sets the dePpGovtId value for this CreateCardRequest.
     * 
     * @param dePpGovtId
     */
    public void setDePpGovtId(java.lang.String dePpGovtId) {
        this.dePpGovtId = dePpGovtId;
    }


    /**
     * Gets the dePpPhoneNumber value for this CreateCardRequest.
     * 
     * @return dePpPhoneNumber
     */
    public java.lang.String getDePpPhoneNumber() {
        return dePpPhoneNumber;
    }


    /**
     * Sets the dePpPhoneNumber value for this CreateCardRequest.
     * 
     * @param dePpPhoneNumber
     */
    public void setDePpPhoneNumber(java.lang.String dePpPhoneNumber) {
        this.dePpPhoneNumber = dePpPhoneNumber;
    }


    /**
     * Gets the dePpComment value for this CreateCardRequest.
     * 
     * @return dePpComment
     */
    public java.lang.String getDePpComment() {
        return dePpComment;
    }


    /**
     * Sets the dePpComment value for this CreateCardRequest.
     * 
     * @param dePpComment
     */
    public void setDePpComment(java.lang.String dePpComment) {
        this.dePpComment = dePpComment;
    }


    /**
     * Gets the dePpDateOfBirth value for this CreateCardRequest.
     * 
     * @return dePpDateOfBirth
     */
    public java.lang.String getDePpDateOfBirth() {
        return dePpDateOfBirth;
    }


    /**
     * Sets the dePpDateOfBirth value for this CreateCardRequest.
     * 
     * @param dePpDateOfBirth
     */
    public void setDePpDateOfBirth(java.lang.String dePpDateOfBirth) {
        this.dePpDateOfBirth = dePpDateOfBirth;
    }


    /**
     * Gets the deBuacctProductId value for this CreateCardRequest.
     * 
     * @return deBuacctProductId
     */
    public java.lang.String getDeBuacctProductId() {
        return deBuacctProductId;
    }


    /**
     * Sets the deBuacctProductId value for this CreateCardRequest.
     * 
     * @param deBuacctProductId
     */
    public void setDeBuacctProductId(java.lang.String deBuacctProductId) {
        this.deBuacctProductId = deBuacctProductId;
    }


    /**
     * Gets the dePpWorkPhoneNumber value for this CreateCardRequest.
     * 
     * @return dePpWorkPhoneNumber
     */
    public java.lang.String getDePpWorkPhoneNumber() {
        return dePpWorkPhoneNumber;
    }


    /**
     * Sets the dePpWorkPhoneNumber value for this CreateCardRequest.
     * 
     * @param dePpWorkPhoneNumber
     */
    public void setDePpWorkPhoneNumber(java.lang.String dePpWorkPhoneNumber) {
        this.dePpWorkPhoneNumber = dePpWorkPhoneNumber;
    }


    /**
     * Gets the dePpMobilePhoneNumber value for this CreateCardRequest.
     * 
     * @return dePpMobilePhoneNumber
     */
    public java.lang.String getDePpMobilePhoneNumber() {
        return dePpMobilePhoneNumber;
    }


    /**
     * Sets the dePpMobilePhoneNumber value for this CreateCardRequest.
     * 
     * @param dePpMobilePhoneNumber
     */
    public void setDePpMobilePhoneNumber(java.lang.String dePpMobilePhoneNumber) {
        this.dePpMobilePhoneNumber = dePpMobilePhoneNumber;
    }


    /**
     * Gets the dePpOtherPhoneNumber value for this CreateCardRequest.
     * 
     * @return dePpOtherPhoneNumber
     */
    public java.lang.String getDePpOtherPhoneNumber() {
        return dePpOtherPhoneNumber;
    }


    /**
     * Sets the dePpOtherPhoneNumber value for this CreateCardRequest.
     * 
     * @param dePpOtherPhoneNumber
     */
    public void setDePpOtherPhoneNumber(java.lang.String dePpOtherPhoneNumber) {
        this.dePpOtherPhoneNumber = dePpOtherPhoneNumber;
    }


    /**
     * Gets the dePpFollowUpDate value for this CreateCardRequest.
     * 
     * @return dePpFollowUpDate
     */
    public java.lang.String getDePpFollowUpDate() {
        return dePpFollowUpDate;
    }


    /**
     * Sets the dePpFollowUpDate value for this CreateCardRequest.
     * 
     * @param dePpFollowUpDate
     */
    public void setDePpFollowUpDate(java.lang.String dePpFollowUpDate) {
        this.dePpFollowUpDate = dePpFollowUpDate;
    }


    /**
     * Gets the dePpEmployername value for this CreateCardRequest.
     * 
     * @return dePpEmployername
     */
    public java.lang.String getDePpEmployername() {
        return dePpEmployername;
    }


    /**
     * Sets the dePpEmployername value for this CreateCardRequest.
     * 
     * @param dePpEmployername
     */
    public void setDePpEmployername(java.lang.String dePpEmployername) {
        this.dePpEmployername = dePpEmployername;
    }


    /**
     * Gets the dePpEmployerContactName value for this CreateCardRequest.
     * 
     * @return dePpEmployerContactName
     */
    public java.lang.String getDePpEmployerContactName() {
        return dePpEmployerContactName;
    }


    /**
     * Sets the dePpEmployerContactName value for this CreateCardRequest.
     * 
     * @param dePpEmployerContactName
     */
    public void setDePpEmployerContactName(java.lang.String dePpEmployerContactName) {
        this.dePpEmployerContactName = dePpEmployerContactName;
    }


    /**
     * Gets the dePpEmployerContactPhoneNumber value for this CreateCardRequest.
     * 
     * @return dePpEmployerContactPhoneNumber
     */
    public java.lang.String getDePpEmployerContactPhoneNumber() {
        return dePpEmployerContactPhoneNumber;
    }


    /**
     * Sets the dePpEmployerContactPhoneNumber value for this CreateCardRequest.
     * 
     * @param dePpEmployerContactPhoneNumber
     */
    public void setDePpEmployerContactPhoneNumber(java.lang.String dePpEmployerContactPhoneNumber) {
        this.dePpEmployerContactPhoneNumber = dePpEmployerContactPhoneNumber;
    }


    /**
     * Gets the dePpEmployerContactFaxNumber value for this CreateCardRequest.
     * 
     * @return dePpEmployerContactFaxNumber
     */
    public java.lang.String getDePpEmployerContactFaxNumber() {
        return dePpEmployerContactFaxNumber;
    }


    /**
     * Sets the dePpEmployerContactFaxNumber value for this CreateCardRequest.
     * 
     * @param dePpEmployerContactFaxNumber
     */
    public void setDePpEmployerContactFaxNumber(java.lang.String dePpEmployerContactFaxNumber) {
        this.dePpEmployerContactFaxNumber = dePpEmployerContactFaxNumber;
    }


    /**
     * Gets the dePpMemos value for this CreateCardRequest.
     * 
     * @return dePpMemos
     */
    public java.lang.String getDePpMemos() {
        return dePpMemos;
    }


    /**
     * Sets the dePpMemos value for this CreateCardRequest.
     * 
     * @param dePpMemos
     */
    public void setDePpMemos(java.lang.String dePpMemos) {
        this.dePpMemos = dePpMemos;
    }


    /**
     * Gets the dePpCustRefName value for this CreateCardRequest.
     * 
     * @return dePpCustRefName
     */
    public java.lang.String getDePpCustRefName() {
        return dePpCustRefName;
    }


    /**
     * Sets the dePpCustRefName value for this CreateCardRequest.
     * 
     * @param dePpCustRefName
     */
    public void setDePpCustRefName(java.lang.String dePpCustRefName) {
        this.dePpCustRefName = dePpCustRefName;
    }


    /**
     * Gets the deSvcStoreName value for this CreateCardRequest.
     * 
     * @return deSvcStoreName
     */
    public java.lang.String getDeSvcStoreName() {
        return deSvcStoreName;
    }


    /**
     * Sets the deSvcStoreName value for this CreateCardRequest.
     * 
     * @param deSvcStoreName
     */
    public void setDeSvcStoreName(java.lang.String deSvcStoreName) {
        this.deSvcStoreName = deSvcStoreName;
    }


    /**
     * Gets the dePpGovtIdIssueDate value for this CreateCardRequest.
     * 
     * @return dePpGovtIdIssueDate
     */
    public java.lang.String getDePpGovtIdIssueDate() {
        return dePpGovtIdIssueDate;
    }


    /**
     * Sets the dePpGovtIdIssueDate value for this CreateCardRequest.
     * 
     * @param dePpGovtIdIssueDate
     */
    public void setDePpGovtIdIssueDate(java.lang.String dePpGovtIdIssueDate) {
        this.dePpGovtIdIssueDate = dePpGovtIdIssueDate;
    }


    /**
     * Gets the dePpGovtIdExpirationDate value for this CreateCardRequest.
     * 
     * @return dePpGovtIdExpirationDate
     */
    public java.lang.String getDePpGovtIdExpirationDate() {
        return dePpGovtIdExpirationDate;
    }


    /**
     * Sets the dePpGovtIdExpirationDate value for this CreateCardRequest.
     * 
     * @param dePpGovtIdExpirationDate
     */
    public void setDePpGovtIdExpirationDate(java.lang.String dePpGovtIdExpirationDate) {
        this.dePpGovtIdExpirationDate = dePpGovtIdExpirationDate;
    }


    /**
     * Gets the dePpGovtIdCountryofIssuance value for this CreateCardRequest.
     * 
     * @return dePpGovtIdCountryofIssuance
     */
    public java.lang.String getDePpGovtIdCountryofIssuance() {
        return dePpGovtIdCountryofIssuance;
    }


    /**
     * Sets the dePpGovtIdCountryofIssuance value for this CreateCardRequest.
     * 
     * @param dePpGovtIdCountryofIssuance
     */
    public void setDePpGovtIdCountryofIssuance(java.lang.String dePpGovtIdCountryofIssuance) {
        this.dePpGovtIdCountryofIssuance = dePpGovtIdCountryofIssuance;
    }


    /**
     * Gets the dePpcipType value for this CreateCardRequest.
     * 
     * @return dePpcipType
     */
    public java.lang.String getDePpcipType() {
        return dePpcipType;
    }


    /**
     * Sets the dePpcipType value for this CreateCardRequest.
     * 
     * @param dePpcipType
     */
    public void setDePpcipType(java.lang.String dePpcipType) {
        this.dePpcipType = dePpcipType;
    }


    /**
     * Gets the dePpcipNumber value for this CreateCardRequest.
     * 
     * @return dePpcipNumber
     */
    public java.lang.String getDePpcipNumber() {
        return dePpcipNumber;
    }


    /**
     * Sets the dePpcipNumber value for this CreateCardRequest.
     * 
     * @param dePpcipNumber
     */
    public void setDePpcipNumber(java.lang.String dePpcipNumber) {
        this.dePpcipNumber = dePpcipNumber;
    }


    /**
     * Gets the dePpcipStatus value for this CreateCardRequest.
     * 
     * @return dePpcipStatus
     */
    public java.lang.String getDePpcipStatus() {
        return dePpcipStatus;
    }


    /**
     * Sets the dePpcipStatus value for this CreateCardRequest.
     * 
     * @param dePpcipStatus
     */
    public void setDePpcipStatus(java.lang.String dePpcipStatus) {
        this.dePpcipStatus = dePpcipStatus;
    }


    /**
     * Gets the dePpssn value for this CreateCardRequest.
     * 
     * @return dePpssn
     */
    public java.lang.String getDePpssn() {
        return dePpssn;
    }


    /**
     * Sets the dePpssn value for this CreateCardRequest.
     * 
     * @param dePpssn
     */
    public void setDePpssn(java.lang.String dePpssn) {
        this.dePpssn = dePpssn;
    }


    /**
     * Gets the deIvrEmbossingLine4 value for this CreateCardRequest.
     * 
     * @return deIvrEmbossingLine4
     */
    public java.lang.String getDeIvrEmbossingLine4() {
        return deIvrEmbossingLine4;
    }


    /**
     * Sets the deIvrEmbossingLine4 value for this CreateCardRequest.
     * 
     * @param deIvrEmbossingLine4
     */
    public void setDeIvrEmbossingLine4(java.lang.String deIvrEmbossingLine4) {
        this.deIvrEmbossingLine4 = deIvrEmbossingLine4;
    }


    /**
     * Gets the deIvrEmbossingHotStamp value for this CreateCardRequest.
     * 
     * @return deIvrEmbossingHotStamp
     */
    public java.lang.String getDeIvrEmbossingHotStamp() {
        return deIvrEmbossingHotStamp;
    }


    /**
     * Sets the deIvrEmbossingHotStamp value for this CreateCardRequest.
     * 
     * @param deIvrEmbossingHotStamp
     */
    public void setDeIvrEmbossingHotStamp(java.lang.String deIvrEmbossingHotStamp) {
        this.deIvrEmbossingHotStamp = deIvrEmbossingHotStamp;
    }


    /**
     * Gets the dePpSecondaryPan value for this CreateCardRequest.
     * 
     * @return dePpSecondaryPan
     */
    public java.lang.String getDePpSecondaryPan() {
        return dePpSecondaryPan;
    }


    /**
     * Sets the dePpSecondaryPan value for this CreateCardRequest.
     * 
     * @param dePpSecondaryPan
     */
    public void setDePpSecondaryPan(java.lang.String dePpSecondaryPan) {
        this.dePpSecondaryPan = dePpSecondaryPan;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateCardRequest)) return false;
        CreateCardRequest other = (CreateCardRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dePpAcctCreationDate==null && other.getDePpAcctCreationDate()==null) || 
             (this.dePpAcctCreationDate!=null &&
              this.dePpAcctCreationDate.equals(other.getDePpAcctCreationDate()))) &&
            ((this.dePpAcctCreationTime==null && other.getDePpAcctCreationTime()==null) || 
             (this.dePpAcctCreationTime!=null &&
              this.dePpAcctCreationTime.equals(other.getDePpAcctCreationTime()))) &&
            ((this.dePpReferenceNo==null && other.getDePpReferenceNo()==null) || 
             (this.dePpReferenceNo!=null &&
              this.dePpReferenceNo.equals(other.getDePpReferenceNo()))) &&
            ((this.dePpCurrencyCode==null && other.getDePpCurrencyCode()==null) || 
             (this.dePpCurrencyCode!=null &&
              this.dePpCurrencyCode.equals(other.getDePpCurrencyCode()))) &&
            ((this.dePpAccountType==null && other.getDePpAccountType()==null) || 
             (this.dePpAccountType!=null &&
              this.dePpAccountType.equals(other.getDePpAccountType()))) &&
            ((this.dePpName==null && other.getDePpName()==null) || 
             (this.dePpName!=null &&
              this.dePpName.equals(other.getDePpName()))) &&
            ((this.deMiddleName==null && other.getDeMiddleName()==null) || 
             (this.deMiddleName!=null &&
              this.deMiddleName.equals(other.getDeMiddleName()))) &&
            ((this.dePpName2==null && other.getDePpName2()==null) || 
             (this.dePpName2!=null &&
              this.dePpName2.equals(other.getDePpName2()))) &&
            ((this.dePpAddress1==null && other.getDePpAddress1()==null) || 
             (this.dePpAddress1!=null &&
              this.dePpAddress1.equals(other.getDePpAddress1()))) &&
            ((this.dePpAddress2==null && other.getDePpAddress2()==null) || 
             (this.dePpAddress2!=null &&
              this.dePpAddress2.equals(other.getDePpAddress2()))) &&
            ((this.dePpState==null && other.getDePpState()==null) || 
             (this.dePpState!=null &&
              this.dePpState.equals(other.getDePpState()))) &&
            ((this.dePpCity==null && other.getDePpCity()==null) || 
             (this.dePpCity!=null &&
              this.dePpCity.equals(other.getDePpCity()))) &&
            ((this.dePpGovtIdType==null && other.getDePpGovtIdType()==null) || 
             (this.dePpGovtIdType!=null &&
              this.dePpGovtIdType.equals(other.getDePpGovtIdType()))) &&
            ((this.dePpGovtIdIssueState==null && other.getDePpGovtIdIssueState()==null) || 
             (this.dePpGovtIdIssueState!=null &&
              this.dePpGovtIdIssueState.equals(other.getDePpGovtIdIssueState()))) &&
            ((this.dePpPostalCode==null && other.getDePpPostalCode()==null) || 
             (this.dePpPostalCode!=null &&
              this.dePpPostalCode.equals(other.getDePpPostalCode()))) &&
            ((this.dePpCountryOfIssue==null && other.getDePpCountryOfIssue()==null) || 
             (this.dePpCountryOfIssue!=null &&
              this.dePpCountryOfIssue.equals(other.getDePpCountryOfIssue()))) &&
            ((this.dePpEmailId==null && other.getDePpEmailId()==null) || 
             (this.dePpEmailId!=null &&
              this.dePpEmailId.equals(other.getDePpEmailId()))) &&
            ((this.dePpGovtId==null && other.getDePpGovtId()==null) || 
             (this.dePpGovtId!=null &&
              this.dePpGovtId.equals(other.getDePpGovtId()))) &&
            ((this.dePpPhoneNumber==null && other.getDePpPhoneNumber()==null) || 
             (this.dePpPhoneNumber!=null &&
              this.dePpPhoneNumber.equals(other.getDePpPhoneNumber()))) &&
            ((this.dePpComment==null && other.getDePpComment()==null) || 
             (this.dePpComment!=null &&
              this.dePpComment.equals(other.getDePpComment()))) &&
            ((this.dePpDateOfBirth==null && other.getDePpDateOfBirth()==null) || 
             (this.dePpDateOfBirth!=null &&
              this.dePpDateOfBirth.equals(other.getDePpDateOfBirth()))) &&
            ((this.deBuacctProductId==null && other.getDeBuacctProductId()==null) || 
             (this.deBuacctProductId!=null &&
              this.deBuacctProductId.equals(other.getDeBuacctProductId()))) &&
            ((this.dePpWorkPhoneNumber==null && other.getDePpWorkPhoneNumber()==null) || 
             (this.dePpWorkPhoneNumber!=null &&
              this.dePpWorkPhoneNumber.equals(other.getDePpWorkPhoneNumber()))) &&
            ((this.dePpMobilePhoneNumber==null && other.getDePpMobilePhoneNumber()==null) || 
             (this.dePpMobilePhoneNumber!=null &&
              this.dePpMobilePhoneNumber.equals(other.getDePpMobilePhoneNumber()))) &&
            ((this.dePpOtherPhoneNumber==null && other.getDePpOtherPhoneNumber()==null) || 
             (this.dePpOtherPhoneNumber!=null &&
              this.dePpOtherPhoneNumber.equals(other.getDePpOtherPhoneNumber()))) &&
            ((this.dePpFollowUpDate==null && other.getDePpFollowUpDate()==null) || 
             (this.dePpFollowUpDate!=null &&
              this.dePpFollowUpDate.equals(other.getDePpFollowUpDate()))) &&
            ((this.dePpEmployername==null && other.getDePpEmployername()==null) || 
             (this.dePpEmployername!=null &&
              this.dePpEmployername.equals(other.getDePpEmployername()))) &&
            ((this.dePpEmployerContactName==null && other.getDePpEmployerContactName()==null) || 
             (this.dePpEmployerContactName!=null &&
              this.dePpEmployerContactName.equals(other.getDePpEmployerContactName()))) &&
            ((this.dePpEmployerContactPhoneNumber==null && other.getDePpEmployerContactPhoneNumber()==null) || 
             (this.dePpEmployerContactPhoneNumber!=null &&
              this.dePpEmployerContactPhoneNumber.equals(other.getDePpEmployerContactPhoneNumber()))) &&
            ((this.dePpEmployerContactFaxNumber==null && other.getDePpEmployerContactFaxNumber()==null) || 
             (this.dePpEmployerContactFaxNumber!=null &&
              this.dePpEmployerContactFaxNumber.equals(other.getDePpEmployerContactFaxNumber()))) &&
            ((this.dePpMemos==null && other.getDePpMemos()==null) || 
             (this.dePpMemos!=null &&
              this.dePpMemos.equals(other.getDePpMemos()))) &&
            ((this.dePpCustRefName==null && other.getDePpCustRefName()==null) || 
             (this.dePpCustRefName!=null &&
              this.dePpCustRefName.equals(other.getDePpCustRefName()))) &&
            ((this.deSvcStoreName==null && other.getDeSvcStoreName()==null) || 
             (this.deSvcStoreName!=null &&
              this.deSvcStoreName.equals(other.getDeSvcStoreName()))) &&
            ((this.dePpGovtIdIssueDate==null && other.getDePpGovtIdIssueDate()==null) || 
             (this.dePpGovtIdIssueDate!=null &&
              this.dePpGovtIdIssueDate.equals(other.getDePpGovtIdIssueDate()))) &&
            ((this.dePpGovtIdExpirationDate==null && other.getDePpGovtIdExpirationDate()==null) || 
             (this.dePpGovtIdExpirationDate!=null &&
              this.dePpGovtIdExpirationDate.equals(other.getDePpGovtIdExpirationDate()))) &&
            ((this.dePpGovtIdCountryofIssuance==null && other.getDePpGovtIdCountryofIssuance()==null) || 
             (this.dePpGovtIdCountryofIssuance!=null &&
              this.dePpGovtIdCountryofIssuance.equals(other.getDePpGovtIdCountryofIssuance()))) &&
            ((this.dePpcipType==null && other.getDePpcipType()==null) || 
             (this.dePpcipType!=null &&
              this.dePpcipType.equals(other.getDePpcipType()))) &&
            ((this.dePpcipNumber==null && other.getDePpcipNumber()==null) || 
             (this.dePpcipNumber!=null &&
              this.dePpcipNumber.equals(other.getDePpcipNumber()))) &&
            ((this.dePpcipStatus==null && other.getDePpcipStatus()==null) || 
             (this.dePpcipStatus!=null &&
              this.dePpcipStatus.equals(other.getDePpcipStatus()))) &&
            ((this.dePpssn==null && other.getDePpssn()==null) || 
             (this.dePpssn!=null &&
              this.dePpssn.equals(other.getDePpssn()))) &&
            ((this.deIvrEmbossingLine4==null && other.getDeIvrEmbossingLine4()==null) || 
             (this.deIvrEmbossingLine4!=null &&
              this.deIvrEmbossingLine4.equals(other.getDeIvrEmbossingLine4()))) &&
            ((this.deIvrEmbossingHotStamp==null && other.getDeIvrEmbossingHotStamp()==null) || 
             (this.deIvrEmbossingHotStamp!=null &&
              this.deIvrEmbossingHotStamp.equals(other.getDeIvrEmbossingHotStamp()))) &&
            ((this.dePpSecondaryPan==null && other.getDePpSecondaryPan()==null) || 
             (this.dePpSecondaryPan!=null &&
              this.dePpSecondaryPan.equals(other.getDePpSecondaryPan())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDePpAcctCreationDate() != null) {
            _hashCode += getDePpAcctCreationDate().hashCode();
        }
        if (getDePpAcctCreationTime() != null) {
            _hashCode += getDePpAcctCreationTime().hashCode();
        }
        if (getDePpReferenceNo() != null) {
            _hashCode += getDePpReferenceNo().hashCode();
        }
        if (getDePpCurrencyCode() != null) {
            _hashCode += getDePpCurrencyCode().hashCode();
        }
        if (getDePpAccountType() != null) {
            _hashCode += getDePpAccountType().hashCode();
        }
        if (getDePpName() != null) {
            _hashCode += getDePpName().hashCode();
        }
        if (getDeMiddleName() != null) {
            _hashCode += getDeMiddleName().hashCode();
        }
        if (getDePpName2() != null) {
            _hashCode += getDePpName2().hashCode();
        }
        if (getDePpAddress1() != null) {
            _hashCode += getDePpAddress1().hashCode();
        }
        if (getDePpAddress2() != null) {
            _hashCode += getDePpAddress2().hashCode();
        }
        if (getDePpState() != null) {
            _hashCode += getDePpState().hashCode();
        }
        if (getDePpCity() != null) {
            _hashCode += getDePpCity().hashCode();
        }
        if (getDePpGovtIdType() != null) {
            _hashCode += getDePpGovtIdType().hashCode();
        }
        if (getDePpGovtIdIssueState() != null) {
            _hashCode += getDePpGovtIdIssueState().hashCode();
        }
        if (getDePpPostalCode() != null) {
            _hashCode += getDePpPostalCode().hashCode();
        }
        if (getDePpCountryOfIssue() != null) {
            _hashCode += getDePpCountryOfIssue().hashCode();
        }
        if (getDePpEmailId() != null) {
            _hashCode += getDePpEmailId().hashCode();
        }
        if (getDePpGovtId() != null) {
            _hashCode += getDePpGovtId().hashCode();
        }
        if (getDePpPhoneNumber() != null) {
            _hashCode += getDePpPhoneNumber().hashCode();
        }
        if (getDePpComment() != null) {
            _hashCode += getDePpComment().hashCode();
        }
        if (getDePpDateOfBirth() != null) {
            _hashCode += getDePpDateOfBirth().hashCode();
        }
        if (getDeBuacctProductId() != null) {
            _hashCode += getDeBuacctProductId().hashCode();
        }
        if (getDePpWorkPhoneNumber() != null) {
            _hashCode += getDePpWorkPhoneNumber().hashCode();
        }
        if (getDePpMobilePhoneNumber() != null) {
            _hashCode += getDePpMobilePhoneNumber().hashCode();
        }
        if (getDePpOtherPhoneNumber() != null) {
            _hashCode += getDePpOtherPhoneNumber().hashCode();
        }
        if (getDePpFollowUpDate() != null) {
            _hashCode += getDePpFollowUpDate().hashCode();
        }
        if (getDePpEmployername() != null) {
            _hashCode += getDePpEmployername().hashCode();
        }
        if (getDePpEmployerContactName() != null) {
            _hashCode += getDePpEmployerContactName().hashCode();
        }
        if (getDePpEmployerContactPhoneNumber() != null) {
            _hashCode += getDePpEmployerContactPhoneNumber().hashCode();
        }
        if (getDePpEmployerContactFaxNumber() != null) {
            _hashCode += getDePpEmployerContactFaxNumber().hashCode();
        }
        if (getDePpMemos() != null) {
            _hashCode += getDePpMemos().hashCode();
        }
        if (getDePpCustRefName() != null) {
            _hashCode += getDePpCustRefName().hashCode();
        }
        if (getDeSvcStoreName() != null) {
            _hashCode += getDeSvcStoreName().hashCode();
        }
        if (getDePpGovtIdIssueDate() != null) {
            _hashCode += getDePpGovtIdIssueDate().hashCode();
        }
        if (getDePpGovtIdExpirationDate() != null) {
            _hashCode += getDePpGovtIdExpirationDate().hashCode();
        }
        if (getDePpGovtIdCountryofIssuance() != null) {
            _hashCode += getDePpGovtIdCountryofIssuance().hashCode();
        }
        if (getDePpcipType() != null) {
            _hashCode += getDePpcipType().hashCode();
        }
        if (getDePpcipNumber() != null) {
            _hashCode += getDePpcipNumber().hashCode();
        }
        if (getDePpcipStatus() != null) {
            _hashCode += getDePpcipStatus().hashCode();
        }
        if (getDePpssn() != null) {
            _hashCode += getDePpssn().hashCode();
        }
        if (getDeIvrEmbossingLine4() != null) {
            _hashCode += getDeIvrEmbossingLine4().hashCode();
        }
        if (getDeIvrEmbossingHotStamp() != null) {
            _hashCode += getDeIvrEmbossingHotStamp().hashCode();
        }
        if (getDePpSecondaryPan() != null) {
            _hashCode += getDePpSecondaryPan().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateCardRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CreateCardRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAcctCreationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAcctCreationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAcctCreationTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAcctCreationTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpReferenceNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpReferenceNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAccountType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAccountType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deMiddleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeMiddleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpName2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpName2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAddress1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAddress1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpAddress2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpAddress2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtIdType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtIdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtIdIssueState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtIdIssueState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpPostalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpPostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpCountryOfIssue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpCountryOfIssue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpEmailId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpEmailId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpComment");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpComment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpDateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpDateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deBuacctProductId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeBuacctProductId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpWorkPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpWorkPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpMobilePhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpMobilePhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpOtherPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpOtherPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpFollowUpDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpFollowUpDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpEmployername");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpEmployername"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpEmployerContactName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpEmployerContactName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpEmployerContactPhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpEmployerContactPhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpEmployerContactFaxNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpEmployerContactFaxNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpMemos");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpMemos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpCustRefName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpCustRefName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deSvcStoreName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeSvcStoreName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtIdIssueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtIdIssueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtIdExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtIdExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpGovtIdCountryofIssuance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpGovtIdCountryofIssuance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpcipType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpcipType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpcipNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpcipNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpcipStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpcipStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpssn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpssn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deIvrEmbossingLine4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeIvrEmbossingLine4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deIvrEmbossingHotStamp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeIvrEmbossingHotStamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dePpSecondaryPan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DePpSecondaryPan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
