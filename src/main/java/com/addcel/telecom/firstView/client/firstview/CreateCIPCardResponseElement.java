//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CreateCIPCardResult" type="{http://tempuri.org/}CreateCIPCardResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "createCIPCardResult"
})
@XmlRootElement(name = "CreateCIPCardResponseElement")
public class CreateCIPCardResponseElement {

    @XmlElement(name = "CreateCIPCardResult")
    protected CreateCIPCardResponse createCIPCardResult;

    /**
     * Obtiene el valor de la propiedad createCIPCardResult.
     * 
     * @return
     *     possible object is
     *     {@link CreateCIPCardResponse }
     *     
     */
    public CreateCIPCardResponse getCreateCIPCardResult() {
        return createCIPCardResult;
    }

    /**
     * Define el valor de la propiedad createCIPCardResult.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateCIPCardResponse }
     *     
     */
    public void setCreateCIPCardResult(CreateCIPCardResponse value) {
        this.createCIPCardResult = value;
    }

}
