/**
 * SMSEnrollmentResponseType9.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SMSEnrollmentResponseType9  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMS_ENROLLMENT_SETTINGS_RESPONSE;

    public SMSEnrollmentResponseType9() {
    }

    public SMSEnrollmentResponseType9(
           com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMS_ENROLLMENT_SETTINGS_RESPONSE) {
           this.SMS_ENROLLMENT_SETTINGS_RESPONSE = SMS_ENROLLMENT_SETTINGS_RESPONSE;
    }


    /**
     * Gets the SMS_ENROLLMENT_SETTINGS_RESPONSE value for this SMSEnrollmentResponseType9.
     * 
     * @return SMS_ENROLLMENT_SETTINGS_RESPONSE
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse getSMS_ENROLLMENT_SETTINGS_RESPONSE() {
        return SMS_ENROLLMENT_SETTINGS_RESPONSE;
    }


    /**
     * Sets the SMS_ENROLLMENT_SETTINGS_RESPONSE value for this SMSEnrollmentResponseType9.
     * 
     * @param SMS_ENROLLMENT_SETTINGS_RESPONSE
     */
    public void setSMS_ENROLLMENT_SETTINGS_RESPONSE(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMS_ENROLLMENT_SETTINGS_RESPONSE) {
        this.SMS_ENROLLMENT_SETTINGS_RESPONSE = SMS_ENROLLMENT_SETTINGS_RESPONSE;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSEnrollmentResponseType9)) return false;
        SMSEnrollmentResponseType9 other = (SMSEnrollmentResponseType9) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SMS_ENROLLMENT_SETTINGS_RESPONSE==null && other.getSMS_ENROLLMENT_SETTINGS_RESPONSE()==null) || 
             (this.SMS_ENROLLMENT_SETTINGS_RESPONSE!=null &&
              this.SMS_ENROLLMENT_SETTINGS_RESPONSE.equals(other.getSMS_ENROLLMENT_SETTINGS_RESPONSE())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSMS_ENROLLMENT_SETTINGS_RESPONSE() != null) {
            _hashCode += getSMS_ENROLLMENT_SETTINGS_RESPONSE().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSEnrollmentResponseType9.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSEnrollmentResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_ENROLLMENT_SETTINGS_RESPONSE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ENROLLMENT_SETTINGS_RESPONSE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentResponse"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
