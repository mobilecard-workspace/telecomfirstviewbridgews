/**
 * CreateCardResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class CreateCardResponse  implements java.io.Serializable {
    private java.lang.String requestType;

    private java.lang.String productType;

    private java.lang.String subProductType;

    private java.lang.String fromCardRange;

    private java.lang.String toCardRange;

    private java.lang.String accountNumber;

    private java.lang.String ddaNo;

    private java.lang.String pan;

    private java.lang.String secondaryPan;

    private java.lang.String accountCreationDate;

    private java.lang.String accountCreationTime;

    private java.lang.String amount;

    private java.lang.String referenceNo;

    private java.lang.String expirationDate;

    private java.lang.String currencyCode;

    private java.lang.String merchantGroup;

    private java.lang.String mcc;

    private java.lang.String governmentIdType;

    private java.lang.String governmentId;

    private java.lang.String terminalId;

    private java.lang.String storeName;

    private java.lang.String responseCode;

    private java.lang.String reasonDescription;

    private java.lang.String deAct;

    public CreateCardResponse() {
    }

    public CreateCardResponse(
           java.lang.String requestType,
           java.lang.String productType,
           java.lang.String subProductType,
           java.lang.String fromCardRange,
           java.lang.String toCardRange,
           java.lang.String accountNumber,
           java.lang.String ddaNo,
           java.lang.String pan,
           java.lang.String secondaryPan,
           java.lang.String accountCreationDate,
           java.lang.String accountCreationTime,
           java.lang.String amount,
           java.lang.String referenceNo,
           java.lang.String expirationDate,
           java.lang.String currencyCode,
           java.lang.String merchantGroup,
           java.lang.String mcc,
           java.lang.String governmentIdType,
           java.lang.String governmentId,
           java.lang.String terminalId,
           java.lang.String storeName,
           java.lang.String responseCode,
           java.lang.String reasonDescription,
           java.lang.String deAct) {
           this.requestType = requestType;
           this.productType = productType;
           this.subProductType = subProductType;
           this.fromCardRange = fromCardRange;
           this.toCardRange = toCardRange;
           this.accountNumber = accountNumber;
           this.ddaNo = ddaNo;
           this.pan = pan;
           this.secondaryPan = secondaryPan;
           this.accountCreationDate = accountCreationDate;
           this.accountCreationTime = accountCreationTime;
           this.amount = amount;
           this.referenceNo = referenceNo;
           this.expirationDate = expirationDate;
           this.currencyCode = currencyCode;
           this.merchantGroup = merchantGroup;
           this.mcc = mcc;
           this.governmentIdType = governmentIdType;
           this.governmentId = governmentId;
           this.terminalId = terminalId;
           this.storeName = storeName;
           this.responseCode = responseCode;
           this.reasonDescription = reasonDescription;
           this.deAct = deAct;
    }


    /**
     * Gets the requestType value for this CreateCardResponse.
     * 
     * @return requestType
     */
    public java.lang.String getRequestType() {
        return requestType;
    }


    /**
     * Sets the requestType value for this CreateCardResponse.
     * 
     * @param requestType
     */
    public void setRequestType(java.lang.String requestType) {
        this.requestType = requestType;
    }


    /**
     * Gets the productType value for this CreateCardResponse.
     * 
     * @return productType
     */
    public java.lang.String getProductType() {
        return productType;
    }


    /**
     * Sets the productType value for this CreateCardResponse.
     * 
     * @param productType
     */
    public void setProductType(java.lang.String productType) {
        this.productType = productType;
    }


    /**
     * Gets the subProductType value for this CreateCardResponse.
     * 
     * @return subProductType
     */
    public java.lang.String getSubProductType() {
        return subProductType;
    }


    /**
     * Sets the subProductType value for this CreateCardResponse.
     * 
     * @param subProductType
     */
    public void setSubProductType(java.lang.String subProductType) {
        this.subProductType = subProductType;
    }


    /**
     * Gets the fromCardRange value for this CreateCardResponse.
     * 
     * @return fromCardRange
     */
    public java.lang.String getFromCardRange() {
        return fromCardRange;
    }


    /**
     * Sets the fromCardRange value for this CreateCardResponse.
     * 
     * @param fromCardRange
     */
    public void setFromCardRange(java.lang.String fromCardRange) {
        this.fromCardRange = fromCardRange;
    }


    /**
     * Gets the toCardRange value for this CreateCardResponse.
     * 
     * @return toCardRange
     */
    public java.lang.String getToCardRange() {
        return toCardRange;
    }


    /**
     * Sets the toCardRange value for this CreateCardResponse.
     * 
     * @param toCardRange
     */
    public void setToCardRange(java.lang.String toCardRange) {
        this.toCardRange = toCardRange;
    }


    /**
     * Gets the accountNumber value for this CreateCardResponse.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this CreateCardResponse.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the ddaNo value for this CreateCardResponse.
     * 
     * @return ddaNo
     */
    public java.lang.String getDdaNo() {
        return ddaNo;
    }


    /**
     * Sets the ddaNo value for this CreateCardResponse.
     * 
     * @param ddaNo
     */
    public void setDdaNo(java.lang.String ddaNo) {
        this.ddaNo = ddaNo;
    }


    /**
     * Gets the pan value for this CreateCardResponse.
     * 
     * @return pan
     */
    public java.lang.String getPan() {
        return pan;
    }


    /**
     * Sets the pan value for this CreateCardResponse.
     * 
     * @param pan
     */
    public void setPan(java.lang.String pan) {
        this.pan = pan;
    }


    /**
     * Gets the secondaryPan value for this CreateCardResponse.
     * 
     * @return secondaryPan
     */
    public java.lang.String getSecondaryPan() {
        return secondaryPan;
    }


    /**
     * Sets the secondaryPan value for this CreateCardResponse.
     * 
     * @param secondaryPan
     */
    public void setSecondaryPan(java.lang.String secondaryPan) {
        this.secondaryPan = secondaryPan;
    }


    /**
     * Gets the accountCreationDate value for this CreateCardResponse.
     * 
     * @return accountCreationDate
     */
    public java.lang.String getAccountCreationDate() {
        return accountCreationDate;
    }


    /**
     * Sets the accountCreationDate value for this CreateCardResponse.
     * 
     * @param accountCreationDate
     */
    public void setAccountCreationDate(java.lang.String accountCreationDate) {
        this.accountCreationDate = accountCreationDate;
    }


    /**
     * Gets the accountCreationTime value for this CreateCardResponse.
     * 
     * @return accountCreationTime
     */
    public java.lang.String getAccountCreationTime() {
        return accountCreationTime;
    }


    /**
     * Sets the accountCreationTime value for this CreateCardResponse.
     * 
     * @param accountCreationTime
     */
    public void setAccountCreationTime(java.lang.String accountCreationTime) {
        this.accountCreationTime = accountCreationTime;
    }


    /**
     * Gets the amount value for this CreateCardResponse.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this CreateCardResponse.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the referenceNo value for this CreateCardResponse.
     * 
     * @return referenceNo
     */
    public java.lang.String getReferenceNo() {
        return referenceNo;
    }


    /**
     * Sets the referenceNo value for this CreateCardResponse.
     * 
     * @param referenceNo
     */
    public void setReferenceNo(java.lang.String referenceNo) {
        this.referenceNo = referenceNo;
    }


    /**
     * Gets the expirationDate value for this CreateCardResponse.
     * 
     * @return expirationDate
     */
    public java.lang.String getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this CreateCardResponse.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.String expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the currencyCode value for this CreateCardResponse.
     * 
     * @return currencyCode
     */
    public java.lang.String getCurrencyCode() {
        return currencyCode;
    }


    /**
     * Sets the currencyCode value for this CreateCardResponse.
     * 
     * @param currencyCode
     */
    public void setCurrencyCode(java.lang.String currencyCode) {
        this.currencyCode = currencyCode;
    }


    /**
     * Gets the merchantGroup value for this CreateCardResponse.
     * 
     * @return merchantGroup
     */
    public java.lang.String getMerchantGroup() {
        return merchantGroup;
    }


    /**
     * Sets the merchantGroup value for this CreateCardResponse.
     * 
     * @param merchantGroup
     */
    public void setMerchantGroup(java.lang.String merchantGroup) {
        this.merchantGroup = merchantGroup;
    }


    /**
     * Gets the mcc value for this CreateCardResponse.
     * 
     * @return mcc
     */
    public java.lang.String getMcc() {
        return mcc;
    }


    /**
     * Sets the mcc value for this CreateCardResponse.
     * 
     * @param mcc
     */
    public void setMcc(java.lang.String mcc) {
        this.mcc = mcc;
    }


    /**
     * Gets the governmentIdType value for this CreateCardResponse.
     * 
     * @return governmentIdType
     */
    public java.lang.String getGovernmentIdType() {
        return governmentIdType;
    }


    /**
     * Sets the governmentIdType value for this CreateCardResponse.
     * 
     * @param governmentIdType
     */
    public void setGovernmentIdType(java.lang.String governmentIdType) {
        this.governmentIdType = governmentIdType;
    }


    /**
     * Gets the governmentId value for this CreateCardResponse.
     * 
     * @return governmentId
     */
    public java.lang.String getGovernmentId() {
        return governmentId;
    }


    /**
     * Sets the governmentId value for this CreateCardResponse.
     * 
     * @param governmentId
     */
    public void setGovernmentId(java.lang.String governmentId) {
        this.governmentId = governmentId;
    }


    /**
     * Gets the terminalId value for this CreateCardResponse.
     * 
     * @return terminalId
     */
    public java.lang.String getTerminalId() {
        return terminalId;
    }


    /**
     * Sets the terminalId value for this CreateCardResponse.
     * 
     * @param terminalId
     */
    public void setTerminalId(java.lang.String terminalId) {
        this.terminalId = terminalId;
    }


    /**
     * Gets the storeName value for this CreateCardResponse.
     * 
     * @return storeName
     */
    public java.lang.String getStoreName() {
        return storeName;
    }


    /**
     * Sets the storeName value for this CreateCardResponse.
     * 
     * @param storeName
     */
    public void setStoreName(java.lang.String storeName) {
        this.storeName = storeName;
    }


    /**
     * Gets the responseCode value for this CreateCardResponse.
     * 
     * @return responseCode
     */
    public java.lang.String getResponseCode() {
        return responseCode;
    }


    /**
     * Sets the responseCode value for this CreateCardResponse.
     * 
     * @param responseCode
     */
    public void setResponseCode(java.lang.String responseCode) {
        this.responseCode = responseCode;
    }


    /**
     * Gets the reasonDescription value for this CreateCardResponse.
     * 
     * @return reasonDescription
     */
    public java.lang.String getReasonDescription() {
        return reasonDescription;
    }


    /**
     * Sets the reasonDescription value for this CreateCardResponse.
     * 
     * @param reasonDescription
     */
    public void setReasonDescription(java.lang.String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }


    /**
     * Gets the deAct value for this CreateCardResponse.
     * 
     * @return deAct
     */
    public java.lang.String getDeAct() {
        return deAct;
    }


    /**
     * Sets the deAct value for this CreateCardResponse.
     * 
     * @param deAct
     */
    public void setDeAct(java.lang.String deAct) {
        this.deAct = deAct;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateCardResponse)) return false;
        CreateCardResponse other = (CreateCardResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestType==null && other.getRequestType()==null) || 
             (this.requestType!=null &&
              this.requestType.equals(other.getRequestType()))) &&
            ((this.productType==null && other.getProductType()==null) || 
             (this.productType!=null &&
              this.productType.equals(other.getProductType()))) &&
            ((this.subProductType==null && other.getSubProductType()==null) || 
             (this.subProductType!=null &&
              this.subProductType.equals(other.getSubProductType()))) &&
            ((this.fromCardRange==null && other.getFromCardRange()==null) || 
             (this.fromCardRange!=null &&
              this.fromCardRange.equals(other.getFromCardRange()))) &&
            ((this.toCardRange==null && other.getToCardRange()==null) || 
             (this.toCardRange!=null &&
              this.toCardRange.equals(other.getToCardRange()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.ddaNo==null && other.getDdaNo()==null) || 
             (this.ddaNo!=null &&
              this.ddaNo.equals(other.getDdaNo()))) &&
            ((this.pan==null && other.getPan()==null) || 
             (this.pan!=null &&
              this.pan.equals(other.getPan()))) &&
            ((this.secondaryPan==null && other.getSecondaryPan()==null) || 
             (this.secondaryPan!=null &&
              this.secondaryPan.equals(other.getSecondaryPan()))) &&
            ((this.accountCreationDate==null && other.getAccountCreationDate()==null) || 
             (this.accountCreationDate!=null &&
              this.accountCreationDate.equals(other.getAccountCreationDate()))) &&
            ((this.accountCreationTime==null && other.getAccountCreationTime()==null) || 
             (this.accountCreationTime!=null &&
              this.accountCreationTime.equals(other.getAccountCreationTime()))) &&
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.referenceNo==null && other.getReferenceNo()==null) || 
             (this.referenceNo!=null &&
              this.referenceNo.equals(other.getReferenceNo()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.currencyCode==null && other.getCurrencyCode()==null) || 
             (this.currencyCode!=null &&
              this.currencyCode.equals(other.getCurrencyCode()))) &&
            ((this.merchantGroup==null && other.getMerchantGroup()==null) || 
             (this.merchantGroup!=null &&
              this.merchantGroup.equals(other.getMerchantGroup()))) &&
            ((this.mcc==null && other.getMcc()==null) || 
             (this.mcc!=null &&
              this.mcc.equals(other.getMcc()))) &&
            ((this.governmentIdType==null && other.getGovernmentIdType()==null) || 
             (this.governmentIdType!=null &&
              this.governmentIdType.equals(other.getGovernmentIdType()))) &&
            ((this.governmentId==null && other.getGovernmentId()==null) || 
             (this.governmentId!=null &&
              this.governmentId.equals(other.getGovernmentId()))) &&
            ((this.terminalId==null && other.getTerminalId()==null) || 
             (this.terminalId!=null &&
              this.terminalId.equals(other.getTerminalId()))) &&
            ((this.storeName==null && other.getStoreName()==null) || 
             (this.storeName!=null &&
              this.storeName.equals(other.getStoreName()))) &&
            ((this.responseCode==null && other.getResponseCode()==null) || 
             (this.responseCode!=null &&
              this.responseCode.equals(other.getResponseCode()))) &&
            ((this.reasonDescription==null && other.getReasonDescription()==null) || 
             (this.reasonDescription!=null &&
              this.reasonDescription.equals(other.getReasonDescription()))) &&
            ((this.deAct==null && other.getDeAct()==null) || 
             (this.deAct!=null &&
              this.deAct.equals(other.getDeAct())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestType() != null) {
            _hashCode += getRequestType().hashCode();
        }
        if (getProductType() != null) {
            _hashCode += getProductType().hashCode();
        }
        if (getSubProductType() != null) {
            _hashCode += getSubProductType().hashCode();
        }
        if (getFromCardRange() != null) {
            _hashCode += getFromCardRange().hashCode();
        }
        if (getToCardRange() != null) {
            _hashCode += getToCardRange().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getDdaNo() != null) {
            _hashCode += getDdaNo().hashCode();
        }
        if (getPan() != null) {
            _hashCode += getPan().hashCode();
        }
        if (getSecondaryPan() != null) {
            _hashCode += getSecondaryPan().hashCode();
        }
        if (getAccountCreationDate() != null) {
            _hashCode += getAccountCreationDate().hashCode();
        }
        if (getAccountCreationTime() != null) {
            _hashCode += getAccountCreationTime().hashCode();
        }
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getReferenceNo() != null) {
            _hashCode += getReferenceNo().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getCurrencyCode() != null) {
            _hashCode += getCurrencyCode().hashCode();
        }
        if (getMerchantGroup() != null) {
            _hashCode += getMerchantGroup().hashCode();
        }
        if (getMcc() != null) {
            _hashCode += getMcc().hashCode();
        }
        if (getGovernmentIdType() != null) {
            _hashCode += getGovernmentIdType().hashCode();
        }
        if (getGovernmentId() != null) {
            _hashCode += getGovernmentId().hashCode();
        }
        if (getTerminalId() != null) {
            _hashCode += getTerminalId().hashCode();
        }
        if (getStoreName() != null) {
            _hashCode += getStoreName().hashCode();
        }
        if (getResponseCode() != null) {
            _hashCode += getResponseCode().hashCode();
        }
        if (getReasonDescription() != null) {
            _hashCode += getReasonDescription().hashCode();
        }
        if (getDeAct() != null) {
            _hashCode += getDeAct().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateCardResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CreateCardResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "RequestType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ProductType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subProductType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SubProductType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromCardRange");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FromCardRange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("toCardRange");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ToCardRange"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ddaNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DdaNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Pan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("secondaryPan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SecondaryPan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCreationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountCreationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountCreationTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "AccountCreationTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referenceNo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ReferenceNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "MerchantGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mcc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Mcc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("governmentIdType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GovernmentIdType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("governmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GovernmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("terminalId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "TerminalId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("storeName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "StoreName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ResponseCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reasonDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ReasonDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deAct");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DeAct"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
