//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para GetStatusResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GetStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DePpStatusDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DePpResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetStatusResponse", propOrder = {
    "dePpStatusDescription",
    "dePpResponseCode"
})
public class GetStatusResponse {

    @XmlElement(name = "DePpStatusDescription")
    protected String dePpStatusDescription;
    @XmlElement(name = "DePpResponseCode")
    protected String dePpResponseCode;

    /**
     * Obtiene el valor de la propiedad dePpStatusDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpStatusDescription() {
        return dePpStatusDescription;
    }

    /**
     * Define el valor de la propiedad dePpStatusDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpStatusDescription(String value) {
        this.dePpStatusDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad dePpResponseCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDePpResponseCode() {
        return dePpResponseCode;
    }

    /**
     * Define el valor de la propiedad dePpResponseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDePpResponseCode(String value) {
        this.dePpResponseCode = value;
    }

}
