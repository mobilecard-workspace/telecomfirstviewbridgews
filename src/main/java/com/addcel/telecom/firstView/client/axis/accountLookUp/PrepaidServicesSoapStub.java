/**
 * PrepaidServicesSoapStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PrepaidServicesSoapStub extends org.apache.axis.client.Stub implements com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[28];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardActivationInquiry");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiry"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiryRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiryResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiry_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("RequestCustomerServiceHelp");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUESTCUSTOMERSERVICEHELP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelpRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelpResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REQUESTCUSTOMERSERVICEHELP_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Login");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOGIN_REQUEST"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoginRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoginRequestResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOGIN_REQUEST_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ResetPassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPassword"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPasswordRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPasswordResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPassword_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardToCardTransfer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSFERCARD"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransferRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransferResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSFERCARD_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("Clearing");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClrRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClrResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("LoadsBasedOnHoldingAccount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccount_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccountRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccountResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccount_Response"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CheckBalance");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHECKBALANCE"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CheckBalanceRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CheckBalanceResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHECKBALANCE_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardActive");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDACTIVE"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActiveRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActiveResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDACTIVE_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SavingAccountsFundsTransfer");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransfer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransferRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransferResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransferResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangePassword");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGE_PASSWORD"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePasswordRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePasswordResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGE_PASSWORD_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SavingAccountsInquiry");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiry"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryResponseResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ReportStolenCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARD"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCardRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCardResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARD_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SMSSetup");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTSMSSETUP"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSSetupRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSSetupResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTSMSSETUP_RESPONSE"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GeneratePIN");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePIN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePINRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePINResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePIN_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardActivation3");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3Request"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Request.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3Response"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("TransactionHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_HISTORY"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_HISTORY_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardUpdate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDUPDATE"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDUPDATE_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ValidateCard");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "VALIDATE_CARD"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCardRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCardResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "VALIDATE_CARD_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("UpdateSelfserviceUserName");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserName"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserNameRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserNameResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserName_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("GetCVC");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVC"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVCRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVCResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVC_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangePin");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGEPIN"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePinRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePinResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGEPIN_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ChangeOverdraftStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGEOVERDRAFTSTATUS"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangeOverdraftStatusRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangeOverdraftStatusResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CHANGEOVERDRAFTSTATUS_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("CardholderDetail");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDHOLDER_DETAIL"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetailRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetailResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDHOLDER_DETAIL_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SMSAvailability");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTSMSAVAILABILITY"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailabilityRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailabilityResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTSMSAVAILABILITY_RESPONSE"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("ReportStolenCard2");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARDEXT"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Request"), com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Response"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARDEXT_RET"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("PromoWork");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWork_Request"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorks"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("SMSEnrollment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ENROLLMENT_SETTINGS"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentRequest"), com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest.class, false, false);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentResponse"));
        oper.setReturnClass(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ENROLLMENT_SETTINGS_RESPONSE"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

    }

    public PrepaidServicesSoapStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public PrepaidServicesSoapStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public PrepaidServicesSoapStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">PromoWork");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWork.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">PromoWorkResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponseType6.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">ReportStolenCard2");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">ReportStolenCard2Response");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2ResponseType22.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSAvailabilityResponse>MESSAGE");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponseMESSAGE.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSEnrollment");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollment.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSEnrollmentResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponseType9.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_LISTGrp");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardDetailArray");
            qName2 = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDDETAIL");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3Request");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3Response");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiryRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiryResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActiveRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActiveResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardDetailArray");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardDetailArray.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetailRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetailResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardPendingActivationArr");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransferRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransferResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangeOverdraftStatusRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangeOverdraftStatusResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePasswordRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePasswordResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePinRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePinResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CheckBalanceRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CheckBalanceResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FeeDetailType");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FeeListType");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.FeeDetailType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FeeDetailType");
            qName2 = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePINRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePINResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVCRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVCResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccountRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccountResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoginRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoginRequestResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Request");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Response");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCardRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCardResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelpRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelpResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPasswordRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPasswordResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransferRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransferResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryResponseResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailabilityRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailabilityResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSMessageSetupType");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSMessageSetupType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSSetupRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSSetupResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionListType");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionType");
            qName2 = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionType");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserNameRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserNameResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UserCredentials");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.UserCredentials.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCardRequest");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCardResponse");
            cachedSerQNames.add(qName);
            cls = com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse cardActivationInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryRequest cardActivationInquiry) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardActivationInquiry");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiry"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cardActivationInquiry});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse requestCustomerServiceHelp(com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpRequest REQUESTCUSTOMERSERVICEHELP) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/RequestCustomerServiceHelp");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelp"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {REQUESTCUSTOMERSERVICEHELP});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse login(com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequest LOGIN_REQUEST) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/Login");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Login"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {LOGIN_REQUEST});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse resetPassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordRequest resetPassword) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ResetPassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResetPassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {resetPassword});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse cardToCardTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferRequest TRANSFERCARD) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardToCardTransfer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransfer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {TRANSFERCARD});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse clearing(com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingRequest clrRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/Clearing");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Clearing"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {clrRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse loadsBasedOnHoldingAccount(com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountRequest loadsBasedOnHoldingAccount_Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/LoadsBasedOnHoldingAccount");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadsBasedOnHoldingAccount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {loadsBasedOnHoldingAccount_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse checkBalance(com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceRequest CHECKBALANCE) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CheckBalance");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CheckBalance"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CHECKBALANCE});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse cardActive(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveRequest CARDACTIVE) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardActive");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActive"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CARDACTIVE});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse savingAccountsFundsTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferRequest savingAccountsFundsTransfer) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/SavingAccountsFundsTransfer");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsFundsTransfer"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {savingAccountsFundsTransfer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse changePassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordRequest CHANGE_PASSWORD) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ChangePassword");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePassword"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CHANGE_PASSWORD});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse savingAccountsInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryRequest savingAccountsInquiry) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/SavingAccountsInquiry");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiry"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {savingAccountsInquiry});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse reportStolenCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardRequest REPORTSTOLENCARD) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ReportStolenCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {REPORTSTOLENCARD});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse SMSSetup(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupRequest ACCOUNTSMSSETUP) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/SMSSetup");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSSetup"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ACCOUNTSMSSETUP});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse generatePIN(com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINRequest generatePIN) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/GeneratePIN");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePIN"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {generatePIN});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response cardActivation3(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Request cardActivation3) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardActivation3");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivation3"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cardActivation3});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse transactionHistory(com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest TRANSACTION_HISTORY) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/TransactionHistory");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {TRANSACTION_HISTORY});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse cardUpdate(com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateRequest CARDUPDATE) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardUpdate");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CARDUPDATE});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse validateCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest VALIDATE_CARD) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ValidateCard");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ValidateCard"));
        _call.addHeader(getSOAPHeaderElement());
        
        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {VALIDATE_CARD});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse updateSelfserviceUserName(com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameRequest updateSelfserviceUserName) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/UpdateSelfserviceUserName");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserName"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateSelfserviceUserName});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse getCVC(com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest getCVC) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/GetCVC");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GetCVC"));
        _call.addHeader(getSOAPHeaderElement());
        
        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getCVC});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse changePin(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinRequest CHANGEPIN) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ChangePin");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePin"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CHANGEPIN});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse changeOverdraftStatus(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusRequest CHANGEOVERDRAFTSTATUS) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ChangeOverdraftStatus");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangeOverdraftStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CHANGEOVERDRAFTSTATUS});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse cardholderDetail(com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailRequest CARDHOLDER_DETAIL) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/CardholderDetail");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetail"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {CARDHOLDER_DETAIL});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse SMSAvailability(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityRequest ACCOUNTSMSAVAILABILITY) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/SMSAvailability");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailability"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {ACCOUNTSMSAVAILABILITY});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response reportStolenCard2(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/ReportStolenCard2");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {REPORTSTOLENCARDEXT});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWork(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/PromoWork");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWork"));
        _call.addHeader(getSOAPHeaderElement());
        
        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {promoWork_Request});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMSEnrollment(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("http://www.corecard.com/Prepaid/SMSEnrollment");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {SMS_ENROLLMENT_SETTINGS});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse) org.apache.axis.utils.JavaUtils.convert(_resp, com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }
    
    private static final String user = "mobilecardusaAPI";
    private static final String pass = "M86uCp39/sR7";
    
    private static org.apache.axis.message.SOAPHeaderElement wsseSecurity = null;

    private org.apache.axis.message.SOAPHeaderElement getSOAPHeaderElement() throws org.apache.axis.AxisFault{
    	try{
    		if(wsseSecurity == null){
    			wsseSecurity =
    	    			new org.apache.axis.message.SOAPHeaderElement("http://www.corecard.com/Prepaid","UserCredentials");
    	    	
    	    	org.apache.axis.message.MessageElement username = 
    	    			new org.apache.axis.message.MessageElement( "userid", "prep", "http://www.corecard.com/Prepaid");
    	    	
    	    	org.apache.axis.message.MessageElement password = 
    	    			new org.apache.axis.message.MessageElement( "password", "prep", "http://www.corecard.com/Prepaid");
    	    	
    	    	wsseSecurity.setPrefix("prep");
    	    	wsseSecurity.setActor(null);
    	    	wsseSecurity.setMustUnderstand(true);
    	    	
    	    	username.setObjectValue(user);
    	    	password.setObjectValue(pass);
    	    	wsseSecurity.addChild(username);
    	    	wsseSecurity.addChild(password);
    		}
    	}catch(javax.xml.soap.SOAPException se){
    		throw new org.apache.axis.AxisFault("Failure trying to get the Security UsernameToken", se);
    	}
    	return wsseSecurity;
    }   

}
