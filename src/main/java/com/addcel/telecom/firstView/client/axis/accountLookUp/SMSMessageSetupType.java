/**
 * SMSMessageSetupType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SMSMessageSetupType  implements java.io.Serializable {
    private java.lang.String MESSAGE_ID;

    private java.lang.String MESSAGE_CATEGORY;

    private java.lang.String PARAMETER_NAME;

    private java.lang.String PARAMETER_VALUE_SET;

    private java.lang.String PARAMETER_NAME_AMOUNT;

    private java.lang.String PARAMETER_VALUE_AMOUNT;

    private java.lang.String PARAMETER_TIME;

    private java.lang.String PARAMETER_VALUE_TIME;

    private java.lang.String MESSAGE_STATUS_SET;

    public SMSMessageSetupType() {
    }

    public SMSMessageSetupType(
           java.lang.String MESSAGE_ID,
           java.lang.String MESSAGE_CATEGORY,
           java.lang.String PARAMETER_NAME,
           java.lang.String PARAMETER_VALUE_SET,
           java.lang.String PARAMETER_NAME_AMOUNT,
           java.lang.String PARAMETER_VALUE_AMOUNT,
           java.lang.String PARAMETER_TIME,
           java.lang.String PARAMETER_VALUE_TIME,
           java.lang.String MESSAGE_STATUS_SET) {
           this.MESSAGE_ID = MESSAGE_ID;
           this.MESSAGE_CATEGORY = MESSAGE_CATEGORY;
           this.PARAMETER_NAME = PARAMETER_NAME;
           this.PARAMETER_VALUE_SET = PARAMETER_VALUE_SET;
           this.PARAMETER_NAME_AMOUNT = PARAMETER_NAME_AMOUNT;
           this.PARAMETER_VALUE_AMOUNT = PARAMETER_VALUE_AMOUNT;
           this.PARAMETER_TIME = PARAMETER_TIME;
           this.PARAMETER_VALUE_TIME = PARAMETER_VALUE_TIME;
           this.MESSAGE_STATUS_SET = MESSAGE_STATUS_SET;
    }


    /**
     * Gets the MESSAGE_ID value for this SMSMessageSetupType.
     * 
     * @return MESSAGE_ID
     */
    public java.lang.String getMESSAGE_ID() {
        return MESSAGE_ID;
    }


    /**
     * Sets the MESSAGE_ID value for this SMSMessageSetupType.
     * 
     * @param MESSAGE_ID
     */
    public void setMESSAGE_ID(java.lang.String MESSAGE_ID) {
        this.MESSAGE_ID = MESSAGE_ID;
    }


    /**
     * Gets the MESSAGE_CATEGORY value for this SMSMessageSetupType.
     * 
     * @return MESSAGE_CATEGORY
     */
    public java.lang.String getMESSAGE_CATEGORY() {
        return MESSAGE_CATEGORY;
    }


    /**
     * Sets the MESSAGE_CATEGORY value for this SMSMessageSetupType.
     * 
     * @param MESSAGE_CATEGORY
     */
    public void setMESSAGE_CATEGORY(java.lang.String MESSAGE_CATEGORY) {
        this.MESSAGE_CATEGORY = MESSAGE_CATEGORY;
    }


    /**
     * Gets the PARAMETER_NAME value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_NAME
     */
    public java.lang.String getPARAMETER_NAME() {
        return PARAMETER_NAME;
    }


    /**
     * Sets the PARAMETER_NAME value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_NAME
     */
    public void setPARAMETER_NAME(java.lang.String PARAMETER_NAME) {
        this.PARAMETER_NAME = PARAMETER_NAME;
    }


    /**
     * Gets the PARAMETER_VALUE_SET value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_VALUE_SET
     */
    public java.lang.String getPARAMETER_VALUE_SET() {
        return PARAMETER_VALUE_SET;
    }


    /**
     * Sets the PARAMETER_VALUE_SET value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_VALUE_SET
     */
    public void setPARAMETER_VALUE_SET(java.lang.String PARAMETER_VALUE_SET) {
        this.PARAMETER_VALUE_SET = PARAMETER_VALUE_SET;
    }


    /**
     * Gets the PARAMETER_NAME_AMOUNT value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_NAME_AMOUNT
     */
    public java.lang.String getPARAMETER_NAME_AMOUNT() {
        return PARAMETER_NAME_AMOUNT;
    }


    /**
     * Sets the PARAMETER_NAME_AMOUNT value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_NAME_AMOUNT
     */
    public void setPARAMETER_NAME_AMOUNT(java.lang.String PARAMETER_NAME_AMOUNT) {
        this.PARAMETER_NAME_AMOUNT = PARAMETER_NAME_AMOUNT;
    }


    /**
     * Gets the PARAMETER_VALUE_AMOUNT value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_VALUE_AMOUNT
     */
    public java.lang.String getPARAMETER_VALUE_AMOUNT() {
        return PARAMETER_VALUE_AMOUNT;
    }


    /**
     * Sets the PARAMETER_VALUE_AMOUNT value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_VALUE_AMOUNT
     */
    public void setPARAMETER_VALUE_AMOUNT(java.lang.String PARAMETER_VALUE_AMOUNT) {
        this.PARAMETER_VALUE_AMOUNT = PARAMETER_VALUE_AMOUNT;
    }


    /**
     * Gets the PARAMETER_TIME value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_TIME
     */
    public java.lang.String getPARAMETER_TIME() {
        return PARAMETER_TIME;
    }


    /**
     * Sets the PARAMETER_TIME value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_TIME
     */
    public void setPARAMETER_TIME(java.lang.String PARAMETER_TIME) {
        this.PARAMETER_TIME = PARAMETER_TIME;
    }


    /**
     * Gets the PARAMETER_VALUE_TIME value for this SMSMessageSetupType.
     * 
     * @return PARAMETER_VALUE_TIME
     */
    public java.lang.String getPARAMETER_VALUE_TIME() {
        return PARAMETER_VALUE_TIME;
    }


    /**
     * Sets the PARAMETER_VALUE_TIME value for this SMSMessageSetupType.
     * 
     * @param PARAMETER_VALUE_TIME
     */
    public void setPARAMETER_VALUE_TIME(java.lang.String PARAMETER_VALUE_TIME) {
        this.PARAMETER_VALUE_TIME = PARAMETER_VALUE_TIME;
    }


    /**
     * Gets the MESSAGE_STATUS_SET value for this SMSMessageSetupType.
     * 
     * @return MESSAGE_STATUS_SET
     */
    public java.lang.String getMESSAGE_STATUS_SET() {
        return MESSAGE_STATUS_SET;
    }


    /**
     * Sets the MESSAGE_STATUS_SET value for this SMSMessageSetupType.
     * 
     * @param MESSAGE_STATUS_SET
     */
    public void setMESSAGE_STATUS_SET(java.lang.String MESSAGE_STATUS_SET) {
        this.MESSAGE_STATUS_SET = MESSAGE_STATUS_SET;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSMessageSetupType)) return false;
        SMSMessageSetupType other = (SMSMessageSetupType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.MESSAGE_ID==null && other.getMESSAGE_ID()==null) || 
             (this.MESSAGE_ID!=null &&
              this.MESSAGE_ID.equals(other.getMESSAGE_ID()))) &&
            ((this.MESSAGE_CATEGORY==null && other.getMESSAGE_CATEGORY()==null) || 
             (this.MESSAGE_CATEGORY!=null &&
              this.MESSAGE_CATEGORY.equals(other.getMESSAGE_CATEGORY()))) &&
            ((this.PARAMETER_NAME==null && other.getPARAMETER_NAME()==null) || 
             (this.PARAMETER_NAME!=null &&
              this.PARAMETER_NAME.equals(other.getPARAMETER_NAME()))) &&
            ((this.PARAMETER_VALUE_SET==null && other.getPARAMETER_VALUE_SET()==null) || 
             (this.PARAMETER_VALUE_SET!=null &&
              this.PARAMETER_VALUE_SET.equals(other.getPARAMETER_VALUE_SET()))) &&
            ((this.PARAMETER_NAME_AMOUNT==null && other.getPARAMETER_NAME_AMOUNT()==null) || 
             (this.PARAMETER_NAME_AMOUNT!=null &&
              this.PARAMETER_NAME_AMOUNT.equals(other.getPARAMETER_NAME_AMOUNT()))) &&
            ((this.PARAMETER_VALUE_AMOUNT==null && other.getPARAMETER_VALUE_AMOUNT()==null) || 
             (this.PARAMETER_VALUE_AMOUNT!=null &&
              this.PARAMETER_VALUE_AMOUNT.equals(other.getPARAMETER_VALUE_AMOUNT()))) &&
            ((this.PARAMETER_TIME==null && other.getPARAMETER_TIME()==null) || 
             (this.PARAMETER_TIME!=null &&
              this.PARAMETER_TIME.equals(other.getPARAMETER_TIME()))) &&
            ((this.PARAMETER_VALUE_TIME==null && other.getPARAMETER_VALUE_TIME()==null) || 
             (this.PARAMETER_VALUE_TIME!=null &&
              this.PARAMETER_VALUE_TIME.equals(other.getPARAMETER_VALUE_TIME()))) &&
            ((this.MESSAGE_STATUS_SET==null && other.getMESSAGE_STATUS_SET()==null) || 
             (this.MESSAGE_STATUS_SET!=null &&
              this.MESSAGE_STATUS_SET.equals(other.getMESSAGE_STATUS_SET())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMESSAGE_ID() != null) {
            _hashCode += getMESSAGE_ID().hashCode();
        }
        if (getMESSAGE_CATEGORY() != null) {
            _hashCode += getMESSAGE_CATEGORY().hashCode();
        }
        if (getPARAMETER_NAME() != null) {
            _hashCode += getPARAMETER_NAME().hashCode();
        }
        if (getPARAMETER_VALUE_SET() != null) {
            _hashCode += getPARAMETER_VALUE_SET().hashCode();
        }
        if (getPARAMETER_NAME_AMOUNT() != null) {
            _hashCode += getPARAMETER_NAME_AMOUNT().hashCode();
        }
        if (getPARAMETER_VALUE_AMOUNT() != null) {
            _hashCode += getPARAMETER_VALUE_AMOUNT().hashCode();
        }
        if (getPARAMETER_TIME() != null) {
            _hashCode += getPARAMETER_TIME().hashCode();
        }
        if (getPARAMETER_VALUE_TIME() != null) {
            _hashCode += getPARAMETER_VALUE_TIME().hashCode();
        }
        if (getMESSAGE_STATUS_SET() != null) {
            _hashCode += getMESSAGE_STATUS_SET().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSMessageSetupType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSMessageSetupType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MESSAGE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGE_CATEGORY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MESSAGE_CATEGORY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_NAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_NAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_VALUE_SET");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_VALUE_SET"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_NAME_AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_NAME_AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_VALUE_AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_VALUE_AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PARAMETER_VALUE_TIME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PARAMETER_VALUE_TIME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGE_STATUS_SET");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MESSAGE_STATUS_SET"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
