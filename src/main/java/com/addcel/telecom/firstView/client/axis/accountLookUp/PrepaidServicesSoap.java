/**
 * PrepaidServicesSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public interface PrepaidServicesSoap extends java.rmi.Remote {
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse cardActivationInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryRequest cardActivationInquiry) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse requestCustomerServiceHelp(com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpRequest REQUESTCUSTOMERSERVICEHELP) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse login(com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequest LOGIN_REQUEST) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse resetPassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordRequest resetPassword) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse cardToCardTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferRequest TRANSFERCARD) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse clearing(com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingRequest clrRequest) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse loadsBasedOnHoldingAccount(com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountRequest loadsBasedOnHoldingAccount_Request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse checkBalance(com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceRequest CHECKBALANCE) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse cardActive(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveRequest CARDACTIVE) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse savingAccountsFundsTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferRequest savingAccountsFundsTransfer) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse changePassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordRequest CHANGE_PASSWORD) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse savingAccountsInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryRequest savingAccountsInquiry) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse reportStolenCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardRequest REPORTSTOLENCARD) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse SMSSetup(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupRequest ACCOUNTSMSSETUP) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse generatePIN(com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINRequest generatePIN) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response cardActivation3(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Request cardActivation3) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse transactionHistory(com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest TRANSACTION_HISTORY) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse cardUpdate(com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateRequest CARDUPDATE) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse validateCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest VALIDATE_CARD) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse updateSelfserviceUserName(com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameRequest updateSelfserviceUserName) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse getCVC(com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest getCVC) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse changePin(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinRequest CHANGEPIN) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse changeOverdraftStatus(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusRequest CHANGEOVERDRAFTSTATUS) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse cardholderDetail(com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailRequest CARDHOLDER_DETAIL) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse SMSAvailability(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityRequest ACCOUNTSMSAVAILABILITY) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response reportStolenCard2(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWork(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request) throws java.rmi.RemoteException;
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMSEnrollment(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS) throws java.rmi.RemoteException;
}
