/**
 * PromoWorkResponseType6.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PromoWorkResponseType6  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWorks;

    public PromoWorkResponseType6() {
    }

    public PromoWorkResponseType6(
           com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWorks) {
           this.promoWorks = promoWorks;
    }


    /**
     * Gets the promoWorks value for this PromoWorkResponseType6.
     * 
     * @return promoWorks
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse getPromoWorks() {
        return promoWorks;
    }


    /**
     * Sets the promoWorks value for this PromoWorkResponseType6.
     * 
     * @param promoWorks
     */
    public void setPromoWorks(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWorks) {
        this.promoWorks = promoWorks;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PromoWorkResponseType6)) return false;
        PromoWorkResponseType6 other = (PromoWorkResponseType6) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.promoWorks==null && other.getPromoWorks()==null) || 
             (this.promoWorks!=null &&
              this.promoWorks.equals(other.getPromoWorks())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPromoWorks() != null) {
            _hashCode += getPromoWorks().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PromoWorkResponseType6.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">PromoWorkResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoWorks");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorks"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkResponse"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
