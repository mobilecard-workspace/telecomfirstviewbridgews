/**
 * CardUpdateResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardUpdateResponse  implements java.io.Serializable {
    private java.lang.String resErrorMsg;

    private java.lang.String resErrorCode;

    private java.lang.String resCode;

    private java.lang.String ACCOUNTNUMBER;

    public CardUpdateResponse() {
    }

    public CardUpdateResponse(
           java.lang.String resErrorMsg,
           java.lang.String resErrorCode,
           java.lang.String resCode,
           java.lang.String ACCOUNTNUMBER) {
           this.resErrorMsg = resErrorMsg;
           this.resErrorCode = resErrorCode;
           this.resCode = resCode;
           this.ACCOUNTNUMBER = ACCOUNTNUMBER;
    }


    /**
     * Gets the resErrorMsg value for this CardUpdateResponse.
     * 
     * @return resErrorMsg
     */
    public java.lang.String getResErrorMsg() {
        return resErrorMsg;
    }


    /**
     * Sets the resErrorMsg value for this CardUpdateResponse.
     * 
     * @param resErrorMsg
     */
    public void setResErrorMsg(java.lang.String resErrorMsg) {
        this.resErrorMsg = resErrorMsg;
    }


    /**
     * Gets the resErrorCode value for this CardUpdateResponse.
     * 
     * @return resErrorCode
     */
    public java.lang.String getResErrorCode() {
        return resErrorCode;
    }


    /**
     * Sets the resErrorCode value for this CardUpdateResponse.
     * 
     * @param resErrorCode
     */
    public void setResErrorCode(java.lang.String resErrorCode) {
        this.resErrorCode = resErrorCode;
    }


    /**
     * Gets the resCode value for this CardUpdateResponse.
     * 
     * @return resCode
     */
    public java.lang.String getResCode() {
        return resCode;
    }


    /**
     * Sets the resCode value for this CardUpdateResponse.
     * 
     * @param resCode
     */
    public void setResCode(java.lang.String resCode) {
        this.resCode = resCode;
    }


    /**
     * Gets the ACCOUNTNUMBER value for this CardUpdateResponse.
     * 
     * @return ACCOUNTNUMBER
     */
    public java.lang.String getACCOUNTNUMBER() {
        return ACCOUNTNUMBER;
    }


    /**
     * Sets the ACCOUNTNUMBER value for this CardUpdateResponse.
     * 
     * @param ACCOUNTNUMBER
     */
    public void setACCOUNTNUMBER(java.lang.String ACCOUNTNUMBER) {
        this.ACCOUNTNUMBER = ACCOUNTNUMBER;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardUpdateResponse)) return false;
        CardUpdateResponse other = (CardUpdateResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resErrorMsg==null && other.getResErrorMsg()==null) || 
             (this.resErrorMsg!=null &&
              this.resErrorMsg.equals(other.getResErrorMsg()))) &&
            ((this.resErrorCode==null && other.getResErrorCode()==null) || 
             (this.resErrorCode!=null &&
              this.resErrorCode.equals(other.getResErrorCode()))) &&
            ((this.resCode==null && other.getResCode()==null) || 
             (this.resCode!=null &&
              this.resCode.equals(other.getResCode()))) &&
            ((this.ACCOUNTNUMBER==null && other.getACCOUNTNUMBER()==null) || 
             (this.ACCOUNTNUMBER!=null &&
              this.ACCOUNTNUMBER.equals(other.getACCOUNTNUMBER())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResErrorMsg() != null) {
            _hashCode += getResErrorMsg().hashCode();
        }
        if (getResErrorCode() != null) {
            _hashCode += getResErrorCode().hashCode();
        }
        if (getResCode() != null) {
            _hashCode += getResCode().hashCode();
        }
        if (getACCOUNTNUMBER() != null) {
            _hashCode += getACCOUNTNUMBER().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardUpdateResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resErrorMsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResErrorMsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resErrorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResErrorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ResCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNTNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
