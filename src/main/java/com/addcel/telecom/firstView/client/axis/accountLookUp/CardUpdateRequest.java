/**
 * CardUpdateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardUpdateRequest  implements java.io.Serializable {
    private java.lang.String CLIENTID;

    private java.lang.String CARDNUMBER;

    private java.lang.String FIRSTNAME;

    private java.lang.String MIDDLENAME;

    private java.lang.String LASTNAME;

    private java.lang.String DATEOFBIRTH;

    private java.lang.String GOVERNMENTID;

    private java.lang.String IDNUMBER;

    private java.lang.String IDISSUEDATE;

    private java.lang.String IDEXPIRATIONDATE;

    private java.lang.String IDISSUECOUNTRY;

    private java.lang.String IDISSUESTATE;

    private java.lang.String ADDRESS1;

    private java.lang.String ADDRESSLINE2;

    private java.lang.String CITY;

    private java.lang.String STATE;

    private java.lang.String COUNTRY;

    private java.lang.String POSTALCODE;

    private java.lang.String HOMEPHONENUMBER;

    private java.lang.String EMAILID;

    private java.lang.String manualCardStatus;

    private java.lang.String USERFIELD1;

    private java.lang.String USERFIELD2;

    private java.lang.String USERFIELD3;

    private java.lang.String USERFIELD4;

    private java.lang.String USERFIELD5;

    private java.lang.String HPhoneCountryCode;

    private java.lang.String HPhoneExtension;

    private java.lang.String SSNNUMBER;

    private java.lang.String shipToCmpnyName;

    private java.lang.String shipToContactName;

    private java.lang.String shipToAddressOne;

    private java.lang.String shipToAddressTwo;

    private java.lang.String shipToCity;

    private java.lang.String shipToState;

    private java.lang.String shipToZipCode;

    private java.lang.String reissue;

    private java.lang.String skipACHLoadEffectiveDate;

    public CardUpdateRequest() {
    }

    public CardUpdateRequest(
           java.lang.String CLIENTID,
           java.lang.String CARDNUMBER,
           java.lang.String FIRSTNAME,
           java.lang.String MIDDLENAME,
           java.lang.String LASTNAME,
           java.lang.String DATEOFBIRTH,
           java.lang.String GOVERNMENTID,
           java.lang.String IDNUMBER,
           java.lang.String IDISSUEDATE,
           java.lang.String IDEXPIRATIONDATE,
           java.lang.String IDISSUECOUNTRY,
           java.lang.String IDISSUESTATE,
           java.lang.String ADDRESS1,
           java.lang.String ADDRESSLINE2,
           java.lang.String CITY,
           java.lang.String STATE,
           java.lang.String COUNTRY,
           java.lang.String POSTALCODE,
           java.lang.String HOMEPHONENUMBER,
           java.lang.String EMAILID,
           java.lang.String manualCardStatus,
           java.lang.String USERFIELD1,
           java.lang.String USERFIELD2,
           java.lang.String USERFIELD3,
           java.lang.String USERFIELD4,
           java.lang.String USERFIELD5,
           java.lang.String HPhoneCountryCode,
           java.lang.String HPhoneExtension,
           java.lang.String SSNNUMBER,
           java.lang.String shipToCmpnyName,
           java.lang.String shipToContactName,
           java.lang.String shipToAddressOne,
           java.lang.String shipToAddressTwo,
           java.lang.String shipToCity,
           java.lang.String shipToState,
           java.lang.String shipToZipCode,
           java.lang.String reissue,
           java.lang.String skipACHLoadEffectiveDate) {
           this.CLIENTID = CLIENTID;
           this.CARDNUMBER = CARDNUMBER;
           this.FIRSTNAME = FIRSTNAME;
           this.MIDDLENAME = MIDDLENAME;
           this.LASTNAME = LASTNAME;
           this.DATEOFBIRTH = DATEOFBIRTH;
           this.GOVERNMENTID = GOVERNMENTID;
           this.IDNUMBER = IDNUMBER;
           this.IDISSUEDATE = IDISSUEDATE;
           this.IDEXPIRATIONDATE = IDEXPIRATIONDATE;
           this.IDISSUECOUNTRY = IDISSUECOUNTRY;
           this.IDISSUESTATE = IDISSUESTATE;
           this.ADDRESS1 = ADDRESS1;
           this.ADDRESSLINE2 = ADDRESSLINE2;
           this.CITY = CITY;
           this.STATE = STATE;
           this.COUNTRY = COUNTRY;
           this.POSTALCODE = POSTALCODE;
           this.HOMEPHONENUMBER = HOMEPHONENUMBER;
           this.EMAILID = EMAILID;
           this.manualCardStatus = manualCardStatus;
           this.USERFIELD1 = USERFIELD1;
           this.USERFIELD2 = USERFIELD2;
           this.USERFIELD3 = USERFIELD3;
           this.USERFIELD4 = USERFIELD4;
           this.USERFIELD5 = USERFIELD5;
           this.HPhoneCountryCode = HPhoneCountryCode;
           this.HPhoneExtension = HPhoneExtension;
           this.SSNNUMBER = SSNNUMBER;
           this.shipToCmpnyName = shipToCmpnyName;
           this.shipToContactName = shipToContactName;
           this.shipToAddressOne = shipToAddressOne;
           this.shipToAddressTwo = shipToAddressTwo;
           this.shipToCity = shipToCity;
           this.shipToState = shipToState;
           this.shipToZipCode = shipToZipCode;
           this.reissue = reissue;
           this.skipACHLoadEffectiveDate = skipACHLoadEffectiveDate;
    }


    /**
     * Gets the CLIENTID value for this CardUpdateRequest.
     * 
     * @return CLIENTID
     */
    public java.lang.String getCLIENTID() {
        return CLIENTID;
    }


    /**
     * Sets the CLIENTID value for this CardUpdateRequest.
     * 
     * @param CLIENTID
     */
    public void setCLIENTID(java.lang.String CLIENTID) {
        this.CLIENTID = CLIENTID;
    }


    /**
     * Gets the CARDNUMBER value for this CardUpdateRequest.
     * 
     * @return CARDNUMBER
     */
    public java.lang.String getCARDNUMBER() {
        return CARDNUMBER;
    }


    /**
     * Sets the CARDNUMBER value for this CardUpdateRequest.
     * 
     * @param CARDNUMBER
     */
    public void setCARDNUMBER(java.lang.String CARDNUMBER) {
        this.CARDNUMBER = CARDNUMBER;
    }


    /**
     * Gets the FIRSTNAME value for this CardUpdateRequest.
     * 
     * @return FIRSTNAME
     */
    public java.lang.String getFIRSTNAME() {
        return FIRSTNAME;
    }


    /**
     * Sets the FIRSTNAME value for this CardUpdateRequest.
     * 
     * @param FIRSTNAME
     */
    public void setFIRSTNAME(java.lang.String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }


    /**
     * Gets the MIDDLENAME value for this CardUpdateRequest.
     * 
     * @return MIDDLENAME
     */
    public java.lang.String getMIDDLENAME() {
        return MIDDLENAME;
    }


    /**
     * Sets the MIDDLENAME value for this CardUpdateRequest.
     * 
     * @param MIDDLENAME
     */
    public void setMIDDLENAME(java.lang.String MIDDLENAME) {
        this.MIDDLENAME = MIDDLENAME;
    }


    /**
     * Gets the LASTNAME value for this CardUpdateRequest.
     * 
     * @return LASTNAME
     */
    public java.lang.String getLASTNAME() {
        return LASTNAME;
    }


    /**
     * Sets the LASTNAME value for this CardUpdateRequest.
     * 
     * @param LASTNAME
     */
    public void setLASTNAME(java.lang.String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }


    /**
     * Gets the DATEOFBIRTH value for this CardUpdateRequest.
     * 
     * @return DATEOFBIRTH
     */
    public java.lang.String getDATEOFBIRTH() {
        return DATEOFBIRTH;
    }


    /**
     * Sets the DATEOFBIRTH value for this CardUpdateRequest.
     * 
     * @param DATEOFBIRTH
     */
    public void setDATEOFBIRTH(java.lang.String DATEOFBIRTH) {
        this.DATEOFBIRTH = DATEOFBIRTH;
    }


    /**
     * Gets the GOVERNMENTID value for this CardUpdateRequest.
     * 
     * @return GOVERNMENTID
     */
    public java.lang.String getGOVERNMENTID() {
        return GOVERNMENTID;
    }


    /**
     * Sets the GOVERNMENTID value for this CardUpdateRequest.
     * 
     * @param GOVERNMENTID
     */
    public void setGOVERNMENTID(java.lang.String GOVERNMENTID) {
        this.GOVERNMENTID = GOVERNMENTID;
    }


    /**
     * Gets the IDNUMBER value for this CardUpdateRequest.
     * 
     * @return IDNUMBER
     */
    public java.lang.String getIDNUMBER() {
        return IDNUMBER;
    }


    /**
     * Sets the IDNUMBER value for this CardUpdateRequest.
     * 
     * @param IDNUMBER
     */
    public void setIDNUMBER(java.lang.String IDNUMBER) {
        this.IDNUMBER = IDNUMBER;
    }


    /**
     * Gets the IDISSUEDATE value for this CardUpdateRequest.
     * 
     * @return IDISSUEDATE
     */
    public java.lang.String getIDISSUEDATE() {
        return IDISSUEDATE;
    }


    /**
     * Sets the IDISSUEDATE value for this CardUpdateRequest.
     * 
     * @param IDISSUEDATE
     */
    public void setIDISSUEDATE(java.lang.String IDISSUEDATE) {
        this.IDISSUEDATE = IDISSUEDATE;
    }


    /**
     * Gets the IDEXPIRATIONDATE value for this CardUpdateRequest.
     * 
     * @return IDEXPIRATIONDATE
     */
    public java.lang.String getIDEXPIRATIONDATE() {
        return IDEXPIRATIONDATE;
    }


    /**
     * Sets the IDEXPIRATIONDATE value for this CardUpdateRequest.
     * 
     * @param IDEXPIRATIONDATE
     */
    public void setIDEXPIRATIONDATE(java.lang.String IDEXPIRATIONDATE) {
        this.IDEXPIRATIONDATE = IDEXPIRATIONDATE;
    }


    /**
     * Gets the IDISSUECOUNTRY value for this CardUpdateRequest.
     * 
     * @return IDISSUECOUNTRY
     */
    public java.lang.String getIDISSUECOUNTRY() {
        return IDISSUECOUNTRY;
    }


    /**
     * Sets the IDISSUECOUNTRY value for this CardUpdateRequest.
     * 
     * @param IDISSUECOUNTRY
     */
    public void setIDISSUECOUNTRY(java.lang.String IDISSUECOUNTRY) {
        this.IDISSUECOUNTRY = IDISSUECOUNTRY;
    }


    /**
     * Gets the IDISSUESTATE value for this CardUpdateRequest.
     * 
     * @return IDISSUESTATE
     */
    public java.lang.String getIDISSUESTATE() {
        return IDISSUESTATE;
    }


    /**
     * Sets the IDISSUESTATE value for this CardUpdateRequest.
     * 
     * @param IDISSUESTATE
     */
    public void setIDISSUESTATE(java.lang.String IDISSUESTATE) {
        this.IDISSUESTATE = IDISSUESTATE;
    }


    /**
     * Gets the ADDRESS1 value for this CardUpdateRequest.
     * 
     * @return ADDRESS1
     */
    public java.lang.String getADDRESS1() {
        return ADDRESS1;
    }


    /**
     * Sets the ADDRESS1 value for this CardUpdateRequest.
     * 
     * @param ADDRESS1
     */
    public void setADDRESS1(java.lang.String ADDRESS1) {
        this.ADDRESS1 = ADDRESS1;
    }


    /**
     * Gets the ADDRESSLINE2 value for this CardUpdateRequest.
     * 
     * @return ADDRESSLINE2
     */
    public java.lang.String getADDRESSLINE2() {
        return ADDRESSLINE2;
    }


    /**
     * Sets the ADDRESSLINE2 value for this CardUpdateRequest.
     * 
     * @param ADDRESSLINE2
     */
    public void setADDRESSLINE2(java.lang.String ADDRESSLINE2) {
        this.ADDRESSLINE2 = ADDRESSLINE2;
    }


    /**
     * Gets the CITY value for this CardUpdateRequest.
     * 
     * @return CITY
     */
    public java.lang.String getCITY() {
        return CITY;
    }


    /**
     * Sets the CITY value for this CardUpdateRequest.
     * 
     * @param CITY
     */
    public void setCITY(java.lang.String CITY) {
        this.CITY = CITY;
    }


    /**
     * Gets the STATE value for this CardUpdateRequest.
     * 
     * @return STATE
     */
    public java.lang.String getSTATE() {
        return STATE;
    }


    /**
     * Sets the STATE value for this CardUpdateRequest.
     * 
     * @param STATE
     */
    public void setSTATE(java.lang.String STATE) {
        this.STATE = STATE;
    }


    /**
     * Gets the COUNTRY value for this CardUpdateRequest.
     * 
     * @return COUNTRY
     */
    public java.lang.String getCOUNTRY() {
        return COUNTRY;
    }


    /**
     * Sets the COUNTRY value for this CardUpdateRequest.
     * 
     * @param COUNTRY
     */
    public void setCOUNTRY(java.lang.String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }


    /**
     * Gets the POSTALCODE value for this CardUpdateRequest.
     * 
     * @return POSTALCODE
     */
    public java.lang.String getPOSTALCODE() {
        return POSTALCODE;
    }


    /**
     * Sets the POSTALCODE value for this CardUpdateRequest.
     * 
     * @param POSTALCODE
     */
    public void setPOSTALCODE(java.lang.String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }


    /**
     * Gets the HOMEPHONENUMBER value for this CardUpdateRequest.
     * 
     * @return HOMEPHONENUMBER
     */
    public java.lang.String getHOMEPHONENUMBER() {
        return HOMEPHONENUMBER;
    }


    /**
     * Sets the HOMEPHONENUMBER value for this CardUpdateRequest.
     * 
     * @param HOMEPHONENUMBER
     */
    public void setHOMEPHONENUMBER(java.lang.String HOMEPHONENUMBER) {
        this.HOMEPHONENUMBER = HOMEPHONENUMBER;
    }


    /**
     * Gets the EMAILID value for this CardUpdateRequest.
     * 
     * @return EMAILID
     */
    public java.lang.String getEMAILID() {
        return EMAILID;
    }


    /**
     * Sets the EMAILID value for this CardUpdateRequest.
     * 
     * @param EMAILID
     */
    public void setEMAILID(java.lang.String EMAILID) {
        this.EMAILID = EMAILID;
    }


    /**
     * Gets the manualCardStatus value for this CardUpdateRequest.
     * 
     * @return manualCardStatus
     */
    public java.lang.String getManualCardStatus() {
        return manualCardStatus;
    }


    /**
     * Sets the manualCardStatus value for this CardUpdateRequest.
     * 
     * @param manualCardStatus
     */
    public void setManualCardStatus(java.lang.String manualCardStatus) {
        this.manualCardStatus = manualCardStatus;
    }


    /**
     * Gets the USERFIELD1 value for this CardUpdateRequest.
     * 
     * @return USERFIELD1
     */
    public java.lang.String getUSERFIELD1() {
        return USERFIELD1;
    }


    /**
     * Sets the USERFIELD1 value for this CardUpdateRequest.
     * 
     * @param USERFIELD1
     */
    public void setUSERFIELD1(java.lang.String USERFIELD1) {
        this.USERFIELD1 = USERFIELD1;
    }


    /**
     * Gets the USERFIELD2 value for this CardUpdateRequest.
     * 
     * @return USERFIELD2
     */
    public java.lang.String getUSERFIELD2() {
        return USERFIELD2;
    }


    /**
     * Sets the USERFIELD2 value for this CardUpdateRequest.
     * 
     * @param USERFIELD2
     */
    public void setUSERFIELD2(java.lang.String USERFIELD2) {
        this.USERFIELD2 = USERFIELD2;
    }


    /**
     * Gets the USERFIELD3 value for this CardUpdateRequest.
     * 
     * @return USERFIELD3
     */
    public java.lang.String getUSERFIELD3() {
        return USERFIELD3;
    }


    /**
     * Sets the USERFIELD3 value for this CardUpdateRequest.
     * 
     * @param USERFIELD3
     */
    public void setUSERFIELD3(java.lang.String USERFIELD3) {
        this.USERFIELD3 = USERFIELD3;
    }


    /**
     * Gets the USERFIELD4 value for this CardUpdateRequest.
     * 
     * @return USERFIELD4
     */
    public java.lang.String getUSERFIELD4() {
        return USERFIELD4;
    }


    /**
     * Sets the USERFIELD4 value for this CardUpdateRequest.
     * 
     * @param USERFIELD4
     */
    public void setUSERFIELD4(java.lang.String USERFIELD4) {
        this.USERFIELD4 = USERFIELD4;
    }


    /**
     * Gets the USERFIELD5 value for this CardUpdateRequest.
     * 
     * @return USERFIELD5
     */
    public java.lang.String getUSERFIELD5() {
        return USERFIELD5;
    }


    /**
     * Sets the USERFIELD5 value for this CardUpdateRequest.
     * 
     * @param USERFIELD5
     */
    public void setUSERFIELD5(java.lang.String USERFIELD5) {
        this.USERFIELD5 = USERFIELD5;
    }


    /**
     * Gets the HPhoneCountryCode value for this CardUpdateRequest.
     * 
     * @return HPhoneCountryCode
     */
    public java.lang.String getHPhoneCountryCode() {
        return HPhoneCountryCode;
    }


    /**
     * Sets the HPhoneCountryCode value for this CardUpdateRequest.
     * 
     * @param HPhoneCountryCode
     */
    public void setHPhoneCountryCode(java.lang.String HPhoneCountryCode) {
        this.HPhoneCountryCode = HPhoneCountryCode;
    }


    /**
     * Gets the HPhoneExtension value for this CardUpdateRequest.
     * 
     * @return HPhoneExtension
     */
    public java.lang.String getHPhoneExtension() {
        return HPhoneExtension;
    }


    /**
     * Sets the HPhoneExtension value for this CardUpdateRequest.
     * 
     * @param HPhoneExtension
     */
    public void setHPhoneExtension(java.lang.String HPhoneExtension) {
        this.HPhoneExtension = HPhoneExtension;
    }


    /**
     * Gets the SSNNUMBER value for this CardUpdateRequest.
     * 
     * @return SSNNUMBER
     */
    public java.lang.String getSSNNUMBER() {
        return SSNNUMBER;
    }


    /**
     * Sets the SSNNUMBER value for this CardUpdateRequest.
     * 
     * @param SSNNUMBER
     */
    public void setSSNNUMBER(java.lang.String SSNNUMBER) {
        this.SSNNUMBER = SSNNUMBER;
    }


    /**
     * Gets the shipToCmpnyName value for this CardUpdateRequest.
     * 
     * @return shipToCmpnyName
     */
    public java.lang.String getShipToCmpnyName() {
        return shipToCmpnyName;
    }


    /**
     * Sets the shipToCmpnyName value for this CardUpdateRequest.
     * 
     * @param shipToCmpnyName
     */
    public void setShipToCmpnyName(java.lang.String shipToCmpnyName) {
        this.shipToCmpnyName = shipToCmpnyName;
    }


    /**
     * Gets the shipToContactName value for this CardUpdateRequest.
     * 
     * @return shipToContactName
     */
    public java.lang.String getShipToContactName() {
        return shipToContactName;
    }


    /**
     * Sets the shipToContactName value for this CardUpdateRequest.
     * 
     * @param shipToContactName
     */
    public void setShipToContactName(java.lang.String shipToContactName) {
        this.shipToContactName = shipToContactName;
    }


    /**
     * Gets the shipToAddressOne value for this CardUpdateRequest.
     * 
     * @return shipToAddressOne
     */
    public java.lang.String getShipToAddressOne() {
        return shipToAddressOne;
    }


    /**
     * Sets the shipToAddressOne value for this CardUpdateRequest.
     * 
     * @param shipToAddressOne
     */
    public void setShipToAddressOne(java.lang.String shipToAddressOne) {
        this.shipToAddressOne = shipToAddressOne;
    }


    /**
     * Gets the shipToAddressTwo value for this CardUpdateRequest.
     * 
     * @return shipToAddressTwo
     */
    public java.lang.String getShipToAddressTwo() {
        return shipToAddressTwo;
    }


    /**
     * Sets the shipToAddressTwo value for this CardUpdateRequest.
     * 
     * @param shipToAddressTwo
     */
    public void setShipToAddressTwo(java.lang.String shipToAddressTwo) {
        this.shipToAddressTwo = shipToAddressTwo;
    }


    /**
     * Gets the shipToCity value for this CardUpdateRequest.
     * 
     * @return shipToCity
     */
    public java.lang.String getShipToCity() {
        return shipToCity;
    }


    /**
     * Sets the shipToCity value for this CardUpdateRequest.
     * 
     * @param shipToCity
     */
    public void setShipToCity(java.lang.String shipToCity) {
        this.shipToCity = shipToCity;
    }


    /**
     * Gets the shipToState value for this CardUpdateRequest.
     * 
     * @return shipToState
     */
    public java.lang.String getShipToState() {
        return shipToState;
    }


    /**
     * Sets the shipToState value for this CardUpdateRequest.
     * 
     * @param shipToState
     */
    public void setShipToState(java.lang.String shipToState) {
        this.shipToState = shipToState;
    }


    /**
     * Gets the shipToZipCode value for this CardUpdateRequest.
     * 
     * @return shipToZipCode
     */
    public java.lang.String getShipToZipCode() {
        return shipToZipCode;
    }


    /**
     * Sets the shipToZipCode value for this CardUpdateRequest.
     * 
     * @param shipToZipCode
     */
    public void setShipToZipCode(java.lang.String shipToZipCode) {
        this.shipToZipCode = shipToZipCode;
    }


    /**
     * Gets the reissue value for this CardUpdateRequest.
     * 
     * @return reissue
     */
    public java.lang.String getReissue() {
        return reissue;
    }


    /**
     * Sets the reissue value for this CardUpdateRequest.
     * 
     * @param reissue
     */
    public void setReissue(java.lang.String reissue) {
        this.reissue = reissue;
    }


    /**
     * Gets the skipACHLoadEffectiveDate value for this CardUpdateRequest.
     * 
     * @return skipACHLoadEffectiveDate
     */
    public java.lang.String getSkipACHLoadEffectiveDate() {
        return skipACHLoadEffectiveDate;
    }


    /**
     * Sets the skipACHLoadEffectiveDate value for this CardUpdateRequest.
     * 
     * @param skipACHLoadEffectiveDate
     */
    public void setSkipACHLoadEffectiveDate(java.lang.String skipACHLoadEffectiveDate) {
        this.skipACHLoadEffectiveDate = skipACHLoadEffectiveDate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardUpdateRequest)) return false;
        CardUpdateRequest other = (CardUpdateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CLIENTID==null && other.getCLIENTID()==null) || 
             (this.CLIENTID!=null &&
              this.CLIENTID.equals(other.getCLIENTID()))) &&
            ((this.CARDNUMBER==null && other.getCARDNUMBER()==null) || 
             (this.CARDNUMBER!=null &&
              this.CARDNUMBER.equals(other.getCARDNUMBER()))) &&
            ((this.FIRSTNAME==null && other.getFIRSTNAME()==null) || 
             (this.FIRSTNAME!=null &&
              this.FIRSTNAME.equals(other.getFIRSTNAME()))) &&
            ((this.MIDDLENAME==null && other.getMIDDLENAME()==null) || 
             (this.MIDDLENAME!=null &&
              this.MIDDLENAME.equals(other.getMIDDLENAME()))) &&
            ((this.LASTNAME==null && other.getLASTNAME()==null) || 
             (this.LASTNAME!=null &&
              this.LASTNAME.equals(other.getLASTNAME()))) &&
            ((this.DATEOFBIRTH==null && other.getDATEOFBIRTH()==null) || 
             (this.DATEOFBIRTH!=null &&
              this.DATEOFBIRTH.equals(other.getDATEOFBIRTH()))) &&
            ((this.GOVERNMENTID==null && other.getGOVERNMENTID()==null) || 
             (this.GOVERNMENTID!=null &&
              this.GOVERNMENTID.equals(other.getGOVERNMENTID()))) &&
            ((this.IDNUMBER==null && other.getIDNUMBER()==null) || 
             (this.IDNUMBER!=null &&
              this.IDNUMBER.equals(other.getIDNUMBER()))) &&
            ((this.IDISSUEDATE==null && other.getIDISSUEDATE()==null) || 
             (this.IDISSUEDATE!=null &&
              this.IDISSUEDATE.equals(other.getIDISSUEDATE()))) &&
            ((this.IDEXPIRATIONDATE==null && other.getIDEXPIRATIONDATE()==null) || 
             (this.IDEXPIRATIONDATE!=null &&
              this.IDEXPIRATIONDATE.equals(other.getIDEXPIRATIONDATE()))) &&
            ((this.IDISSUECOUNTRY==null && other.getIDISSUECOUNTRY()==null) || 
             (this.IDISSUECOUNTRY!=null &&
              this.IDISSUECOUNTRY.equals(other.getIDISSUECOUNTRY()))) &&
            ((this.IDISSUESTATE==null && other.getIDISSUESTATE()==null) || 
             (this.IDISSUESTATE!=null &&
              this.IDISSUESTATE.equals(other.getIDISSUESTATE()))) &&
            ((this.ADDRESS1==null && other.getADDRESS1()==null) || 
             (this.ADDRESS1!=null &&
              this.ADDRESS1.equals(other.getADDRESS1()))) &&
            ((this.ADDRESSLINE2==null && other.getADDRESSLINE2()==null) || 
             (this.ADDRESSLINE2!=null &&
              this.ADDRESSLINE2.equals(other.getADDRESSLINE2()))) &&
            ((this.CITY==null && other.getCITY()==null) || 
             (this.CITY!=null &&
              this.CITY.equals(other.getCITY()))) &&
            ((this.STATE==null && other.getSTATE()==null) || 
             (this.STATE!=null &&
              this.STATE.equals(other.getSTATE()))) &&
            ((this.COUNTRY==null && other.getCOUNTRY()==null) || 
             (this.COUNTRY!=null &&
              this.COUNTRY.equals(other.getCOUNTRY()))) &&
            ((this.POSTALCODE==null && other.getPOSTALCODE()==null) || 
             (this.POSTALCODE!=null &&
              this.POSTALCODE.equals(other.getPOSTALCODE()))) &&
            ((this.HOMEPHONENUMBER==null && other.getHOMEPHONENUMBER()==null) || 
             (this.HOMEPHONENUMBER!=null &&
              this.HOMEPHONENUMBER.equals(other.getHOMEPHONENUMBER()))) &&
            ((this.EMAILID==null && other.getEMAILID()==null) || 
             (this.EMAILID!=null &&
              this.EMAILID.equals(other.getEMAILID()))) &&
            ((this.manualCardStatus==null && other.getManualCardStatus()==null) || 
             (this.manualCardStatus!=null &&
              this.manualCardStatus.equals(other.getManualCardStatus()))) &&
            ((this.USERFIELD1==null && other.getUSERFIELD1()==null) || 
             (this.USERFIELD1!=null &&
              this.USERFIELD1.equals(other.getUSERFIELD1()))) &&
            ((this.USERFIELD2==null && other.getUSERFIELD2()==null) || 
             (this.USERFIELD2!=null &&
              this.USERFIELD2.equals(other.getUSERFIELD2()))) &&
            ((this.USERFIELD3==null && other.getUSERFIELD3()==null) || 
             (this.USERFIELD3!=null &&
              this.USERFIELD3.equals(other.getUSERFIELD3()))) &&
            ((this.USERFIELD4==null && other.getUSERFIELD4()==null) || 
             (this.USERFIELD4!=null &&
              this.USERFIELD4.equals(other.getUSERFIELD4()))) &&
            ((this.USERFIELD5==null && other.getUSERFIELD5()==null) || 
             (this.USERFIELD5!=null &&
              this.USERFIELD5.equals(other.getUSERFIELD5()))) &&
            ((this.HPhoneCountryCode==null && other.getHPhoneCountryCode()==null) || 
             (this.HPhoneCountryCode!=null &&
              this.HPhoneCountryCode.equals(other.getHPhoneCountryCode()))) &&
            ((this.HPhoneExtension==null && other.getHPhoneExtension()==null) || 
             (this.HPhoneExtension!=null &&
              this.HPhoneExtension.equals(other.getHPhoneExtension()))) &&
            ((this.SSNNUMBER==null && other.getSSNNUMBER()==null) || 
             (this.SSNNUMBER!=null &&
              this.SSNNUMBER.equals(other.getSSNNUMBER()))) &&
            ((this.shipToCmpnyName==null && other.getShipToCmpnyName()==null) || 
             (this.shipToCmpnyName!=null &&
              this.shipToCmpnyName.equals(other.getShipToCmpnyName()))) &&
            ((this.shipToContactName==null && other.getShipToContactName()==null) || 
             (this.shipToContactName!=null &&
              this.shipToContactName.equals(other.getShipToContactName()))) &&
            ((this.shipToAddressOne==null && other.getShipToAddressOne()==null) || 
             (this.shipToAddressOne!=null &&
              this.shipToAddressOne.equals(other.getShipToAddressOne()))) &&
            ((this.shipToAddressTwo==null && other.getShipToAddressTwo()==null) || 
             (this.shipToAddressTwo!=null &&
              this.shipToAddressTwo.equals(other.getShipToAddressTwo()))) &&
            ((this.shipToCity==null && other.getShipToCity()==null) || 
             (this.shipToCity!=null &&
              this.shipToCity.equals(other.getShipToCity()))) &&
            ((this.shipToState==null && other.getShipToState()==null) || 
             (this.shipToState!=null &&
              this.shipToState.equals(other.getShipToState()))) &&
            ((this.shipToZipCode==null && other.getShipToZipCode()==null) || 
             (this.shipToZipCode!=null &&
              this.shipToZipCode.equals(other.getShipToZipCode()))) &&
            ((this.reissue==null && other.getReissue()==null) || 
             (this.reissue!=null &&
              this.reissue.equals(other.getReissue()))) &&
            ((this.skipACHLoadEffectiveDate==null && other.getSkipACHLoadEffectiveDate()==null) || 
             (this.skipACHLoadEffectiveDate!=null &&
              this.skipACHLoadEffectiveDate.equals(other.getSkipACHLoadEffectiveDate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCLIENTID() != null) {
            _hashCode += getCLIENTID().hashCode();
        }
        if (getCARDNUMBER() != null) {
            _hashCode += getCARDNUMBER().hashCode();
        }
        if (getFIRSTNAME() != null) {
            _hashCode += getFIRSTNAME().hashCode();
        }
        if (getMIDDLENAME() != null) {
            _hashCode += getMIDDLENAME().hashCode();
        }
        if (getLASTNAME() != null) {
            _hashCode += getLASTNAME().hashCode();
        }
        if (getDATEOFBIRTH() != null) {
            _hashCode += getDATEOFBIRTH().hashCode();
        }
        if (getGOVERNMENTID() != null) {
            _hashCode += getGOVERNMENTID().hashCode();
        }
        if (getIDNUMBER() != null) {
            _hashCode += getIDNUMBER().hashCode();
        }
        if (getIDISSUEDATE() != null) {
            _hashCode += getIDISSUEDATE().hashCode();
        }
        if (getIDEXPIRATIONDATE() != null) {
            _hashCode += getIDEXPIRATIONDATE().hashCode();
        }
        if (getIDISSUECOUNTRY() != null) {
            _hashCode += getIDISSUECOUNTRY().hashCode();
        }
        if (getIDISSUESTATE() != null) {
            _hashCode += getIDISSUESTATE().hashCode();
        }
        if (getADDRESS1() != null) {
            _hashCode += getADDRESS1().hashCode();
        }
        if (getADDRESSLINE2() != null) {
            _hashCode += getADDRESSLINE2().hashCode();
        }
        if (getCITY() != null) {
            _hashCode += getCITY().hashCode();
        }
        if (getSTATE() != null) {
            _hashCode += getSTATE().hashCode();
        }
        if (getCOUNTRY() != null) {
            _hashCode += getCOUNTRY().hashCode();
        }
        if (getPOSTALCODE() != null) {
            _hashCode += getPOSTALCODE().hashCode();
        }
        if (getHOMEPHONENUMBER() != null) {
            _hashCode += getHOMEPHONENUMBER().hashCode();
        }
        if (getEMAILID() != null) {
            _hashCode += getEMAILID().hashCode();
        }
        if (getManualCardStatus() != null) {
            _hashCode += getManualCardStatus().hashCode();
        }
        if (getUSERFIELD1() != null) {
            _hashCode += getUSERFIELD1().hashCode();
        }
        if (getUSERFIELD2() != null) {
            _hashCode += getUSERFIELD2().hashCode();
        }
        if (getUSERFIELD3() != null) {
            _hashCode += getUSERFIELD3().hashCode();
        }
        if (getUSERFIELD4() != null) {
            _hashCode += getUSERFIELD4().hashCode();
        }
        if (getUSERFIELD5() != null) {
            _hashCode += getUSERFIELD5().hashCode();
        }
        if (getHPhoneCountryCode() != null) {
            _hashCode += getHPhoneCountryCode().hashCode();
        }
        if (getHPhoneExtension() != null) {
            _hashCode += getHPhoneExtension().hashCode();
        }
        if (getSSNNUMBER() != null) {
            _hashCode += getSSNNUMBER().hashCode();
        }
        if (getShipToCmpnyName() != null) {
            _hashCode += getShipToCmpnyName().hashCode();
        }
        if (getShipToContactName() != null) {
            _hashCode += getShipToContactName().hashCode();
        }
        if (getShipToAddressOne() != null) {
            _hashCode += getShipToAddressOne().hashCode();
        }
        if (getShipToAddressTwo() != null) {
            _hashCode += getShipToAddressTwo().hashCode();
        }
        if (getShipToCity() != null) {
            _hashCode += getShipToCity().hashCode();
        }
        if (getShipToState() != null) {
            _hashCode += getShipToState().hashCode();
        }
        if (getShipToZipCode() != null) {
            _hashCode += getShipToZipCode().hashCode();
        }
        if (getReissue() != null) {
            _hashCode += getReissue().hashCode();
        }
        if (getSkipACHLoadEffectiveDate() != null) {
            _hashCode += getSkipACHLoadEffectiveDate().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardUpdateRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardUpdateRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENTID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRSTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FIRSTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MIDDLENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MIDDLENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DATEOFBIRTH");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DATEOFBIRTH"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("GOVERNMENTID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GOVERNMENTID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "IDNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDISSUEDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "IDISSUEDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDEXPIRATIONDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "IDEXPIRATIONDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDISSUECOUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "IDISSUECOUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IDISSUESTATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "IDISSUESTATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESSLINE2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESSLINE2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTALCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "POSTALCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HOMEPHONENUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "HOMEPHONENUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMAILID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "EMAILID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manualCardStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ManualCardStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USERFIELD1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USERFIELD1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USERFIELD2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USERFIELD2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USERFIELD3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USERFIELD3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USERFIELD4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USERFIELD4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USERFIELD5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USERFIELD5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HPhoneCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "HPhoneCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HPhoneExtension");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "HPhoneExtension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSNNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SSNNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToCmpnyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToCmpnyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToContactName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToContactName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddressOne");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToAddressOne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddressTwo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToAddressTwo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ShipToZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reissue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Reissue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skipACHLoadEffectiveDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SkipACHLoadEffectiveDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
