/**
 * SMSEnrollmentRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SMSEnrollmentRequest  implements java.io.Serializable {
    private java.lang.String ACCOUNT_NO;

    private java.lang.String CARD_NUMBER;

    private java.lang.String ADMIN_NO;

    private java.lang.String SMS_STATUS_SET;

    private java.lang.String MOBILE_NUMBER;

    private java.lang.String SMS_ADDRESS;

    private java.lang.String ENROLL_ALL_SMS;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public SMSEnrollmentRequest() {
    }

    public SMSEnrollmentRequest(
           java.lang.String ACCOUNT_NO,
           java.lang.String CARD_NUMBER,
           java.lang.String ADMIN_NO,
           java.lang.String SMS_STATUS_SET,
           java.lang.String MOBILE_NUMBER,
           java.lang.String SMS_ADDRESS,
           java.lang.String ENROLL_ALL_SMS,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ADMIN_NO = ADMIN_NO;
           this.SMS_STATUS_SET = SMS_STATUS_SET;
           this.MOBILE_NUMBER = MOBILE_NUMBER;
           this.SMS_ADDRESS = SMS_ADDRESS;
           this.ENROLL_ALL_SMS = ENROLL_ALL_SMS;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the ACCOUNT_NO value for this SMSEnrollmentRequest.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this SMSEnrollmentRequest.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the CARD_NUMBER value for this SMSEnrollmentRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this SMSEnrollmentRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ADMIN_NO value for this SMSEnrollmentRequest.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this SMSEnrollmentRequest.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the SMS_STATUS_SET value for this SMSEnrollmentRequest.
     * 
     * @return SMS_STATUS_SET
     */
    public java.lang.String getSMS_STATUS_SET() {
        return SMS_STATUS_SET;
    }


    /**
     * Sets the SMS_STATUS_SET value for this SMSEnrollmentRequest.
     * 
     * @param SMS_STATUS_SET
     */
    public void setSMS_STATUS_SET(java.lang.String SMS_STATUS_SET) {
        this.SMS_STATUS_SET = SMS_STATUS_SET;
    }


    /**
     * Gets the MOBILE_NUMBER value for this SMSEnrollmentRequest.
     * 
     * @return MOBILE_NUMBER
     */
    public java.lang.String getMOBILE_NUMBER() {
        return MOBILE_NUMBER;
    }


    /**
     * Sets the MOBILE_NUMBER value for this SMSEnrollmentRequest.
     * 
     * @param MOBILE_NUMBER
     */
    public void setMOBILE_NUMBER(java.lang.String MOBILE_NUMBER) {
        this.MOBILE_NUMBER = MOBILE_NUMBER;
    }


    /**
     * Gets the SMS_ADDRESS value for this SMSEnrollmentRequest.
     * 
     * @return SMS_ADDRESS
     */
    public java.lang.String getSMS_ADDRESS() {
        return SMS_ADDRESS;
    }


    /**
     * Sets the SMS_ADDRESS value for this SMSEnrollmentRequest.
     * 
     * @param SMS_ADDRESS
     */
    public void setSMS_ADDRESS(java.lang.String SMS_ADDRESS) {
        this.SMS_ADDRESS = SMS_ADDRESS;
    }


    /**
     * Gets the ENROLL_ALL_SMS value for this SMSEnrollmentRequest.
     * 
     * @return ENROLL_ALL_SMS
     */
    public java.lang.String getENROLL_ALL_SMS() {
        return ENROLL_ALL_SMS;
    }


    /**
     * Sets the ENROLL_ALL_SMS value for this SMSEnrollmentRequest.
     * 
     * @param ENROLL_ALL_SMS
     */
    public void setENROLL_ALL_SMS(java.lang.String ENROLL_ALL_SMS) {
        this.ENROLL_ALL_SMS = ENROLL_ALL_SMS;
    }


    /**
     * Gets the ANI value for this SMSEnrollmentRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this SMSEnrollmentRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this SMSEnrollmentRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this SMSEnrollmentRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSEnrollmentRequest)) return false;
        SMSEnrollmentRequest other = (SMSEnrollmentRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.SMS_STATUS_SET==null && other.getSMS_STATUS_SET()==null) || 
             (this.SMS_STATUS_SET!=null &&
              this.SMS_STATUS_SET.equals(other.getSMS_STATUS_SET()))) &&
            ((this.MOBILE_NUMBER==null && other.getMOBILE_NUMBER()==null) || 
             (this.MOBILE_NUMBER!=null &&
              this.MOBILE_NUMBER.equals(other.getMOBILE_NUMBER()))) &&
            ((this.SMS_ADDRESS==null && other.getSMS_ADDRESS()==null) || 
             (this.SMS_ADDRESS!=null &&
              this.SMS_ADDRESS.equals(other.getSMS_ADDRESS()))) &&
            ((this.ENROLL_ALL_SMS==null && other.getENROLL_ALL_SMS()==null) || 
             (this.ENROLL_ALL_SMS!=null &&
              this.ENROLL_ALL_SMS.equals(other.getENROLL_ALL_SMS()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getSMS_STATUS_SET() != null) {
            _hashCode += getSMS_STATUS_SET().hashCode();
        }
        if (getMOBILE_NUMBER() != null) {
            _hashCode += getMOBILE_NUMBER().hashCode();
        }
        if (getSMS_ADDRESS() != null) {
            _hashCode += getSMS_ADDRESS().hashCode();
        }
        if (getENROLL_ALL_SMS() != null) {
            _hashCode += getENROLL_ALL_SMS().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSEnrollmentRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_STATUS_SET");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_STATUS_SET"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILE_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MOBILE_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_ADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ENROLL_ALL_SMS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ENROLL_ALL_SMS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
