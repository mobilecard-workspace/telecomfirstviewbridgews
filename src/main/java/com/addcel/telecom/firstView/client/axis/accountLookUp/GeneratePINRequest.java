/**
 * GeneratePINRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class GeneratePINRequest  implements java.io.Serializable {
    private java.lang.String ACCOUNTNUMBER;

    private java.lang.String CARD_NUMBER;

    private java.lang.String ADMIN_NO;

    private java.lang.String SSNLast4Digits;

    private java.lang.String ZIP;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public GeneratePINRequest() {
    }

    public GeneratePINRequest(
           java.lang.String ACCOUNTNUMBER,
           java.lang.String CARD_NUMBER,
           java.lang.String ADMIN_NO,
           java.lang.String SSNLast4Digits,
           java.lang.String ZIP,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.ACCOUNTNUMBER = ACCOUNTNUMBER;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ADMIN_NO = ADMIN_NO;
           this.SSNLast4Digits = SSNLast4Digits;
           this.ZIP = ZIP;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the ACCOUNTNUMBER value for this GeneratePINRequest.
     * 
     * @return ACCOUNTNUMBER
     */
    public java.lang.String getACCOUNTNUMBER() {
        return ACCOUNTNUMBER;
    }


    /**
     * Sets the ACCOUNTNUMBER value for this GeneratePINRequest.
     * 
     * @param ACCOUNTNUMBER
     */
    public void setACCOUNTNUMBER(java.lang.String ACCOUNTNUMBER) {
        this.ACCOUNTNUMBER = ACCOUNTNUMBER;
    }


    /**
     * Gets the CARD_NUMBER value for this GeneratePINRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this GeneratePINRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ADMIN_NO value for this GeneratePINRequest.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this GeneratePINRequest.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the SSNLast4Digits value for this GeneratePINRequest.
     * 
     * @return SSNLast4Digits
     */
    public java.lang.String getSSNLast4Digits() {
        return SSNLast4Digits;
    }


    /**
     * Sets the SSNLast4Digits value for this GeneratePINRequest.
     * 
     * @param SSNLast4Digits
     */
    public void setSSNLast4Digits(java.lang.String SSNLast4Digits) {
        this.SSNLast4Digits = SSNLast4Digits;
    }


    /**
     * Gets the ZIP value for this GeneratePINRequest.
     * 
     * @return ZIP
     */
    public java.lang.String getZIP() {
        return ZIP;
    }


    /**
     * Sets the ZIP value for this GeneratePINRequest.
     * 
     * @param ZIP
     */
    public void setZIP(java.lang.String ZIP) {
        this.ZIP = ZIP;
    }


    /**
     * Gets the ANI value for this GeneratePINRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this GeneratePINRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this GeneratePINRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this GeneratePINRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GeneratePINRequest)) return false;
        GeneratePINRequest other = (GeneratePINRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ACCOUNTNUMBER==null && other.getACCOUNTNUMBER()==null) || 
             (this.ACCOUNTNUMBER!=null &&
              this.ACCOUNTNUMBER.equals(other.getACCOUNTNUMBER()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.SSNLast4Digits==null && other.getSSNLast4Digits()==null) || 
             (this.SSNLast4Digits!=null &&
              this.SSNLast4Digits.equals(other.getSSNLast4Digits()))) &&
            ((this.ZIP==null && other.getZIP()==null) || 
             (this.ZIP!=null &&
              this.ZIP.equals(other.getZIP()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getACCOUNTNUMBER() != null) {
            _hashCode += getACCOUNTNUMBER().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getSSNLast4Digits() != null) {
            _hashCode += getSSNLast4Digits().hashCode();
        }
        if (getZIP() != null) {
            _hashCode += getZIP().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GeneratePINRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "GeneratePINRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNTNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNTNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SSNLast4Digits");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SSNLast4Digits"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ZIP");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ZIP"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
