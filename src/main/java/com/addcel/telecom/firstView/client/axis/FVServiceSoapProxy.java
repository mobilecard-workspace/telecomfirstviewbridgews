package com.addcel.telecom.firstView.client.axis;

public class FVServiceSoapProxy implements com.addcel.telecom.firstView.client.axis.FVServiceSoap {
  private String _endpoint = null;
  private com.addcel.telecom.firstView.client.axis.FVServiceSoap fVServiceSoap = null;
  
  public FVServiceSoapProxy() {
    _initFVServiceSoapProxy();
  }
  
  public FVServiceSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initFVServiceSoapProxy();
  }
  
  private void _initFVServiceSoapProxy() {
    try {
      fVServiceSoap = (new com.addcel.telecom.firstView.client.axis.FVServiceLocator()).getFVServiceSoap();
      if (fVServiceSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fVServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fVServiceSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fVServiceSoap != null)
      ((javax.xml.rpc.Stub)fVServiceSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.firstView.client.axis.FVServiceSoap getFVServiceSoap() {
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap;
  }
  
  public com.addcel.telecom.firstView.client.axis.GetCvcResponse getCVC(com.addcel.telecom.firstView.client.axis.GetCvcRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.getCVC(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.ClearingResponse clearing(com.addcel.telecom.firstView.client.axis.ClearingRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.clearing(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.CardUpdateResponse cardUpdate(com.addcel.telecom.firstView.client.axis.CardUpdateRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.cardUpdate(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.ValidatePinResponse validatePin(com.addcel.telecom.firstView.client.axis.ValidatePinRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.validatePin(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.CreateCardResponse createCard(com.addcel.telecom.firstView.client.axis.CreateCardRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.createCard(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.PictureCardResponse pictureCard(com.addcel.telecom.firstView.client.axis.PictureCardRequest request) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.pictureCard(request);
  }
  
  public com.addcel.telecom.firstView.client.axis.GetStatusResponse getStatusDesc(com.addcel.telecom.firstView.client.axis.GetStatusRequest statReq) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.getStatusDesc(statReq);
  }
  
  public com.addcel.telecom.firstView.client.axis.CreateCIPCardResponse createCIPCard(com.addcel.telecom.firstView.client.axis.CreateCIPCardRequest CACr) throws java.rmi.RemoteException{
    if (fVServiceSoap == null)
      _initFVServiceSoapProxy();
    return fVServiceSoap.createCIPCard(CACr);
  }
  
  
}