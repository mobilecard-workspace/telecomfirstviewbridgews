//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CardUpdateRequest complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CardUpdateRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Addressline2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Cardnumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ClientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Country" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GovernmentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HPhoneCountryCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HPhoneExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="HomePhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdIssueCountry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdIssueDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdIssueState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="LastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ManualCardStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Reissue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToAddressOne" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToAddressTwo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToCmpnyName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToContactName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToState" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShipToZipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SsnNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="State" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Userfield1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Userfield2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Userfield3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Userfield4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Userfield5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardUpdateRequest", propOrder = {
    "address1",
    "addressline2",
    "cardnumber",
    "city",
    "clientId",
    "country",
    "dateOfBirth",
    "emailId",
    "firstName",
    "governmentId",
    "hPhoneCountryCode",
    "hPhoneExtension",
    "homePhoneNumber",
    "idExpirationDate",
    "idIssueCountry",
    "idIssueDate",
    "idIssueState",
    "idNumber",
    "lastName",
    "manualCardStatus",
    "middleName",
    "postalCode",
    "reissue",
    "shipToAddressOne",
    "shipToAddressTwo",
    "shipToCity",
    "shipToCmpnyName",
    "shipToContactName",
    "shipToState",
    "shipToZipCode",
    "ssnNumber",
    "state",
    "userfield1",
    "userfield2",
    "userfield3",
    "userfield4",
    "userfield5"
})
public class CardUpdateRequest {

    @XmlElement(name = "Address1")
    protected String address1;
    @XmlElement(name = "Addressline2")
    protected String addressline2;
    @XmlElement(name = "Cardnumber")
    protected String cardnumber;
    @XmlElement(name = "City")
    protected String city;
    @XmlElement(name = "ClientId")
    protected String clientId;
    @XmlElement(name = "Country")
    protected String country;
    @XmlElement(name = "DateOfBirth")
    protected String dateOfBirth;
    @XmlElement(name = "EmailId")
    protected String emailId;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "GovernmentId")
    protected String governmentId;
    @XmlElement(name = "HPhoneCountryCode")
    protected String hPhoneCountryCode;
    @XmlElement(name = "HPhoneExtension")
    protected String hPhoneExtension;
    @XmlElement(name = "HomePhoneNumber")
    protected String homePhoneNumber;
    @XmlElement(name = "IdExpirationDate")
    protected String idExpirationDate;
    @XmlElement(name = "IdIssueCountry")
    protected String idIssueCountry;
    @XmlElement(name = "IdIssueDate")
    protected String idIssueDate;
    @XmlElement(name = "IdIssueState")
    protected String idIssueState;
    @XmlElement(name = "IdNumber")
    protected String idNumber;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "ManualCardStatus")
    protected String manualCardStatus;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "PostalCode")
    protected String postalCode;
    @XmlElement(name = "Reissue")
    protected String reissue;
    @XmlElement(name = "ShipToAddressOne")
    protected String shipToAddressOne;
    @XmlElement(name = "ShipToAddressTwo")
    protected String shipToAddressTwo;
    @XmlElement(name = "ShipToCity")
    protected String shipToCity;
    @XmlElement(name = "ShipToCmpnyName")
    protected String shipToCmpnyName;
    @XmlElement(name = "ShipToContactName")
    protected String shipToContactName;
    @XmlElement(name = "ShipToState")
    protected String shipToState;
    @XmlElement(name = "ShipToZipCode")
    protected String shipToZipCode;
    @XmlElement(name = "SsnNumber")
    protected String ssnNumber;
    @XmlElement(name = "State")
    protected String state;
    @XmlElement(name = "Userfield1")
    protected String userfield1;
    @XmlElement(name = "Userfield2")
    protected String userfield2;
    @XmlElement(name = "Userfield3")
    protected String userfield3;
    @XmlElement(name = "Userfield4")
    protected String userfield4;
    @XmlElement(name = "Userfield5")
    protected String userfield5;

    /**
     * Obtiene el valor de la propiedad address1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Define el valor de la propiedad address1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Obtiene el valor de la propiedad addressline2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddressline2() {
        return addressline2;
    }

    /**
     * Define el valor de la propiedad addressline2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddressline2(String value) {
        this.addressline2 = value;
    }

    /**
     * Obtiene el valor de la propiedad cardnumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * Define el valor de la propiedad cardnumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardnumber(String value) {
        this.cardnumber = value;
    }

    /**
     * Obtiene el valor de la propiedad city.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Define el valor de la propiedad city.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Obtiene el valor de la propiedad clientId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Define el valor de la propiedad clientId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Obtiene el valor de la propiedad country.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Define el valor de la propiedad country.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Obtiene el valor de la propiedad dateOfBirth.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Define el valor de la propiedad dateOfBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfBirth(String value) {
        this.dateOfBirth = value;
    }

    /**
     * Obtiene el valor de la propiedad emailId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * Define el valor de la propiedad emailId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailId(String value) {
        this.emailId = value;
    }

    /**
     * Obtiene el valor de la propiedad firstName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Define el valor de la propiedad firstName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Obtiene el valor de la propiedad governmentId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovernmentId() {
        return governmentId;
    }

    /**
     * Define el valor de la propiedad governmentId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovernmentId(String value) {
        this.governmentId = value;
    }

    /**
     * Obtiene el valor de la propiedad hPhoneCountryCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPhoneCountryCode() {
        return hPhoneCountryCode;
    }

    /**
     * Define el valor de la propiedad hPhoneCountryCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPhoneCountryCode(String value) {
        this.hPhoneCountryCode = value;
    }

    /**
     * Obtiene el valor de la propiedad hPhoneExtension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPhoneExtension() {
        return hPhoneExtension;
    }

    /**
     * Define el valor de la propiedad hPhoneExtension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPhoneExtension(String value) {
        this.hPhoneExtension = value;
    }

    /**
     * Obtiene el valor de la propiedad homePhoneNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomePhoneNumber() {
        return homePhoneNumber;
    }

    /**
     * Define el valor de la propiedad homePhoneNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomePhoneNumber(String value) {
        this.homePhoneNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpirationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpirationDate() {
        return idExpirationDate;
    }

    /**
     * Define el valor de la propiedad idExpirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpirationDate(String value) {
        this.idExpirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad idIssueCountry.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdIssueCountry() {
        return idIssueCountry;
    }

    /**
     * Define el valor de la propiedad idIssueCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdIssueCountry(String value) {
        this.idIssueCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad idIssueDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdIssueDate() {
        return idIssueDate;
    }

    /**
     * Define el valor de la propiedad idIssueDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdIssueDate(String value) {
        this.idIssueDate = value;
    }

    /**
     * Obtiene el valor de la propiedad idIssueState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdIssueState() {
        return idIssueState;
    }

    /**
     * Define el valor de la propiedad idIssueState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdIssueState(String value) {
        this.idIssueState = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Define el valor de la propiedad idNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad lastName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Define el valor de la propiedad lastName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Obtiene el valor de la propiedad manualCardStatus.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getManualCardStatus() {
        return manualCardStatus;
    }

    /**
     * Define el valor de la propiedad manualCardStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setManualCardStatus(String value) {
        this.manualCardStatus = value;
    }

    /**
     * Obtiene el valor de la propiedad middleName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Define el valor de la propiedad middleName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Obtiene el valor de la propiedad postalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Define el valor de la propiedad postalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Obtiene el valor de la propiedad reissue.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReissue() {
        return reissue;
    }

    /**
     * Define el valor de la propiedad reissue.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReissue(String value) {
        this.reissue = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToAddressOne.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddressOne() {
        return shipToAddressOne;
    }

    /**
     * Define el valor de la propiedad shipToAddressOne.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddressOne(String value) {
        this.shipToAddressOne = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToAddressTwo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddressTwo() {
        return shipToAddressTwo;
    }

    /**
     * Define el valor de la propiedad shipToAddressTwo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddressTwo(String value) {
        this.shipToAddressTwo = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToCity.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToCity() {
        return shipToCity;
    }

    /**
     * Define el valor de la propiedad shipToCity.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToCity(String value) {
        this.shipToCity = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToCmpnyName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToCmpnyName() {
        return shipToCmpnyName;
    }

    /**
     * Define el valor de la propiedad shipToCmpnyName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToCmpnyName(String value) {
        this.shipToCmpnyName = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToContactName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToContactName() {
        return shipToContactName;
    }

    /**
     * Define el valor de la propiedad shipToContactName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToContactName(String value) {
        this.shipToContactName = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToState.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToState() {
        return shipToState;
    }

    /**
     * Define el valor de la propiedad shipToState.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToState(String value) {
        this.shipToState = value;
    }

    /**
     * Obtiene el valor de la propiedad shipToZipCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToZipCode() {
        return shipToZipCode;
    }

    /**
     * Define el valor de la propiedad shipToZipCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToZipCode(String value) {
        this.shipToZipCode = value;
    }

    /**
     * Obtiene el valor de la propiedad ssnNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSsnNumber() {
        return ssnNumber;
    }

    /**
     * Define el valor de la propiedad ssnNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSsnNumber(String value) {
        this.ssnNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad state.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * Define el valor de la propiedad state.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Obtiene el valor de la propiedad userfield1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserfield1() {
        return userfield1;
    }

    /**
     * Define el valor de la propiedad userfield1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserfield1(String value) {
        this.userfield1 = value;
    }

    /**
     * Obtiene el valor de la propiedad userfield2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserfield2() {
        return userfield2;
    }

    /**
     * Define el valor de la propiedad userfield2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserfield2(String value) {
        this.userfield2 = value;
    }

    /**
     * Obtiene el valor de la propiedad userfield3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserfield3() {
        return userfield3;
    }

    /**
     * Define el valor de la propiedad userfield3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserfield3(String value) {
        this.userfield3 = value;
    }

    /**
     * Obtiene el valor de la propiedad userfield4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserfield4() {
        return userfield4;
    }

    /**
     * Define el valor de la propiedad userfield4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserfield4(String value) {
        this.userfield4 = value;
    }

    /**
     * Obtiene el valor de la propiedad userfield5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserfield5() {
        return userfield5;
    }

    /**
     * Define el valor de la propiedad userfield5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserfield5(String value) {
        this.userfield5 = value;
    }

}
