/**
 * GetCvcRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class GetCvcRequest  implements java.io.Serializable {
    private java.lang.String ani;

    private java.lang.String cardNumber;

    private java.lang.String dnis;

    public GetCvcRequest() {
    }

    public GetCvcRequest(
           java.lang.String ani,
           java.lang.String cardNumber,
           java.lang.String dnis) {
           this.ani = ani;
           this.cardNumber = cardNumber;
           this.dnis = dnis;
    }


    /**
     * Gets the ani value for this GetCvcRequest.
     * 
     * @return ani
     */
    public java.lang.String getAni() {
        return ani;
    }


    /**
     * Sets the ani value for this GetCvcRequest.
     * 
     * @param ani
     */
    public void setAni(java.lang.String ani) {
        this.ani = ani;
    }


    /**
     * Gets the cardNumber value for this GetCvcRequest.
     * 
     * @return cardNumber
     */
    public java.lang.String getCardNumber() {
        return cardNumber;
    }


    /**
     * Sets the cardNumber value for this GetCvcRequest.
     * 
     * @param cardNumber
     */
    public void setCardNumber(java.lang.String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /**
     * Gets the dnis value for this GetCvcRequest.
     * 
     * @return dnis
     */
    public java.lang.String getDnis() {
        return dnis;
    }


    /**
     * Sets the dnis value for this GetCvcRequest.
     * 
     * @param dnis
     */
    public void setDnis(java.lang.String dnis) {
        this.dnis = dnis;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCvcRequest)) return false;
        GetCvcRequest other = (GetCvcRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ani==null && other.getAni()==null) || 
             (this.ani!=null &&
              this.ani.equals(other.getAni()))) &&
            ((this.cardNumber==null && other.getCardNumber()==null) || 
             (this.cardNumber!=null &&
              this.cardNumber.equals(other.getCardNumber()))) &&
            ((this.dnis==null && other.getDnis()==null) || 
             (this.dnis!=null &&
              this.dnis.equals(other.getDnis())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAni() != null) {
            _hashCode += getAni().hashCode();
        }
        if (getCardNumber() != null) {
            _hashCode += getCardNumber().hashCode();
        }
        if (getDnis() != null) {
            _hashCode += getDnis().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCvcRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "GetCvcRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ani");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Ani"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "CardNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dnis");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Dnis"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
