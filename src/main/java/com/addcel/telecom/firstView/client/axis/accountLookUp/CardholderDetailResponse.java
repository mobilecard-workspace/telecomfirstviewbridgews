/**
 * CardholderDetailResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardholderDetailResponse  implements java.io.Serializable {
    private java.lang.String ERROR_FOUND;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERRMSG;

    private java.lang.String CARDHOLDER_IDENTIFIED;

    private java.lang.String CURRENT_CARD_BALANCE;

    private java.lang.String FIRSTNAME;

    private java.lang.String MIDDLENAME;

    private java.lang.String LASTNAME;

    private java.lang.String ADDRESS1;

    private java.lang.String ADDRESS2;

    private java.lang.String CITY;

    private java.lang.String STATE;

    private java.lang.String COUNTRY_CODE;

    private java.lang.String POSTALCODE;

    private java.lang.String ID_CODE;

    private java.lang.String ID_NUMBER;

    private java.lang.String ID_COUNTRY;

    private java.lang.String DOB;

    private java.lang.String PHONE;

    private java.lang.String EMAIL;

    public CardholderDetailResponse() {
    }

    public CardholderDetailResponse(
           java.lang.String ERROR_FOUND,
           java.lang.String ERR_NUMBER,
           java.lang.String ERRMSG,
           java.lang.String CARDHOLDER_IDENTIFIED,
           java.lang.String CURRENT_CARD_BALANCE,
           java.lang.String FIRSTNAME,
           java.lang.String MIDDLENAME,
           java.lang.String LASTNAME,
           java.lang.String ADDRESS1,
           java.lang.String ADDRESS2,
           java.lang.String CITY,
           java.lang.String STATE,
           java.lang.String COUNTRY_CODE,
           java.lang.String POSTALCODE,
           java.lang.String ID_CODE,
           java.lang.String ID_NUMBER,
           java.lang.String ID_COUNTRY,
           java.lang.String DOB,
           java.lang.String PHONE,
           java.lang.String EMAIL) {
           this.ERROR_FOUND = ERROR_FOUND;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERRMSG = ERRMSG;
           this.CARDHOLDER_IDENTIFIED = CARDHOLDER_IDENTIFIED;
           this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
           this.FIRSTNAME = FIRSTNAME;
           this.MIDDLENAME = MIDDLENAME;
           this.LASTNAME = LASTNAME;
           this.ADDRESS1 = ADDRESS1;
           this.ADDRESS2 = ADDRESS2;
           this.CITY = CITY;
           this.STATE = STATE;
           this.COUNTRY_CODE = COUNTRY_CODE;
           this.POSTALCODE = POSTALCODE;
           this.ID_CODE = ID_CODE;
           this.ID_NUMBER = ID_NUMBER;
           this.ID_COUNTRY = ID_COUNTRY;
           this.DOB = DOB;
           this.PHONE = PHONE;
           this.EMAIL = EMAIL;
    }


    /**
     * Gets the ERROR_FOUND value for this CardholderDetailResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this CardholderDetailResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the ERR_NUMBER value for this CardholderDetailResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this CardholderDetailResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERRMSG value for this CardholderDetailResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this CardholderDetailResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the CARDHOLDER_IDENTIFIED value for this CardholderDetailResponse.
     * 
     * @return CARDHOLDER_IDENTIFIED
     */
    public java.lang.String getCARDHOLDER_IDENTIFIED() {
        return CARDHOLDER_IDENTIFIED;
    }


    /**
     * Sets the CARDHOLDER_IDENTIFIED value for this CardholderDetailResponse.
     * 
     * @param CARDHOLDER_IDENTIFIED
     */
    public void setCARDHOLDER_IDENTIFIED(java.lang.String CARDHOLDER_IDENTIFIED) {
        this.CARDHOLDER_IDENTIFIED = CARDHOLDER_IDENTIFIED;
    }


    /**
     * Gets the CURRENT_CARD_BALANCE value for this CardholderDetailResponse.
     * 
     * @return CURRENT_CARD_BALANCE
     */
    public java.lang.String getCURRENT_CARD_BALANCE() {
        return CURRENT_CARD_BALANCE;
    }


    /**
     * Sets the CURRENT_CARD_BALANCE value for this CardholderDetailResponse.
     * 
     * @param CURRENT_CARD_BALANCE
     */
    public void setCURRENT_CARD_BALANCE(java.lang.String CURRENT_CARD_BALANCE) {
        this.CURRENT_CARD_BALANCE = CURRENT_CARD_BALANCE;
    }


    /**
     * Gets the FIRSTNAME value for this CardholderDetailResponse.
     * 
     * @return FIRSTNAME
     */
    public java.lang.String getFIRSTNAME() {
        return FIRSTNAME;
    }


    /**
     * Sets the FIRSTNAME value for this CardholderDetailResponse.
     * 
     * @param FIRSTNAME
     */
    public void setFIRSTNAME(java.lang.String FIRSTNAME) {
        this.FIRSTNAME = FIRSTNAME;
    }


    /**
     * Gets the MIDDLENAME value for this CardholderDetailResponse.
     * 
     * @return MIDDLENAME
     */
    public java.lang.String getMIDDLENAME() {
        return MIDDLENAME;
    }


    /**
     * Sets the MIDDLENAME value for this CardholderDetailResponse.
     * 
     * @param MIDDLENAME
     */
    public void setMIDDLENAME(java.lang.String MIDDLENAME) {
        this.MIDDLENAME = MIDDLENAME;
    }


    /**
     * Gets the LASTNAME value for this CardholderDetailResponse.
     * 
     * @return LASTNAME
     */
    public java.lang.String getLASTNAME() {
        return LASTNAME;
    }


    /**
     * Sets the LASTNAME value for this CardholderDetailResponse.
     * 
     * @param LASTNAME
     */
    public void setLASTNAME(java.lang.String LASTNAME) {
        this.LASTNAME = LASTNAME;
    }


    /**
     * Gets the ADDRESS1 value for this CardholderDetailResponse.
     * 
     * @return ADDRESS1
     */
    public java.lang.String getADDRESS1() {
        return ADDRESS1;
    }


    /**
     * Sets the ADDRESS1 value for this CardholderDetailResponse.
     * 
     * @param ADDRESS1
     */
    public void setADDRESS1(java.lang.String ADDRESS1) {
        this.ADDRESS1 = ADDRESS1;
    }


    /**
     * Gets the ADDRESS2 value for this CardholderDetailResponse.
     * 
     * @return ADDRESS2
     */
    public java.lang.String getADDRESS2() {
        return ADDRESS2;
    }


    /**
     * Sets the ADDRESS2 value for this CardholderDetailResponse.
     * 
     * @param ADDRESS2
     */
    public void setADDRESS2(java.lang.String ADDRESS2) {
        this.ADDRESS2 = ADDRESS2;
    }


    /**
     * Gets the CITY value for this CardholderDetailResponse.
     * 
     * @return CITY
     */
    public java.lang.String getCITY() {
        return CITY;
    }


    /**
     * Sets the CITY value for this CardholderDetailResponse.
     * 
     * @param CITY
     */
    public void setCITY(java.lang.String CITY) {
        this.CITY = CITY;
    }


    /**
     * Gets the STATE value for this CardholderDetailResponse.
     * 
     * @return STATE
     */
    public java.lang.String getSTATE() {
        return STATE;
    }


    /**
     * Sets the STATE value for this CardholderDetailResponse.
     * 
     * @param STATE
     */
    public void setSTATE(java.lang.String STATE) {
        this.STATE = STATE;
    }


    /**
     * Gets the COUNTRY_CODE value for this CardholderDetailResponse.
     * 
     * @return COUNTRY_CODE
     */
    public java.lang.String getCOUNTRY_CODE() {
        return COUNTRY_CODE;
    }


    /**
     * Sets the COUNTRY_CODE value for this CardholderDetailResponse.
     * 
     * @param COUNTRY_CODE
     */
    public void setCOUNTRY_CODE(java.lang.String COUNTRY_CODE) {
        this.COUNTRY_CODE = COUNTRY_CODE;
    }


    /**
     * Gets the POSTALCODE value for this CardholderDetailResponse.
     * 
     * @return POSTALCODE
     */
    public java.lang.String getPOSTALCODE() {
        return POSTALCODE;
    }


    /**
     * Sets the POSTALCODE value for this CardholderDetailResponse.
     * 
     * @param POSTALCODE
     */
    public void setPOSTALCODE(java.lang.String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }


    /**
     * Gets the ID_CODE value for this CardholderDetailResponse.
     * 
     * @return ID_CODE
     */
    public java.lang.String getID_CODE() {
        return ID_CODE;
    }


    /**
     * Sets the ID_CODE value for this CardholderDetailResponse.
     * 
     * @param ID_CODE
     */
    public void setID_CODE(java.lang.String ID_CODE) {
        this.ID_CODE = ID_CODE;
    }


    /**
     * Gets the ID_NUMBER value for this CardholderDetailResponse.
     * 
     * @return ID_NUMBER
     */
    public java.lang.String getID_NUMBER() {
        return ID_NUMBER;
    }


    /**
     * Sets the ID_NUMBER value for this CardholderDetailResponse.
     * 
     * @param ID_NUMBER
     */
    public void setID_NUMBER(java.lang.String ID_NUMBER) {
        this.ID_NUMBER = ID_NUMBER;
    }


    /**
     * Gets the ID_COUNTRY value for this CardholderDetailResponse.
     * 
     * @return ID_COUNTRY
     */
    public java.lang.String getID_COUNTRY() {
        return ID_COUNTRY;
    }


    /**
     * Sets the ID_COUNTRY value for this CardholderDetailResponse.
     * 
     * @param ID_COUNTRY
     */
    public void setID_COUNTRY(java.lang.String ID_COUNTRY) {
        this.ID_COUNTRY = ID_COUNTRY;
    }


    /**
     * Gets the DOB value for this CardholderDetailResponse.
     * 
     * @return DOB
     */
    public java.lang.String getDOB() {
        return DOB;
    }


    /**
     * Sets the DOB value for this CardholderDetailResponse.
     * 
     * @param DOB
     */
    public void setDOB(java.lang.String DOB) {
        this.DOB = DOB;
    }


    /**
     * Gets the PHONE value for this CardholderDetailResponse.
     * 
     * @return PHONE
     */
    public java.lang.String getPHONE() {
        return PHONE;
    }


    /**
     * Sets the PHONE value for this CardholderDetailResponse.
     * 
     * @param PHONE
     */
    public void setPHONE(java.lang.String PHONE) {
        this.PHONE = PHONE;
    }


    /**
     * Gets the EMAIL value for this CardholderDetailResponse.
     * 
     * @return EMAIL
     */
    public java.lang.String getEMAIL() {
        return EMAIL;
    }


    /**
     * Sets the EMAIL value for this CardholderDetailResponse.
     * 
     * @param EMAIL
     */
    public void setEMAIL(java.lang.String EMAIL) {
        this.EMAIL = EMAIL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardholderDetailResponse)) return false;
        CardholderDetailResponse other = (CardholderDetailResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.CARDHOLDER_IDENTIFIED==null && other.getCARDHOLDER_IDENTIFIED()==null) || 
             (this.CARDHOLDER_IDENTIFIED!=null &&
              this.CARDHOLDER_IDENTIFIED.equals(other.getCARDHOLDER_IDENTIFIED()))) &&
            ((this.CURRENT_CARD_BALANCE==null && other.getCURRENT_CARD_BALANCE()==null) || 
             (this.CURRENT_CARD_BALANCE!=null &&
              this.CURRENT_CARD_BALANCE.equals(other.getCURRENT_CARD_BALANCE()))) &&
            ((this.FIRSTNAME==null && other.getFIRSTNAME()==null) || 
             (this.FIRSTNAME!=null &&
              this.FIRSTNAME.equals(other.getFIRSTNAME()))) &&
            ((this.MIDDLENAME==null && other.getMIDDLENAME()==null) || 
             (this.MIDDLENAME!=null &&
              this.MIDDLENAME.equals(other.getMIDDLENAME()))) &&
            ((this.LASTNAME==null && other.getLASTNAME()==null) || 
             (this.LASTNAME!=null &&
              this.LASTNAME.equals(other.getLASTNAME()))) &&
            ((this.ADDRESS1==null && other.getADDRESS1()==null) || 
             (this.ADDRESS1!=null &&
              this.ADDRESS1.equals(other.getADDRESS1()))) &&
            ((this.ADDRESS2==null && other.getADDRESS2()==null) || 
             (this.ADDRESS2!=null &&
              this.ADDRESS2.equals(other.getADDRESS2()))) &&
            ((this.CITY==null && other.getCITY()==null) || 
             (this.CITY!=null &&
              this.CITY.equals(other.getCITY()))) &&
            ((this.STATE==null && other.getSTATE()==null) || 
             (this.STATE!=null &&
              this.STATE.equals(other.getSTATE()))) &&
            ((this.COUNTRY_CODE==null && other.getCOUNTRY_CODE()==null) || 
             (this.COUNTRY_CODE!=null &&
              this.COUNTRY_CODE.equals(other.getCOUNTRY_CODE()))) &&
            ((this.POSTALCODE==null && other.getPOSTALCODE()==null) || 
             (this.POSTALCODE!=null &&
              this.POSTALCODE.equals(other.getPOSTALCODE()))) &&
            ((this.ID_CODE==null && other.getID_CODE()==null) || 
             (this.ID_CODE!=null &&
              this.ID_CODE.equals(other.getID_CODE()))) &&
            ((this.ID_NUMBER==null && other.getID_NUMBER()==null) || 
             (this.ID_NUMBER!=null &&
              this.ID_NUMBER.equals(other.getID_NUMBER()))) &&
            ((this.ID_COUNTRY==null && other.getID_COUNTRY()==null) || 
             (this.ID_COUNTRY!=null &&
              this.ID_COUNTRY.equals(other.getID_COUNTRY()))) &&
            ((this.DOB==null && other.getDOB()==null) || 
             (this.DOB!=null &&
              this.DOB.equals(other.getDOB()))) &&
            ((this.PHONE==null && other.getPHONE()==null) || 
             (this.PHONE!=null &&
              this.PHONE.equals(other.getPHONE()))) &&
            ((this.EMAIL==null && other.getEMAIL()==null) || 
             (this.EMAIL!=null &&
              this.EMAIL.equals(other.getEMAIL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getCARDHOLDER_IDENTIFIED() != null) {
            _hashCode += getCARDHOLDER_IDENTIFIED().hashCode();
        }
        if (getCURRENT_CARD_BALANCE() != null) {
            _hashCode += getCURRENT_CARD_BALANCE().hashCode();
        }
        if (getFIRSTNAME() != null) {
            _hashCode += getFIRSTNAME().hashCode();
        }
        if (getMIDDLENAME() != null) {
            _hashCode += getMIDDLENAME().hashCode();
        }
        if (getLASTNAME() != null) {
            _hashCode += getLASTNAME().hashCode();
        }
        if (getADDRESS1() != null) {
            _hashCode += getADDRESS1().hashCode();
        }
        if (getADDRESS2() != null) {
            _hashCode += getADDRESS2().hashCode();
        }
        if (getCITY() != null) {
            _hashCode += getCITY().hashCode();
        }
        if (getSTATE() != null) {
            _hashCode += getSTATE().hashCode();
        }
        if (getCOUNTRY_CODE() != null) {
            _hashCode += getCOUNTRY_CODE().hashCode();
        }
        if (getPOSTALCODE() != null) {
            _hashCode += getPOSTALCODE().hashCode();
        }
        if (getID_CODE() != null) {
            _hashCode += getID_CODE().hashCode();
        }
        if (getID_NUMBER() != null) {
            _hashCode += getID_NUMBER().hashCode();
        }
        if (getID_COUNTRY() != null) {
            _hashCode += getID_COUNTRY().hashCode();
        }
        if (getDOB() != null) {
            _hashCode += getDOB().hashCode();
        }
        if (getPHONE() != null) {
            _hashCode += getPHONE().hashCode();
        }
        if (getEMAIL() != null) {
            _hashCode += getEMAIL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardholderDetailResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardholderDetailResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDHOLDER_IDENTIFIED");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDHOLDER_IDENTIFIED"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CURRENT_CARD_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CURRENT_CARD_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FIRSTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FIRSTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MIDDLENAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MIDDLENAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTNAME");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTNAME"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADDRESS2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADDRESS2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("COUNTRY_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "COUNTRY_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("POSTALCODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "POSTALCODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_CODE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ID_CODE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ID_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ID_COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PHONE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PHONE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMAIL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "EMAIL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
