/**
 * ClearingResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ClearingResponse  implements java.io.Serializable {
    private java.lang.String messageTypeIdentifier;

    private java.lang.String custNbr;

    private java.lang.String primaryAccountNumber;

    private java.lang.String tranType;

    private java.lang.String transactionAmount;

    private java.lang.String transmissionDateTime;

    private java.lang.String lineItemSeqNumber;

    private java.lang.String inventoryCode;

    private java.lang.String quantity;

    private java.lang.String unitPrice;

    private java.lang.String creditPlanMaster;

    private java.lang.String transactionDescription;

    private java.lang.String transactionCurrencyCode;

    private java.lang.String specialMerchantIdentifier;

    private java.lang.String cardAcceptorIdCode;

    private java.lang.String cardAcceptorTerminalID;

    private java.lang.String cardAcceptorBusinessCode;

    private java.lang.String dateTimeLocalTransaction;

    private java.lang.String systemTraceAuditNumber;

    private java.lang.String merchantType;

    private java.lang.String retrievalReferenceNumber;

    private java.lang.String approvalCode;

    private java.lang.String transactionFeeBnp;

    private java.lang.String lateFeeBnp;

    private java.lang.String membershipFeeBnp;

    private java.lang.String overLimitFeeBnp;

    private java.lang.String insufficientFundFeeBnp;

    private java.lang.String collectionFeeBnp;

    private java.lang.String recoveryFeeBnp;

    private java.lang.String insuranceBnp;

    private java.lang.String interestBnp;

    private java.lang.String currentBalance;

    private java.lang.String principal;

    private java.lang.String daysDelinquent;

    private java.lang.String amountDue;

    private java.lang.String pastDue;

    private java.lang.String paymentDueDate;

    private java.lang.String postingFlag;

    private java.lang.String postingNote;

    private java.lang.String ADMIN_NO;

    private java.lang.String TRANSACTION_ID;

    public ClearingResponse() {
    }

    public ClearingResponse(
           java.lang.String messageTypeIdentifier,
           java.lang.String custNbr,
           java.lang.String primaryAccountNumber,
           java.lang.String tranType,
           java.lang.String transactionAmount,
           java.lang.String transmissionDateTime,
           java.lang.String lineItemSeqNumber,
           java.lang.String inventoryCode,
           java.lang.String quantity,
           java.lang.String unitPrice,
           java.lang.String creditPlanMaster,
           java.lang.String transactionDescription,
           java.lang.String transactionCurrencyCode,
           java.lang.String specialMerchantIdentifier,
           java.lang.String cardAcceptorIdCode,
           java.lang.String cardAcceptorTerminalID,
           java.lang.String cardAcceptorBusinessCode,
           java.lang.String dateTimeLocalTransaction,
           java.lang.String systemTraceAuditNumber,
           java.lang.String merchantType,
           java.lang.String retrievalReferenceNumber,
           java.lang.String approvalCode,
           java.lang.String transactionFeeBnp,
           java.lang.String lateFeeBnp,
           java.lang.String membershipFeeBnp,
           java.lang.String overLimitFeeBnp,
           java.lang.String insufficientFundFeeBnp,
           java.lang.String collectionFeeBnp,
           java.lang.String recoveryFeeBnp,
           java.lang.String insuranceBnp,
           java.lang.String interestBnp,
           java.lang.String currentBalance,
           java.lang.String principal,
           java.lang.String daysDelinquent,
           java.lang.String amountDue,
           java.lang.String pastDue,
           java.lang.String paymentDueDate,
           java.lang.String postingFlag,
           java.lang.String postingNote,
           java.lang.String ADMIN_NO,
           java.lang.String TRANSACTION_ID) {
           this.messageTypeIdentifier = messageTypeIdentifier;
           this.custNbr = custNbr;
           this.primaryAccountNumber = primaryAccountNumber;
           this.tranType = tranType;
           this.transactionAmount = transactionAmount;
           this.transmissionDateTime = transmissionDateTime;
           this.lineItemSeqNumber = lineItemSeqNumber;
           this.inventoryCode = inventoryCode;
           this.quantity = quantity;
           this.unitPrice = unitPrice;
           this.creditPlanMaster = creditPlanMaster;
           this.transactionDescription = transactionDescription;
           this.transactionCurrencyCode = transactionCurrencyCode;
           this.specialMerchantIdentifier = specialMerchantIdentifier;
           this.cardAcceptorIdCode = cardAcceptorIdCode;
           this.cardAcceptorTerminalID = cardAcceptorTerminalID;
           this.cardAcceptorBusinessCode = cardAcceptorBusinessCode;
           this.dateTimeLocalTransaction = dateTimeLocalTransaction;
           this.systemTraceAuditNumber = systemTraceAuditNumber;
           this.merchantType = merchantType;
           this.retrievalReferenceNumber = retrievalReferenceNumber;
           this.approvalCode = approvalCode;
           this.transactionFeeBnp = transactionFeeBnp;
           this.lateFeeBnp = lateFeeBnp;
           this.membershipFeeBnp = membershipFeeBnp;
           this.overLimitFeeBnp = overLimitFeeBnp;
           this.insufficientFundFeeBnp = insufficientFundFeeBnp;
           this.collectionFeeBnp = collectionFeeBnp;
           this.recoveryFeeBnp = recoveryFeeBnp;
           this.insuranceBnp = insuranceBnp;
           this.interestBnp = interestBnp;
           this.currentBalance = currentBalance;
           this.principal = principal;
           this.daysDelinquent = daysDelinquent;
           this.amountDue = amountDue;
           this.pastDue = pastDue;
           this.paymentDueDate = paymentDueDate;
           this.postingFlag = postingFlag;
           this.postingNote = postingNote;
           this.ADMIN_NO = ADMIN_NO;
           this.TRANSACTION_ID = TRANSACTION_ID;
    }


    /**
     * Gets the messageTypeIdentifier value for this ClearingResponse.
     * 
     * @return messageTypeIdentifier
     */
    public java.lang.String getMessageTypeIdentifier() {
        return messageTypeIdentifier;
    }


    /**
     * Sets the messageTypeIdentifier value for this ClearingResponse.
     * 
     * @param messageTypeIdentifier
     */
    public void setMessageTypeIdentifier(java.lang.String messageTypeIdentifier) {
        this.messageTypeIdentifier = messageTypeIdentifier;
    }


    /**
     * Gets the custNbr value for this ClearingResponse.
     * 
     * @return custNbr
     */
    public java.lang.String getCustNbr() {
        return custNbr;
    }


    /**
     * Sets the custNbr value for this ClearingResponse.
     * 
     * @param custNbr
     */
    public void setCustNbr(java.lang.String custNbr) {
        this.custNbr = custNbr;
    }


    /**
     * Gets the primaryAccountNumber value for this ClearingResponse.
     * 
     * @return primaryAccountNumber
     */
    public java.lang.String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }


    /**
     * Sets the primaryAccountNumber value for this ClearingResponse.
     * 
     * @param primaryAccountNumber
     */
    public void setPrimaryAccountNumber(java.lang.String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }


    /**
     * Gets the tranType value for this ClearingResponse.
     * 
     * @return tranType
     */
    public java.lang.String getTranType() {
        return tranType;
    }


    /**
     * Sets the tranType value for this ClearingResponse.
     * 
     * @param tranType
     */
    public void setTranType(java.lang.String tranType) {
        this.tranType = tranType;
    }


    /**
     * Gets the transactionAmount value for this ClearingResponse.
     * 
     * @return transactionAmount
     */
    public java.lang.String getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this ClearingResponse.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.lang.String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transmissionDateTime value for this ClearingResponse.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this ClearingResponse.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the lineItemSeqNumber value for this ClearingResponse.
     * 
     * @return lineItemSeqNumber
     */
    public java.lang.String getLineItemSeqNumber() {
        return lineItemSeqNumber;
    }


    /**
     * Sets the lineItemSeqNumber value for this ClearingResponse.
     * 
     * @param lineItemSeqNumber
     */
    public void setLineItemSeqNumber(java.lang.String lineItemSeqNumber) {
        this.lineItemSeqNumber = lineItemSeqNumber;
    }


    /**
     * Gets the inventoryCode value for this ClearingResponse.
     * 
     * @return inventoryCode
     */
    public java.lang.String getInventoryCode() {
        return inventoryCode;
    }


    /**
     * Sets the inventoryCode value for this ClearingResponse.
     * 
     * @param inventoryCode
     */
    public void setInventoryCode(java.lang.String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }


    /**
     * Gets the quantity value for this ClearingResponse.
     * 
     * @return quantity
     */
    public java.lang.String getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this ClearingResponse.
     * 
     * @param quantity
     */
    public void setQuantity(java.lang.String quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the unitPrice value for this ClearingResponse.
     * 
     * @return unitPrice
     */
    public java.lang.String getUnitPrice() {
        return unitPrice;
    }


    /**
     * Sets the unitPrice value for this ClearingResponse.
     * 
     * @param unitPrice
     */
    public void setUnitPrice(java.lang.String unitPrice) {
        this.unitPrice = unitPrice;
    }


    /**
     * Gets the creditPlanMaster value for this ClearingResponse.
     * 
     * @return creditPlanMaster
     */
    public java.lang.String getCreditPlanMaster() {
        return creditPlanMaster;
    }


    /**
     * Sets the creditPlanMaster value for this ClearingResponse.
     * 
     * @param creditPlanMaster
     */
    public void setCreditPlanMaster(java.lang.String creditPlanMaster) {
        this.creditPlanMaster = creditPlanMaster;
    }


    /**
     * Gets the transactionDescription value for this ClearingResponse.
     * 
     * @return transactionDescription
     */
    public java.lang.String getTransactionDescription() {
        return transactionDescription;
    }


    /**
     * Sets the transactionDescription value for this ClearingResponse.
     * 
     * @param transactionDescription
     */
    public void setTransactionDescription(java.lang.String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }


    /**
     * Gets the transactionCurrencyCode value for this ClearingResponse.
     * 
     * @return transactionCurrencyCode
     */
    public java.lang.String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }


    /**
     * Sets the transactionCurrencyCode value for this ClearingResponse.
     * 
     * @param transactionCurrencyCode
     */
    public void setTransactionCurrencyCode(java.lang.String transactionCurrencyCode) {
        this.transactionCurrencyCode = transactionCurrencyCode;
    }


    /**
     * Gets the specialMerchantIdentifier value for this ClearingResponse.
     * 
     * @return specialMerchantIdentifier
     */
    public java.lang.String getSpecialMerchantIdentifier() {
        return specialMerchantIdentifier;
    }


    /**
     * Sets the specialMerchantIdentifier value for this ClearingResponse.
     * 
     * @param specialMerchantIdentifier
     */
    public void setSpecialMerchantIdentifier(java.lang.String specialMerchantIdentifier) {
        this.specialMerchantIdentifier = specialMerchantIdentifier;
    }


    /**
     * Gets the cardAcceptorIdCode value for this ClearingResponse.
     * 
     * @return cardAcceptorIdCode
     */
    public java.lang.String getCardAcceptorIdCode() {
        return cardAcceptorIdCode;
    }


    /**
     * Sets the cardAcceptorIdCode value for this ClearingResponse.
     * 
     * @param cardAcceptorIdCode
     */
    public void setCardAcceptorIdCode(java.lang.String cardAcceptorIdCode) {
        this.cardAcceptorIdCode = cardAcceptorIdCode;
    }


    /**
     * Gets the cardAcceptorTerminalID value for this ClearingResponse.
     * 
     * @return cardAcceptorTerminalID
     */
    public java.lang.String getCardAcceptorTerminalID() {
        return cardAcceptorTerminalID;
    }


    /**
     * Sets the cardAcceptorTerminalID value for this ClearingResponse.
     * 
     * @param cardAcceptorTerminalID
     */
    public void setCardAcceptorTerminalID(java.lang.String cardAcceptorTerminalID) {
        this.cardAcceptorTerminalID = cardAcceptorTerminalID;
    }


    /**
     * Gets the cardAcceptorBusinessCode value for this ClearingResponse.
     * 
     * @return cardAcceptorBusinessCode
     */
    public java.lang.String getCardAcceptorBusinessCode() {
        return cardAcceptorBusinessCode;
    }


    /**
     * Sets the cardAcceptorBusinessCode value for this ClearingResponse.
     * 
     * @param cardAcceptorBusinessCode
     */
    public void setCardAcceptorBusinessCode(java.lang.String cardAcceptorBusinessCode) {
        this.cardAcceptorBusinessCode = cardAcceptorBusinessCode;
    }


    /**
     * Gets the dateTimeLocalTransaction value for this ClearingResponse.
     * 
     * @return dateTimeLocalTransaction
     */
    public java.lang.String getDateTimeLocalTransaction() {
        return dateTimeLocalTransaction;
    }


    /**
     * Sets the dateTimeLocalTransaction value for this ClearingResponse.
     * 
     * @param dateTimeLocalTransaction
     */
    public void setDateTimeLocalTransaction(java.lang.String dateTimeLocalTransaction) {
        this.dateTimeLocalTransaction = dateTimeLocalTransaction;
    }


    /**
     * Gets the systemTraceAuditNumber value for this ClearingResponse.
     * 
     * @return systemTraceAuditNumber
     */
    public java.lang.String getSystemTraceAuditNumber() {
        return systemTraceAuditNumber;
    }


    /**
     * Sets the systemTraceAuditNumber value for this ClearingResponse.
     * 
     * @param systemTraceAuditNumber
     */
    public void setSystemTraceAuditNumber(java.lang.String systemTraceAuditNumber) {
        this.systemTraceAuditNumber = systemTraceAuditNumber;
    }


    /**
     * Gets the merchantType value for this ClearingResponse.
     * 
     * @return merchantType
     */
    public java.lang.String getMerchantType() {
        return merchantType;
    }


    /**
     * Sets the merchantType value for this ClearingResponse.
     * 
     * @param merchantType
     */
    public void setMerchantType(java.lang.String merchantType) {
        this.merchantType = merchantType;
    }


    /**
     * Gets the retrievalReferenceNumber value for this ClearingResponse.
     * 
     * @return retrievalReferenceNumber
     */
    public java.lang.String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }


    /**
     * Sets the retrievalReferenceNumber value for this ClearingResponse.
     * 
     * @param retrievalReferenceNumber
     */
    public void setRetrievalReferenceNumber(java.lang.String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }


    /**
     * Gets the approvalCode value for this ClearingResponse.
     * 
     * @return approvalCode
     */
    public java.lang.String getApprovalCode() {
        return approvalCode;
    }


    /**
     * Sets the approvalCode value for this ClearingResponse.
     * 
     * @param approvalCode
     */
    public void setApprovalCode(java.lang.String approvalCode) {
        this.approvalCode = approvalCode;
    }


    /**
     * Gets the transactionFeeBnp value for this ClearingResponse.
     * 
     * @return transactionFeeBnp
     */
    public java.lang.String getTransactionFeeBnp() {
        return transactionFeeBnp;
    }


    /**
     * Sets the transactionFeeBnp value for this ClearingResponse.
     * 
     * @param transactionFeeBnp
     */
    public void setTransactionFeeBnp(java.lang.String transactionFeeBnp) {
        this.transactionFeeBnp = transactionFeeBnp;
    }


    /**
     * Gets the lateFeeBnp value for this ClearingResponse.
     * 
     * @return lateFeeBnp
     */
    public java.lang.String getLateFeeBnp() {
        return lateFeeBnp;
    }


    /**
     * Sets the lateFeeBnp value for this ClearingResponse.
     * 
     * @param lateFeeBnp
     */
    public void setLateFeeBnp(java.lang.String lateFeeBnp) {
        this.lateFeeBnp = lateFeeBnp;
    }


    /**
     * Gets the membershipFeeBnp value for this ClearingResponse.
     * 
     * @return membershipFeeBnp
     */
    public java.lang.String getMembershipFeeBnp() {
        return membershipFeeBnp;
    }


    /**
     * Sets the membershipFeeBnp value for this ClearingResponse.
     * 
     * @param membershipFeeBnp
     */
    public void setMembershipFeeBnp(java.lang.String membershipFeeBnp) {
        this.membershipFeeBnp = membershipFeeBnp;
    }


    /**
     * Gets the overLimitFeeBnp value for this ClearingResponse.
     * 
     * @return overLimitFeeBnp
     */
    public java.lang.String getOverLimitFeeBnp() {
        return overLimitFeeBnp;
    }


    /**
     * Sets the overLimitFeeBnp value for this ClearingResponse.
     * 
     * @param overLimitFeeBnp
     */
    public void setOverLimitFeeBnp(java.lang.String overLimitFeeBnp) {
        this.overLimitFeeBnp = overLimitFeeBnp;
    }


    /**
     * Gets the insufficientFundFeeBnp value for this ClearingResponse.
     * 
     * @return insufficientFundFeeBnp
     */
    public java.lang.String getInsufficientFundFeeBnp() {
        return insufficientFundFeeBnp;
    }


    /**
     * Sets the insufficientFundFeeBnp value for this ClearingResponse.
     * 
     * @param insufficientFundFeeBnp
     */
    public void setInsufficientFundFeeBnp(java.lang.String insufficientFundFeeBnp) {
        this.insufficientFundFeeBnp = insufficientFundFeeBnp;
    }


    /**
     * Gets the collectionFeeBnp value for this ClearingResponse.
     * 
     * @return collectionFeeBnp
     */
    public java.lang.String getCollectionFeeBnp() {
        return collectionFeeBnp;
    }


    /**
     * Sets the collectionFeeBnp value for this ClearingResponse.
     * 
     * @param collectionFeeBnp
     */
    public void setCollectionFeeBnp(java.lang.String collectionFeeBnp) {
        this.collectionFeeBnp = collectionFeeBnp;
    }


    /**
     * Gets the recoveryFeeBnp value for this ClearingResponse.
     * 
     * @return recoveryFeeBnp
     */
    public java.lang.String getRecoveryFeeBnp() {
        return recoveryFeeBnp;
    }


    /**
     * Sets the recoveryFeeBnp value for this ClearingResponse.
     * 
     * @param recoveryFeeBnp
     */
    public void setRecoveryFeeBnp(java.lang.String recoveryFeeBnp) {
        this.recoveryFeeBnp = recoveryFeeBnp;
    }


    /**
     * Gets the insuranceBnp value for this ClearingResponse.
     * 
     * @return insuranceBnp
     */
    public java.lang.String getInsuranceBnp() {
        return insuranceBnp;
    }


    /**
     * Sets the insuranceBnp value for this ClearingResponse.
     * 
     * @param insuranceBnp
     */
    public void setInsuranceBnp(java.lang.String insuranceBnp) {
        this.insuranceBnp = insuranceBnp;
    }


    /**
     * Gets the interestBnp value for this ClearingResponse.
     * 
     * @return interestBnp
     */
    public java.lang.String getInterestBnp() {
        return interestBnp;
    }


    /**
     * Sets the interestBnp value for this ClearingResponse.
     * 
     * @param interestBnp
     */
    public void setInterestBnp(java.lang.String interestBnp) {
        this.interestBnp = interestBnp;
    }


    /**
     * Gets the currentBalance value for this ClearingResponse.
     * 
     * @return currentBalance
     */
    public java.lang.String getCurrentBalance() {
        return currentBalance;
    }


    /**
     * Sets the currentBalance value for this ClearingResponse.
     * 
     * @param currentBalance
     */
    public void setCurrentBalance(java.lang.String currentBalance) {
        this.currentBalance = currentBalance;
    }


    /**
     * Gets the principal value for this ClearingResponse.
     * 
     * @return principal
     */
    public java.lang.String getPrincipal() {
        return principal;
    }


    /**
     * Sets the principal value for this ClearingResponse.
     * 
     * @param principal
     */
    public void setPrincipal(java.lang.String principal) {
        this.principal = principal;
    }


    /**
     * Gets the daysDelinquent value for this ClearingResponse.
     * 
     * @return daysDelinquent
     */
    public java.lang.String getDaysDelinquent() {
        return daysDelinquent;
    }


    /**
     * Sets the daysDelinquent value for this ClearingResponse.
     * 
     * @param daysDelinquent
     */
    public void setDaysDelinquent(java.lang.String daysDelinquent) {
        this.daysDelinquent = daysDelinquent;
    }


    /**
     * Gets the amountDue value for this ClearingResponse.
     * 
     * @return amountDue
     */
    public java.lang.String getAmountDue() {
        return amountDue;
    }


    /**
     * Sets the amountDue value for this ClearingResponse.
     * 
     * @param amountDue
     */
    public void setAmountDue(java.lang.String amountDue) {
        this.amountDue = amountDue;
    }


    /**
     * Gets the pastDue value for this ClearingResponse.
     * 
     * @return pastDue
     */
    public java.lang.String getPastDue() {
        return pastDue;
    }


    /**
     * Sets the pastDue value for this ClearingResponse.
     * 
     * @param pastDue
     */
    public void setPastDue(java.lang.String pastDue) {
        this.pastDue = pastDue;
    }


    /**
     * Gets the paymentDueDate value for this ClearingResponse.
     * 
     * @return paymentDueDate
     */
    public java.lang.String getPaymentDueDate() {
        return paymentDueDate;
    }


    /**
     * Sets the paymentDueDate value for this ClearingResponse.
     * 
     * @param paymentDueDate
     */
    public void setPaymentDueDate(java.lang.String paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }


    /**
     * Gets the postingFlag value for this ClearingResponse.
     * 
     * @return postingFlag
     */
    public java.lang.String getPostingFlag() {
        return postingFlag;
    }


    /**
     * Sets the postingFlag value for this ClearingResponse.
     * 
     * @param postingFlag
     */
    public void setPostingFlag(java.lang.String postingFlag) {
        this.postingFlag = postingFlag;
    }


    /**
     * Gets the postingNote value for this ClearingResponse.
     * 
     * @return postingNote
     */
    public java.lang.String getPostingNote() {
        return postingNote;
    }


    /**
     * Sets the postingNote value for this ClearingResponse.
     * 
     * @param postingNote
     */
    public void setPostingNote(java.lang.String postingNote) {
        this.postingNote = postingNote;
    }


    /**
     * Gets the ADMIN_NO value for this ClearingResponse.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this ClearingResponse.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the TRANSACTION_ID value for this ClearingResponse.
     * 
     * @return TRANSACTION_ID
     */
    public java.lang.String getTRANSACTION_ID() {
        return TRANSACTION_ID;
    }


    /**
     * Sets the TRANSACTION_ID value for this ClearingResponse.
     * 
     * @param TRANSACTION_ID
     */
    public void setTRANSACTION_ID(java.lang.String TRANSACTION_ID) {
        this.TRANSACTION_ID = TRANSACTION_ID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClearingResponse)) return false;
        ClearingResponse other = (ClearingResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageTypeIdentifier==null && other.getMessageTypeIdentifier()==null) || 
             (this.messageTypeIdentifier!=null &&
              this.messageTypeIdentifier.equals(other.getMessageTypeIdentifier()))) &&
            ((this.custNbr==null && other.getCustNbr()==null) || 
             (this.custNbr!=null &&
              this.custNbr.equals(other.getCustNbr()))) &&
            ((this.primaryAccountNumber==null && other.getPrimaryAccountNumber()==null) || 
             (this.primaryAccountNumber!=null &&
              this.primaryAccountNumber.equals(other.getPrimaryAccountNumber()))) &&
            ((this.tranType==null && other.getTranType()==null) || 
             (this.tranType!=null &&
              this.tranType.equals(other.getTranType()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.lineItemSeqNumber==null && other.getLineItemSeqNumber()==null) || 
             (this.lineItemSeqNumber!=null &&
              this.lineItemSeqNumber.equals(other.getLineItemSeqNumber()))) &&
            ((this.inventoryCode==null && other.getInventoryCode()==null) || 
             (this.inventoryCode!=null &&
              this.inventoryCode.equals(other.getInventoryCode()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.unitPrice==null && other.getUnitPrice()==null) || 
             (this.unitPrice!=null &&
              this.unitPrice.equals(other.getUnitPrice()))) &&
            ((this.creditPlanMaster==null && other.getCreditPlanMaster()==null) || 
             (this.creditPlanMaster!=null &&
              this.creditPlanMaster.equals(other.getCreditPlanMaster()))) &&
            ((this.transactionDescription==null && other.getTransactionDescription()==null) || 
             (this.transactionDescription!=null &&
              this.transactionDescription.equals(other.getTransactionDescription()))) &&
            ((this.transactionCurrencyCode==null && other.getTransactionCurrencyCode()==null) || 
             (this.transactionCurrencyCode!=null &&
              this.transactionCurrencyCode.equals(other.getTransactionCurrencyCode()))) &&
            ((this.specialMerchantIdentifier==null && other.getSpecialMerchantIdentifier()==null) || 
             (this.specialMerchantIdentifier!=null &&
              this.specialMerchantIdentifier.equals(other.getSpecialMerchantIdentifier()))) &&
            ((this.cardAcceptorIdCode==null && other.getCardAcceptorIdCode()==null) || 
             (this.cardAcceptorIdCode!=null &&
              this.cardAcceptorIdCode.equals(other.getCardAcceptorIdCode()))) &&
            ((this.cardAcceptorTerminalID==null && other.getCardAcceptorTerminalID()==null) || 
             (this.cardAcceptorTerminalID!=null &&
              this.cardAcceptorTerminalID.equals(other.getCardAcceptorTerminalID()))) &&
            ((this.cardAcceptorBusinessCode==null && other.getCardAcceptorBusinessCode()==null) || 
             (this.cardAcceptorBusinessCode!=null &&
              this.cardAcceptorBusinessCode.equals(other.getCardAcceptorBusinessCode()))) &&
            ((this.dateTimeLocalTransaction==null && other.getDateTimeLocalTransaction()==null) || 
             (this.dateTimeLocalTransaction!=null &&
              this.dateTimeLocalTransaction.equals(other.getDateTimeLocalTransaction()))) &&
            ((this.systemTraceAuditNumber==null && other.getSystemTraceAuditNumber()==null) || 
             (this.systemTraceAuditNumber!=null &&
              this.systemTraceAuditNumber.equals(other.getSystemTraceAuditNumber()))) &&
            ((this.merchantType==null && other.getMerchantType()==null) || 
             (this.merchantType!=null &&
              this.merchantType.equals(other.getMerchantType()))) &&
            ((this.retrievalReferenceNumber==null && other.getRetrievalReferenceNumber()==null) || 
             (this.retrievalReferenceNumber!=null &&
              this.retrievalReferenceNumber.equals(other.getRetrievalReferenceNumber()))) &&
            ((this.approvalCode==null && other.getApprovalCode()==null) || 
             (this.approvalCode!=null &&
              this.approvalCode.equals(other.getApprovalCode()))) &&
            ((this.transactionFeeBnp==null && other.getTransactionFeeBnp()==null) || 
             (this.transactionFeeBnp!=null &&
              this.transactionFeeBnp.equals(other.getTransactionFeeBnp()))) &&
            ((this.lateFeeBnp==null && other.getLateFeeBnp()==null) || 
             (this.lateFeeBnp!=null &&
              this.lateFeeBnp.equals(other.getLateFeeBnp()))) &&
            ((this.membershipFeeBnp==null && other.getMembershipFeeBnp()==null) || 
             (this.membershipFeeBnp!=null &&
              this.membershipFeeBnp.equals(other.getMembershipFeeBnp()))) &&
            ((this.overLimitFeeBnp==null && other.getOverLimitFeeBnp()==null) || 
             (this.overLimitFeeBnp!=null &&
              this.overLimitFeeBnp.equals(other.getOverLimitFeeBnp()))) &&
            ((this.insufficientFundFeeBnp==null && other.getInsufficientFundFeeBnp()==null) || 
             (this.insufficientFundFeeBnp!=null &&
              this.insufficientFundFeeBnp.equals(other.getInsufficientFundFeeBnp()))) &&
            ((this.collectionFeeBnp==null && other.getCollectionFeeBnp()==null) || 
             (this.collectionFeeBnp!=null &&
              this.collectionFeeBnp.equals(other.getCollectionFeeBnp()))) &&
            ((this.recoveryFeeBnp==null && other.getRecoveryFeeBnp()==null) || 
             (this.recoveryFeeBnp!=null &&
              this.recoveryFeeBnp.equals(other.getRecoveryFeeBnp()))) &&
            ((this.insuranceBnp==null && other.getInsuranceBnp()==null) || 
             (this.insuranceBnp!=null &&
              this.insuranceBnp.equals(other.getInsuranceBnp()))) &&
            ((this.interestBnp==null && other.getInterestBnp()==null) || 
             (this.interestBnp!=null &&
              this.interestBnp.equals(other.getInterestBnp()))) &&
            ((this.currentBalance==null && other.getCurrentBalance()==null) || 
             (this.currentBalance!=null &&
              this.currentBalance.equals(other.getCurrentBalance()))) &&
            ((this.principal==null && other.getPrincipal()==null) || 
             (this.principal!=null &&
              this.principal.equals(other.getPrincipal()))) &&
            ((this.daysDelinquent==null && other.getDaysDelinquent()==null) || 
             (this.daysDelinquent!=null &&
              this.daysDelinquent.equals(other.getDaysDelinquent()))) &&
            ((this.amountDue==null && other.getAmountDue()==null) || 
             (this.amountDue!=null &&
              this.amountDue.equals(other.getAmountDue()))) &&
            ((this.pastDue==null && other.getPastDue()==null) || 
             (this.pastDue!=null &&
              this.pastDue.equals(other.getPastDue()))) &&
            ((this.paymentDueDate==null && other.getPaymentDueDate()==null) || 
             (this.paymentDueDate!=null &&
              this.paymentDueDate.equals(other.getPaymentDueDate()))) &&
            ((this.postingFlag==null && other.getPostingFlag()==null) || 
             (this.postingFlag!=null &&
              this.postingFlag.equals(other.getPostingFlag()))) &&
            ((this.postingNote==null && other.getPostingNote()==null) || 
             (this.postingNote!=null &&
              this.postingNote.equals(other.getPostingNote()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.TRANSACTION_ID==null && other.getTRANSACTION_ID()==null) || 
             (this.TRANSACTION_ID!=null &&
              this.TRANSACTION_ID.equals(other.getTRANSACTION_ID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageTypeIdentifier() != null) {
            _hashCode += getMessageTypeIdentifier().hashCode();
        }
        if (getCustNbr() != null) {
            _hashCode += getCustNbr().hashCode();
        }
        if (getPrimaryAccountNumber() != null) {
            _hashCode += getPrimaryAccountNumber().hashCode();
        }
        if (getTranType() != null) {
            _hashCode += getTranType().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getLineItemSeqNumber() != null) {
            _hashCode += getLineItemSeqNumber().hashCode();
        }
        if (getInventoryCode() != null) {
            _hashCode += getInventoryCode().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getUnitPrice() != null) {
            _hashCode += getUnitPrice().hashCode();
        }
        if (getCreditPlanMaster() != null) {
            _hashCode += getCreditPlanMaster().hashCode();
        }
        if (getTransactionDescription() != null) {
            _hashCode += getTransactionDescription().hashCode();
        }
        if (getTransactionCurrencyCode() != null) {
            _hashCode += getTransactionCurrencyCode().hashCode();
        }
        if (getSpecialMerchantIdentifier() != null) {
            _hashCode += getSpecialMerchantIdentifier().hashCode();
        }
        if (getCardAcceptorIdCode() != null) {
            _hashCode += getCardAcceptorIdCode().hashCode();
        }
        if (getCardAcceptorTerminalID() != null) {
            _hashCode += getCardAcceptorTerminalID().hashCode();
        }
        if (getCardAcceptorBusinessCode() != null) {
            _hashCode += getCardAcceptorBusinessCode().hashCode();
        }
        if (getDateTimeLocalTransaction() != null) {
            _hashCode += getDateTimeLocalTransaction().hashCode();
        }
        if (getSystemTraceAuditNumber() != null) {
            _hashCode += getSystemTraceAuditNumber().hashCode();
        }
        if (getMerchantType() != null) {
            _hashCode += getMerchantType().hashCode();
        }
        if (getRetrievalReferenceNumber() != null) {
            _hashCode += getRetrievalReferenceNumber().hashCode();
        }
        if (getApprovalCode() != null) {
            _hashCode += getApprovalCode().hashCode();
        }
        if (getTransactionFeeBnp() != null) {
            _hashCode += getTransactionFeeBnp().hashCode();
        }
        if (getLateFeeBnp() != null) {
            _hashCode += getLateFeeBnp().hashCode();
        }
        if (getMembershipFeeBnp() != null) {
            _hashCode += getMembershipFeeBnp().hashCode();
        }
        if (getOverLimitFeeBnp() != null) {
            _hashCode += getOverLimitFeeBnp().hashCode();
        }
        if (getInsufficientFundFeeBnp() != null) {
            _hashCode += getInsufficientFundFeeBnp().hashCode();
        }
        if (getCollectionFeeBnp() != null) {
            _hashCode += getCollectionFeeBnp().hashCode();
        }
        if (getRecoveryFeeBnp() != null) {
            _hashCode += getRecoveryFeeBnp().hashCode();
        }
        if (getInsuranceBnp() != null) {
            _hashCode += getInsuranceBnp().hashCode();
        }
        if (getInterestBnp() != null) {
            _hashCode += getInterestBnp().hashCode();
        }
        if (getCurrentBalance() != null) {
            _hashCode += getCurrentBalance().hashCode();
        }
        if (getPrincipal() != null) {
            _hashCode += getPrincipal().hashCode();
        }
        if (getDaysDelinquent() != null) {
            _hashCode += getDaysDelinquent().hashCode();
        }
        if (getAmountDue() != null) {
            _hashCode += getAmountDue().hashCode();
        }
        if (getPastDue() != null) {
            _hashCode += getPastDue().hashCode();
        }
        if (getPaymentDueDate() != null) {
            _hashCode += getPaymentDueDate().hashCode();
        }
        if (getPostingFlag() != null) {
            _hashCode += getPostingFlag().hashCode();
        }
        if (getPostingNote() != null) {
            _hashCode += getPostingNote().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getTRANSACTION_ID() != null) {
            _hashCode += getTRANSACTION_ID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClearingResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageTypeIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MessageTypeIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CustNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PrimaryAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tranType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TranType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineItemSeqNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LineItemSeqNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "InventoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UnitPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditPlanMaster");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CreditPlanMaster"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialMerchantIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SpecialMerchantIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorIdCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorIdCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorTerminalID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorTerminalID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorBusinessCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorBusinessCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTimeLocalTransaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DateTimeLocalTransaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceAuditNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SystemTraceAuditNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MerchantType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retrievalReferenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RetrievalReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ApprovalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lateFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LateFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("membershipFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MembershipFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overLimitFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "OverLimitFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insufficientFundFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "InsufficientFundFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("collectionFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CollectionFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recoveryFeeBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RecoveryFeeBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("insuranceBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "InsuranceBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interestBnp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "InterestBnp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currentBalance");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CurrentBalance"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("principal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Principal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("daysDelinquent");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DaysDelinquent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amountDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "AmountDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pastDue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PastDue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentDueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PaymentDueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postingFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PostingFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postingNote");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PostingNote"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TRANSACTION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TRANSACTION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
