package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PrepaidServicesSoapProxy implements com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap {
  private String _endpoint = null;
  private com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap prepaidServicesSoap = null;
  
  public PrepaidServicesSoapProxy() {
    _initPrepaidServicesSoapProxy();
  }
  
  public PrepaidServicesSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initPrepaidServicesSoapProxy();
  }
  
  private void _initPrepaidServicesSoapProxy() {
    try {
      prepaidServicesSoap = (new com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesLocator()).getPrepaidServicesSoap();
      if (prepaidServicesSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)prepaidServicesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)prepaidServicesSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (prepaidServicesSoap != null)
      ((javax.xml.rpc.Stub)prepaidServicesSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.PrepaidServicesSoap getPrepaidServicesSoap() {
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap;
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryResponse cardActivationInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivationInquiryRequest cardActivationInquiry) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardActivationInquiry(cardActivationInquiry);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpResponse requestCustomerServiceHelp(com.addcel.telecom.firstView.client.axis.accountLookUp.RequestCustomerServiceHelpRequest REQUESTCUSTOMERSERVICEHELP) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.requestCustomerServiceHelp(REQUESTCUSTOMERSERVICEHELP);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequestResponse login(com.addcel.telecom.firstView.client.axis.accountLookUp.LoginRequest LOGIN_REQUEST) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.login(LOGIN_REQUEST);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordResponse resetPassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ResetPasswordRequest resetPassword) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.resetPassword(resetPassword);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferResponse cardToCardTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.CardToCardTransferRequest TRANSFERCARD) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardToCardTransfer(TRANSFERCARD);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingResponse clearing(com.addcel.telecom.firstView.client.axis.accountLookUp.ClearingRequest clrRequest) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.clearing(clrRequest);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountResponse loadsBasedOnHoldingAccount(com.addcel.telecom.firstView.client.axis.accountLookUp.LoadsBasedOnHoldingAccountRequest loadsBasedOnHoldingAccount_Request) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.loadsBasedOnHoldingAccount(loadsBasedOnHoldingAccount_Request);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceResponse checkBalance(com.addcel.telecom.firstView.client.axis.accountLookUp.CheckBalanceRequest CHECKBALANCE) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.checkBalance(CHECKBALANCE);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveResponse cardActive(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActiveRequest CARDACTIVE) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardActive(CARDACTIVE);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferResponse savingAccountsFundsTransfer(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsFundsTransferRequest savingAccountsFundsTransfer) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.savingAccountsFundsTransfer(savingAccountsFundsTransfer);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordResponse changePassword(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePasswordRequest CHANGE_PASSWORD) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.changePassword(CHANGE_PASSWORD);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryResponseResponse savingAccountsInquiry(com.addcel.telecom.firstView.client.axis.accountLookUp.SavingAccountsInquiryRequest savingAccountsInquiry) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.savingAccountsInquiry(savingAccountsInquiry);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardResponse reportStolenCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCardRequest REPORTSTOLENCARD) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.reportStolenCard(REPORTSTOLENCARD);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupResponse SMSSetup(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSSetupRequest ACCOUNTSMSSETUP) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.SMSSetup(ACCOUNTSMSSETUP);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINResponse generatePIN(com.addcel.telecom.firstView.client.axis.accountLookUp.GeneratePINRequest generatePIN) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.generatePIN(generatePIN);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Response cardActivation3(com.addcel.telecom.firstView.client.axis.accountLookUp.CardActivation3Request cardActivation3) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardActivation3(cardActivation3);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryResponse transactionHistory(com.addcel.telecom.firstView.client.axis.accountLookUp.TransactionHistoryRequest TRANSACTION_HISTORY) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.transactionHistory(TRANSACTION_HISTORY);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateResponse cardUpdate(com.addcel.telecom.firstView.client.axis.accountLookUp.CardUpdateRequest CARDUPDATE) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardUpdate(CARDUPDATE);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardResponse validateCard(com.addcel.telecom.firstView.client.axis.accountLookUp.ValidateCardRequest VALIDATE_CARD) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.validateCard(VALIDATE_CARD);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameResponse updateSelfserviceUserName(com.addcel.telecom.firstView.client.axis.accountLookUp.UpdateSelfserviceUserNameRequest updateSelfserviceUserName) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.updateSelfserviceUserName(updateSelfserviceUserName);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCResponse getCVC(com.addcel.telecom.firstView.client.axis.accountLookUp.GetCVCRequest getCVC) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.getCVC(getCVC);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinResponse changePin(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangePinRequest CHANGEPIN) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.changePin(CHANGEPIN);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusResponse changeOverdraftStatus(com.addcel.telecom.firstView.client.axis.accountLookUp.ChangeOverdraftStatusRequest CHANGEOVERDRAFTSTATUS) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.changeOverdraftStatus(CHANGEOVERDRAFTSTATUS);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailResponse cardholderDetail(com.addcel.telecom.firstView.client.axis.accountLookUp.CardholderDetailRequest CARDHOLDER_DETAIL) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.cardholderDetail(CARDHOLDER_DETAIL);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponse SMSAvailability(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityRequest ACCOUNTSMSAVAILABILITY) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.SMSAvailability(ACCOUNTSMSAVAILABILITY);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response reportStolenCard2(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.reportStolenCard2(REPORTSTOLENCARDEXT);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkResponse promoWork(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.promoWork(promoWork_Request);
  }
  
  public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentResponse SMSEnrollment(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS) throws java.rmi.RemoteException{
    if (prepaidServicesSoap == null)
      _initPrepaidServicesSoapProxy();
    return prepaidServicesSoap.SMSEnrollment(SMS_ENROLLMENT_SETTINGS);
  }
  
  
}