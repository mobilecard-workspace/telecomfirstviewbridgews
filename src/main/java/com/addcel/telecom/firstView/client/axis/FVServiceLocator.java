/**
 * FVServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class FVServiceLocator extends org.apache.axis.client.Service implements com.addcel.telecom.firstView.client.axis.FVService {

    public FVServiceLocator() {
    }


    public FVServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FVServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FVServiceSoap
    private java.lang.String FVServiceSoap_address = "https://firstviewcorp.com/fvservice/fvservice.asmx";

    public java.lang.String getFVServiceSoapAddress() {
        return FVServiceSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FVServiceSoapWSDDServiceName = "FVServiceSoap";

    public java.lang.String getFVServiceSoapWSDDServiceName() {
        return FVServiceSoapWSDDServiceName;
    }

    public void setFVServiceSoapWSDDServiceName(java.lang.String name) {
        FVServiceSoapWSDDServiceName = name;
    }

    public com.addcel.telecom.firstView.client.axis.FVServiceSoap getFVServiceSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FVServiceSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFVServiceSoap(endpoint);
    }

    public com.addcel.telecom.firstView.client.axis.FVServiceSoap getFVServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.addcel.telecom.firstView.client.axis.FVServiceSoapStub _stub = new com.addcel.telecom.firstView.client.axis.FVServiceSoapStub(portAddress, this);
            _stub.setPortName(getFVServiceSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFVServiceSoapEndpointAddress(java.lang.String address) {
        FVServiceSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.addcel.telecom.firstView.client.axis.FVServiceSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                com.addcel.telecom.firstView.client.axis.FVServiceSoapStub _stub = new com.addcel.telecom.firstView.client.axis.FVServiceSoapStub(new java.net.URL(FVServiceSoap_address), this);
                _stub.setPortName(getFVServiceSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FVServiceSoap".equals(inputPortName)) {
            return getFVServiceSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "FVService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "FVServiceSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FVServiceSoap".equals(portName)) {
            setFVServiceSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
