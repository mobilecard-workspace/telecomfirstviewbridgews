/**
 * FeeDetailType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class FeeDetailType  implements java.io.Serializable {
    private java.lang.String FEE_TYPE;

    private java.lang.String FEE_DESC;

    private java.lang.String FEE_AMT;

    public FeeDetailType() {
    }

    public FeeDetailType(
           java.lang.String FEE_TYPE,
           java.lang.String FEE_DESC,
           java.lang.String FEE_AMT) {
           this.FEE_TYPE = FEE_TYPE;
           this.FEE_DESC = FEE_DESC;
           this.FEE_AMT = FEE_AMT;
    }


    /**
     * Gets the FEE_TYPE value for this FeeDetailType.
     * 
     * @return FEE_TYPE
     */
    public java.lang.String getFEE_TYPE() {
        return FEE_TYPE;
    }


    /**
     * Sets the FEE_TYPE value for this FeeDetailType.
     * 
     * @param FEE_TYPE
     */
    public void setFEE_TYPE(java.lang.String FEE_TYPE) {
        this.FEE_TYPE = FEE_TYPE;
    }


    /**
     * Gets the FEE_DESC value for this FeeDetailType.
     * 
     * @return FEE_DESC
     */
    public java.lang.String getFEE_DESC() {
        return FEE_DESC;
    }


    /**
     * Sets the FEE_DESC value for this FeeDetailType.
     * 
     * @param FEE_DESC
     */
    public void setFEE_DESC(java.lang.String FEE_DESC) {
        this.FEE_DESC = FEE_DESC;
    }


    /**
     * Gets the FEE_AMT value for this FeeDetailType.
     * 
     * @return FEE_AMT
     */
    public java.lang.String getFEE_AMT() {
        return FEE_AMT;
    }


    /**
     * Sets the FEE_AMT value for this FeeDetailType.
     * 
     * @param FEE_AMT
     */
    public void setFEE_AMT(java.lang.String FEE_AMT) {
        this.FEE_AMT = FEE_AMT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FeeDetailType)) return false;
        FeeDetailType other = (FeeDetailType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.FEE_TYPE==null && other.getFEE_TYPE()==null) || 
             (this.FEE_TYPE!=null &&
              this.FEE_TYPE.equals(other.getFEE_TYPE()))) &&
            ((this.FEE_DESC==null && other.getFEE_DESC()==null) || 
             (this.FEE_DESC!=null &&
              this.FEE_DESC.equals(other.getFEE_DESC()))) &&
            ((this.FEE_AMT==null && other.getFEE_AMT()==null) || 
             (this.FEE_AMT!=null &&
              this.FEE_AMT.equals(other.getFEE_AMT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFEE_TYPE() != null) {
            _hashCode += getFEE_TYPE().hashCode();
        }
        if (getFEE_DESC() != null) {
            _hashCode += getFEE_DESC().hashCode();
        }
        if (getFEE_AMT() != null) {
            _hashCode += getFEE_AMT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FeeDetailType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FeeDetailType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FEE_TYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FEE_TYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FEE_DESC");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FEE_DESC"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FEE_AMT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FEE_AMT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
