/**
 * ClearingRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ClearingRequest  implements java.io.Serializable {
    private java.lang.String messageTypeIdentifier;

    private java.lang.String custNbr;

    private java.lang.String primaryAccountNumber;

    private java.lang.String tranType;

    private java.lang.String transactionAmount;

    private java.lang.String transmissionDateTime;

    private java.lang.String lineItemSeqNumber;

    private java.lang.String inventoryCode;

    private java.lang.String quantity;

    private java.lang.String unitPrice;

    private java.lang.String creditPlanMaster;

    private java.lang.String transactionDescription;

    private java.lang.String transactionCurrencyCode;

    private java.lang.String specialMerchantIdentifier;

    private java.lang.String cardAcceptorIdCode;

    private java.lang.String cardAcceptorTerminalID;

    private java.lang.String cardAcceptorBusinessCode;

    private java.lang.String dateTimeLocalTransaction;

    private java.lang.String systemTraceAuditNumber;

    private java.lang.String merchantType;

    private java.lang.String retrievalReferenceNumber;

    private java.lang.String approvalCode;

    private java.lang.String TNPFlag;

    private java.lang.String ADMIN_NO;

    private java.lang.String loadType;

    public ClearingRequest() {
    }

    public ClearingRequest(
           java.lang.String messageTypeIdentifier,
           java.lang.String custNbr,
           java.lang.String primaryAccountNumber,
           java.lang.String tranType,
           java.lang.String transactionAmount,
           java.lang.String transmissionDateTime,
           java.lang.String lineItemSeqNumber,
           java.lang.String inventoryCode,
           java.lang.String quantity,
           java.lang.String unitPrice,
           java.lang.String creditPlanMaster,
           java.lang.String transactionDescription,
           java.lang.String transactionCurrencyCode,
           java.lang.String specialMerchantIdentifier,
           java.lang.String cardAcceptorIdCode,
           java.lang.String cardAcceptorTerminalID,
           java.lang.String cardAcceptorBusinessCode,
           java.lang.String dateTimeLocalTransaction,
           java.lang.String systemTraceAuditNumber,
           java.lang.String merchantType,
           java.lang.String retrievalReferenceNumber,
           java.lang.String approvalCode,
           java.lang.String TNPFlag,
           java.lang.String ADMIN_NO,
           java.lang.String loadType) {
           this.messageTypeIdentifier = messageTypeIdentifier;
           this.custNbr = custNbr;
           this.primaryAccountNumber = primaryAccountNumber;
           this.tranType = tranType;
           this.transactionAmount = transactionAmount;
           this.transmissionDateTime = transmissionDateTime;
           this.lineItemSeqNumber = lineItemSeqNumber;
           this.inventoryCode = inventoryCode;
           this.quantity = quantity;
           this.unitPrice = unitPrice;
           this.creditPlanMaster = creditPlanMaster;
           this.transactionDescription = transactionDescription;
           this.transactionCurrencyCode = transactionCurrencyCode;
           this.specialMerchantIdentifier = specialMerchantIdentifier;
           this.cardAcceptorIdCode = cardAcceptorIdCode;
           this.cardAcceptorTerminalID = cardAcceptorTerminalID;
           this.cardAcceptorBusinessCode = cardAcceptorBusinessCode;
           this.dateTimeLocalTransaction = dateTimeLocalTransaction;
           this.systemTraceAuditNumber = systemTraceAuditNumber;
           this.merchantType = merchantType;
           this.retrievalReferenceNumber = retrievalReferenceNumber;
           this.approvalCode = approvalCode;
           this.TNPFlag = TNPFlag;
           this.ADMIN_NO = ADMIN_NO;
           this.loadType = loadType;
    }


    /**
     * Gets the messageTypeIdentifier value for this ClearingRequest.
     * 
     * @return messageTypeIdentifier
     */
    public java.lang.String getMessageTypeIdentifier() {
        return messageTypeIdentifier;
    }


    /**
     * Sets the messageTypeIdentifier value for this ClearingRequest.
     * 
     * @param messageTypeIdentifier
     */
    public void setMessageTypeIdentifier(java.lang.String messageTypeIdentifier) {
        this.messageTypeIdentifier = messageTypeIdentifier;
    }


    /**
     * Gets the custNbr value for this ClearingRequest.
     * 
     * @return custNbr
     */
    public java.lang.String getCustNbr() {
        return custNbr;
    }


    /**
     * Sets the custNbr value for this ClearingRequest.
     * 
     * @param custNbr
     */
    public void setCustNbr(java.lang.String custNbr) {
        this.custNbr = custNbr;
    }


    /**
     * Gets the primaryAccountNumber value for this ClearingRequest.
     * 
     * @return primaryAccountNumber
     */
    public java.lang.String getPrimaryAccountNumber() {
        return primaryAccountNumber;
    }


    /**
     * Sets the primaryAccountNumber value for this ClearingRequest.
     * 
     * @param primaryAccountNumber
     */
    public void setPrimaryAccountNumber(java.lang.String primaryAccountNumber) {
        this.primaryAccountNumber = primaryAccountNumber;
    }


    /**
     * Gets the tranType value for this ClearingRequest.
     * 
     * @return tranType
     */
    public java.lang.String getTranType() {
        return tranType;
    }


    /**
     * Sets the tranType value for this ClearingRequest.
     * 
     * @param tranType
     */
    public void setTranType(java.lang.String tranType) {
        this.tranType = tranType;
    }


    /**
     * Gets the transactionAmount value for this ClearingRequest.
     * 
     * @return transactionAmount
     */
    public java.lang.String getTransactionAmount() {
        return transactionAmount;
    }


    /**
     * Sets the transactionAmount value for this ClearingRequest.
     * 
     * @param transactionAmount
     */
    public void setTransactionAmount(java.lang.String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }


    /**
     * Gets the transmissionDateTime value for this ClearingRequest.
     * 
     * @return transmissionDateTime
     */
    public java.lang.String getTransmissionDateTime() {
        return transmissionDateTime;
    }


    /**
     * Sets the transmissionDateTime value for this ClearingRequest.
     * 
     * @param transmissionDateTime
     */
    public void setTransmissionDateTime(java.lang.String transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }


    /**
     * Gets the lineItemSeqNumber value for this ClearingRequest.
     * 
     * @return lineItemSeqNumber
     */
    public java.lang.String getLineItemSeqNumber() {
        return lineItemSeqNumber;
    }


    /**
     * Sets the lineItemSeqNumber value for this ClearingRequest.
     * 
     * @param lineItemSeqNumber
     */
    public void setLineItemSeqNumber(java.lang.String lineItemSeqNumber) {
        this.lineItemSeqNumber = lineItemSeqNumber;
    }


    /**
     * Gets the inventoryCode value for this ClearingRequest.
     * 
     * @return inventoryCode
     */
    public java.lang.String getInventoryCode() {
        return inventoryCode;
    }


    /**
     * Sets the inventoryCode value for this ClearingRequest.
     * 
     * @param inventoryCode
     */
    public void setInventoryCode(java.lang.String inventoryCode) {
        this.inventoryCode = inventoryCode;
    }


    /**
     * Gets the quantity value for this ClearingRequest.
     * 
     * @return quantity
     */
    public java.lang.String getQuantity() {
        return quantity;
    }


    /**
     * Sets the quantity value for this ClearingRequest.
     * 
     * @param quantity
     */
    public void setQuantity(java.lang.String quantity) {
        this.quantity = quantity;
    }


    /**
     * Gets the unitPrice value for this ClearingRequest.
     * 
     * @return unitPrice
     */
    public java.lang.String getUnitPrice() {
        return unitPrice;
    }


    /**
     * Sets the unitPrice value for this ClearingRequest.
     * 
     * @param unitPrice
     */
    public void setUnitPrice(java.lang.String unitPrice) {
        this.unitPrice = unitPrice;
    }


    /**
     * Gets the creditPlanMaster value for this ClearingRequest.
     * 
     * @return creditPlanMaster
     */
    public java.lang.String getCreditPlanMaster() {
        return creditPlanMaster;
    }


    /**
     * Sets the creditPlanMaster value for this ClearingRequest.
     * 
     * @param creditPlanMaster
     */
    public void setCreditPlanMaster(java.lang.String creditPlanMaster) {
        this.creditPlanMaster = creditPlanMaster;
    }


    /**
     * Gets the transactionDescription value for this ClearingRequest.
     * 
     * @return transactionDescription
     */
    public java.lang.String getTransactionDescription() {
        return transactionDescription;
    }


    /**
     * Sets the transactionDescription value for this ClearingRequest.
     * 
     * @param transactionDescription
     */
    public void setTransactionDescription(java.lang.String transactionDescription) {
        this.transactionDescription = transactionDescription;
    }


    /**
     * Gets the transactionCurrencyCode value for this ClearingRequest.
     * 
     * @return transactionCurrencyCode
     */
    public java.lang.String getTransactionCurrencyCode() {
        return transactionCurrencyCode;
    }


    /**
     * Sets the transactionCurrencyCode value for this ClearingRequest.
     * 
     * @param transactionCurrencyCode
     */
    public void setTransactionCurrencyCode(java.lang.String transactionCurrencyCode) {
        this.transactionCurrencyCode = transactionCurrencyCode;
    }


    /**
     * Gets the specialMerchantIdentifier value for this ClearingRequest.
     * 
     * @return specialMerchantIdentifier
     */
    public java.lang.String getSpecialMerchantIdentifier() {
        return specialMerchantIdentifier;
    }


    /**
     * Sets the specialMerchantIdentifier value for this ClearingRequest.
     * 
     * @param specialMerchantIdentifier
     */
    public void setSpecialMerchantIdentifier(java.lang.String specialMerchantIdentifier) {
        this.specialMerchantIdentifier = specialMerchantIdentifier;
    }


    /**
     * Gets the cardAcceptorIdCode value for this ClearingRequest.
     * 
     * @return cardAcceptorIdCode
     */
    public java.lang.String getCardAcceptorIdCode() {
        return cardAcceptorIdCode;
    }


    /**
     * Sets the cardAcceptorIdCode value for this ClearingRequest.
     * 
     * @param cardAcceptorIdCode
     */
    public void setCardAcceptorIdCode(java.lang.String cardAcceptorIdCode) {
        this.cardAcceptorIdCode = cardAcceptorIdCode;
    }


    /**
     * Gets the cardAcceptorTerminalID value for this ClearingRequest.
     * 
     * @return cardAcceptorTerminalID
     */
    public java.lang.String getCardAcceptorTerminalID() {
        return cardAcceptorTerminalID;
    }


    /**
     * Sets the cardAcceptorTerminalID value for this ClearingRequest.
     * 
     * @param cardAcceptorTerminalID
     */
    public void setCardAcceptorTerminalID(java.lang.String cardAcceptorTerminalID) {
        this.cardAcceptorTerminalID = cardAcceptorTerminalID;
    }


    /**
     * Gets the cardAcceptorBusinessCode value for this ClearingRequest.
     * 
     * @return cardAcceptorBusinessCode
     */
    public java.lang.String getCardAcceptorBusinessCode() {
        return cardAcceptorBusinessCode;
    }


    /**
     * Sets the cardAcceptorBusinessCode value for this ClearingRequest.
     * 
     * @param cardAcceptorBusinessCode
     */
    public void setCardAcceptorBusinessCode(java.lang.String cardAcceptorBusinessCode) {
        this.cardAcceptorBusinessCode = cardAcceptorBusinessCode;
    }


    /**
     * Gets the dateTimeLocalTransaction value for this ClearingRequest.
     * 
     * @return dateTimeLocalTransaction
     */
    public java.lang.String getDateTimeLocalTransaction() {
        return dateTimeLocalTransaction;
    }


    /**
     * Sets the dateTimeLocalTransaction value for this ClearingRequest.
     * 
     * @param dateTimeLocalTransaction
     */
    public void setDateTimeLocalTransaction(java.lang.String dateTimeLocalTransaction) {
        this.dateTimeLocalTransaction = dateTimeLocalTransaction;
    }


    /**
     * Gets the systemTraceAuditNumber value for this ClearingRequest.
     * 
     * @return systemTraceAuditNumber
     */
    public java.lang.String getSystemTraceAuditNumber() {
        return systemTraceAuditNumber;
    }


    /**
     * Sets the systemTraceAuditNumber value for this ClearingRequest.
     * 
     * @param systemTraceAuditNumber
     */
    public void setSystemTraceAuditNumber(java.lang.String systemTraceAuditNumber) {
        this.systemTraceAuditNumber = systemTraceAuditNumber;
    }


    /**
     * Gets the merchantType value for this ClearingRequest.
     * 
     * @return merchantType
     */
    public java.lang.String getMerchantType() {
        return merchantType;
    }


    /**
     * Sets the merchantType value for this ClearingRequest.
     * 
     * @param merchantType
     */
    public void setMerchantType(java.lang.String merchantType) {
        this.merchantType = merchantType;
    }


    /**
     * Gets the retrievalReferenceNumber value for this ClearingRequest.
     * 
     * @return retrievalReferenceNumber
     */
    public java.lang.String getRetrievalReferenceNumber() {
        return retrievalReferenceNumber;
    }


    /**
     * Sets the retrievalReferenceNumber value for this ClearingRequest.
     * 
     * @param retrievalReferenceNumber
     */
    public void setRetrievalReferenceNumber(java.lang.String retrievalReferenceNumber) {
        this.retrievalReferenceNumber = retrievalReferenceNumber;
    }


    /**
     * Gets the approvalCode value for this ClearingRequest.
     * 
     * @return approvalCode
     */
    public java.lang.String getApprovalCode() {
        return approvalCode;
    }


    /**
     * Sets the approvalCode value for this ClearingRequest.
     * 
     * @param approvalCode
     */
    public void setApprovalCode(java.lang.String approvalCode) {
        this.approvalCode = approvalCode;
    }


    /**
     * Gets the TNPFlag value for this ClearingRequest.
     * 
     * @return TNPFlag
     */
    public java.lang.String getTNPFlag() {
        return TNPFlag;
    }


    /**
     * Sets the TNPFlag value for this ClearingRequest.
     * 
     * @param TNPFlag
     */
    public void setTNPFlag(java.lang.String TNPFlag) {
        this.TNPFlag = TNPFlag;
    }


    /**
     * Gets the ADMIN_NO value for this ClearingRequest.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this ClearingRequest.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the loadType value for this ClearingRequest.
     * 
     * @return loadType
     */
    public java.lang.String getLoadType() {
        return loadType;
    }


    /**
     * Sets the loadType value for this ClearingRequest.
     * 
     * @param loadType
     */
    public void setLoadType(java.lang.String loadType) {
        this.loadType = loadType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ClearingRequest)) return false;
        ClearingRequest other = (ClearingRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.messageTypeIdentifier==null && other.getMessageTypeIdentifier()==null) || 
             (this.messageTypeIdentifier!=null &&
              this.messageTypeIdentifier.equals(other.getMessageTypeIdentifier()))) &&
            ((this.custNbr==null && other.getCustNbr()==null) || 
             (this.custNbr!=null &&
              this.custNbr.equals(other.getCustNbr()))) &&
            ((this.primaryAccountNumber==null && other.getPrimaryAccountNumber()==null) || 
             (this.primaryAccountNumber!=null &&
              this.primaryAccountNumber.equals(other.getPrimaryAccountNumber()))) &&
            ((this.tranType==null && other.getTranType()==null) || 
             (this.tranType!=null &&
              this.tranType.equals(other.getTranType()))) &&
            ((this.transactionAmount==null && other.getTransactionAmount()==null) || 
             (this.transactionAmount!=null &&
              this.transactionAmount.equals(other.getTransactionAmount()))) &&
            ((this.transmissionDateTime==null && other.getTransmissionDateTime()==null) || 
             (this.transmissionDateTime!=null &&
              this.transmissionDateTime.equals(other.getTransmissionDateTime()))) &&
            ((this.lineItemSeqNumber==null && other.getLineItemSeqNumber()==null) || 
             (this.lineItemSeqNumber!=null &&
              this.lineItemSeqNumber.equals(other.getLineItemSeqNumber()))) &&
            ((this.inventoryCode==null && other.getInventoryCode()==null) || 
             (this.inventoryCode!=null &&
              this.inventoryCode.equals(other.getInventoryCode()))) &&
            ((this.quantity==null && other.getQuantity()==null) || 
             (this.quantity!=null &&
              this.quantity.equals(other.getQuantity()))) &&
            ((this.unitPrice==null && other.getUnitPrice()==null) || 
             (this.unitPrice!=null &&
              this.unitPrice.equals(other.getUnitPrice()))) &&
            ((this.creditPlanMaster==null && other.getCreditPlanMaster()==null) || 
             (this.creditPlanMaster!=null &&
              this.creditPlanMaster.equals(other.getCreditPlanMaster()))) &&
            ((this.transactionDescription==null && other.getTransactionDescription()==null) || 
             (this.transactionDescription!=null &&
              this.transactionDescription.equals(other.getTransactionDescription()))) &&
            ((this.transactionCurrencyCode==null && other.getTransactionCurrencyCode()==null) || 
             (this.transactionCurrencyCode!=null &&
              this.transactionCurrencyCode.equals(other.getTransactionCurrencyCode()))) &&
            ((this.specialMerchantIdentifier==null && other.getSpecialMerchantIdentifier()==null) || 
             (this.specialMerchantIdentifier!=null &&
              this.specialMerchantIdentifier.equals(other.getSpecialMerchantIdentifier()))) &&
            ((this.cardAcceptorIdCode==null && other.getCardAcceptorIdCode()==null) || 
             (this.cardAcceptorIdCode!=null &&
              this.cardAcceptorIdCode.equals(other.getCardAcceptorIdCode()))) &&
            ((this.cardAcceptorTerminalID==null && other.getCardAcceptorTerminalID()==null) || 
             (this.cardAcceptorTerminalID!=null &&
              this.cardAcceptorTerminalID.equals(other.getCardAcceptorTerminalID()))) &&
            ((this.cardAcceptorBusinessCode==null && other.getCardAcceptorBusinessCode()==null) || 
             (this.cardAcceptorBusinessCode!=null &&
              this.cardAcceptorBusinessCode.equals(other.getCardAcceptorBusinessCode()))) &&
            ((this.dateTimeLocalTransaction==null && other.getDateTimeLocalTransaction()==null) || 
             (this.dateTimeLocalTransaction!=null &&
              this.dateTimeLocalTransaction.equals(other.getDateTimeLocalTransaction()))) &&
            ((this.systemTraceAuditNumber==null && other.getSystemTraceAuditNumber()==null) || 
             (this.systemTraceAuditNumber!=null &&
              this.systemTraceAuditNumber.equals(other.getSystemTraceAuditNumber()))) &&
            ((this.merchantType==null && other.getMerchantType()==null) || 
             (this.merchantType!=null &&
              this.merchantType.equals(other.getMerchantType()))) &&
            ((this.retrievalReferenceNumber==null && other.getRetrievalReferenceNumber()==null) || 
             (this.retrievalReferenceNumber!=null &&
              this.retrievalReferenceNumber.equals(other.getRetrievalReferenceNumber()))) &&
            ((this.approvalCode==null && other.getApprovalCode()==null) || 
             (this.approvalCode!=null &&
              this.approvalCode.equals(other.getApprovalCode()))) &&
            ((this.TNPFlag==null && other.getTNPFlag()==null) || 
             (this.TNPFlag!=null &&
              this.TNPFlag.equals(other.getTNPFlag()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.loadType==null && other.getLoadType()==null) || 
             (this.loadType!=null &&
              this.loadType.equals(other.getLoadType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMessageTypeIdentifier() != null) {
            _hashCode += getMessageTypeIdentifier().hashCode();
        }
        if (getCustNbr() != null) {
            _hashCode += getCustNbr().hashCode();
        }
        if (getPrimaryAccountNumber() != null) {
            _hashCode += getPrimaryAccountNumber().hashCode();
        }
        if (getTranType() != null) {
            _hashCode += getTranType().hashCode();
        }
        if (getTransactionAmount() != null) {
            _hashCode += getTransactionAmount().hashCode();
        }
        if (getTransmissionDateTime() != null) {
            _hashCode += getTransmissionDateTime().hashCode();
        }
        if (getLineItemSeqNumber() != null) {
            _hashCode += getLineItemSeqNumber().hashCode();
        }
        if (getInventoryCode() != null) {
            _hashCode += getInventoryCode().hashCode();
        }
        if (getQuantity() != null) {
            _hashCode += getQuantity().hashCode();
        }
        if (getUnitPrice() != null) {
            _hashCode += getUnitPrice().hashCode();
        }
        if (getCreditPlanMaster() != null) {
            _hashCode += getCreditPlanMaster().hashCode();
        }
        if (getTransactionDescription() != null) {
            _hashCode += getTransactionDescription().hashCode();
        }
        if (getTransactionCurrencyCode() != null) {
            _hashCode += getTransactionCurrencyCode().hashCode();
        }
        if (getSpecialMerchantIdentifier() != null) {
            _hashCode += getSpecialMerchantIdentifier().hashCode();
        }
        if (getCardAcceptorIdCode() != null) {
            _hashCode += getCardAcceptorIdCode().hashCode();
        }
        if (getCardAcceptorTerminalID() != null) {
            _hashCode += getCardAcceptorTerminalID().hashCode();
        }
        if (getCardAcceptorBusinessCode() != null) {
            _hashCode += getCardAcceptorBusinessCode().hashCode();
        }
        if (getDateTimeLocalTransaction() != null) {
            _hashCode += getDateTimeLocalTransaction().hashCode();
        }
        if (getSystemTraceAuditNumber() != null) {
            _hashCode += getSystemTraceAuditNumber().hashCode();
        }
        if (getMerchantType() != null) {
            _hashCode += getMerchantType().hashCode();
        }
        if (getRetrievalReferenceNumber() != null) {
            _hashCode += getRetrievalReferenceNumber().hashCode();
        }
        if (getApprovalCode() != null) {
            _hashCode += getApprovalCode().hashCode();
        }
        if (getTNPFlag() != null) {
            _hashCode += getTNPFlag().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getLoadType() != null) {
            _hashCode += getLoadType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ClearingRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ClearingRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageTypeIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MessageTypeIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("custNbr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CustNbr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("primaryAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PrimaryAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tranType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TranType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransmissionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineItemSeqNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LineItemSeqNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("inventoryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "InventoryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quantity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "Quantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UnitPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creditPlanMaster");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CreditPlanMaster"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionCurrencyCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionCurrencyCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("specialMerchantIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SpecialMerchantIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorIdCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorIdCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorTerminalID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorTerminalID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardAcceptorBusinessCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardAcceptorBusinessCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateTimeLocalTransaction");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DateTimeLocalTransaction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemTraceAuditNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SystemTraceAuditNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("merchantType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MerchantType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("retrievalReferenceNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RetrievalReferenceNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("approvalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ApprovalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TNPFlag");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TNPFlag"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("loadType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LoadType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
