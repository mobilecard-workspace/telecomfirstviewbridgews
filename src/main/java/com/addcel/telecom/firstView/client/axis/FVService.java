/**
 * FVService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public interface FVService extends javax.xml.rpc.Service {
    public java.lang.String getFVServiceSoapAddress();

    public com.addcel.telecom.firstView.client.axis.FVServiceSoap getFVServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.addcel.telecom.firstView.client.axis.FVServiceSoap getFVServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
