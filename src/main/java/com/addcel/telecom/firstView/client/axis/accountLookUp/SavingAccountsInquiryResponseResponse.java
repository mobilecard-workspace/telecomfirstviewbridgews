/**
 * SavingAccountsInquiryResponseResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SavingAccountsInquiryResponseResponse  implements java.io.Serializable {
    private java.lang.String ERROR_FOUND;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERRMSG;

    private java.lang.String SAVING_ACCOUNT_STATUS;

    private java.lang.String savingAccountNumber;

    private java.lang.String SAVING_ACCOUNT_BALANCE;

    private java.lang.String SAVING_ACCOUNT_ADMINNUMBER;

    public SavingAccountsInquiryResponseResponse() {
    }

    public SavingAccountsInquiryResponseResponse(
           java.lang.String ERROR_FOUND,
           java.lang.String ERR_NUMBER,
           java.lang.String ERRMSG,
           java.lang.String SAVING_ACCOUNT_STATUS,
           java.lang.String savingAccountNumber,
           java.lang.String SAVING_ACCOUNT_BALANCE,
           java.lang.String SAVING_ACCOUNT_ADMINNUMBER) {
           this.ERROR_FOUND = ERROR_FOUND;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERRMSG = ERRMSG;
           this.SAVING_ACCOUNT_STATUS = SAVING_ACCOUNT_STATUS;
           this.savingAccountNumber = savingAccountNumber;
           this.SAVING_ACCOUNT_BALANCE = SAVING_ACCOUNT_BALANCE;
           this.SAVING_ACCOUNT_ADMINNUMBER = SAVING_ACCOUNT_ADMINNUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the ERR_NUMBER value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERRMSG value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the SAVING_ACCOUNT_STATUS value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return SAVING_ACCOUNT_STATUS
     */
    public java.lang.String getSAVING_ACCOUNT_STATUS() {
        return SAVING_ACCOUNT_STATUS;
    }


    /**
     * Sets the SAVING_ACCOUNT_STATUS value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param SAVING_ACCOUNT_STATUS
     */
    public void setSAVING_ACCOUNT_STATUS(java.lang.String SAVING_ACCOUNT_STATUS) {
        this.SAVING_ACCOUNT_STATUS = SAVING_ACCOUNT_STATUS;
    }


    /**
     * Gets the savingAccountNumber value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return savingAccountNumber
     */
    public java.lang.String getSavingAccountNumber() {
        return savingAccountNumber;
    }


    /**
     * Sets the savingAccountNumber value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param savingAccountNumber
     */
    public void setSavingAccountNumber(java.lang.String savingAccountNumber) {
        this.savingAccountNumber = savingAccountNumber;
    }


    /**
     * Gets the SAVING_ACCOUNT_BALANCE value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return SAVING_ACCOUNT_BALANCE
     */
    public java.lang.String getSAVING_ACCOUNT_BALANCE() {
        return SAVING_ACCOUNT_BALANCE;
    }


    /**
     * Sets the SAVING_ACCOUNT_BALANCE value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param SAVING_ACCOUNT_BALANCE
     */
    public void setSAVING_ACCOUNT_BALANCE(java.lang.String SAVING_ACCOUNT_BALANCE) {
        this.SAVING_ACCOUNT_BALANCE = SAVING_ACCOUNT_BALANCE;
    }


    /**
     * Gets the SAVING_ACCOUNT_ADMINNUMBER value for this SavingAccountsInquiryResponseResponse.
     * 
     * @return SAVING_ACCOUNT_ADMINNUMBER
     */
    public java.lang.String getSAVING_ACCOUNT_ADMINNUMBER() {
        return SAVING_ACCOUNT_ADMINNUMBER;
    }


    /**
     * Sets the SAVING_ACCOUNT_ADMINNUMBER value for this SavingAccountsInquiryResponseResponse.
     * 
     * @param SAVING_ACCOUNT_ADMINNUMBER
     */
    public void setSAVING_ACCOUNT_ADMINNUMBER(java.lang.String SAVING_ACCOUNT_ADMINNUMBER) {
        this.SAVING_ACCOUNT_ADMINNUMBER = SAVING_ACCOUNT_ADMINNUMBER;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SavingAccountsInquiryResponseResponse)) return false;
        SavingAccountsInquiryResponseResponse other = (SavingAccountsInquiryResponseResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.SAVING_ACCOUNT_STATUS==null && other.getSAVING_ACCOUNT_STATUS()==null) || 
             (this.SAVING_ACCOUNT_STATUS!=null &&
              this.SAVING_ACCOUNT_STATUS.equals(other.getSAVING_ACCOUNT_STATUS()))) &&
            ((this.savingAccountNumber==null && other.getSavingAccountNumber()==null) || 
             (this.savingAccountNumber!=null &&
              this.savingAccountNumber.equals(other.getSavingAccountNumber()))) &&
            ((this.SAVING_ACCOUNT_BALANCE==null && other.getSAVING_ACCOUNT_BALANCE()==null) || 
             (this.SAVING_ACCOUNT_BALANCE!=null &&
              this.SAVING_ACCOUNT_BALANCE.equals(other.getSAVING_ACCOUNT_BALANCE()))) &&
            ((this.SAVING_ACCOUNT_ADMINNUMBER==null && other.getSAVING_ACCOUNT_ADMINNUMBER()==null) || 
             (this.SAVING_ACCOUNT_ADMINNUMBER!=null &&
              this.SAVING_ACCOUNT_ADMINNUMBER.equals(other.getSAVING_ACCOUNT_ADMINNUMBER())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getSAVING_ACCOUNT_STATUS() != null) {
            _hashCode += getSAVING_ACCOUNT_STATUS().hashCode();
        }
        if (getSavingAccountNumber() != null) {
            _hashCode += getSavingAccountNumber().hashCode();
        }
        if (getSAVING_ACCOUNT_BALANCE() != null) {
            _hashCode += getSAVING_ACCOUNT_BALANCE().hashCode();
        }
        if (getSAVING_ACCOUNT_ADMINNUMBER() != null) {
            _hashCode += getSAVING_ACCOUNT_ADMINNUMBER().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SavingAccountsInquiryResponseResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountsInquiryResponseResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SAVING_ACCOUNT_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SAVING_ACCOUNT_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("savingAccountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SavingAccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SAVING_ACCOUNT_BALANCE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SAVING_ACCOUNT_BALANCE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SAVING_ACCOUNT_ADMINNUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SAVING_ACCOUNT_ADMINNUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
