/**
 * GetCvcResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class GetCvcResponse  implements java.io.Serializable {
    private java.lang.String cvc;

    private java.lang.String errNumber;

    private java.lang.String errmsg;

    private java.lang.String errorFound;

    public GetCvcResponse() {
    }

    public GetCvcResponse(
           java.lang.String cvc,
           java.lang.String errNumber,
           java.lang.String errmsg,
           java.lang.String errorFound) {
           this.cvc = cvc;
           this.errNumber = errNumber;
           this.errmsg = errmsg;
           this.errorFound = errorFound;
    }


    /**
     * Gets the cvc value for this GetCvcResponse.
     * 
     * @return cvc
     */
    public java.lang.String getCvc() {
        return cvc;
    }


    /**
     * Sets the cvc value for this GetCvcResponse.
     * 
     * @param cvc
     */
    public void setCvc(java.lang.String cvc) {
        this.cvc = cvc;
    }


    /**
     * Gets the errNumber value for this GetCvcResponse.
     * 
     * @return errNumber
     */
    public java.lang.String getErrNumber() {
        return errNumber;
    }


    /**
     * Sets the errNumber value for this GetCvcResponse.
     * 
     * @param errNumber
     */
    public void setErrNumber(java.lang.String errNumber) {
        this.errNumber = errNumber;
    }


    /**
     * Gets the errmsg value for this GetCvcResponse.
     * 
     * @return errmsg
     */
    public java.lang.String getErrmsg() {
        return errmsg;
    }


    /**
     * Sets the errmsg value for this GetCvcResponse.
     * 
     * @param errmsg
     */
    public void setErrmsg(java.lang.String errmsg) {
        this.errmsg = errmsg;
    }


    /**
     * Gets the errorFound value for this GetCvcResponse.
     * 
     * @return errorFound
     */
    public java.lang.String getErrorFound() {
        return errorFound;
    }


    /**
     * Sets the errorFound value for this GetCvcResponse.
     * 
     * @param errorFound
     */
    public void setErrorFound(java.lang.String errorFound) {
        this.errorFound = errorFound;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCvcResponse)) return false;
        GetCvcResponse other = (GetCvcResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.cvc==null && other.getCvc()==null) || 
             (this.cvc!=null &&
              this.cvc.equals(other.getCvc()))) &&
            ((this.errNumber==null && other.getErrNumber()==null) || 
             (this.errNumber!=null &&
              this.errNumber.equals(other.getErrNumber()))) &&
            ((this.errmsg==null && other.getErrmsg()==null) || 
             (this.errmsg!=null &&
              this.errmsg.equals(other.getErrmsg()))) &&
            ((this.errorFound==null && other.getErrorFound()==null) || 
             (this.errorFound!=null &&
              this.errorFound.equals(other.getErrorFound())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCvc() != null) {
            _hashCode += getCvc().hashCode();
        }
        if (getErrNumber() != null) {
            _hashCode += getErrNumber().hashCode();
        }
        if (getErrmsg() != null) {
            _hashCode += getErrmsg().hashCode();
        }
        if (getErrorFound() != null) {
            _hashCode += getErrorFound().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCvcResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "GetCvcResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cvc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Cvc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ErrNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errmsg");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Errmsg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorFound");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ErrorFound"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
