/**
 * RequestCustomerServiceHelpRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class RequestCustomerServiceHelpRequest  implements java.io.Serializable {
    private java.lang.String CARD_USERID;

    private java.lang.String USER_PASSWORD;

    private java.lang.String ACCOUNT_NO;

    private java.lang.String CARD_NUMBER;

    private java.lang.String PIN;

    private java.lang.String CARDTYPE;

    private java.lang.String LANG;

    private java.lang.String REGARDING;

    private java.lang.String CONTACT_BY;

    private java.lang.String CONTACT_ON;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public RequestCustomerServiceHelpRequest() {
    }

    public RequestCustomerServiceHelpRequest(
           java.lang.String CARD_USERID,
           java.lang.String USER_PASSWORD,
           java.lang.String ACCOUNT_NO,
           java.lang.String CARD_NUMBER,
           java.lang.String PIN,
           java.lang.String CARDTYPE,
           java.lang.String LANG,
           java.lang.String REGARDING,
           java.lang.String CONTACT_BY,
           java.lang.String CONTACT_ON,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.CARD_USERID = CARD_USERID;
           this.USER_PASSWORD = USER_PASSWORD;
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.CARD_NUMBER = CARD_NUMBER;
           this.PIN = PIN;
           this.CARDTYPE = CARDTYPE;
           this.LANG = LANG;
           this.REGARDING = REGARDING;
           this.CONTACT_BY = CONTACT_BY;
           this.CONTACT_ON = CONTACT_ON;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the CARD_USERID value for this RequestCustomerServiceHelpRequest.
     * 
     * @return CARD_USERID
     */
    public java.lang.String getCARD_USERID() {
        return CARD_USERID;
    }


    /**
     * Sets the CARD_USERID value for this RequestCustomerServiceHelpRequest.
     * 
     * @param CARD_USERID
     */
    public void setCARD_USERID(java.lang.String CARD_USERID) {
        this.CARD_USERID = CARD_USERID;
    }


    /**
     * Gets the USER_PASSWORD value for this RequestCustomerServiceHelpRequest.
     * 
     * @return USER_PASSWORD
     */
    public java.lang.String getUSER_PASSWORD() {
        return USER_PASSWORD;
    }


    /**
     * Sets the USER_PASSWORD value for this RequestCustomerServiceHelpRequest.
     * 
     * @param USER_PASSWORD
     */
    public void setUSER_PASSWORD(java.lang.String USER_PASSWORD) {
        this.USER_PASSWORD = USER_PASSWORD;
    }


    /**
     * Gets the ACCOUNT_NO value for this RequestCustomerServiceHelpRequest.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this RequestCustomerServiceHelpRequest.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the CARD_NUMBER value for this RequestCustomerServiceHelpRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this RequestCustomerServiceHelpRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the PIN value for this RequestCustomerServiceHelpRequest.
     * 
     * @return PIN
     */
    public java.lang.String getPIN() {
        return PIN;
    }


    /**
     * Sets the PIN value for this RequestCustomerServiceHelpRequest.
     * 
     * @param PIN
     */
    public void setPIN(java.lang.String PIN) {
        this.PIN = PIN;
    }


    /**
     * Gets the CARDTYPE value for this RequestCustomerServiceHelpRequest.
     * 
     * @return CARDTYPE
     */
    public java.lang.String getCARDTYPE() {
        return CARDTYPE;
    }


    /**
     * Sets the CARDTYPE value for this RequestCustomerServiceHelpRequest.
     * 
     * @param CARDTYPE
     */
    public void setCARDTYPE(java.lang.String CARDTYPE) {
        this.CARDTYPE = CARDTYPE;
    }


    /**
     * Gets the LANG value for this RequestCustomerServiceHelpRequest.
     * 
     * @return LANG
     */
    public java.lang.String getLANG() {
        return LANG;
    }


    /**
     * Sets the LANG value for this RequestCustomerServiceHelpRequest.
     * 
     * @param LANG
     */
    public void setLANG(java.lang.String LANG) {
        this.LANG = LANG;
    }


    /**
     * Gets the REGARDING value for this RequestCustomerServiceHelpRequest.
     * 
     * @return REGARDING
     */
    public java.lang.String getREGARDING() {
        return REGARDING;
    }


    /**
     * Sets the REGARDING value for this RequestCustomerServiceHelpRequest.
     * 
     * @param REGARDING
     */
    public void setREGARDING(java.lang.String REGARDING) {
        this.REGARDING = REGARDING;
    }


    /**
     * Gets the CONTACT_BY value for this RequestCustomerServiceHelpRequest.
     * 
     * @return CONTACT_BY
     */
    public java.lang.String getCONTACT_BY() {
        return CONTACT_BY;
    }


    /**
     * Sets the CONTACT_BY value for this RequestCustomerServiceHelpRequest.
     * 
     * @param CONTACT_BY
     */
    public void setCONTACT_BY(java.lang.String CONTACT_BY) {
        this.CONTACT_BY = CONTACT_BY;
    }


    /**
     * Gets the CONTACT_ON value for this RequestCustomerServiceHelpRequest.
     * 
     * @return CONTACT_ON
     */
    public java.lang.String getCONTACT_ON() {
        return CONTACT_ON;
    }


    /**
     * Sets the CONTACT_ON value for this RequestCustomerServiceHelpRequest.
     * 
     * @param CONTACT_ON
     */
    public void setCONTACT_ON(java.lang.String CONTACT_ON) {
        this.CONTACT_ON = CONTACT_ON;
    }


    /**
     * Gets the ANI value for this RequestCustomerServiceHelpRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this RequestCustomerServiceHelpRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this RequestCustomerServiceHelpRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this RequestCustomerServiceHelpRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestCustomerServiceHelpRequest)) return false;
        RequestCustomerServiceHelpRequest other = (RequestCustomerServiceHelpRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CARD_USERID==null && other.getCARD_USERID()==null) || 
             (this.CARD_USERID!=null &&
              this.CARD_USERID.equals(other.getCARD_USERID()))) &&
            ((this.USER_PASSWORD==null && other.getUSER_PASSWORD()==null) || 
             (this.USER_PASSWORD!=null &&
              this.USER_PASSWORD.equals(other.getUSER_PASSWORD()))) &&
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.PIN==null && other.getPIN()==null) || 
             (this.PIN!=null &&
              this.PIN.equals(other.getPIN()))) &&
            ((this.CARDTYPE==null && other.getCARDTYPE()==null) || 
             (this.CARDTYPE!=null &&
              this.CARDTYPE.equals(other.getCARDTYPE()))) &&
            ((this.LANG==null && other.getLANG()==null) || 
             (this.LANG!=null &&
              this.LANG.equals(other.getLANG()))) &&
            ((this.REGARDING==null && other.getREGARDING()==null) || 
             (this.REGARDING!=null &&
              this.REGARDING.equals(other.getREGARDING()))) &&
            ((this.CONTACT_BY==null && other.getCONTACT_BY()==null) || 
             (this.CONTACT_BY!=null &&
              this.CONTACT_BY.equals(other.getCONTACT_BY()))) &&
            ((this.CONTACT_ON==null && other.getCONTACT_ON()==null) || 
             (this.CONTACT_ON!=null &&
              this.CONTACT_ON.equals(other.getCONTACT_ON()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCARD_USERID() != null) {
            _hashCode += getCARD_USERID().hashCode();
        }
        if (getUSER_PASSWORD() != null) {
            _hashCode += getUSER_PASSWORD().hashCode();
        }
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getPIN() != null) {
            _hashCode += getPIN().hashCode();
        }
        if (getCARDTYPE() != null) {
            _hashCode += getCARDTYPE().hashCode();
        }
        if (getLANG() != null) {
            _hashCode += getLANG().hashCode();
        }
        if (getREGARDING() != null) {
            _hashCode += getREGARDING().hashCode();
        }
        if (getCONTACT_BY() != null) {
            _hashCode += getCONTACT_BY().hashCode();
        }
        if (getCONTACT_ON() != null) {
            _hashCode += getCONTACT_ON().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestCustomerServiceHelpRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "RequestCustomerServiceHelpRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_USERID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_USERID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USER_PASSWORD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USER_PASSWORD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LANG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LANG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REGARDING");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REGARDING"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTACT_BY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CONTACT_BY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CONTACT_ON");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CONTACT_ON"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
