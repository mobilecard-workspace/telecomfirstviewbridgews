/**
 * SMSEnrollment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SMSEnrollment  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS;

    public SMSEnrollment() {
    }

    public SMSEnrollment(
           com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS) {
           this.SMS_ENROLLMENT_SETTINGS = SMS_ENROLLMENT_SETTINGS;
    }


    /**
     * Gets the SMS_ENROLLMENT_SETTINGS value for this SMSEnrollment.
     * 
     * @return SMS_ENROLLMENT_SETTINGS
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest getSMS_ENROLLMENT_SETTINGS() {
        return SMS_ENROLLMENT_SETTINGS;
    }


    /**
     * Sets the SMS_ENROLLMENT_SETTINGS value for this SMSEnrollment.
     * 
     * @param SMS_ENROLLMENT_SETTINGS
     */
    public void setSMS_ENROLLMENT_SETTINGS(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSEnrollmentRequest SMS_ENROLLMENT_SETTINGS) {
        this.SMS_ENROLLMENT_SETTINGS = SMS_ENROLLMENT_SETTINGS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSEnrollment)) return false;
        SMSEnrollment other = (SMSEnrollment) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.SMS_ENROLLMENT_SETTINGS==null && other.getSMS_ENROLLMENT_SETTINGS()==null) || 
             (this.SMS_ENROLLMENT_SETTINGS!=null &&
              this.SMS_ENROLLMENT_SETTINGS.equals(other.getSMS_ENROLLMENT_SETTINGS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSMS_ENROLLMENT_SETTINGS() != null) {
            _hashCode += getSMS_ENROLLMENT_SETTINGS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSEnrollment.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSEnrollment"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_ENROLLMENT_SETTINGS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ENROLLMENT_SETTINGS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSEnrollmentRequest"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
