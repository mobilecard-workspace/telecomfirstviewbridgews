//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CardUpdateResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CardUpdateResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResErrorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResErrorMsg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardUpdateResponse", propOrder = {
    "accountNumber",
    "resCode",
    "resErrorCode",
    "resErrorMsg"
})
public class CardUpdateResponse {

    @XmlElement(name = "AccountNumber")
    protected String accountNumber;
    @XmlElement(name = "ResCode")
    protected String resCode;
    @XmlElement(name = "ResErrorCode")
    protected String resErrorCode;
    @XmlElement(name = "ResErrorMsg")
    protected String resErrorMsg;

    /**
     * Obtiene el valor de la propiedad accountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Define el valor de la propiedad accountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad resCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResCode() {
        return resCode;
    }

    /**
     * Define el valor de la propiedad resCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResCode(String value) {
        this.resCode = value;
    }

    /**
     * Obtiene el valor de la propiedad resErrorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResErrorCode() {
        return resErrorCode;
    }

    /**
     * Define el valor de la propiedad resErrorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResErrorCode(String value) {
        this.resErrorCode = value;
    }

    /**
     * Obtiene el valor de la propiedad resErrorMsg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResErrorMsg() {
        return resErrorMsg;
    }

    /**
     * Define el valor de la propiedad resErrorMsg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResErrorMsg(String value) {
        this.resErrorMsg = value;
    }

}
