/**
 * SMSAvailabilityResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class SMSAvailabilityResponse  implements java.io.Serializable {
    private java.lang.String ACCOUNT_NO;

    private java.lang.String ADMIN_NO;

    private java.lang.String MOBILE_NUMBER;

    private java.lang.String SMS_STATUS;

    private java.lang.String SMS_ADDRESS;

    private com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponseMESSAGE MESSAGE;

    private java.lang.String ERRMSG;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERROR_FOUND;

    public SMSAvailabilityResponse() {
    }

    public SMSAvailabilityResponse(
           java.lang.String ACCOUNT_NO,
           java.lang.String ADMIN_NO,
           java.lang.String MOBILE_NUMBER,
           java.lang.String SMS_STATUS,
           java.lang.String SMS_ADDRESS,
           com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponseMESSAGE MESSAGE,
           java.lang.String ERRMSG,
           java.lang.String ERR_NUMBER,
           java.lang.String ERROR_FOUND) {
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.ADMIN_NO = ADMIN_NO;
           this.MOBILE_NUMBER = MOBILE_NUMBER;
           this.SMS_STATUS = SMS_STATUS;
           this.SMS_ADDRESS = SMS_ADDRESS;
           this.MESSAGE = MESSAGE;
           this.ERRMSG = ERRMSG;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the ACCOUNT_NO value for this SMSAvailabilityResponse.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this SMSAvailabilityResponse.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the ADMIN_NO value for this SMSAvailabilityResponse.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this SMSAvailabilityResponse.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the MOBILE_NUMBER value for this SMSAvailabilityResponse.
     * 
     * @return MOBILE_NUMBER
     */
    public java.lang.String getMOBILE_NUMBER() {
        return MOBILE_NUMBER;
    }


    /**
     * Sets the MOBILE_NUMBER value for this SMSAvailabilityResponse.
     * 
     * @param MOBILE_NUMBER
     */
    public void setMOBILE_NUMBER(java.lang.String MOBILE_NUMBER) {
        this.MOBILE_NUMBER = MOBILE_NUMBER;
    }


    /**
     * Gets the SMS_STATUS value for this SMSAvailabilityResponse.
     * 
     * @return SMS_STATUS
     */
    public java.lang.String getSMS_STATUS() {
        return SMS_STATUS;
    }


    /**
     * Sets the SMS_STATUS value for this SMSAvailabilityResponse.
     * 
     * @param SMS_STATUS
     */
    public void setSMS_STATUS(java.lang.String SMS_STATUS) {
        this.SMS_STATUS = SMS_STATUS;
    }


    /**
     * Gets the SMS_ADDRESS value for this SMSAvailabilityResponse.
     * 
     * @return SMS_ADDRESS
     */
    public java.lang.String getSMS_ADDRESS() {
        return SMS_ADDRESS;
    }


    /**
     * Sets the SMS_ADDRESS value for this SMSAvailabilityResponse.
     * 
     * @param SMS_ADDRESS
     */
    public void setSMS_ADDRESS(java.lang.String SMS_ADDRESS) {
        this.SMS_ADDRESS = SMS_ADDRESS;
    }


    /**
     * Gets the MESSAGE value for this SMSAvailabilityResponse.
     * 
     * @return MESSAGE
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponseMESSAGE getMESSAGE() {
        return MESSAGE;
    }


    /**
     * Sets the MESSAGE value for this SMSAvailabilityResponse.
     * 
     * @param MESSAGE
     */
    public void setMESSAGE(com.addcel.telecom.firstView.client.axis.accountLookUp.SMSAvailabilityResponseMESSAGE MESSAGE) {
        this.MESSAGE = MESSAGE;
    }


    /**
     * Gets the ERRMSG value for this SMSAvailabilityResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this SMSAvailabilityResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the ERR_NUMBER value for this SMSAvailabilityResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this SMSAvailabilityResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this SMSAvailabilityResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this SMSAvailabilityResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SMSAvailabilityResponse)) return false;
        SMSAvailabilityResponse other = (SMSAvailabilityResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.MOBILE_NUMBER==null && other.getMOBILE_NUMBER()==null) || 
             (this.MOBILE_NUMBER!=null &&
              this.MOBILE_NUMBER.equals(other.getMOBILE_NUMBER()))) &&
            ((this.SMS_STATUS==null && other.getSMS_STATUS()==null) || 
             (this.SMS_STATUS!=null &&
              this.SMS_STATUS.equals(other.getSMS_STATUS()))) &&
            ((this.SMS_ADDRESS==null && other.getSMS_ADDRESS()==null) || 
             (this.SMS_ADDRESS!=null &&
              this.SMS_ADDRESS.equals(other.getSMS_ADDRESS()))) &&
            ((this.MESSAGE==null && other.getMESSAGE()==null) || 
             (this.MESSAGE!=null &&
              this.MESSAGE.equals(other.getMESSAGE()))) &&
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getMOBILE_NUMBER() != null) {
            _hashCode += getMOBILE_NUMBER().hashCode();
        }
        if (getSMS_STATUS() != null) {
            _hashCode += getSMS_STATUS().hashCode();
        }
        if (getSMS_ADDRESS() != null) {
            _hashCode += getSMS_ADDRESS().hashCode();
        }
        if (getMESSAGE() != null) {
            _hashCode += getMESSAGE().hashCode();
        }
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SMSAvailabilityResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMSAvailabilityResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MOBILE_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MOBILE_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_STATUS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_STATUS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("SMS_ADDRESS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "SMS_ADDRESS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("MESSAGE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "MESSAGE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">SMSAvailabilityResponse>MESSAGE"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
