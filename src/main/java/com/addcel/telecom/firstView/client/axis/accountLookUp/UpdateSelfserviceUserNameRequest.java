/**
 * UpdateSelfserviceUserNameRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class UpdateSelfserviceUserNameRequest  implements java.io.Serializable {
    private java.lang.String CLIENT_ID;

    private java.lang.String CLIENT_PASSWORD;

    private java.lang.String LOCATION_ID;

    private java.lang.String LOCATION_CITY;

    private java.lang.String LOCATION_STATE;

    private java.lang.String LOCATION_COUNTRY;

    private java.lang.String CARD_NUMBER;

    private java.lang.String accountNumber;

    private java.lang.String ADMIN_NO;

    private java.lang.String DDA_NO;

    private java.lang.String newSSUserName;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public UpdateSelfserviceUserNameRequest() {
    }

    public UpdateSelfserviceUserNameRequest(
           java.lang.String CLIENT_ID,
           java.lang.String CLIENT_PASSWORD,
           java.lang.String LOCATION_ID,
           java.lang.String LOCATION_CITY,
           java.lang.String LOCATION_STATE,
           java.lang.String LOCATION_COUNTRY,
           java.lang.String CARD_NUMBER,
           java.lang.String accountNumber,
           java.lang.String ADMIN_NO,
           java.lang.String DDA_NO,
           java.lang.String newSSUserName,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.CLIENT_ID = CLIENT_ID;
           this.CLIENT_PASSWORD = CLIENT_PASSWORD;
           this.LOCATION_ID = LOCATION_ID;
           this.LOCATION_CITY = LOCATION_CITY;
           this.LOCATION_STATE = LOCATION_STATE;
           this.LOCATION_COUNTRY = LOCATION_COUNTRY;
           this.CARD_NUMBER = CARD_NUMBER;
           this.accountNumber = accountNumber;
           this.ADMIN_NO = ADMIN_NO;
           this.DDA_NO = DDA_NO;
           this.newSSUserName = newSSUserName;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the CLIENT_ID value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return CLIENT_ID
     */
    public java.lang.String getCLIENT_ID() {
        return CLIENT_ID;
    }


    /**
     * Sets the CLIENT_ID value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param CLIENT_ID
     */
    public void setCLIENT_ID(java.lang.String CLIENT_ID) {
        this.CLIENT_ID = CLIENT_ID;
    }


    /**
     * Gets the CLIENT_PASSWORD value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return CLIENT_PASSWORD
     */
    public java.lang.String getCLIENT_PASSWORD() {
        return CLIENT_PASSWORD;
    }


    /**
     * Sets the CLIENT_PASSWORD value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param CLIENT_PASSWORD
     */
    public void setCLIENT_PASSWORD(java.lang.String CLIENT_PASSWORD) {
        this.CLIENT_PASSWORD = CLIENT_PASSWORD;
    }


    /**
     * Gets the LOCATION_ID value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return LOCATION_ID
     */
    public java.lang.String getLOCATION_ID() {
        return LOCATION_ID;
    }


    /**
     * Sets the LOCATION_ID value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param LOCATION_ID
     */
    public void setLOCATION_ID(java.lang.String LOCATION_ID) {
        this.LOCATION_ID = LOCATION_ID;
    }


    /**
     * Gets the LOCATION_CITY value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return LOCATION_CITY
     */
    public java.lang.String getLOCATION_CITY() {
        return LOCATION_CITY;
    }


    /**
     * Sets the LOCATION_CITY value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param LOCATION_CITY
     */
    public void setLOCATION_CITY(java.lang.String LOCATION_CITY) {
        this.LOCATION_CITY = LOCATION_CITY;
    }


    /**
     * Gets the LOCATION_STATE value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return LOCATION_STATE
     */
    public java.lang.String getLOCATION_STATE() {
        return LOCATION_STATE;
    }


    /**
     * Sets the LOCATION_STATE value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param LOCATION_STATE
     */
    public void setLOCATION_STATE(java.lang.String LOCATION_STATE) {
        this.LOCATION_STATE = LOCATION_STATE;
    }


    /**
     * Gets the LOCATION_COUNTRY value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return LOCATION_COUNTRY
     */
    public java.lang.String getLOCATION_COUNTRY() {
        return LOCATION_COUNTRY;
    }


    /**
     * Sets the LOCATION_COUNTRY value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param LOCATION_COUNTRY
     */
    public void setLOCATION_COUNTRY(java.lang.String LOCATION_COUNTRY) {
        this.LOCATION_COUNTRY = LOCATION_COUNTRY;
    }


    /**
     * Gets the CARD_NUMBER value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the accountNumber value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return accountNumber
     */
    public java.lang.String getAccountNumber() {
        return accountNumber;
    }


    /**
     * Sets the accountNumber value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param accountNumber
     */
    public void setAccountNumber(java.lang.String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Gets the ADMIN_NO value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return ADMIN_NO
     */
    public java.lang.String getADMIN_NO() {
        return ADMIN_NO;
    }


    /**
     * Sets the ADMIN_NO value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param ADMIN_NO
     */
    public void setADMIN_NO(java.lang.String ADMIN_NO) {
        this.ADMIN_NO = ADMIN_NO;
    }


    /**
     * Gets the DDA_NO value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return DDA_NO
     */
    public java.lang.String getDDA_NO() {
        return DDA_NO;
    }


    /**
     * Sets the DDA_NO value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param DDA_NO
     */
    public void setDDA_NO(java.lang.String DDA_NO) {
        this.DDA_NO = DDA_NO;
    }


    /**
     * Gets the newSSUserName value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return newSSUserName
     */
    public java.lang.String getNewSSUserName() {
        return newSSUserName;
    }


    /**
     * Sets the newSSUserName value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param newSSUserName
     */
    public void setNewSSUserName(java.lang.String newSSUserName) {
        this.newSSUserName = newSSUserName;
    }


    /**
     * Gets the ANI value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this UpdateSelfserviceUserNameRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this UpdateSelfserviceUserNameRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateSelfserviceUserNameRequest)) return false;
        UpdateSelfserviceUserNameRequest other = (UpdateSelfserviceUserNameRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CLIENT_ID==null && other.getCLIENT_ID()==null) || 
             (this.CLIENT_ID!=null &&
              this.CLIENT_ID.equals(other.getCLIENT_ID()))) &&
            ((this.CLIENT_PASSWORD==null && other.getCLIENT_PASSWORD()==null) || 
             (this.CLIENT_PASSWORD!=null &&
              this.CLIENT_PASSWORD.equals(other.getCLIENT_PASSWORD()))) &&
            ((this.LOCATION_ID==null && other.getLOCATION_ID()==null) || 
             (this.LOCATION_ID!=null &&
              this.LOCATION_ID.equals(other.getLOCATION_ID()))) &&
            ((this.LOCATION_CITY==null && other.getLOCATION_CITY()==null) || 
             (this.LOCATION_CITY!=null &&
              this.LOCATION_CITY.equals(other.getLOCATION_CITY()))) &&
            ((this.LOCATION_STATE==null && other.getLOCATION_STATE()==null) || 
             (this.LOCATION_STATE!=null &&
              this.LOCATION_STATE.equals(other.getLOCATION_STATE()))) &&
            ((this.LOCATION_COUNTRY==null && other.getLOCATION_COUNTRY()==null) || 
             (this.LOCATION_COUNTRY!=null &&
              this.LOCATION_COUNTRY.equals(other.getLOCATION_COUNTRY()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.accountNumber==null && other.getAccountNumber()==null) || 
             (this.accountNumber!=null &&
              this.accountNumber.equals(other.getAccountNumber()))) &&
            ((this.ADMIN_NO==null && other.getADMIN_NO()==null) || 
             (this.ADMIN_NO!=null &&
              this.ADMIN_NO.equals(other.getADMIN_NO()))) &&
            ((this.DDA_NO==null && other.getDDA_NO()==null) || 
             (this.DDA_NO!=null &&
              this.DDA_NO.equals(other.getDDA_NO()))) &&
            ((this.newSSUserName==null && other.getNewSSUserName()==null) || 
             (this.newSSUserName!=null &&
              this.newSSUserName.equals(other.getNewSSUserName()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCLIENT_ID() != null) {
            _hashCode += getCLIENT_ID().hashCode();
        }
        if (getCLIENT_PASSWORD() != null) {
            _hashCode += getCLIENT_PASSWORD().hashCode();
        }
        if (getLOCATION_ID() != null) {
            _hashCode += getLOCATION_ID().hashCode();
        }
        if (getLOCATION_CITY() != null) {
            _hashCode += getLOCATION_CITY().hashCode();
        }
        if (getLOCATION_STATE() != null) {
            _hashCode += getLOCATION_STATE().hashCode();
        }
        if (getLOCATION_COUNTRY() != null) {
            _hashCode += getLOCATION_COUNTRY().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getAccountNumber() != null) {
            _hashCode += getAccountNumber().hashCode();
        }
        if (getADMIN_NO() != null) {
            _hashCode += getADMIN_NO().hashCode();
        }
        if (getDDA_NO() != null) {
            _hashCode += getDDA_NO().hashCode();
        }
        if (getNewSSUserName() != null) {
            _hashCode += getNewSSUserName().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateSelfserviceUserNameRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "UpdateSelfserviceUserNameRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_PASSWORD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_PASSWORD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accountNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "AccountNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ADMIN_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ADMIN_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDA_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DDA_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newSSUserName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NewSSUserName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
