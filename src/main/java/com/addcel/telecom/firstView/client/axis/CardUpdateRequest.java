/**
 * CardUpdateRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis;

public class CardUpdateRequest  implements java.io.Serializable {
    private java.lang.String address1;

    private java.lang.String addressline2;

    private java.lang.String cardnumber;

    private java.lang.String city;

    private java.lang.String clientId;

    private java.lang.String country;

    private java.lang.String dateOfBirth;

    private java.lang.String emailId;

    private java.lang.String firstName;

    private java.lang.String governmentId;

    private java.lang.String HPhoneCountryCode;

    private java.lang.String HPhoneExtension;

    private java.lang.String homePhoneNumber;

    private java.lang.String idExpirationDate;

    private java.lang.String idIssueCountry;

    private java.lang.String idIssueDate;

    private java.lang.String idIssueState;

    private java.lang.String idNumber;

    private java.lang.String lastName;

    private java.lang.String manualCardStatus;

    private java.lang.String middleName;

    private java.lang.String postalCode;

    private java.lang.String reissue;

    private java.lang.String shipToAddressOne;

    private java.lang.String shipToAddressTwo;

    private java.lang.String shipToCity;

    private java.lang.String shipToCmpnyName;

    private java.lang.String shipToContactName;

    private java.lang.String shipToState;

    private java.lang.String shipToZipCode;

    private java.lang.String ssnNumber;

    private java.lang.String state;

    private java.lang.String userfield1;

    private java.lang.String userfield2;

    private java.lang.String userfield3;

    private java.lang.String userfield4;

    private java.lang.String userfield5;

    public CardUpdateRequest() {
    }

    public CardUpdateRequest(
           java.lang.String address1,
           java.lang.String addressline2,
           java.lang.String cardnumber,
           java.lang.String city,
           java.lang.String clientId,
           java.lang.String country,
           java.lang.String dateOfBirth,
           java.lang.String emailId,
           java.lang.String firstName,
           java.lang.String governmentId,
           java.lang.String HPhoneCountryCode,
           java.lang.String HPhoneExtension,
           java.lang.String homePhoneNumber,
           java.lang.String idExpirationDate,
           java.lang.String idIssueCountry,
           java.lang.String idIssueDate,
           java.lang.String idIssueState,
           java.lang.String idNumber,
           java.lang.String lastName,
           java.lang.String manualCardStatus,
           java.lang.String middleName,
           java.lang.String postalCode,
           java.lang.String reissue,
           java.lang.String shipToAddressOne,
           java.lang.String shipToAddressTwo,
           java.lang.String shipToCity,
           java.lang.String shipToCmpnyName,
           java.lang.String shipToContactName,
           java.lang.String shipToState,
           java.lang.String shipToZipCode,
           java.lang.String ssnNumber,
           java.lang.String state,
           java.lang.String userfield1,
           java.lang.String userfield2,
           java.lang.String userfield3,
           java.lang.String userfield4,
           java.lang.String userfield5) {
           this.address1 = address1;
           this.addressline2 = addressline2;
           this.cardnumber = cardnumber;
           this.city = city;
           this.clientId = clientId;
           this.country = country;
           this.dateOfBirth = dateOfBirth;
           this.emailId = emailId;
           this.firstName = firstName;
           this.governmentId = governmentId;
           this.HPhoneCountryCode = HPhoneCountryCode;
           this.HPhoneExtension = HPhoneExtension;
           this.homePhoneNumber = homePhoneNumber;
           this.idExpirationDate = idExpirationDate;
           this.idIssueCountry = idIssueCountry;
           this.idIssueDate = idIssueDate;
           this.idIssueState = idIssueState;
           this.idNumber = idNumber;
           this.lastName = lastName;
           this.manualCardStatus = manualCardStatus;
           this.middleName = middleName;
           this.postalCode = postalCode;
           this.reissue = reissue;
           this.shipToAddressOne = shipToAddressOne;
           this.shipToAddressTwo = shipToAddressTwo;
           this.shipToCity = shipToCity;
           this.shipToCmpnyName = shipToCmpnyName;
           this.shipToContactName = shipToContactName;
           this.shipToState = shipToState;
           this.shipToZipCode = shipToZipCode;
           this.ssnNumber = ssnNumber;
           this.state = state;
           this.userfield1 = userfield1;
           this.userfield2 = userfield2;
           this.userfield3 = userfield3;
           this.userfield4 = userfield4;
           this.userfield5 = userfield5;
    }


    /**
     * Gets the address1 value for this CardUpdateRequest.
     * 
     * @return address1
     */
    public java.lang.String getAddress1() {
        return address1;
    }


    /**
     * Sets the address1 value for this CardUpdateRequest.
     * 
     * @param address1
     */
    public void setAddress1(java.lang.String address1) {
        this.address1 = address1;
    }


    /**
     * Gets the addressline2 value for this CardUpdateRequest.
     * 
     * @return addressline2
     */
    public java.lang.String getAddressline2() {
        return addressline2;
    }


    /**
     * Sets the addressline2 value for this CardUpdateRequest.
     * 
     * @param addressline2
     */
    public void setAddressline2(java.lang.String addressline2) {
        this.addressline2 = addressline2;
    }


    /**
     * Gets the cardnumber value for this CardUpdateRequest.
     * 
     * @return cardnumber
     */
    public java.lang.String getCardnumber() {
        return cardnumber;
    }


    /**
     * Sets the cardnumber value for this CardUpdateRequest.
     * 
     * @param cardnumber
     */
    public void setCardnumber(java.lang.String cardnumber) {
        this.cardnumber = cardnumber;
    }


    /**
     * Gets the city value for this CardUpdateRequest.
     * 
     * @return city
     */
    public java.lang.String getCity() {
        return city;
    }


    /**
     * Sets the city value for this CardUpdateRequest.
     * 
     * @param city
     */
    public void setCity(java.lang.String city) {
        this.city = city;
    }


    /**
     * Gets the clientId value for this CardUpdateRequest.
     * 
     * @return clientId
     */
    public java.lang.String getClientId() {
        return clientId;
    }


    /**
     * Sets the clientId value for this CardUpdateRequest.
     * 
     * @param clientId
     */
    public void setClientId(java.lang.String clientId) {
        this.clientId = clientId;
    }


    /**
     * Gets the country value for this CardUpdateRequest.
     * 
     * @return country
     */
    public java.lang.String getCountry() {
        return country;
    }


    /**
     * Sets the country value for this CardUpdateRequest.
     * 
     * @param country
     */
    public void setCountry(java.lang.String country) {
        this.country = country;
    }


    /**
     * Gets the dateOfBirth value for this CardUpdateRequest.
     * 
     * @return dateOfBirth
     */
    public java.lang.String getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this CardUpdateRequest.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(java.lang.String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the emailId value for this CardUpdateRequest.
     * 
     * @return emailId
     */
    public java.lang.String getEmailId() {
        return emailId;
    }


    /**
     * Sets the emailId value for this CardUpdateRequest.
     * 
     * @param emailId
     */
    public void setEmailId(java.lang.String emailId) {
        this.emailId = emailId;
    }


    /**
     * Gets the firstName value for this CardUpdateRequest.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this CardUpdateRequest.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the governmentId value for this CardUpdateRequest.
     * 
     * @return governmentId
     */
    public java.lang.String getGovernmentId() {
        return governmentId;
    }


    /**
     * Sets the governmentId value for this CardUpdateRequest.
     * 
     * @param governmentId
     */
    public void setGovernmentId(java.lang.String governmentId) {
        this.governmentId = governmentId;
    }


    /**
     * Gets the HPhoneCountryCode value for this CardUpdateRequest.
     * 
     * @return HPhoneCountryCode
     */
    public java.lang.String getHPhoneCountryCode() {
        return HPhoneCountryCode;
    }


    /**
     * Sets the HPhoneCountryCode value for this CardUpdateRequest.
     * 
     * @param HPhoneCountryCode
     */
    public void setHPhoneCountryCode(java.lang.String HPhoneCountryCode) {
        this.HPhoneCountryCode = HPhoneCountryCode;
    }


    /**
     * Gets the HPhoneExtension value for this CardUpdateRequest.
     * 
     * @return HPhoneExtension
     */
    public java.lang.String getHPhoneExtension() {
        return HPhoneExtension;
    }


    /**
     * Sets the HPhoneExtension value for this CardUpdateRequest.
     * 
     * @param HPhoneExtension
     */
    public void setHPhoneExtension(java.lang.String HPhoneExtension) {
        this.HPhoneExtension = HPhoneExtension;
    }


    /**
     * Gets the homePhoneNumber value for this CardUpdateRequest.
     * 
     * @return homePhoneNumber
     */
    public java.lang.String getHomePhoneNumber() {
        return homePhoneNumber;
    }


    /**
     * Sets the homePhoneNumber value for this CardUpdateRequest.
     * 
     * @param homePhoneNumber
     */
    public void setHomePhoneNumber(java.lang.String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }


    /**
     * Gets the idExpirationDate value for this CardUpdateRequest.
     * 
     * @return idExpirationDate
     */
    public java.lang.String getIdExpirationDate() {
        return idExpirationDate;
    }


    /**
     * Sets the idExpirationDate value for this CardUpdateRequest.
     * 
     * @param idExpirationDate
     */
    public void setIdExpirationDate(java.lang.String idExpirationDate) {
        this.idExpirationDate = idExpirationDate;
    }


    /**
     * Gets the idIssueCountry value for this CardUpdateRequest.
     * 
     * @return idIssueCountry
     */
    public java.lang.String getIdIssueCountry() {
        return idIssueCountry;
    }


    /**
     * Sets the idIssueCountry value for this CardUpdateRequest.
     * 
     * @param idIssueCountry
     */
    public void setIdIssueCountry(java.lang.String idIssueCountry) {
        this.idIssueCountry = idIssueCountry;
    }


    /**
     * Gets the idIssueDate value for this CardUpdateRequest.
     * 
     * @return idIssueDate
     */
    public java.lang.String getIdIssueDate() {
        return idIssueDate;
    }


    /**
     * Sets the idIssueDate value for this CardUpdateRequest.
     * 
     * @param idIssueDate
     */
    public void setIdIssueDate(java.lang.String idIssueDate) {
        this.idIssueDate = idIssueDate;
    }


    /**
     * Gets the idIssueState value for this CardUpdateRequest.
     * 
     * @return idIssueState
     */
    public java.lang.String getIdIssueState() {
        return idIssueState;
    }


    /**
     * Sets the idIssueState value for this CardUpdateRequest.
     * 
     * @param idIssueState
     */
    public void setIdIssueState(java.lang.String idIssueState) {
        this.idIssueState = idIssueState;
    }


    /**
     * Gets the idNumber value for this CardUpdateRequest.
     * 
     * @return idNumber
     */
    public java.lang.String getIdNumber() {
        return idNumber;
    }


    /**
     * Sets the idNumber value for this CardUpdateRequest.
     * 
     * @param idNumber
     */
    public void setIdNumber(java.lang.String idNumber) {
        this.idNumber = idNumber;
    }


    /**
     * Gets the lastName value for this CardUpdateRequest.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this CardUpdateRequest.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the manualCardStatus value for this CardUpdateRequest.
     * 
     * @return manualCardStatus
     */
    public java.lang.String getManualCardStatus() {
        return manualCardStatus;
    }


    /**
     * Sets the manualCardStatus value for this CardUpdateRequest.
     * 
     * @param manualCardStatus
     */
    public void setManualCardStatus(java.lang.String manualCardStatus) {
        this.manualCardStatus = manualCardStatus;
    }


    /**
     * Gets the middleName value for this CardUpdateRequest.
     * 
     * @return middleName
     */
    public java.lang.String getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this CardUpdateRequest.
     * 
     * @param middleName
     */
    public void setMiddleName(java.lang.String middleName) {
        this.middleName = middleName;
    }


    /**
     * Gets the postalCode value for this CardUpdateRequest.
     * 
     * @return postalCode
     */
    public java.lang.String getPostalCode() {
        return postalCode;
    }


    /**
     * Sets the postalCode value for this CardUpdateRequest.
     * 
     * @param postalCode
     */
    public void setPostalCode(java.lang.String postalCode) {
        this.postalCode = postalCode;
    }


    /**
     * Gets the reissue value for this CardUpdateRequest.
     * 
     * @return reissue
     */
    public java.lang.String getReissue() {
        return reissue;
    }


    /**
     * Sets the reissue value for this CardUpdateRequest.
     * 
     * @param reissue
     */
    public void setReissue(java.lang.String reissue) {
        this.reissue = reissue;
    }


    /**
     * Gets the shipToAddressOne value for this CardUpdateRequest.
     * 
     * @return shipToAddressOne
     */
    public java.lang.String getShipToAddressOne() {
        return shipToAddressOne;
    }


    /**
     * Sets the shipToAddressOne value for this CardUpdateRequest.
     * 
     * @param shipToAddressOne
     */
    public void setShipToAddressOne(java.lang.String shipToAddressOne) {
        this.shipToAddressOne = shipToAddressOne;
    }


    /**
     * Gets the shipToAddressTwo value for this CardUpdateRequest.
     * 
     * @return shipToAddressTwo
     */
    public java.lang.String getShipToAddressTwo() {
        return shipToAddressTwo;
    }


    /**
     * Sets the shipToAddressTwo value for this CardUpdateRequest.
     * 
     * @param shipToAddressTwo
     */
    public void setShipToAddressTwo(java.lang.String shipToAddressTwo) {
        this.shipToAddressTwo = shipToAddressTwo;
    }


    /**
     * Gets the shipToCity value for this CardUpdateRequest.
     * 
     * @return shipToCity
     */
    public java.lang.String getShipToCity() {
        return shipToCity;
    }


    /**
     * Sets the shipToCity value for this CardUpdateRequest.
     * 
     * @param shipToCity
     */
    public void setShipToCity(java.lang.String shipToCity) {
        this.shipToCity = shipToCity;
    }


    /**
     * Gets the shipToCmpnyName value for this CardUpdateRequest.
     * 
     * @return shipToCmpnyName
     */
    public java.lang.String getShipToCmpnyName() {
        return shipToCmpnyName;
    }


    /**
     * Sets the shipToCmpnyName value for this CardUpdateRequest.
     * 
     * @param shipToCmpnyName
     */
    public void setShipToCmpnyName(java.lang.String shipToCmpnyName) {
        this.shipToCmpnyName = shipToCmpnyName;
    }


    /**
     * Gets the shipToContactName value for this CardUpdateRequest.
     * 
     * @return shipToContactName
     */
    public java.lang.String getShipToContactName() {
        return shipToContactName;
    }


    /**
     * Sets the shipToContactName value for this CardUpdateRequest.
     * 
     * @param shipToContactName
     */
    public void setShipToContactName(java.lang.String shipToContactName) {
        this.shipToContactName = shipToContactName;
    }


    /**
     * Gets the shipToState value for this CardUpdateRequest.
     * 
     * @return shipToState
     */
    public java.lang.String getShipToState() {
        return shipToState;
    }


    /**
     * Sets the shipToState value for this CardUpdateRequest.
     * 
     * @param shipToState
     */
    public void setShipToState(java.lang.String shipToState) {
        this.shipToState = shipToState;
    }


    /**
     * Gets the shipToZipCode value for this CardUpdateRequest.
     * 
     * @return shipToZipCode
     */
    public java.lang.String getShipToZipCode() {
        return shipToZipCode;
    }


    /**
     * Sets the shipToZipCode value for this CardUpdateRequest.
     * 
     * @param shipToZipCode
     */
    public void setShipToZipCode(java.lang.String shipToZipCode) {
        this.shipToZipCode = shipToZipCode;
    }


    /**
     * Gets the ssnNumber value for this CardUpdateRequest.
     * 
     * @return ssnNumber
     */
    public java.lang.String getSsnNumber() {
        return ssnNumber;
    }


    /**
     * Sets the ssnNumber value for this CardUpdateRequest.
     * 
     * @param ssnNumber
     */
    public void setSsnNumber(java.lang.String ssnNumber) {
        this.ssnNumber = ssnNumber;
    }


    /**
     * Gets the state value for this CardUpdateRequest.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this CardUpdateRequest.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the userfield1 value for this CardUpdateRequest.
     * 
     * @return userfield1
     */
    public java.lang.String getUserfield1() {
        return userfield1;
    }


    /**
     * Sets the userfield1 value for this CardUpdateRequest.
     * 
     * @param userfield1
     */
    public void setUserfield1(java.lang.String userfield1) {
        this.userfield1 = userfield1;
    }


    /**
     * Gets the userfield2 value for this CardUpdateRequest.
     * 
     * @return userfield2
     */
    public java.lang.String getUserfield2() {
        return userfield2;
    }


    /**
     * Sets the userfield2 value for this CardUpdateRequest.
     * 
     * @param userfield2
     */
    public void setUserfield2(java.lang.String userfield2) {
        this.userfield2 = userfield2;
    }


    /**
     * Gets the userfield3 value for this CardUpdateRequest.
     * 
     * @return userfield3
     */
    public java.lang.String getUserfield3() {
        return userfield3;
    }


    /**
     * Sets the userfield3 value for this CardUpdateRequest.
     * 
     * @param userfield3
     */
    public void setUserfield3(java.lang.String userfield3) {
        this.userfield3 = userfield3;
    }


    /**
     * Gets the userfield4 value for this CardUpdateRequest.
     * 
     * @return userfield4
     */
    public java.lang.String getUserfield4() {
        return userfield4;
    }


    /**
     * Sets the userfield4 value for this CardUpdateRequest.
     * 
     * @param userfield4
     */
    public void setUserfield4(java.lang.String userfield4) {
        this.userfield4 = userfield4;
    }


    /**
     * Gets the userfield5 value for this CardUpdateRequest.
     * 
     * @return userfield5
     */
    public java.lang.String getUserfield5() {
        return userfield5;
    }


    /**
     * Sets the userfield5 value for this CardUpdateRequest.
     * 
     * @param userfield5
     */
    public void setUserfield5(java.lang.String userfield5) {
        this.userfield5 = userfield5;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardUpdateRequest)) return false;
        CardUpdateRequest other = (CardUpdateRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.address1==null && other.getAddress1()==null) || 
             (this.address1!=null &&
              this.address1.equals(other.getAddress1()))) &&
            ((this.addressline2==null && other.getAddressline2()==null) || 
             (this.addressline2!=null &&
              this.addressline2.equals(other.getAddressline2()))) &&
            ((this.cardnumber==null && other.getCardnumber()==null) || 
             (this.cardnumber!=null &&
              this.cardnumber.equals(other.getCardnumber()))) &&
            ((this.city==null && other.getCity()==null) || 
             (this.city!=null &&
              this.city.equals(other.getCity()))) &&
            ((this.clientId==null && other.getClientId()==null) || 
             (this.clientId!=null &&
              this.clientId.equals(other.getClientId()))) &&
            ((this.country==null && other.getCountry()==null) || 
             (this.country!=null &&
              this.country.equals(other.getCountry()))) &&
            ((this.dateOfBirth==null && other.getDateOfBirth()==null) || 
             (this.dateOfBirth!=null &&
              this.dateOfBirth.equals(other.getDateOfBirth()))) &&
            ((this.emailId==null && other.getEmailId()==null) || 
             (this.emailId!=null &&
              this.emailId.equals(other.getEmailId()))) &&
            ((this.firstName==null && other.getFirstName()==null) || 
             (this.firstName!=null &&
              this.firstName.equals(other.getFirstName()))) &&
            ((this.governmentId==null && other.getGovernmentId()==null) || 
             (this.governmentId!=null &&
              this.governmentId.equals(other.getGovernmentId()))) &&
            ((this.HPhoneCountryCode==null && other.getHPhoneCountryCode()==null) || 
             (this.HPhoneCountryCode!=null &&
              this.HPhoneCountryCode.equals(other.getHPhoneCountryCode()))) &&
            ((this.HPhoneExtension==null && other.getHPhoneExtension()==null) || 
             (this.HPhoneExtension!=null &&
              this.HPhoneExtension.equals(other.getHPhoneExtension()))) &&
            ((this.homePhoneNumber==null && other.getHomePhoneNumber()==null) || 
             (this.homePhoneNumber!=null &&
              this.homePhoneNumber.equals(other.getHomePhoneNumber()))) &&
            ((this.idExpirationDate==null && other.getIdExpirationDate()==null) || 
             (this.idExpirationDate!=null &&
              this.idExpirationDate.equals(other.getIdExpirationDate()))) &&
            ((this.idIssueCountry==null && other.getIdIssueCountry()==null) || 
             (this.idIssueCountry!=null &&
              this.idIssueCountry.equals(other.getIdIssueCountry()))) &&
            ((this.idIssueDate==null && other.getIdIssueDate()==null) || 
             (this.idIssueDate!=null &&
              this.idIssueDate.equals(other.getIdIssueDate()))) &&
            ((this.idIssueState==null && other.getIdIssueState()==null) || 
             (this.idIssueState!=null &&
              this.idIssueState.equals(other.getIdIssueState()))) &&
            ((this.idNumber==null && other.getIdNumber()==null) || 
             (this.idNumber!=null &&
              this.idNumber.equals(other.getIdNumber()))) &&
            ((this.lastName==null && other.getLastName()==null) || 
             (this.lastName!=null &&
              this.lastName.equals(other.getLastName()))) &&
            ((this.manualCardStatus==null && other.getManualCardStatus()==null) || 
             (this.manualCardStatus!=null &&
              this.manualCardStatus.equals(other.getManualCardStatus()))) &&
            ((this.middleName==null && other.getMiddleName()==null) || 
             (this.middleName!=null &&
              this.middleName.equals(other.getMiddleName()))) &&
            ((this.postalCode==null && other.getPostalCode()==null) || 
             (this.postalCode!=null &&
              this.postalCode.equals(other.getPostalCode()))) &&
            ((this.reissue==null && other.getReissue()==null) || 
             (this.reissue!=null &&
              this.reissue.equals(other.getReissue()))) &&
            ((this.shipToAddressOne==null && other.getShipToAddressOne()==null) || 
             (this.shipToAddressOne!=null &&
              this.shipToAddressOne.equals(other.getShipToAddressOne()))) &&
            ((this.shipToAddressTwo==null && other.getShipToAddressTwo()==null) || 
             (this.shipToAddressTwo!=null &&
              this.shipToAddressTwo.equals(other.getShipToAddressTwo()))) &&
            ((this.shipToCity==null && other.getShipToCity()==null) || 
             (this.shipToCity!=null &&
              this.shipToCity.equals(other.getShipToCity()))) &&
            ((this.shipToCmpnyName==null && other.getShipToCmpnyName()==null) || 
             (this.shipToCmpnyName!=null &&
              this.shipToCmpnyName.equals(other.getShipToCmpnyName()))) &&
            ((this.shipToContactName==null && other.getShipToContactName()==null) || 
             (this.shipToContactName!=null &&
              this.shipToContactName.equals(other.getShipToContactName()))) &&
            ((this.shipToState==null && other.getShipToState()==null) || 
             (this.shipToState!=null &&
              this.shipToState.equals(other.getShipToState()))) &&
            ((this.shipToZipCode==null && other.getShipToZipCode()==null) || 
             (this.shipToZipCode!=null &&
              this.shipToZipCode.equals(other.getShipToZipCode()))) &&
            ((this.ssnNumber==null && other.getSsnNumber()==null) || 
             (this.ssnNumber!=null &&
              this.ssnNumber.equals(other.getSsnNumber()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.userfield1==null && other.getUserfield1()==null) || 
             (this.userfield1!=null &&
              this.userfield1.equals(other.getUserfield1()))) &&
            ((this.userfield2==null && other.getUserfield2()==null) || 
             (this.userfield2!=null &&
              this.userfield2.equals(other.getUserfield2()))) &&
            ((this.userfield3==null && other.getUserfield3()==null) || 
             (this.userfield3!=null &&
              this.userfield3.equals(other.getUserfield3()))) &&
            ((this.userfield4==null && other.getUserfield4()==null) || 
             (this.userfield4!=null &&
              this.userfield4.equals(other.getUserfield4()))) &&
            ((this.userfield5==null && other.getUserfield5()==null) || 
             (this.userfield5!=null &&
              this.userfield5.equals(other.getUserfield5())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAddress1() != null) {
            _hashCode += getAddress1().hashCode();
        }
        if (getAddressline2() != null) {
            _hashCode += getAddressline2().hashCode();
        }
        if (getCardnumber() != null) {
            _hashCode += getCardnumber().hashCode();
        }
        if (getCity() != null) {
            _hashCode += getCity().hashCode();
        }
        if (getClientId() != null) {
            _hashCode += getClientId().hashCode();
        }
        if (getCountry() != null) {
            _hashCode += getCountry().hashCode();
        }
        if (getDateOfBirth() != null) {
            _hashCode += getDateOfBirth().hashCode();
        }
        if (getEmailId() != null) {
            _hashCode += getEmailId().hashCode();
        }
        if (getFirstName() != null) {
            _hashCode += getFirstName().hashCode();
        }
        if (getGovernmentId() != null) {
            _hashCode += getGovernmentId().hashCode();
        }
        if (getHPhoneCountryCode() != null) {
            _hashCode += getHPhoneCountryCode().hashCode();
        }
        if (getHPhoneExtension() != null) {
            _hashCode += getHPhoneExtension().hashCode();
        }
        if (getHomePhoneNumber() != null) {
            _hashCode += getHomePhoneNumber().hashCode();
        }
        if (getIdExpirationDate() != null) {
            _hashCode += getIdExpirationDate().hashCode();
        }
        if (getIdIssueCountry() != null) {
            _hashCode += getIdIssueCountry().hashCode();
        }
        if (getIdIssueDate() != null) {
            _hashCode += getIdIssueDate().hashCode();
        }
        if (getIdIssueState() != null) {
            _hashCode += getIdIssueState().hashCode();
        }
        if (getIdNumber() != null) {
            _hashCode += getIdNumber().hashCode();
        }
        if (getLastName() != null) {
            _hashCode += getLastName().hashCode();
        }
        if (getManualCardStatus() != null) {
            _hashCode += getManualCardStatus().hashCode();
        }
        if (getMiddleName() != null) {
            _hashCode += getMiddleName().hashCode();
        }
        if (getPostalCode() != null) {
            _hashCode += getPostalCode().hashCode();
        }
        if (getReissue() != null) {
            _hashCode += getReissue().hashCode();
        }
        if (getShipToAddressOne() != null) {
            _hashCode += getShipToAddressOne().hashCode();
        }
        if (getShipToAddressTwo() != null) {
            _hashCode += getShipToAddressTwo().hashCode();
        }
        if (getShipToCity() != null) {
            _hashCode += getShipToCity().hashCode();
        }
        if (getShipToCmpnyName() != null) {
            _hashCode += getShipToCmpnyName().hashCode();
        }
        if (getShipToContactName() != null) {
            _hashCode += getShipToContactName().hashCode();
        }
        if (getShipToState() != null) {
            _hashCode += getShipToState().hashCode();
        }
        if (getShipToZipCode() != null) {
            _hashCode += getShipToZipCode().hashCode();
        }
        if (getSsnNumber() != null) {
            _hashCode += getSsnNumber().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getUserfield1() != null) {
            _hashCode += getUserfield1().hashCode();
        }
        if (getUserfield2() != null) {
            _hashCode += getUserfield2().hashCode();
        }
        if (getUserfield3() != null) {
            _hashCode += getUserfield3().hashCode();
        }
        if (getUserfield4() != null) {
            _hashCode += getUserfield4().hashCode();
        }
        if (getUserfield5() != null) {
            _hashCode += getUserfield5().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardUpdateRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://tempuri.org/", "CardUpdateRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Address1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("addressline2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Addressline2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardnumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Cardnumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("city");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "City"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("clientId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ClientId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("country");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Country"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "DateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "EmailId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("firstName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "FirstName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("governmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "GovernmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HPhoneCountryCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "HPhoneCountryCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("HPhoneExtension");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "HPhoneExtension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("homePhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "HomePhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idExpirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdExpirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idIssueCountry");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdIssueCountry"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idIssueDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdIssueDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idIssueState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdIssueState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "IdNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "LastName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("manualCardStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ManualCardStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("middleName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "MiddleName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("postalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "PostalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reissue");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Reissue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddressOne");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToAddressOne"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddressTwo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToAddressTwo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToCity");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToCity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToCmpnyName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToCmpnyName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToContactName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToContactName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToState");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToState"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToZipCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "ShipToZipCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ssnNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "SsnNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "State"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userfield1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Userfield1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userfield2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Userfield2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userfield3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Userfield3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userfield4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Userfield4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userfield5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://tempuri.org/", "Userfield5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
