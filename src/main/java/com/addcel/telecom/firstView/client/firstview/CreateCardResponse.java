//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CreateCardResponse complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CreateCardResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ProductType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SubProductType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FromCardRange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ToCardRange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DdaNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Pan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SecondaryPan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountCreationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AccountCreationTime" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReferenceNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ExpirationDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CurrencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MerchantGroup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Mcc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GovernmentIdType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="GovernmentId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TerminalId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StoreName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ResponseCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ReasonDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DeAct" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateCardResponse", propOrder = {
    "requestType",
    "productType",
    "subProductType",
    "fromCardRange",
    "toCardRange",
    "accountNumber",
    "ddaNo",
    "pan",
    "secondaryPan",
    "accountCreationDate",
    "accountCreationTime",
    "amount",
    "referenceNo",
    "expirationDate",
    "currencyCode",
    "merchantGroup",
    "mcc",
    "governmentIdType",
    "governmentId",
    "terminalId",
    "storeName",
    "responseCode",
    "reasonDescription",
    "deAct"
})
public class CreateCardResponse {

    @XmlElement(name = "RequestType")
    protected String requestType;
    @XmlElement(name = "ProductType")
    protected String productType;
    @XmlElement(name = "SubProductType")
    protected String subProductType;
    @XmlElement(name = "FromCardRange")
    protected String fromCardRange;
    @XmlElement(name = "ToCardRange")
    protected String toCardRange;
    @XmlElement(name = "AccountNumber")
    protected String accountNumber;
    @XmlElement(name = "DdaNo")
    protected String ddaNo;
    @XmlElement(name = "Pan")
    protected String pan;
    @XmlElement(name = "SecondaryPan")
    protected String secondaryPan;
    @XmlElement(name = "AccountCreationDate")
    protected String accountCreationDate;
    @XmlElement(name = "AccountCreationTime")
    protected String accountCreationTime;
    @XmlElement(name = "Amount")
    protected String amount;
    @XmlElement(name = "ReferenceNo")
    protected String referenceNo;
    @XmlElement(name = "ExpirationDate")
    protected String expirationDate;
    @XmlElement(name = "CurrencyCode")
    protected String currencyCode;
    @XmlElement(name = "MerchantGroup")
    protected String merchantGroup;
    @XmlElement(name = "Mcc")
    protected String mcc;
    @XmlElement(name = "GovernmentIdType")
    protected String governmentIdType;
    @XmlElement(name = "GovernmentId")
    protected String governmentId;
    @XmlElement(name = "TerminalId")
    protected String terminalId;
    @XmlElement(name = "StoreName")
    protected String storeName;
    @XmlElement(name = "ResponseCode")
    protected String responseCode;
    @XmlElement(name = "ReasonDescription")
    protected String reasonDescription;
    @XmlElement(name = "DeAct")
    protected String deAct;

    /**
     * Obtiene el valor de la propiedad requestType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * Define el valor de la propiedad requestType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestType(String value) {
        this.requestType = value;
    }

    /**
     * Obtiene el valor de la propiedad productType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Define el valor de la propiedad productType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductType(String value) {
        this.productType = value;
    }

    /**
     * Obtiene el valor de la propiedad subProductType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubProductType() {
        return subProductType;
    }

    /**
     * Define el valor de la propiedad subProductType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubProductType(String value) {
        this.subProductType = value;
    }

    /**
     * Obtiene el valor de la propiedad fromCardRange.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromCardRange() {
        return fromCardRange;
    }

    /**
     * Define el valor de la propiedad fromCardRange.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromCardRange(String value) {
        this.fromCardRange = value;
    }

    /**
     * Obtiene el valor de la propiedad toCardRange.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getToCardRange() {
        return toCardRange;
    }

    /**
     * Define el valor de la propiedad toCardRange.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setToCardRange(String value) {
        this.toCardRange = value;
    }

    /**
     * Obtiene el valor de la propiedad accountNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Define el valor de la propiedad accountNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad ddaNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDdaNo() {
        return ddaNo;
    }

    /**
     * Define el valor de la propiedad ddaNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDdaNo(String value) {
        this.ddaNo = value;
    }

    /**
     * Obtiene el valor de la propiedad pan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPan() {
        return pan;
    }

    /**
     * Define el valor de la propiedad pan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPan(String value) {
        this.pan = value;
    }

    /**
     * Obtiene el valor de la propiedad secondaryPan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecondaryPan() {
        return secondaryPan;
    }

    /**
     * Define el valor de la propiedad secondaryPan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecondaryPan(String value) {
        this.secondaryPan = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCreationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCreationDate() {
        return accountCreationDate;
    }

    /**
     * Define el valor de la propiedad accountCreationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCreationDate(String value) {
        this.accountCreationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad accountCreationTime.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountCreationTime() {
        return accountCreationTime;
    }

    /**
     * Define el valor de la propiedad accountCreationTime.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountCreationTime(String value) {
        this.accountCreationTime = value;
    }

    /**
     * Obtiene el valor de la propiedad amount.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Define el valor de la propiedad amount.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmount(String value) {
        this.amount = value;
    }

    /**
     * Obtiene el valor de la propiedad referenceNo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferenceNo() {
        return referenceNo;
    }

    /**
     * Define el valor de la propiedad referenceNo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferenceNo(String value) {
        this.referenceNo = value;
    }

    /**
     * Obtiene el valor de la propiedad expirationDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExpirationDate() {
        return expirationDate;
    }

    /**
     * Define el valor de la propiedad expirationDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExpirationDate(String value) {
        this.expirationDate = value;
    }

    /**
     * Obtiene el valor de la propiedad currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Define el valor de la propiedad currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Obtiene el valor de la propiedad merchantGroup.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMerchantGroup() {
        return merchantGroup;
    }

    /**
     * Define el valor de la propiedad merchantGroup.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMerchantGroup(String value) {
        this.merchantGroup = value;
    }

    /**
     * Obtiene el valor de la propiedad mcc.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcc() {
        return mcc;
    }

    /**
     * Define el valor de la propiedad mcc.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcc(String value) {
        this.mcc = value;
    }

    /**
     * Obtiene el valor de la propiedad governmentIdType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovernmentIdType() {
        return governmentIdType;
    }

    /**
     * Define el valor de la propiedad governmentIdType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovernmentIdType(String value) {
        this.governmentIdType = value;
    }

    /**
     * Obtiene el valor de la propiedad governmentId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGovernmentId() {
        return governmentId;
    }

    /**
     * Define el valor de la propiedad governmentId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGovernmentId(String value) {
        this.governmentId = value;
    }

    /**
     * Obtiene el valor de la propiedad terminalId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTerminalId() {
        return terminalId;
    }

    /**
     * Define el valor de la propiedad terminalId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTerminalId(String value) {
        this.terminalId = value;
    }

    /**
     * Obtiene el valor de la propiedad storeName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     * Define el valor de la propiedad storeName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStoreName(String value) {
        this.storeName = value;
    }

    /**
     * Obtiene el valor de la propiedad responseCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * Define el valor de la propiedad responseCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseCode(String value) {
        this.responseCode = value;
    }

    /**
     * Obtiene el valor de la propiedad reasonDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonDescription() {
        return reasonDescription;
    }

    /**
     * Define el valor de la propiedad reasonDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonDescription(String value) {
        this.reasonDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad deAct.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeAct() {
        return deAct;
    }

    /**
     * Define el valor de la propiedad deAct.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeAct(String value) {
        this.deAct = value;
    }

}
