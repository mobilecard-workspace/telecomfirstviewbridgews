/**
 * CardDetailArray.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardDetailArray  implements java.io.Serializable {
    private java.lang.String CARD_NUM;

    public CardDetailArray() {
    }

    public CardDetailArray(
           java.lang.String CARD_NUM) {
           this.CARD_NUM = CARD_NUM;
    }


    /**
     * Gets the CARD_NUM value for this CardDetailArray.
     * 
     * @return CARD_NUM
     */
    public java.lang.String getCARD_NUM() {
        return CARD_NUM;
    }


    /**
     * Sets the CARD_NUM value for this CardDetailArray.
     * 
     * @param CARD_NUM
     */
    public void setCARD_NUM(java.lang.String CARD_NUM) {
        this.CARD_NUM = CARD_NUM;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardDetailArray)) return false;
        CardDetailArray other = (CardDetailArray) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CARD_NUM==null && other.getCARD_NUM()==null) || 
             (this.CARD_NUM!=null &&
              this.CARD_NUM.equals(other.getCARD_NUM())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCARD_NUM() != null) {
            _hashCode += getCARD_NUM().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardDetailArray.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardDetailArray"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
