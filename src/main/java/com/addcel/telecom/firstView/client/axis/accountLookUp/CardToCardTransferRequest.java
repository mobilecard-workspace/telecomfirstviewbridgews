/**
 * CardToCardTransferRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardToCardTransferRequest  implements java.io.Serializable {
    private java.lang.String CARD_NUMBER;

    private java.lang.String PIN;

    private java.lang.String CARD_USERID;

    private java.lang.String USER_PASSWORD;

    private java.lang.String ACCOUNT_NO;

    private java.lang.String CARDNUM;

    private java.lang.String CARDTYPE;

    private java.lang.String LANG;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    private java.lang.String AMOUNT;

    public CardToCardTransferRequest() {
    }

    public CardToCardTransferRequest(
           java.lang.String CARD_NUMBER,
           java.lang.String PIN,
           java.lang.String CARD_USERID,
           java.lang.String USER_PASSWORD,
           java.lang.String ACCOUNT_NO,
           java.lang.String CARDNUM,
           java.lang.String CARDTYPE,
           java.lang.String LANG,
           java.lang.String ANI,
           java.lang.String DNIS,
           java.lang.String AMOUNT) {
           this.CARD_NUMBER = CARD_NUMBER;
           this.PIN = PIN;
           this.CARD_USERID = CARD_USERID;
           this.USER_PASSWORD = USER_PASSWORD;
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.CARDNUM = CARDNUM;
           this.CARDTYPE = CARDTYPE;
           this.LANG = LANG;
           this.ANI = ANI;
           this.DNIS = DNIS;
           this.AMOUNT = AMOUNT;
    }


    /**
     * Gets the CARD_NUMBER value for this CardToCardTransferRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this CardToCardTransferRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the PIN value for this CardToCardTransferRequest.
     * 
     * @return PIN
     */
    public java.lang.String getPIN() {
        return PIN;
    }


    /**
     * Sets the PIN value for this CardToCardTransferRequest.
     * 
     * @param PIN
     */
    public void setPIN(java.lang.String PIN) {
        this.PIN = PIN;
    }


    /**
     * Gets the CARD_USERID value for this CardToCardTransferRequest.
     * 
     * @return CARD_USERID
     */
    public java.lang.String getCARD_USERID() {
        return CARD_USERID;
    }


    /**
     * Sets the CARD_USERID value for this CardToCardTransferRequest.
     * 
     * @param CARD_USERID
     */
    public void setCARD_USERID(java.lang.String CARD_USERID) {
        this.CARD_USERID = CARD_USERID;
    }


    /**
     * Gets the USER_PASSWORD value for this CardToCardTransferRequest.
     * 
     * @return USER_PASSWORD
     */
    public java.lang.String getUSER_PASSWORD() {
        return USER_PASSWORD;
    }


    /**
     * Sets the USER_PASSWORD value for this CardToCardTransferRequest.
     * 
     * @param USER_PASSWORD
     */
    public void setUSER_PASSWORD(java.lang.String USER_PASSWORD) {
        this.USER_PASSWORD = USER_PASSWORD;
    }


    /**
     * Gets the ACCOUNT_NO value for this CardToCardTransferRequest.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this CardToCardTransferRequest.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the CARDNUM value for this CardToCardTransferRequest.
     * 
     * @return CARDNUM
     */
    public java.lang.String getCARDNUM() {
        return CARDNUM;
    }


    /**
     * Sets the CARDNUM value for this CardToCardTransferRequest.
     * 
     * @param CARDNUM
     */
    public void setCARDNUM(java.lang.String CARDNUM) {
        this.CARDNUM = CARDNUM;
    }


    /**
     * Gets the CARDTYPE value for this CardToCardTransferRequest.
     * 
     * @return CARDTYPE
     */
    public java.lang.String getCARDTYPE() {
        return CARDTYPE;
    }


    /**
     * Sets the CARDTYPE value for this CardToCardTransferRequest.
     * 
     * @param CARDTYPE
     */
    public void setCARDTYPE(java.lang.String CARDTYPE) {
        this.CARDTYPE = CARDTYPE;
    }


    /**
     * Gets the LANG value for this CardToCardTransferRequest.
     * 
     * @return LANG
     */
    public java.lang.String getLANG() {
        return LANG;
    }


    /**
     * Sets the LANG value for this CardToCardTransferRequest.
     * 
     * @param LANG
     */
    public void setLANG(java.lang.String LANG) {
        this.LANG = LANG;
    }


    /**
     * Gets the ANI value for this CardToCardTransferRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this CardToCardTransferRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this CardToCardTransferRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this CardToCardTransferRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }


    /**
     * Gets the AMOUNT value for this CardToCardTransferRequest.
     * 
     * @return AMOUNT
     */
    public java.lang.String getAMOUNT() {
        return AMOUNT;
    }


    /**
     * Sets the AMOUNT value for this CardToCardTransferRequest.
     * 
     * @param AMOUNT
     */
    public void setAMOUNT(java.lang.String AMOUNT) {
        this.AMOUNT = AMOUNT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardToCardTransferRequest)) return false;
        CardToCardTransferRequest other = (CardToCardTransferRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.PIN==null && other.getPIN()==null) || 
             (this.PIN!=null &&
              this.PIN.equals(other.getPIN()))) &&
            ((this.CARD_USERID==null && other.getCARD_USERID()==null) || 
             (this.CARD_USERID!=null &&
              this.CARD_USERID.equals(other.getCARD_USERID()))) &&
            ((this.USER_PASSWORD==null && other.getUSER_PASSWORD()==null) || 
             (this.USER_PASSWORD!=null &&
              this.USER_PASSWORD.equals(other.getUSER_PASSWORD()))) &&
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.CARDNUM==null && other.getCARDNUM()==null) || 
             (this.CARDNUM!=null &&
              this.CARDNUM.equals(other.getCARDNUM()))) &&
            ((this.CARDTYPE==null && other.getCARDTYPE()==null) || 
             (this.CARDTYPE!=null &&
              this.CARDTYPE.equals(other.getCARDTYPE()))) &&
            ((this.LANG==null && other.getLANG()==null) || 
             (this.LANG!=null &&
              this.LANG.equals(other.getLANG()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS()))) &&
            ((this.AMOUNT==null && other.getAMOUNT()==null) || 
             (this.AMOUNT!=null &&
              this.AMOUNT.equals(other.getAMOUNT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getPIN() != null) {
            _hashCode += getPIN().hashCode();
        }
        if (getCARD_USERID() != null) {
            _hashCode += getCARD_USERID().hashCode();
        }
        if (getUSER_PASSWORD() != null) {
            _hashCode += getUSER_PASSWORD().hashCode();
        }
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getCARDNUM() != null) {
            _hashCode += getCARDNUM().hashCode();
        }
        if (getCARDTYPE() != null) {
            _hashCode += getCARDTYPE().hashCode();
        }
        if (getLANG() != null) {
            _hashCode += getLANG().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        if (getAMOUNT() != null) {
            _hashCode += getAMOUNT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardToCardTransferRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardToCardTransferRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_USERID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_USERID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("USER_PASSWORD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "USER_PASSWORD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDNUM");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDNUM"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARDTYPE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARDTYPE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LANG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LANG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("AMOUNT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "AMOUNT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
