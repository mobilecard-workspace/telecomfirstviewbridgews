/**
 * CardActivationInquiryResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class CardActivationInquiryResponse  implements java.io.Serializable {
    private java.lang.String ERRMSG;

    private java.lang.String ERR_NUMBER;

    private java.lang.String ERROR_FOUND;

    private java.lang.String CARD_NUMBER;

    private com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr[] cardPendingActivation;

    public CardActivationInquiryResponse() {
    }

    public CardActivationInquiryResponse(
           java.lang.String ERRMSG,
           java.lang.String ERR_NUMBER,
           java.lang.String ERROR_FOUND,
           java.lang.String CARD_NUMBER,
           com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr[] cardPendingActivation) {
           this.ERRMSG = ERRMSG;
           this.ERR_NUMBER = ERR_NUMBER;
           this.ERROR_FOUND = ERROR_FOUND;
           this.CARD_NUMBER = CARD_NUMBER;
           this.cardPendingActivation = cardPendingActivation;
    }


    /**
     * Gets the ERRMSG value for this CardActivationInquiryResponse.
     * 
     * @return ERRMSG
     */
    public java.lang.String getERRMSG() {
        return ERRMSG;
    }


    /**
     * Sets the ERRMSG value for this CardActivationInquiryResponse.
     * 
     * @param ERRMSG
     */
    public void setERRMSG(java.lang.String ERRMSG) {
        this.ERRMSG = ERRMSG;
    }


    /**
     * Gets the ERR_NUMBER value for this CardActivationInquiryResponse.
     * 
     * @return ERR_NUMBER
     */
    public java.lang.String getERR_NUMBER() {
        return ERR_NUMBER;
    }


    /**
     * Sets the ERR_NUMBER value for this CardActivationInquiryResponse.
     * 
     * @param ERR_NUMBER
     */
    public void setERR_NUMBER(java.lang.String ERR_NUMBER) {
        this.ERR_NUMBER = ERR_NUMBER;
    }


    /**
     * Gets the ERROR_FOUND value for this CardActivationInquiryResponse.
     * 
     * @return ERROR_FOUND
     */
    public java.lang.String getERROR_FOUND() {
        return ERROR_FOUND;
    }


    /**
     * Sets the ERROR_FOUND value for this CardActivationInquiryResponse.
     * 
     * @param ERROR_FOUND
     */
    public void setERROR_FOUND(java.lang.String ERROR_FOUND) {
        this.ERROR_FOUND = ERROR_FOUND;
    }


    /**
     * Gets the CARD_NUMBER value for this CardActivationInquiryResponse.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this CardActivationInquiryResponse.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the cardPendingActivation value for this CardActivationInquiryResponse.
     * 
     * @return cardPendingActivation
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr[] getCardPendingActivation() {
        return cardPendingActivation;
    }


    /**
     * Sets the cardPendingActivation value for this CardActivationInquiryResponse.
     * 
     * @param cardPendingActivation
     */
    public void setCardPendingActivation(com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr[] cardPendingActivation) {
        this.cardPendingActivation = cardPendingActivation;
    }

    public com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr getCardPendingActivation(int i) {
        return this.cardPendingActivation[i];
    }

    public void setCardPendingActivation(int i, com.addcel.telecom.firstView.client.axis.accountLookUp.CardPendingActivationArr _value) {
        this.cardPendingActivation[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CardActivationInquiryResponse)) return false;
        CardActivationInquiryResponse other = (CardActivationInquiryResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.ERRMSG==null && other.getERRMSG()==null) || 
             (this.ERRMSG!=null &&
              this.ERRMSG.equals(other.getERRMSG()))) &&
            ((this.ERR_NUMBER==null && other.getERR_NUMBER()==null) || 
             (this.ERR_NUMBER!=null &&
              this.ERR_NUMBER.equals(other.getERR_NUMBER()))) &&
            ((this.ERROR_FOUND==null && other.getERROR_FOUND()==null) || 
             (this.ERROR_FOUND!=null &&
              this.ERROR_FOUND.equals(other.getERROR_FOUND()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.cardPendingActivation==null && other.getCardPendingActivation()==null) || 
             (this.cardPendingActivation!=null &&
              java.util.Arrays.equals(this.cardPendingActivation, other.getCardPendingActivation())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getERRMSG() != null) {
            _hashCode += getERRMSG().hashCode();
        }
        if (getERR_NUMBER() != null) {
            _hashCode += getERR_NUMBER().hashCode();
        }
        if (getERROR_FOUND() != null) {
            _hashCode += getERROR_FOUND().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getCardPendingActivation() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCardPendingActivation());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCardPendingActivation(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CardActivationInquiryResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardActivationInquiryResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERRMSG");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERRMSG"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERR_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERR_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ERROR_FOUND");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ERROR_FOUND"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cardPendingActivation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardPendingActivation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CardPendingActivationArr"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
