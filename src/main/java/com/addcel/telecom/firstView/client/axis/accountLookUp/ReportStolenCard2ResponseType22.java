/**
 * ReportStolenCard2ResponseType22.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ReportStolenCard2ResponseType22  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response REPORTSTOLENCARDEXT_RET;

    public ReportStolenCard2ResponseType22() {
    }

    public ReportStolenCard2ResponseType22(
           com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response REPORTSTOLENCARDEXT_RET) {
           this.REPORTSTOLENCARDEXT_RET = REPORTSTOLENCARDEXT_RET;
    }


    /**
     * Gets the REPORTSTOLENCARDEXT_RET value for this ReportStolenCard2ResponseType22.
     * 
     * @return REPORTSTOLENCARDEXT_RET
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response getREPORTSTOLENCARDEXT_RET() {
        return REPORTSTOLENCARDEXT_RET;
    }


    /**
     * Sets the REPORTSTOLENCARDEXT_RET value for this ReportStolenCard2ResponseType22.
     * 
     * @param REPORTSTOLENCARDEXT_RET
     */
    public void setREPORTSTOLENCARDEXT_RET(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Response REPORTSTOLENCARDEXT_RET) {
        this.REPORTSTOLENCARDEXT_RET = REPORTSTOLENCARDEXT_RET;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReportStolenCard2ResponseType22)) return false;
        ReportStolenCard2ResponseType22 other = (ReportStolenCard2ResponseType22) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.REPORTSTOLENCARDEXT_RET==null && other.getREPORTSTOLENCARDEXT_RET()==null) || 
             (this.REPORTSTOLENCARDEXT_RET!=null &&
              this.REPORTSTOLENCARDEXT_RET.equals(other.getREPORTSTOLENCARDEXT_RET())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getREPORTSTOLENCARDEXT_RET() != null) {
            _hashCode += getREPORTSTOLENCARDEXT_RET().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReportStolenCard2ResponseType22.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">ReportStolenCard2Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REPORTSTOLENCARDEXT_RET");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARDEXT_RET"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Response"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
