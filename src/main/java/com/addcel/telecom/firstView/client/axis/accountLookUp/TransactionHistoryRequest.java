/**
 * TransactionHistoryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class TransactionHistoryRequest  implements java.io.Serializable {
    private java.lang.String CLIENT_ID;

    private java.lang.String CLIENT_PASSWORD;

    private java.lang.String LOCATION_ID;

    private java.lang.String LOCATION_CITY;

    private java.lang.String LOCATION_STATE;

    private java.lang.String LOCATION_COUNTRY;

    private java.lang.String EMPLOYEE_PIN;

    private java.lang.String CLIENT_REFERENCE_ID;

    private java.lang.String CARD_NUMBER;

    private java.lang.String ACCOUNT_NO;

    private java.lang.String NUMBEROFDAYS;

    private java.lang.String NOOFTRANSACTIONS;

    private java.lang.String LASTTRANSACTION;

    private java.lang.String FROMDATE;

    private java.lang.String TODATE;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public TransactionHistoryRequest() {
    }

    public TransactionHistoryRequest(
           java.lang.String CLIENT_ID,
           java.lang.String CLIENT_PASSWORD,
           java.lang.String LOCATION_ID,
           java.lang.String LOCATION_CITY,
           java.lang.String LOCATION_STATE,
           java.lang.String LOCATION_COUNTRY,
           java.lang.String EMPLOYEE_PIN,
           java.lang.String CLIENT_REFERENCE_ID,
           java.lang.String CARD_NUMBER,
           java.lang.String ACCOUNT_NO,
           java.lang.String NUMBEROFDAYS,
           java.lang.String NOOFTRANSACTIONS,
           java.lang.String LASTTRANSACTION,
           java.lang.String FROMDATE,
           java.lang.String TODATE,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.CLIENT_ID = CLIENT_ID;
           this.CLIENT_PASSWORD = CLIENT_PASSWORD;
           this.LOCATION_ID = LOCATION_ID;
           this.LOCATION_CITY = LOCATION_CITY;
           this.LOCATION_STATE = LOCATION_STATE;
           this.LOCATION_COUNTRY = LOCATION_COUNTRY;
           this.EMPLOYEE_PIN = EMPLOYEE_PIN;
           this.CLIENT_REFERENCE_ID = CLIENT_REFERENCE_ID;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.NUMBEROFDAYS = NUMBEROFDAYS;
           this.NOOFTRANSACTIONS = NOOFTRANSACTIONS;
           this.LASTTRANSACTION = LASTTRANSACTION;
           this.FROMDATE = FROMDATE;
           this.TODATE = TODATE;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the CLIENT_ID value for this TransactionHistoryRequest.
     * 
     * @return CLIENT_ID
     */
    public java.lang.String getCLIENT_ID() {
        return CLIENT_ID;
    }


    /**
     * Sets the CLIENT_ID value for this TransactionHistoryRequest.
     * 
     * @param CLIENT_ID
     */
    public void setCLIENT_ID(java.lang.String CLIENT_ID) {
        this.CLIENT_ID = CLIENT_ID;
    }


    /**
     * Gets the CLIENT_PASSWORD value for this TransactionHistoryRequest.
     * 
     * @return CLIENT_PASSWORD
     */
    public java.lang.String getCLIENT_PASSWORD() {
        return CLIENT_PASSWORD;
    }


    /**
     * Sets the CLIENT_PASSWORD value for this TransactionHistoryRequest.
     * 
     * @param CLIENT_PASSWORD
     */
    public void setCLIENT_PASSWORD(java.lang.String CLIENT_PASSWORD) {
        this.CLIENT_PASSWORD = CLIENT_PASSWORD;
    }


    /**
     * Gets the LOCATION_ID value for this TransactionHistoryRequest.
     * 
     * @return LOCATION_ID
     */
    public java.lang.String getLOCATION_ID() {
        return LOCATION_ID;
    }


    /**
     * Sets the LOCATION_ID value for this TransactionHistoryRequest.
     * 
     * @param LOCATION_ID
     */
    public void setLOCATION_ID(java.lang.String LOCATION_ID) {
        this.LOCATION_ID = LOCATION_ID;
    }


    /**
     * Gets the LOCATION_CITY value for this TransactionHistoryRequest.
     * 
     * @return LOCATION_CITY
     */
    public java.lang.String getLOCATION_CITY() {
        return LOCATION_CITY;
    }


    /**
     * Sets the LOCATION_CITY value for this TransactionHistoryRequest.
     * 
     * @param LOCATION_CITY
     */
    public void setLOCATION_CITY(java.lang.String LOCATION_CITY) {
        this.LOCATION_CITY = LOCATION_CITY;
    }


    /**
     * Gets the LOCATION_STATE value for this TransactionHistoryRequest.
     * 
     * @return LOCATION_STATE
     */
    public java.lang.String getLOCATION_STATE() {
        return LOCATION_STATE;
    }


    /**
     * Sets the LOCATION_STATE value for this TransactionHistoryRequest.
     * 
     * @param LOCATION_STATE
     */
    public void setLOCATION_STATE(java.lang.String LOCATION_STATE) {
        this.LOCATION_STATE = LOCATION_STATE;
    }


    /**
     * Gets the LOCATION_COUNTRY value for this TransactionHistoryRequest.
     * 
     * @return LOCATION_COUNTRY
     */
    public java.lang.String getLOCATION_COUNTRY() {
        return LOCATION_COUNTRY;
    }


    /**
     * Sets the LOCATION_COUNTRY value for this TransactionHistoryRequest.
     * 
     * @param LOCATION_COUNTRY
     */
    public void setLOCATION_COUNTRY(java.lang.String LOCATION_COUNTRY) {
        this.LOCATION_COUNTRY = LOCATION_COUNTRY;
    }


    /**
     * Gets the EMPLOYEE_PIN value for this TransactionHistoryRequest.
     * 
     * @return EMPLOYEE_PIN
     */
    public java.lang.String getEMPLOYEE_PIN() {
        return EMPLOYEE_PIN;
    }


    /**
     * Sets the EMPLOYEE_PIN value for this TransactionHistoryRequest.
     * 
     * @param EMPLOYEE_PIN
     */
    public void setEMPLOYEE_PIN(java.lang.String EMPLOYEE_PIN) {
        this.EMPLOYEE_PIN = EMPLOYEE_PIN;
    }


    /**
     * Gets the CLIENT_REFERENCE_ID value for this TransactionHistoryRequest.
     * 
     * @return CLIENT_REFERENCE_ID
     */
    public java.lang.String getCLIENT_REFERENCE_ID() {
        return CLIENT_REFERENCE_ID;
    }


    /**
     * Sets the CLIENT_REFERENCE_ID value for this TransactionHistoryRequest.
     * 
     * @param CLIENT_REFERENCE_ID
     */
    public void setCLIENT_REFERENCE_ID(java.lang.String CLIENT_REFERENCE_ID) {
        this.CLIENT_REFERENCE_ID = CLIENT_REFERENCE_ID;
    }


    /**
     * Gets the CARD_NUMBER value for this TransactionHistoryRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this TransactionHistoryRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ACCOUNT_NO value for this TransactionHistoryRequest.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this TransactionHistoryRequest.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the NUMBEROFDAYS value for this TransactionHistoryRequest.
     * 
     * @return NUMBEROFDAYS
     */
    public java.lang.String getNUMBEROFDAYS() {
        return NUMBEROFDAYS;
    }


    /**
     * Sets the NUMBEROFDAYS value for this TransactionHistoryRequest.
     * 
     * @param NUMBEROFDAYS
     */
    public void setNUMBEROFDAYS(java.lang.String NUMBEROFDAYS) {
        this.NUMBEROFDAYS = NUMBEROFDAYS;
    }


    /**
     * Gets the NOOFTRANSACTIONS value for this TransactionHistoryRequest.
     * 
     * @return NOOFTRANSACTIONS
     */
    public java.lang.String getNOOFTRANSACTIONS() {
        return NOOFTRANSACTIONS;
    }


    /**
     * Sets the NOOFTRANSACTIONS value for this TransactionHistoryRequest.
     * 
     * @param NOOFTRANSACTIONS
     */
    public void setNOOFTRANSACTIONS(java.lang.String NOOFTRANSACTIONS) {
        this.NOOFTRANSACTIONS = NOOFTRANSACTIONS;
    }


    /**
     * Gets the LASTTRANSACTION value for this TransactionHistoryRequest.
     * 
     * @return LASTTRANSACTION
     */
    public java.lang.String getLASTTRANSACTION() {
        return LASTTRANSACTION;
    }


    /**
     * Sets the LASTTRANSACTION value for this TransactionHistoryRequest.
     * 
     * @param LASTTRANSACTION
     */
    public void setLASTTRANSACTION(java.lang.String LASTTRANSACTION) {
        this.LASTTRANSACTION = LASTTRANSACTION;
    }


    /**
     * Gets the FROMDATE value for this TransactionHistoryRequest.
     * 
     * @return FROMDATE
     */
    public java.lang.String getFROMDATE() {
        return FROMDATE;
    }


    /**
     * Sets the FROMDATE value for this TransactionHistoryRequest.
     * 
     * @param FROMDATE
     */
    public void setFROMDATE(java.lang.String FROMDATE) {
        this.FROMDATE = FROMDATE;
    }


    /**
     * Gets the TODATE value for this TransactionHistoryRequest.
     * 
     * @return TODATE
     */
    public java.lang.String getTODATE() {
        return TODATE;
    }


    /**
     * Sets the TODATE value for this TransactionHistoryRequest.
     * 
     * @param TODATE
     */
    public void setTODATE(java.lang.String TODATE) {
        this.TODATE = TODATE;
    }


    /**
     * Gets the ANI value for this TransactionHistoryRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this TransactionHistoryRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this TransactionHistoryRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this TransactionHistoryRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransactionHistoryRequest)) return false;
        TransactionHistoryRequest other = (TransactionHistoryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CLIENT_ID==null && other.getCLIENT_ID()==null) || 
             (this.CLIENT_ID!=null &&
              this.CLIENT_ID.equals(other.getCLIENT_ID()))) &&
            ((this.CLIENT_PASSWORD==null && other.getCLIENT_PASSWORD()==null) || 
             (this.CLIENT_PASSWORD!=null &&
              this.CLIENT_PASSWORD.equals(other.getCLIENT_PASSWORD()))) &&
            ((this.LOCATION_ID==null && other.getLOCATION_ID()==null) || 
             (this.LOCATION_ID!=null &&
              this.LOCATION_ID.equals(other.getLOCATION_ID()))) &&
            ((this.LOCATION_CITY==null && other.getLOCATION_CITY()==null) || 
             (this.LOCATION_CITY!=null &&
              this.LOCATION_CITY.equals(other.getLOCATION_CITY()))) &&
            ((this.LOCATION_STATE==null && other.getLOCATION_STATE()==null) || 
             (this.LOCATION_STATE!=null &&
              this.LOCATION_STATE.equals(other.getLOCATION_STATE()))) &&
            ((this.LOCATION_COUNTRY==null && other.getLOCATION_COUNTRY()==null) || 
             (this.LOCATION_COUNTRY!=null &&
              this.LOCATION_COUNTRY.equals(other.getLOCATION_COUNTRY()))) &&
            ((this.EMPLOYEE_PIN==null && other.getEMPLOYEE_PIN()==null) || 
             (this.EMPLOYEE_PIN!=null &&
              this.EMPLOYEE_PIN.equals(other.getEMPLOYEE_PIN()))) &&
            ((this.CLIENT_REFERENCE_ID==null && other.getCLIENT_REFERENCE_ID()==null) || 
             (this.CLIENT_REFERENCE_ID!=null &&
              this.CLIENT_REFERENCE_ID.equals(other.getCLIENT_REFERENCE_ID()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.NUMBEROFDAYS==null && other.getNUMBEROFDAYS()==null) || 
             (this.NUMBEROFDAYS!=null &&
              this.NUMBEROFDAYS.equals(other.getNUMBEROFDAYS()))) &&
            ((this.NOOFTRANSACTIONS==null && other.getNOOFTRANSACTIONS()==null) || 
             (this.NOOFTRANSACTIONS!=null &&
              this.NOOFTRANSACTIONS.equals(other.getNOOFTRANSACTIONS()))) &&
            ((this.LASTTRANSACTION==null && other.getLASTTRANSACTION()==null) || 
             (this.LASTTRANSACTION!=null &&
              this.LASTTRANSACTION.equals(other.getLASTTRANSACTION()))) &&
            ((this.FROMDATE==null && other.getFROMDATE()==null) || 
             (this.FROMDATE!=null &&
              this.FROMDATE.equals(other.getFROMDATE()))) &&
            ((this.TODATE==null && other.getTODATE()==null) || 
             (this.TODATE!=null &&
              this.TODATE.equals(other.getTODATE()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCLIENT_ID() != null) {
            _hashCode += getCLIENT_ID().hashCode();
        }
        if (getCLIENT_PASSWORD() != null) {
            _hashCode += getCLIENT_PASSWORD().hashCode();
        }
        if (getLOCATION_ID() != null) {
            _hashCode += getLOCATION_ID().hashCode();
        }
        if (getLOCATION_CITY() != null) {
            _hashCode += getLOCATION_CITY().hashCode();
        }
        if (getLOCATION_STATE() != null) {
            _hashCode += getLOCATION_STATE().hashCode();
        }
        if (getLOCATION_COUNTRY() != null) {
            _hashCode += getLOCATION_COUNTRY().hashCode();
        }
        if (getEMPLOYEE_PIN() != null) {
            _hashCode += getEMPLOYEE_PIN().hashCode();
        }
        if (getCLIENT_REFERENCE_ID() != null) {
            _hashCode += getCLIENT_REFERENCE_ID().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getNUMBEROFDAYS() != null) {
            _hashCode += getNUMBEROFDAYS().hashCode();
        }
        if (getNOOFTRANSACTIONS() != null) {
            _hashCode += getNOOFTRANSACTIONS().hashCode();
        }
        if (getLASTTRANSACTION() != null) {
            _hashCode += getLASTTRANSACTION().hashCode();
        }
        if (getFROMDATE() != null) {
            _hashCode += getFROMDATE().hashCode();
        }
        if (getTODATE() != null) {
            _hashCode += getTODATE().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransactionHistoryRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TransactionHistoryRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_PASSWORD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_PASSWORD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMPLOYEE_PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "EMPLOYEE_PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_REFERENCE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_REFERENCE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NUMBEROFDAYS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NUMBEROFDAYS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOOFTRANSACTIONS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NOOFTRANSACTIONS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LASTTRANSACTION");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LASTTRANSACTION"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FROMDATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "FROMDATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("TODATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "TODATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
