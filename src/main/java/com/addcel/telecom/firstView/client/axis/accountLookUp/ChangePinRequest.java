/**
 * ChangePinRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ChangePinRequest  implements java.io.Serializable {
    private java.lang.String CLIENT_ID;

    private java.lang.String CLIENT_PASSWORD;

    private java.lang.String LOCATION_ID;

    private java.lang.String LOCATION_CITY;

    private java.lang.String LOCATION_STATE;

    private java.lang.String LOCATION_COUNTRY;

    private java.lang.String EMPLOYEE_PIN;

    private java.lang.String CARD_NUMBER;

    private java.lang.String ACCOUNT_NO;

    private java.lang.String CARD_PIN;

    private java.lang.String NEWPIN;

    private java.lang.String ID_NUMBER;

    private java.lang.String DOB;

    private java.lang.String ANI;

    private java.lang.String DNIS;

    public ChangePinRequest() {
    }

    public ChangePinRequest(
           java.lang.String CLIENT_ID,
           java.lang.String CLIENT_PASSWORD,
           java.lang.String LOCATION_ID,
           java.lang.String LOCATION_CITY,
           java.lang.String LOCATION_STATE,
           java.lang.String LOCATION_COUNTRY,
           java.lang.String EMPLOYEE_PIN,
           java.lang.String CARD_NUMBER,
           java.lang.String ACCOUNT_NO,
           java.lang.String CARD_PIN,
           java.lang.String NEWPIN,
           java.lang.String ID_NUMBER,
           java.lang.String DOB,
           java.lang.String ANI,
           java.lang.String DNIS) {
           this.CLIENT_ID = CLIENT_ID;
           this.CLIENT_PASSWORD = CLIENT_PASSWORD;
           this.LOCATION_ID = LOCATION_ID;
           this.LOCATION_CITY = LOCATION_CITY;
           this.LOCATION_STATE = LOCATION_STATE;
           this.LOCATION_COUNTRY = LOCATION_COUNTRY;
           this.EMPLOYEE_PIN = EMPLOYEE_PIN;
           this.CARD_NUMBER = CARD_NUMBER;
           this.ACCOUNT_NO = ACCOUNT_NO;
           this.CARD_PIN = CARD_PIN;
           this.NEWPIN = NEWPIN;
           this.ID_NUMBER = ID_NUMBER;
           this.DOB = DOB;
           this.ANI = ANI;
           this.DNIS = DNIS;
    }


    /**
     * Gets the CLIENT_ID value for this ChangePinRequest.
     * 
     * @return CLIENT_ID
     */
    public java.lang.String getCLIENT_ID() {
        return CLIENT_ID;
    }


    /**
     * Sets the CLIENT_ID value for this ChangePinRequest.
     * 
     * @param CLIENT_ID
     */
    public void setCLIENT_ID(java.lang.String CLIENT_ID) {
        this.CLIENT_ID = CLIENT_ID;
    }


    /**
     * Gets the CLIENT_PASSWORD value for this ChangePinRequest.
     * 
     * @return CLIENT_PASSWORD
     */
    public java.lang.String getCLIENT_PASSWORD() {
        return CLIENT_PASSWORD;
    }


    /**
     * Sets the CLIENT_PASSWORD value for this ChangePinRequest.
     * 
     * @param CLIENT_PASSWORD
     */
    public void setCLIENT_PASSWORD(java.lang.String CLIENT_PASSWORD) {
        this.CLIENT_PASSWORD = CLIENT_PASSWORD;
    }


    /**
     * Gets the LOCATION_ID value for this ChangePinRequest.
     * 
     * @return LOCATION_ID
     */
    public java.lang.String getLOCATION_ID() {
        return LOCATION_ID;
    }


    /**
     * Sets the LOCATION_ID value for this ChangePinRequest.
     * 
     * @param LOCATION_ID
     */
    public void setLOCATION_ID(java.lang.String LOCATION_ID) {
        this.LOCATION_ID = LOCATION_ID;
    }


    /**
     * Gets the LOCATION_CITY value for this ChangePinRequest.
     * 
     * @return LOCATION_CITY
     */
    public java.lang.String getLOCATION_CITY() {
        return LOCATION_CITY;
    }


    /**
     * Sets the LOCATION_CITY value for this ChangePinRequest.
     * 
     * @param LOCATION_CITY
     */
    public void setLOCATION_CITY(java.lang.String LOCATION_CITY) {
        this.LOCATION_CITY = LOCATION_CITY;
    }


    /**
     * Gets the LOCATION_STATE value for this ChangePinRequest.
     * 
     * @return LOCATION_STATE
     */
    public java.lang.String getLOCATION_STATE() {
        return LOCATION_STATE;
    }


    /**
     * Sets the LOCATION_STATE value for this ChangePinRequest.
     * 
     * @param LOCATION_STATE
     */
    public void setLOCATION_STATE(java.lang.String LOCATION_STATE) {
        this.LOCATION_STATE = LOCATION_STATE;
    }


    /**
     * Gets the LOCATION_COUNTRY value for this ChangePinRequest.
     * 
     * @return LOCATION_COUNTRY
     */
    public java.lang.String getLOCATION_COUNTRY() {
        return LOCATION_COUNTRY;
    }


    /**
     * Sets the LOCATION_COUNTRY value for this ChangePinRequest.
     * 
     * @param LOCATION_COUNTRY
     */
    public void setLOCATION_COUNTRY(java.lang.String LOCATION_COUNTRY) {
        this.LOCATION_COUNTRY = LOCATION_COUNTRY;
    }


    /**
     * Gets the EMPLOYEE_PIN value for this ChangePinRequest.
     * 
     * @return EMPLOYEE_PIN
     */
    public java.lang.String getEMPLOYEE_PIN() {
        return EMPLOYEE_PIN;
    }


    /**
     * Sets the EMPLOYEE_PIN value for this ChangePinRequest.
     * 
     * @param EMPLOYEE_PIN
     */
    public void setEMPLOYEE_PIN(java.lang.String EMPLOYEE_PIN) {
        this.EMPLOYEE_PIN = EMPLOYEE_PIN;
    }


    /**
     * Gets the CARD_NUMBER value for this ChangePinRequest.
     * 
     * @return CARD_NUMBER
     */
    public java.lang.String getCARD_NUMBER() {
        return CARD_NUMBER;
    }


    /**
     * Sets the CARD_NUMBER value for this ChangePinRequest.
     * 
     * @param CARD_NUMBER
     */
    public void setCARD_NUMBER(java.lang.String CARD_NUMBER) {
        this.CARD_NUMBER = CARD_NUMBER;
    }


    /**
     * Gets the ACCOUNT_NO value for this ChangePinRequest.
     * 
     * @return ACCOUNT_NO
     */
    public java.lang.String getACCOUNT_NO() {
        return ACCOUNT_NO;
    }


    /**
     * Sets the ACCOUNT_NO value for this ChangePinRequest.
     * 
     * @param ACCOUNT_NO
     */
    public void setACCOUNT_NO(java.lang.String ACCOUNT_NO) {
        this.ACCOUNT_NO = ACCOUNT_NO;
    }


    /**
     * Gets the CARD_PIN value for this ChangePinRequest.
     * 
     * @return CARD_PIN
     */
    public java.lang.String getCARD_PIN() {
        return CARD_PIN;
    }


    /**
     * Sets the CARD_PIN value for this ChangePinRequest.
     * 
     * @param CARD_PIN
     */
    public void setCARD_PIN(java.lang.String CARD_PIN) {
        this.CARD_PIN = CARD_PIN;
    }


    /**
     * Gets the NEWPIN value for this ChangePinRequest.
     * 
     * @return NEWPIN
     */
    public java.lang.String getNEWPIN() {
        return NEWPIN;
    }


    /**
     * Sets the NEWPIN value for this ChangePinRequest.
     * 
     * @param NEWPIN
     */
    public void setNEWPIN(java.lang.String NEWPIN) {
        this.NEWPIN = NEWPIN;
    }


    /**
     * Gets the ID_NUMBER value for this ChangePinRequest.
     * 
     * @return ID_NUMBER
     */
    public java.lang.String getID_NUMBER() {
        return ID_NUMBER;
    }


    /**
     * Sets the ID_NUMBER value for this ChangePinRequest.
     * 
     * @param ID_NUMBER
     */
    public void setID_NUMBER(java.lang.String ID_NUMBER) {
        this.ID_NUMBER = ID_NUMBER;
    }


    /**
     * Gets the DOB value for this ChangePinRequest.
     * 
     * @return DOB
     */
    public java.lang.String getDOB() {
        return DOB;
    }


    /**
     * Sets the DOB value for this ChangePinRequest.
     * 
     * @param DOB
     */
    public void setDOB(java.lang.String DOB) {
        this.DOB = DOB;
    }


    /**
     * Gets the ANI value for this ChangePinRequest.
     * 
     * @return ANI
     */
    public java.lang.String getANI() {
        return ANI;
    }


    /**
     * Sets the ANI value for this ChangePinRequest.
     * 
     * @param ANI
     */
    public void setANI(java.lang.String ANI) {
        this.ANI = ANI;
    }


    /**
     * Gets the DNIS value for this ChangePinRequest.
     * 
     * @return DNIS
     */
    public java.lang.String getDNIS() {
        return DNIS;
    }


    /**
     * Sets the DNIS value for this ChangePinRequest.
     * 
     * @param DNIS
     */
    public void setDNIS(java.lang.String DNIS) {
        this.DNIS = DNIS;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ChangePinRequest)) return false;
        ChangePinRequest other = (ChangePinRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.CLIENT_ID==null && other.getCLIENT_ID()==null) || 
             (this.CLIENT_ID!=null &&
              this.CLIENT_ID.equals(other.getCLIENT_ID()))) &&
            ((this.CLIENT_PASSWORD==null && other.getCLIENT_PASSWORD()==null) || 
             (this.CLIENT_PASSWORD!=null &&
              this.CLIENT_PASSWORD.equals(other.getCLIENT_PASSWORD()))) &&
            ((this.LOCATION_ID==null && other.getLOCATION_ID()==null) || 
             (this.LOCATION_ID!=null &&
              this.LOCATION_ID.equals(other.getLOCATION_ID()))) &&
            ((this.LOCATION_CITY==null && other.getLOCATION_CITY()==null) || 
             (this.LOCATION_CITY!=null &&
              this.LOCATION_CITY.equals(other.getLOCATION_CITY()))) &&
            ((this.LOCATION_STATE==null && other.getLOCATION_STATE()==null) || 
             (this.LOCATION_STATE!=null &&
              this.LOCATION_STATE.equals(other.getLOCATION_STATE()))) &&
            ((this.LOCATION_COUNTRY==null && other.getLOCATION_COUNTRY()==null) || 
             (this.LOCATION_COUNTRY!=null &&
              this.LOCATION_COUNTRY.equals(other.getLOCATION_COUNTRY()))) &&
            ((this.EMPLOYEE_PIN==null && other.getEMPLOYEE_PIN()==null) || 
             (this.EMPLOYEE_PIN!=null &&
              this.EMPLOYEE_PIN.equals(other.getEMPLOYEE_PIN()))) &&
            ((this.CARD_NUMBER==null && other.getCARD_NUMBER()==null) || 
             (this.CARD_NUMBER!=null &&
              this.CARD_NUMBER.equals(other.getCARD_NUMBER()))) &&
            ((this.ACCOUNT_NO==null && other.getACCOUNT_NO()==null) || 
             (this.ACCOUNT_NO!=null &&
              this.ACCOUNT_NO.equals(other.getACCOUNT_NO()))) &&
            ((this.CARD_PIN==null && other.getCARD_PIN()==null) || 
             (this.CARD_PIN!=null &&
              this.CARD_PIN.equals(other.getCARD_PIN()))) &&
            ((this.NEWPIN==null && other.getNEWPIN()==null) || 
             (this.NEWPIN!=null &&
              this.NEWPIN.equals(other.getNEWPIN()))) &&
            ((this.ID_NUMBER==null && other.getID_NUMBER()==null) || 
             (this.ID_NUMBER!=null &&
              this.ID_NUMBER.equals(other.getID_NUMBER()))) &&
            ((this.DOB==null && other.getDOB()==null) || 
             (this.DOB!=null &&
              this.DOB.equals(other.getDOB()))) &&
            ((this.ANI==null && other.getANI()==null) || 
             (this.ANI!=null &&
              this.ANI.equals(other.getANI()))) &&
            ((this.DNIS==null && other.getDNIS()==null) || 
             (this.DNIS!=null &&
              this.DNIS.equals(other.getDNIS())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCLIENT_ID() != null) {
            _hashCode += getCLIENT_ID().hashCode();
        }
        if (getCLIENT_PASSWORD() != null) {
            _hashCode += getCLIENT_PASSWORD().hashCode();
        }
        if (getLOCATION_ID() != null) {
            _hashCode += getLOCATION_ID().hashCode();
        }
        if (getLOCATION_CITY() != null) {
            _hashCode += getLOCATION_CITY().hashCode();
        }
        if (getLOCATION_STATE() != null) {
            _hashCode += getLOCATION_STATE().hashCode();
        }
        if (getLOCATION_COUNTRY() != null) {
            _hashCode += getLOCATION_COUNTRY().hashCode();
        }
        if (getEMPLOYEE_PIN() != null) {
            _hashCode += getEMPLOYEE_PIN().hashCode();
        }
        if (getCARD_NUMBER() != null) {
            _hashCode += getCARD_NUMBER().hashCode();
        }
        if (getACCOUNT_NO() != null) {
            _hashCode += getACCOUNT_NO().hashCode();
        }
        if (getCARD_PIN() != null) {
            _hashCode += getCARD_PIN().hashCode();
        }
        if (getNEWPIN() != null) {
            _hashCode += getNEWPIN().hashCode();
        }
        if (getID_NUMBER() != null) {
            _hashCode += getID_NUMBER().hashCode();
        }
        if (getDOB() != null) {
            _hashCode += getDOB().hashCode();
        }
        if (getANI() != null) {
            _hashCode += getANI().hashCode();
        }
        if (getDNIS() != null) {
            _hashCode += getDNIS().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ChangePinRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ChangePinRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CLIENT_PASSWORD");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CLIENT_PASSWORD"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_CITY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_CITY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_STATE");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_STATE"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("LOCATION_COUNTRY");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "LOCATION_COUNTRY"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("EMPLOYEE_PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "EMPLOYEE_PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ACCOUNT_NO");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ACCOUNT_NO"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("CARD_PIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "CARD_PIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NEWPIN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "NEWPIN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ID_NUMBER");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ID_NUMBER"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOB");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DOB"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ANI");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ANI"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DNIS");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "DNIS"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
