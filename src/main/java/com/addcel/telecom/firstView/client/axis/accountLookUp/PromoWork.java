/**
 * PromoWork.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class PromoWork  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request;

    public PromoWork() {
    }

    public PromoWork(
           com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request) {
           this.promoWork_Request = promoWork_Request;
    }


    /**
     * Gets the promoWork_Request value for this PromoWork.
     * 
     * @return promoWork_Request
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest getPromoWork_Request() {
        return promoWork_Request;
    }


    /**
     * Sets the promoWork_Request value for this PromoWork.
     * 
     * @param promoWork_Request
     */
    public void setPromoWork_Request(com.addcel.telecom.firstView.client.axis.accountLookUp.PromoWorkRequest promoWork_Request) {
        this.promoWork_Request = promoWork_Request;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PromoWork)) return false;
        PromoWork other = (PromoWork) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.promoWork_Request==null && other.getPromoWork_Request()==null) || 
             (this.promoWork_Request!=null &&
              this.promoWork_Request.equals(other.getPromoWork_Request())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getPromoWork_Request() != null) {
            _hashCode += getPromoWork_Request().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PromoWork.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">PromoWork"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoWork_Request");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWork_Request"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "PromoWorkRequest"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
