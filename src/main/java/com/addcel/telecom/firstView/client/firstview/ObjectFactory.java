//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2016.10.05 a las 11:46:50 PM CDT 
//


package com.addcel.telecom.firstView.client.firstview;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.addcel.firstView.client.firstview package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserCredentials_QNAME = new QName("http://tempuri.org/", "UserCredentials");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.addcel.firstView.client.firstview
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateCIPCardResponseElement }
     * 
     */
    public CreateCIPCardResponseElement createCreateCIPCardResponseElement() {
        return new CreateCIPCardResponseElement();
    }

    /**
     * Create an instance of {@link CreateCIPCardResponse }
     * 
     */
    public CreateCIPCardResponse createCreateCIPCardResponse() {
        return new CreateCIPCardResponse();
    }

    /**
     * Create an instance of {@link GetCVC }
     * 
     */
    public GetCVC createGetCVC() {
        return new GetCVC();
    }

    /**
     * Create an instance of {@link GetCvcRequest }
     * 
     */
    public GetCvcRequest createGetCvcRequest() {
        return new GetCvcRequest();
    }

    /**
     * Create an instance of {@link CreateCard }
     * 
     */
    public CreateCard createCreateCard() {
        return new CreateCard();
    }

    /**
     * Create an instance of {@link CreateCardRequest }
     * 
     */
    public CreateCardRequest createCreateCardRequest() {
        return new CreateCardRequest();
    }

    /**
     * Create an instance of {@link PictureCardResponseElement }
     * 
     */
    public PictureCardResponseElement createPictureCardResponseElement() {
        return new PictureCardResponseElement();
    }

    /**
     * Create an instance of {@link PictureCardResponse }
     * 
     */
    public PictureCardResponse createPictureCardResponse() {
        return new PictureCardResponse();
    }

    /**
     * Create an instance of {@link ClearingResponseElement }
     * 
     */
    public ClearingResponseElement createClearingResponseElement() {
        return new ClearingResponseElement();
    }

    /**
     * Create an instance of {@link ClearingResponse }
     * 
     */
    public ClearingResponse createClearingResponse() {
        return new ClearingResponse();
    }

    /**
     * Create an instance of {@link GetStatusDescResponse }
     * 
     */
    public GetStatusDescResponse createGetStatusDescResponse() {
        return new GetStatusDescResponse();
    }

    /**
     * Create an instance of {@link GetStatusResponse }
     * 
     */
    public GetStatusResponse createGetStatusResponse() {
        return new GetStatusResponse();
    }

    /**
     * Create an instance of {@link ValidatePin }
     * 
     */
    public ValidatePin createValidatePin() {
        return new ValidatePin();
    }

    /**
     * Create an instance of {@link ValidatePinRequest }
     * 
     */
    public ValidatePinRequest createValidatePinRequest() {
        return new ValidatePinRequest();
    }

    /**
     * Create an instance of {@link CardUpdateResponseElement }
     * 
     */
    public CardUpdateResponseElement createCardUpdateResponseElement() {
        return new CardUpdateResponseElement();
    }

    /**
     * Create an instance of {@link CardUpdateResponse }
     * 
     */
    public CardUpdateResponse createCardUpdateResponse() {
        return new CardUpdateResponse();
    }

    /**
     * Create an instance of {@link GetCVCResponse }
     * 
     */
    public GetCvcResponse createGetCVCResponse() {
        return new GetCvcResponse();
    }

    /**
     * Create an instance of {@link GetCvcResponse }
     * 
     */
    public GetCvcResponse createGetCvcResponse() {
        return new GetCvcResponse();
    }

    /**
     * Create an instance of {@link CreateCIPCard }
     * 
     */
    public CreateCIPCard createCreateCIPCard() {
        return new CreateCIPCard();
    }

    /**
     * Create an instance of {@link CreateCIPCardRequest }
     * 
     */
    public CreateCIPCardRequest createCreateCIPCardRequest() {
        return new CreateCIPCardRequest();
    }

    /**
     * Create an instance of {@link GetStatusDesc }
     * 
     */
    public GetStatusDesc createGetStatusDesc() {
        return new GetStatusDesc();
    }

    /**
     * Create an instance of {@link GetStatusRequest }
     * 
     */
    public GetStatusRequest createGetStatusRequest() {
        return new GetStatusRequest();
    }

    /**
     * Create an instance of {@link CardUpdate }
     * 
     */
    public CardUpdate createCardUpdate() {
        return new CardUpdate();
    }

    /**
     * Create an instance of {@link CardUpdateRequest }
     * 
     */
    public CardUpdateRequest createCardUpdateRequest() {
        return new CardUpdateRequest();
    }

    /**
     * Create an instance of {@link Clearing }
     * 
     */
    public Clearing createClearing() {
        return new Clearing();
    }

    /**
     * Create an instance of {@link ClearingRequest }
     * 
     */
    public ClearingRequest createClearingRequest() {
        return new ClearingRequest();
    }

    /**
     * Create an instance of {@link CreateCardResponseElement }
     * 
     */
    public CreateCardResponseElement createCreateCardResponseElement() {
        return new CreateCardResponseElement();
    }

    /**
     * Create an instance of {@link CreateCardResponse }
     * 
     */
    public CreateCardResponse createCreateCardResponse() {
        return new CreateCardResponse();
    }

    /**
     * Create an instance of {@link PictureCard }
     * 
     */
    public PictureCard createPictureCard() {
        return new PictureCard();
    }

    /**
     * Create an instance of {@link PictureCardRequest }
     * 
     */
    public PictureCardRequest createPictureCardRequest() {
        return new PictureCardRequest();
    }

    /**
     * Create an instance of {@link UserCredentials }
     * 
     */
    public UserCredentials createUserCredentials() {
        return new UserCredentials();
    }

    /**
     * Create an instance of {@link ValidatePinResponseElement }
     * 
     */
    public ValidatePinResponseElement createValidatePinResponseElement() {
        return new ValidatePinResponseElement();
    }

    /**
     * Create an instance of {@link ValidatePinResponse }
     * 
     */
    public ValidatePinResponse createValidatePinResponse() {
        return new ValidatePinResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserCredentials }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "UserCredentials")
    public JAXBElement<UserCredentials> createUserCredentials(UserCredentials value) {
        return new JAXBElement<UserCredentials>(_UserCredentials_QNAME, UserCredentials.class, null, value);
    }

}
