/**
 * ReportStolenCard2.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.addcel.telecom.firstView.client.axis.accountLookUp;

public class ReportStolenCard2  implements java.io.Serializable {
    private com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT;

    public ReportStolenCard2() {
    }

    public ReportStolenCard2(
           com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT) {
           this.REPORTSTOLENCARDEXT = REPORTSTOLENCARDEXT;
    }


    /**
     * Gets the REPORTSTOLENCARDEXT value for this ReportStolenCard2.
     * 
     * @return REPORTSTOLENCARDEXT
     */
    public com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request getREPORTSTOLENCARDEXT() {
        return REPORTSTOLENCARDEXT;
    }


    /**
     * Sets the REPORTSTOLENCARDEXT value for this ReportStolenCard2.
     * 
     * @param REPORTSTOLENCARDEXT
     */
    public void setREPORTSTOLENCARDEXT(com.addcel.telecom.firstView.client.axis.accountLookUp.ReportStolenCard2Request REPORTSTOLENCARDEXT) {
        this.REPORTSTOLENCARDEXT = REPORTSTOLENCARDEXT;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReportStolenCard2)) return false;
        ReportStolenCard2 other = (ReportStolenCard2) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.REPORTSTOLENCARDEXT==null && other.getREPORTSTOLENCARDEXT()==null) || 
             (this.REPORTSTOLENCARDEXT!=null &&
              this.REPORTSTOLENCARDEXT.equals(other.getREPORTSTOLENCARDEXT())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getREPORTSTOLENCARDEXT() != null) {
            _hashCode += getREPORTSTOLENCARDEXT().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReportStolenCard2.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", ">ReportStolenCard2"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("REPORTSTOLENCARDEXT");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "REPORTSTOLENCARDEXT"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.corecard.com/Prepaid", "ReportStolenCard2Request"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
