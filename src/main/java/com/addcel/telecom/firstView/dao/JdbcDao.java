package com.addcel.telecom.firstView.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.addcel.telecom.firstView.model.Clearing;
import com.addcel.telecom.firstView.model.CreateCard;
import com.addcel.telecom.firstView.model.UserCredentials;

public class JdbcDao extends SqlMapClientDaoSupport{

	private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDao.class);
	
	private static final String SQL_FV_CREDENTIALS = "getFVCredentials";
	
	private static final String SQL_CREATE_CARD = "crearCuenta";
	
	private static final String SQL_INSERT_FV_TRAN = "insertaFVTransaccion";
	
	private static final String SQL_UPDATE_FV_TRAN = "updateFVTransaccion";
	
	public UserCredentials getFVCredentials(String id) {
		UserCredentials info = null;
		try {
			LOGGER.info("[FIRSTVIEW] -CONSULTANDO TRANSACCION ANTAD - ID TRANSACCION: "+id);
			info = (UserCredentials) getSqlMapClientTemplate().queryForObject(SQL_FV_CREDENTIALS, id);
			LOGGER.info("[FIRSTVIEW] - TRANSACCION ANTAD ACTUALIZADA EXITOSAMENTE - ID TRANSACCION: "+id);
		} catch (Exception e) {
			LOGGER.error("[FIRSTVIEW] - OCURRIO UN ERROR AL ACTUALIZAR LA TRANSACCION DEL USUARIO: "
					+id+" EXCEPTION: "+e.getCause());
			e.printStackTrace();
		}
		return info;
	}
	
	public void createCard(CreateCard card){
		try {
			LOGGER.info("[FIRSTVIEW] - GUARDANDO RESPUESTA FIRST VIEW - CREATE CARD - ID USUARIO: "+card.getIdUsuario());
			getSqlMapClientTemplate().insert(SQL_CREATE_CARD, card);
		} catch (Exception e) {
			LOGGER.info("[FIRSTVIEW] - ERROR AL GUARDAR RESPUESTA FIRST VIEW - CREATE CARD - ID USUARIO: {} - ERROR - {}", 
					card.getIdUsuario(), e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public void insertTransacionFirstView(Clearing request){
		try {
			LOGGER.info("[FIRSTVIEW] - GUARDANDO TRANSACCION FIRST VIEW - ID USUARIO: "+request.getIdUsuario());
			getSqlMapClientTemplate().insert(SQL_INSERT_FV_TRAN, request);
		} catch (Exception e) {
			LOGGER.info("[FIRSTVIEW] - ERROR AL GUARDAR RESPUESTA FIRST VIEW - ID USUARIO: {} - ERROR - {}", 
					request.getIdUsuario(), e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
	public void updateTransacionFirstView(Clearing request){
		try {
			LOGGER.info("[FIRSTVIEW] - ACTUALIZANDO TRANSACCION FIRST VIEW - ID USUARIO: "+request.getIdUsuario());
			getSqlMapClientTemplate().update(SQL_UPDATE_FV_TRAN, request);
		} catch (Exception e) {
			LOGGER.info("[FIRSTVIEW] - ERROR AL ACTUALIZANDO TRANSACCION FIRST VIEW - ID USUARIO: {} - ERROR - {}", 
					request.getIdUsuario(), e.getLocalizedMessage());
			e.printStackTrace();
		}
	}
	
}