package com.addcel.telecom.firstView.ibatis;
/**
 * 
 */


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.addcel.telecom.firstView.spring.SpringFactory;


/**
 * @author JOCAMPO
 *
 */
public abstract class AbstractService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);
	
	public Object getBean(String bean) throws Exception{
		ClassPathXmlApplicationContext ctxt = null;
		Object obj = null;
		try{
			ctxt = getSqlSessionInstance();
			obj =  ctxt.getBean(bean);
		}catch(Exception e){
			LOGGER.error("Ocurrio un error al obtener el DAOBean.", e);
			throw new Exception(e);
		}
		return obj;
	}
	
	public ClassPathXmlApplicationContext getSqlSessionInstance(){
		return SpringFactory.getApplicationContexInstance();
	}

}
