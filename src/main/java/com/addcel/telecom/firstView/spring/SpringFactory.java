/**
 * 
 */
package com.addcel.telecom.firstView.spring;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author JOCAMPO
 * 
 */

public class SpringFactory {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SpringFactory.class);

	private static final String SPRING_CONFIG = "com/addcel/telecom/services/ws/spring/spring-context.xml";
	
	private static ClassPathXmlApplicationContext ctxt;

	public static ClassPathXmlApplicationContext getApplicationContexInstance() {
		try {
			if(ctxt==null){
	            LOGGER.debug("[FIRST VIEW WS] **** SE HA CARGADO EL CONTEXTO DE SPRING ***");
	            ctxt = new ClassPathXmlApplicationContext(SPRING_CONFIG);
	        }
		} catch (Exception e) {
			LOGGER.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.",e);
		}
		return ctxt;
	}

	public static ClassPathXmlApplicationContext reloadApplicationContex() {
		try {
			ctxt = new ClassPathXmlApplicationContext(SPRING_CONFIG);
		} catch (Exception e) {
			LOGGER.error("Error al tratar de crear la instancia SqlSessionFActoryImpl.",e);
		}
		return ctxt;
	}

}
